package org.andengine.util.levelstats;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.andengine.util.StreamUtils;
import org.andengine.util.call.Callback;
import org.andengine.util.debug.Debug;
import org.andengine.util.math.MathUtils;
import org.andengine.util.preferences.SimplePreferences;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga Inc.
 *
 * @author Nicolas Gramlich
 * @since 21:13:55 - 18.10.2010
 */
public class LevelStatsDBConnector {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final String PREFERENCES_LEVELSTATSDBCONNECTOR_PLAYERID_KEY = "preferences.levelstatsdbconnector.playerid";

	// ===========================================================
	// Fields
	// ===========================================================

	private final String mSecret;
	private final String mSubmitURL;
	private final int mPlayerID;

	// ===========================================================
	// Constructors
	// ===========================================================

	public LevelStatsDBConnector(final Context pContext, final String pSecret, final String pSubmitURL) {
		this.mSecret = pSecret;
		this.mSubmitURL = pSubmitURL;

		final SharedPreferences simplePreferences = SimplePreferences.getInstance(pContext);
		final int playerID = simplePreferences.getInt(PREFERENCES_LEVELSTATSDBCONNECTOR_PLAYERID_KEY, -1);
		if (playerID != -1) {
			this.mPlayerID = playerID;
		} else {
			this.mPlayerID = MathUtils.random(1000000000, Integer.MAX_VALUE);
			SimplePreferences.getEditorInstance(pContext).putInt(PREFERENCES_LEVELSTATSDBCONNECTOR_PLAYERID_KEY, this.mPlayerID).commit();
		}
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	public void submitAsync(final int pLevelID, final boolean pSolved, final int pSecondsElapsed) {
		this.submitAsync(pLevelID, pSolved, pSecondsElapsed, null);
	}

	public void submitAsync(final int pLevelID, final boolean pSolved, final int pSecondsElapsed, final Callback<Boolean> pCallback) {

	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
