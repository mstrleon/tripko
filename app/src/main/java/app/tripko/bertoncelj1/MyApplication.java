package app.tripko.bertoncelj1;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import org.andengine.util.Constants;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.utils.L;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.PoiTrackService;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by bertoncelj1 on 10/22/15.
 */
@ReportsCrashes(mailTo = "harnest64@gmail.com", //formUri = "http://poljch.home.kg:8080/report",
        mode = ReportingInteractionMode.TOAST,
        logcatArguments = { "-t", "200", "-s", Dbg.APP_DEBUG_TAG, Constants.DEBUGTAG},
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
        forceCloseDialogAfterToast = false, // optional, default false
        resToastText = R.string.acra_toast_text)
public class MyApplication extends Application {

    private static Context context;

    public static Context getAppContext() {
        return MyApplication.context;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();

        ACRA.init(this);
        initImageLoader(getApplicationContext());
        DataStorage.init(this.getCacheDir());
        PoiType.Colors.init(getResources());

        MapboxAccountManager.start(this, getString(R.string.mapbox_access_token));

        Settings settings = Settings.getInstance();
        if(settings.getNavigationMode() == NavigationMode.TRIP &&
                Settings.isTrackValid(settings.getTrack())) {
            PoiTrackService.start(getApplicationContext());
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }



    public static void initImageLoader(Context context) {

        final DisplayImageOptions imageLoaderOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.missing_image)
                .showImageForEmptyUri(R.drawable.missing_image)
                .showImageOnFail(R.drawable.missing_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();

        // you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.defaultDisplayImageOptions(imageLoaderOptions);
        //config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());

        L.writeLogs(true);
        L.writeDebugLogs(true);
    }
}
