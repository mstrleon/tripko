package app.tripko.bertoncelj1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.Serializable;

import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigationActivity;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.poiCard.PoiCardActivity;
import app.tripko.bertoncelj1.trips.ChooseDateActivity;
import app.tripko.bertoncelj1.trips.TripPlannerActivity;
import app.tripko.bertoncelj1.trips.TripShowActivity;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.Settings;

public class SplashScreenActivity extends Activity {

    /** First screen shown after splash screen and after user has chose destination **/
    public static final Class FIRST_SCREEN = ArrowNavigation.class;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        hideKeyboard(this);

        User user = User.getUserLogin(this);

        if(user != null){
            ApiCaller.login(user);
            user.initUserSettings();

            startActivityAfterUserLogin(this);
        } else {
            Intent intent = new Intent(this, PinActivity.class);
            startActivity(intent);
        }

        finish();

    }

    public static void startActivityAfterUserLogin(Context context){
        //TODO 11 load all POI dynamically
        PoiDataManager.getInstance().downloadNewData(new LatLng(46, 14), 300,
                ApiCaller.getInstance(), null);
        /*
         First activity shown after splash screen.
         It depends if user has destination already set.
         If it is already set there is no need for user to choose destination again and
         FIRST_SCREEN is immediately shown
        */
        Class firstActivity = ApiCaller.getInstance().getUser().getDestination() == null ?
                TripPlannerActivity.class : FIRST_SCREEN;

        Intent intent = new Intent(context, FIRST_SCREEN);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
