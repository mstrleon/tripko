package app.tripko.bertoncelj1.onlineAPI.user.settings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.HttpCalls;

/**
 * Created by openstackmaser on 12.5.2016.
 */
public class VisitedSetting extends SettingsHelper<VisitedSetting.Setting> implements SettingsHelper.OnlineSetting {

    public static final String FILE_NAME = "visited";

    public static class Setting implements Serializable {
        public int id;
        public long timeVisited;
    }

    public VisitedSetting(String filePath){
        super(filePath);
    }

    public static Setting createSetting(int id) {
        Setting setting = new Setting();
        setting.id = id;
        setting.timeVisited = System.currentTimeMillis();

        return setting;
    }

    @Override
    public void onSettingRemoved(int id, Setting setting) {
    }

    @Override
    public void saveSettings() {
        super.saveOnDisk(FILE_NAME, Setting.class);
    }

    @Override
    public void loadSettings(OnSettingsLoadedCallBack callback) {
        super.loadFromDisk(FILE_NAME);
    }

    @Override
    public void saveOnline(OnSettingsLoadedCallBack callBack) {
        for(Setting setting: mSettingNotUpdated.values()) {
            saveSettingOnline(setting, setting.id);
            mSettingNotUpdated.remove(setting);
        }
    }

    public void saveSettingOnline(Setting s, final int id) {
        if(true) {
            ApiCaller.getInstance().setPoiVisited(id, new HttpCalls.ResponseFunction() {
                @Override
                public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                    Dbg.d(DEBUG_TAG, "save visited id:" + id + ": " + report.toString());
                }
            });
        }else{
            ApiCaller.getInstance().removePoiVisited(id, new HttpCalls.ResponseFunction() {
                @Override
                public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                    Dbg.d(DEBUG_TAG, "remove visited id:" + id + ": " + report.toString());
                }
            });
        }
    }

    @Override
    public void loadOnline(OnSettingsLoadedCallBack callBack) {
        ApiCaller.getInstance().getUserVisited(new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                if(report.isSuccessful()){
                    Map<String, Object> map = HttpCalls.getHashMap((String) data);
                    //String message = (String) map.get("msg");
                    //Integer type = (Integer) map.get("type");
                    ArrayList<LinkedHashMap> visited = (ArrayList<LinkedHashMap>) map.get("visited");

                    //VisitedPoiSetting.super.setNewSettings(visited, callback);
                }
            }
        });
    }

}
