package app.tripko.bertoncelj1.onlineAPI.user.settings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.HttpCalls;

/**
 * Created by openstackmaser on 12.5.2016.
 */
public class FavouriteSetting extends SettingsHelper<FavouriteSetting.Setting> implements SettingsHelper.OnlineSetting, Serializable {

    public static final String FILE_NAME = "favourite";

    public static class Setting implements Serializable {
        public int id;
        public boolean set;
    }

    public static Setting createSetting(int id, boolean set) {
        Setting setting = new Setting();
        setting.id = id;
        setting.set = set;

        return setting;
    }

    public FavouriteSetting(String filePath){
        super(filePath);
    }

    @Override
    public void onSettingRemoved(int id, Setting setting) {
    }

    @Override
    public void saveSettings() {
        super.saveOnDisk(FILE_NAME, Setting.class);
    }

    @Override
    public void loadSettings(OnSettingsLoadedCallBack callback) {
        super.loadFromDisk(FILE_NAME);
    }


    private void setFavouriteSettings(ArrayList<LinkedHashMap> settings, boolean needsUpdate){
        for(LinkedHashMap setting: settings){
            Integer poiId = (Integer) setting.get("poi_id");

            super.setSetting(poiId, createSetting(poiId, true), needsUpdate);
        }
    }

    @Override
    public void loadOnline(final OnSettingsLoadedCallBack callBack) {
        ApiCaller.getInstance().getUserFavourites(new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                if(report.isSuccessful()){
                    Map<String, Object> map = HttpCalls.getHashMap((String) data);
                    //String message = (String) map.get("msg");
                    //Integer type = (Integer) map.get("type");
                    ArrayList<LinkedHashMap> favourites = (ArrayList<LinkedHashMap>) map.get("favorites");

                    setFavouriteSettings(favourites, false);
                    if(callBack != null)callBack.settingsLoaded();
                }
            }
        });
    }

    @Override
    public void saveOnline(OnSettingsLoadedCallBack callBack) {
        for(Setting setting: mSettingNotUpdated.values()) {
            saveSetting(setting, setting.id);
            mSettingNotUpdated.remove(setting);
        }
    }

    private void saveSetting(final Setting setting, final int id) {
        if(setting.set) {
            ApiCaller.getInstance().setPoiFavourite(id, new HttpCalls.ResponseFunction() {
                @Override
                public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                    Dbg.d(DEBUG_TAG, "save fav id:" + id + ": " + report.toString());
                }
            });
        }else{
            ApiCaller.getInstance().removePoiFavourite(id, new HttpCalls.ResponseFunction() {
                @Override
                public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                    Dbg.d(DEBUG_TAG, "remove fav id:" + id + ": " + report.toString());
                }
            });
        }
    }
}
