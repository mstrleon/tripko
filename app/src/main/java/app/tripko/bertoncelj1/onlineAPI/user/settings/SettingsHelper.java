
package app.tripko.bertoncelj1.onlineAPI.user.settings;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Set;

import app.tripko.bertoncelj1.DataStorage;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by openstackmaser on 12.5.2016.
 */
public abstract class SettingsHelper <T>{

    public static final String DEBUG_TAG = "UserSetting";

    public T getSetting(int id) {
        return mSetting.get(id);
    }

    public interface OnSettingsLoadedCallBack{
        void settingsLoaded();
    }

    public interface OnlineSetting {
        void saveOnline(final OnSettingsLoadedCallBack callBack);
        void loadOnline(final OnSettingsLoadedCallBack callBack);
    }

    private String mSaveFilePath;

    protected HashMap<Integer, T> mSetting = new HashMap<>();
    public HashMap<Integer, T> mSettingNotUpdated = new HashMap<>();

    protected abstract void onSettingRemoved(int id, T setting);
    public abstract void saveSettings();
    public abstract void loadSettings(OnSettingsLoadedCallBack callback);

    public SettingsHelper(String saveFilePath){
        mSaveFilePath = saveFilePath;
        loadSettings(null);
    }

    public void setSaveFilePath(String pSaveFilePath){
        mSaveFilePath = pSaveFilePath;
    }

    public boolean isSettingSet(int id){
        return mSetting.containsKey(id);
    }

    public void setSetting(int id, T newSetting, boolean needsUpdate){
        if(newSetting == null) throw new NullPointerException("newSetting is null!");

        mSetting.put(id, newSetting);
        if(needsUpdate)mSettingNotUpdated.put(id, newSetting);
    }

    public void unsetSetting(int id){
        if(isSettingSet(id)){
            mSettingNotUpdated.remove(id);

            T removedSetting = mSetting.remove(id);
            onSettingRemoved(id, removedSetting);
        }
    }

    public Set<Integer> getSettingsIds(){
        return mSetting.keySet();
    }

    public T[] getSettings(Class<T> c) {
        @SuppressWarnings("unchecked")
        final T[] a = (T[]) Array.newInstance(c, mSetting.size());

        return mSetting.values().toArray(a);
    }


    protected void saveOnDisk(String pFileName, Class<T> c){
        final DataStorage dataStorage = DataStorage.getInstance();

        T[] set = getSettings(c);
        if(set != null && set.length > 0 && set[0] instanceof Serializable) {
            dataStorage.save(getFilePath(pFileName), mSetting);
        }
    }

    protected boolean loadFromDisk(String pFileName){
        final DataStorage dataStorage = DataStorage.getInstance();

        final String pFilePath = getFilePath(pFileName);

        Object loadedData = dataStorage.loadFileObject(pFilePath);
        if(loadedData == null){
            Dbg.e(DEBUG_TAG, "Unable to load data, filePath:" + pFilePath);
            return false;
        }

        if(loadedData instanceof HashMap){
            mSetting = (HashMap<Integer, T>) loadedData;
            Dbg.i(DEBUG_TAG, this.getClass().getName() + ", " + mSetting.size() + " settings loaded from disk");
            return true;
        }else{
            Dbg.e(DEBUG_TAG, "Data loaded are not the right type: " + loadedData.getClass());
            return false;
        }
    }

    private String getFilePath(final String pFileName){
        return mSaveFilePath + "/" +  pFileName + ".sav";
    }

}
