package app.tripko.bertoncelj1.onlineAPI;

import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.server.converter.StringToIntConverter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.HttpCalls;

import static app.tripko.bertoncelj1.util.Util.extractIntegerValue;
import static app.tripko.bertoncelj1.util.Util.extractStringValue;

/**
 * Created by anze on 11/2/16.
 */
public class Partners {

    // TODO create save and load function so it will also work in offline mode

    /*
    List of all partners
     */
    private List<Partner> mPartners;

    private static final String DEBUG_TAG = "Partners";

    public interface OnPartnersLoadedListener{
        void onPartnersLoaded(Partners partners, boolean success);
    }

    public static Partners sInstance;

    public static Partners getInstance(){
        if(sInstance == null) sInstance = new Partners();

        return sInstance;
    }

    private Partners(){}

    /**
     * Call this when you want to load partners from the internet
     * @param listener
     */
    public void loadPartners(final OnPartnersLoadedListener listener){

        // TODO set dynamically
        int customerId = 1;

        ApiCaller.getInstance().getPartners(customerId, new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                boolean success = false;

                if(report.isSuccessful()){
                    Dbg.d(DEBUG_TAG, "Loaded new partners data");

                    success = readPartnersData((String) data);
                } else {
                    Dbg.e(DEBUG_TAG, "Didn't get partners data. Error:" + report.getMessage());
                }

                // notify user that new partners have been loaded
                if(listener != null) listener.onPartnersLoaded(Partners.this, success);
            }
        });
    }

    /**
     * @return the list of all partners loaded. If null no partners
     * have been loaded. You need to call loadPartners() method
     */
    public List<Partner> getPartners(){
        return mPartners;
    }

    /**
     * @return if partners have been loaded. This means that loadPartners has been caled
     */
    public boolean partnersLoaded(){
        return mPartners != null;
    }

    /**
     * Check if poiId is partner. If it is on the partner's list
     * @param poiId .
     * @return .
     */
    public Partner getPartnerFroPoi(int poiId){
        for(Partner partner : mPartners){
            if(partner.poiId == poiId) return partner;
        }

        return null;
    }



    /**
     * Reads partners from serverData
     * @param rawServerData .
     * @return if they were successfully loaded
     */
    private boolean readPartnersData(String rawServerData){
        HashMap<String,Object> map;

        try {
            map = new ObjectMapper().readValue(rawServerData.toString() ,HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        if(map == null){
            Dbg.e(DEBUG_TAG, "Error getting partners from rawServerData");
            return false;
        }

        String msg = extractStringValue("msg", map);
        int type = extractIntegerValue("type", map);

        final ArrayList<LinkedHashMap> data = (ArrayList<LinkedHashMap>) map.get("data");

        if(data == null){
            Dbg.e(DEBUG_TAG, "Unable to get data from rawServerData");
            return false;
        }

        mPartners = new ArrayList<>();

        // loades list of partners from received data
        for (LinkedHashMap partnerData : data) {

            // creates new partner from data server send
            Partner partner = Partner.createPartner(partnerData);

            if(partner != null) {
                mPartners.add(partner);
            } else {
                Dbg.e(DEBUG_TAG, "Unable to read partner data");
            }
        }

        return mPartners.size() > 0;
    }


    /**
     * Used when some integer is not set. It's value is unknown
     */
    public static final int NOT_SET = -1;

    /**
     * Guido partner
     * This is some poi that has enabled coupons and other partner stuff
     */
    public static class Partner{
        private int partnerId;
        private int poiId;
        private String poiName;

        /*
        Create partner form HashMap data from API server
        If there is a problem extracting data from map it returns null
         */
        private static Partner createPartner(HashMap<String,Object> map){
            if(map == null) return null;

            Partner partner = new Partner();

            partner.partnerId = extractIntegerValue("partner_id", map, NOT_SET);
            if(partner.partnerId == NOT_SET){
                Dbg.e(DEBUG_TAG, "Unable to extract partnerId.");
                return null;
            }

            partner.poiId = extractIntegerValue("poi_id", map, NOT_SET);
            if(partner.poiId == NOT_SET){
                Dbg.e(DEBUG_TAG, "Unable to extract poiId for partner.");
                return null;
            }

            partner.poiName = extractStringValue("poi_name", map);

            return partner;
        }

        private Partner(){}

        /*
        Partner Id some identifier for partner
         */
        public int getPartnerId(){
            return partnerId;
        }

        /*
        To witch poi is this partner connected
         */
        public int getPoiId(){
            return poiId;
        }

        /*
        PoiName to witch this partner is connected to
        This is just to make data easier to read
         */
        public String getPoiName(){
            return poiName;
        }

        @Override
        public String toString() {
            return String.format("id:%d, poiId:%d, poiName:%s", partnerId, poiId, poiName);
        }
    }
}
