package app.tripko.bertoncelj1.onlineAPI.user.settings;

import java.io.Serializable;

/**
 * Created by openstackmaser on 12.5.2016.
 */
public class RecentSetting extends SettingsHelper<RecentSetting.Setting> implements Serializable {

    public final String FILE_NAME = "recent";

    public static class Setting implements Serializable {
        public int id;
        public long timeAdded;
    }

    public static Setting createSetting(int id) {
        Setting setting = new Setting();
        setting.id = id;
        setting.timeAdded = System.currentTimeMillis();

        return setting;
    }

    public RecentSetting(String filePath){
        super(filePath);
    }


    @Override
    public void onSettingRemoved(int id, Setting setting) {
    }


    @Override
    public void saveSettings() {
        super.saveOnDisk(FILE_NAME, Setting.class);
    }

    @Override
    public void loadSettings(OnSettingsLoadedCallBack callback) {
        super.loadFromDisk(FILE_NAME);
    }
}
