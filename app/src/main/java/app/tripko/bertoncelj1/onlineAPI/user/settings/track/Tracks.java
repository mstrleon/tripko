package app.tripko.bertoncelj1.onlineAPI.user.settings.track;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import app.tripko.bertoncelj1.onlineAPI.user.settings.SettingsHelper;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 5/30/16.
 */
public class Tracks extends SettingsHelper<Track> implements SettingsHelper.OnlineSetting, Serializable {

    public static final String FILE_NAME = "Tracks";



    public Tracks(String filePath){
        super(filePath);
    }

    @Override
    public void onSettingRemoved(int id, Track track) {
    }

    @Override
    public void saveSettings() {
        super.saveOnDisk(FILE_NAME, Track.class);
    }

    @Override
    public void loadSettings(SettingsHelper.OnSettingsLoadedCallBack callback) {
        super.loadFromDisk(FILE_NAME);
    }

    @Override
    public void saveOnline(SettingsHelper.OnSettingsLoadedCallBack callBack) {
        for(Track track : mSettingNotUpdated.values()) {
            mSettingNotUpdated.remove(track);
        }
    }

    public void saveSettingOnline(Track s, final int id) {
        //TODO
    }


    @Override
    public void loadOnline(SettingsHelper.OnSettingsLoadedCallBack callBack) {
        //TODO
    }

}
