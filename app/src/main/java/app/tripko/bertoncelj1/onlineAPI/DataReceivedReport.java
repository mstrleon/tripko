package app.tripko.bertoncelj1.onlineAPI;

/**
 * Created by openstackmaser on 13.2.2016.
 */
public class DataReceivedReport {
    final boolean success;
    final String message;

    public DataReceivedReport(boolean success, String message){
        this.success = success;
        this.message = message;
    }

    public boolean isSuccessful(){
        return success;
    }

    public String getMessage(){
        return message;
    }

    @Override
    public String toString() {
        return "success:" + success + ", message:" + message;
    }
}
