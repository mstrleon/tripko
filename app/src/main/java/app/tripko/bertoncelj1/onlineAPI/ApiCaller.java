package app.tripko.bertoncelj1.onlineAPI;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import app.tripko.bertoncelj1.BitmapTest;
import app.tripko.bertoncelj1.MyApplication;
import app.tripko.bertoncelj1.PinActivity;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.util.HttpCalls;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.Dbg;


/**
 * Created by bertoncelj1 on 10/15/15.
 */
public class ApiCaller {

    private static final String DEBUG_TAG = "APIManager";

    public static final String API_URL = "http://api.guido.si";

    private User mUser;

    private static ApiCaller sInstance;

    public static void login(User user){
        sInstance = new ApiCaller(user);
    }

    public static ApiCaller getInstance(){
        if(sInstance == null) {
            //user should already be logged in when getInstance is called this is just
            //to try fix the problem
            tryLogin();
        }
        return sInstance;
    }

    //search if user api key exists then login
    private static void tryLogin(){
        Dbg.e("User not found, trying to login user from memory!");
        User user = User.getUserLogin(MyApplication.getAppContext());
        if(user != null){
            ApiCaller.login(user);
            user.initUserSettings();
        }else{
            throw new IllegalStateException("User not yet set!");
        }
    }

    private ApiCaller(final User pUser){
        setUser(pUser);
    }

    public void setUser(final User pUser){
        if(pUser == null)throw new IllegalArgumentException("User is null!");
        mUser = pUser;
    }

    public User getUser(){
        return mUser;
    }

    public void logout(Activity activity) {
        getUser().setUserLogout(activity);
        this.mUser = null;

        Intent intent = new Intent(activity, PinActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    public void getPoisFromGpsAsync(LatLng location, int radius, HttpCalls.ResponseFunction fun){
        if(location == null)new IllegalArgumentException("Location == null");
        if(radius < 0 || radius > 1000)new IllegalArgumentException("Radius out of range: 0 < " + radius + " < 1000");;
        Dbg.i(DEBUG_TAG, "getPoisFromGpsAsync:" + location + ", radius:" + radius);

        //TODO set language dynamically
        String url = API_URL + "/api/pois/get_pois_from_gps?lang_id=20";
        url += "&lon=" + location.getLongitude();
        url += "&lat=" + location.getLatitude();
        url += "&limit=250";
        url += "&unit=km";
        url += "&radius=" + radius;


        HttpCalls.getCall(mUser, url, fun);
    }

    public void getPoiById (int poiId, HttpCalls.ResponseFunction fun){
        if (poiId < 0) new IllegalArgumentException("Invalid poi Id " + poiId);
        Dbg.i(DEBUG_TAG, "getPoiById:" + poiId);

        //TODO set language dynamically
        String url = API_URL + "/api/pois/get_poi_by_id?lang_id=20";
        url += "&poi_id=" + poiId;

        HttpCalls.getCall(mUser, url, fun);
    }

    public void setPoiFavourite(final int poiId,  HttpCalls.ResponseFunction fun) {
        if (poiId < 0) new IllegalArgumentException("Invalid poi Id " + poiId);
        Dbg.i(DEBUG_TAG, "setPoiFavourite:" + poiId);

        String url = API_URL + "/api/users/set_poi_favorite";
        String postData =
                "{" +
                "\"poi_id\":\""+ poiId +"\"" +
                "}";

        HttpCalls.postCall(mUser, url, postData, fun);
    }

    public void removePoiFavourite(final int poiId,  HttpCalls.ResponseFunction fun) {
        if (poiId < 0) new IllegalArgumentException("Invalid poi Id " + poiId);
        Dbg.i(DEBUG_TAG, "removePoiFavourite:" + poiId);

        String url = API_URL + "/api/users/remove_poi_favorite";

        String postData =
                "{" +
                "\"poi_id\":\""+ poiId +"\"" +
                "}";

        HttpCalls.postCall(mUser, url, postData, fun);
    }

    public void getUserFavourites(HttpCalls.ResponseFunction fun) {
        Dbg.i(DEBUG_TAG, "getUserFavourites");

        String url = API_URL + "/api/users/get_user_favorites";

        HttpCalls.getCall(mUser, url, fun);
    }

    public void setPoiRank(int id, int rank, String comment, HttpCalls.ResponseFunction responseFunction) {
        if(rank < 0 || rank > 5) throw new IllegalArgumentException("Rank is out of range: " + rank);
        if(comment == null) comment = "";

        Dbg.i(DEBUG_TAG, "setPoiRank:" + id);

        String url = API_URL + "/api/users/set_poi_rank";

        String postData =
                "{" +
                "\"poi_id\":\""+ id +"\"," +
                "\"rank\":\""+ rank +"\"," +
                "\"comment\":\""+ comment +"\"" +
                "}";

        HttpCalls.postCall(mUser, url, postData, responseFunction);
    }

    public void setPoiVisited(int id, HttpCalls.ResponseFunction responseFunction) {
        Dbg.i(DEBUG_TAG, "setPoiVisited:" + id);

        String url = API_URL + "/api/users/set_poi_visit";

        String postData =
                "{" +
                        "\"poi_id\":\""+ id +"\"" +
                        "}";

        HttpCalls.postCall(mUser, url, postData, responseFunction);
    }

    public void removePoiVisited(int id, HttpCalls.ResponseFunction responseFunction) {
        Dbg.i(DEBUG_TAG, "removePoiVisited:" + id);

        String url = API_URL + "/api/users/remove_poi_visit";

        String postData =
                "{" +
                "\"poi_id\":\""+ id +"\"" +
                "}";

        HttpCalls.postCall(mUser, url, postData, responseFunction);
    }

    public void getPoiRanks(int poiId, HttpCalls.ResponseFunction responseFunction) {
        Dbg.i(DEBUG_TAG, "getPoiRanks:" + poiId);

        String url = API_URL + "/api/pois/get_poi_ranks?" +
                    "poi_id=" + poiId;;

        HttpCalls.getCall(mUser, url, responseFunction);
    }

    public void getUserVisited(HttpCalls.ResponseFunction fun) {
        Dbg.i(DEBUG_TAG, "getUserVisited");

        String url = API_URL + "/api/users/get_user_visited?";

        HttpCalls.getCall(mUser, url, fun);
    }

    public void getNavigationTypesTree(HttpCalls.ResponseFunction fun) {
        Dbg.i(DEBUG_TAG, "getNavigationTypesTree");

        String url = API_URL + "/api/pois/get_navigation_types_tree?lang_id=20";

        HttpCalls.getCall(mUser, url, fun);
    }

    public void getPoiTypesTreeForNavigationTypeId(int navigationTypeId, HttpCalls.ResponseFunction fun) {
        Dbg.i(DEBUG_TAG, "getNavigationTypesTree");

        String url = API_URL + "/api/pois/get_poi_types_tree_for_navigation_type_id?" +
                "lang_id=20&" +
                "navigation_type_id=" + navigationTypeId;

        HttpCalls.getCall(mUser, url, fun);
    }

    /**
     * Send partner transaction. It contains qr, pin and data needed to process the transaction.
     *
     * @param pin PIN number to authenticate the partner
     * @param numberOfItems Number of customers, items,…
     * @param responseFunction Function called when server returns data
     */
    public void sendTransaction(int partnerId, int pin, int numberOfItems, HttpCalls.ResponseFunction responseFunction){
        Dbg.i(DEBUG_TAG, "sendTransaction pin:" + pin + ", numberOfItems:" + numberOfItems);

        if(pin < 0 || pin > 9999) throw new IllegalArgumentException("Pin is out of range: " + pin);
        if(numberOfItems < 0) throw new IllegalArgumentException("Number of items are out of range: " + numberOfItems);
        if(partnerId < 0) throw new IllegalArgumentException("PartnerId is out of range: " + partnerId);

        String url = API_URL + "/api/partners/send_transaction";

        String postData = "{" +
                        "\"partner_id\":\""+ partnerId +"\"," +
                        "\"pin\":\""+ pin +"\"," +
                        "\"number_of_items\":\""+ numberOfItems +"\"" +
                        "}";

        HttpCalls.postCall(mUser, url, postData, responseFunction);
    }

    /**
     * Get customers (hotel) partners (restaurants,shops,...) which use the Guido partner system.
     *
     * http://api.tripko.si/apidoc/1.0/customers/get_partners.html
     * @param customerId ID of a customer
     * @param fun Function called when server returns data
     */
    public void getPartners(int customerId, HttpCalls.ResponseFunction fun){
        Dbg.i(DEBUG_TAG, "getPartners customerId:" + customerId);

        if(customerId < 0) throw new IllegalArgumentException("CustomerId is out of range: " + customerId);

        String url = API_URL + "/api/customers/get_partners?" +
                "customer_id=" + customerId;

        HttpCalls.getCall(mUser, url, fun);
    }



}