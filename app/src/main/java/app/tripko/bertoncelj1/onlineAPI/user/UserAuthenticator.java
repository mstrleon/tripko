package app.tripko.bertoncelj1.onlineAPI.user;

import android.test.suitebuilder.annotation.Smoke;

import java.util.HashMap;
import java.util.Map;

import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.util.HttpCalls;

/**
 * Created by openstackmaser on 13.2.2016.
 */
public class UserAuthenticator implements HttpCalls.ResponseFunction {

    public interface UserAuthenticateResponse{
        void onReturn(final DataReceivedReport pReport, String userName, String apiKey);
    }

    public static  final String DEBUG_TAG = "UserAuthenticator";

    private static final String MSG_SUCCESS = "User authenticated.";
    private static final int CODE_SUCCESS = 12345;//TODO there is no code in apidoc

    private static final String MSG_ALREADY_AUTH = "User already authenticated.";
    private static final int CODE_ALREADY_AUTH = 1068;

    private static final String MSG_ERROR = "Authentication error.";
    private static final int CODE_ERROR = 1005;

    private static final String MSG_DISABLED = "User is disabled.";
    private static final int CODE_DISABLED = 1059;

    private UserAuthenticateResponse mResponseFunction;

    public void authenticateUser(final String pName, final String pPassword, final UserAuthenticateResponse response){
        mResponseFunction = response;

        final String data =
                "{" +
                        "\"username\":\""+ pName +"\"," +
                        "\"password\":\""+ pPassword +"\"" +
                        "}";
        final String url = ApiCaller.API_URL + "/api/users/authenticate_user";

        HttpCalls.postCallNoUser(url, data, this);
    }

    public void authenticateUser(final String pPin, final UserAuthenticateResponse response){
        mResponseFunction = response;

        final String data =
                        "{\n" +
                        "    \"token\":" + pPin + "\n" +
                        "}";

        final String url = ApiCaller.API_URL + "/api/users/authenticate_user_with_token";

        HttpCalls.postCallNoUser(url, data, this);
    }


    private DataReceivedReport evaluateMessage(final String pMessage){
        if(pMessage == null){
            return new DataReceivedReport(false, "Unable to read received JSON");
        }

        switch(pMessage){
            case MSG_SUCCESS:
                return new DataReceivedReport(true, "User authenticated.");

            case MSG_ALREADY_AUTH:
                return new DataReceivedReport(true, "User already authenticated.");

            case MSG_DISABLED:
                return new DataReceivedReport(false, "User disabled.");

            case MSG_ERROR:
                return new DataReceivedReport(false, "User authentication failed.");

            default:
                return new DataReceivedReport(false, "Undefined message" + pMessage);
        }
    }

    @Smoke
    public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
        if(mResponseFunction != null){

            if(!report.isSuccessful()){
                mResponseFunction.onReturn(report, null, null);
                return;
            }

            HashMap<String, Object> map = HttpCalls.getHashMap((String) data);

            //converts report to userAuthenticated report
            final String message = (String) map.get("msg");
            final DataReceivedReport reportMessage = evaluateMessage(message);

            final int type = (int) map.get("type");
            final String userName = (String) map.get("name");
            final String apiKey = (String) map.get("api_key");

            mResponseFunction.onReturn(reportMessage, userName, apiKey);
        }
    }

}