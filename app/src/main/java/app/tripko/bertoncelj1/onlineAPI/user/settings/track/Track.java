package app.tripko.bertoncelj1.onlineAPI.user.settings.track;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 7/17/16.
 */
public class Track implements Serializable {
    public final int id;
    public String name;
    private ArrayList<Integer> selectedPoiIds;
    public long timePlaned; //for what day did user decided to use this trip
    public long timeCreated;
    private String imageUrl;
    private int imagePoiId;
    public boolean isVisible = true; //is it visible in trackList. Invisible tracks are usually the type "take me there".

    public Track(){
        id = Util.rand.nextInt();
        //TODO 1 check if track with this id already exist

        timeCreated = System.currentTimeMillis();
        timePlaned = -1;
        selectedPoiIds = new ArrayList<>(5);
    }

    public Integer[] getSelectedPoisArray(){
        return selectedPoiIds.toArray(new Integer[selectedPoiIds.size()]);
    }

    public ArrayList<Integer> getSelectedPois(){
        return selectedPoiIds;
    }

    public int getSize(){
        return selectedPoiIds.size();
    }

    public String getImageUrl(){
        return imageUrl;
    }

    public boolean containsPoi(int poiId){
        return selectedPoiIds.contains((Object) poiId);
    }

    public void addSelectedPoi(int poiId){
        final int position = selectedPoiIds.size();
        addSelectedPoi(position, poiId);
    }

    public void addSelectedPoi(int position, int poiId){
        if(!selectedPoiIds.contains(poiId)) {

            if (position > selectedPoiIds.size()) {
                position = selectedPoiIds.size();
            }
            selectedPoiIds.add(position, poiId);
        }
    }

    public void removeSelectedPoi(int poiId){
        if(selectedPoiIds.contains(poiId)) {
            selectedPoiIds.remove((Object) poiId);

            updateImage();
        }
    }

    /*
    updates image with image of poi with id poiId
     */
    public void setImage(int poiId){
        //image from poiId already matches this poi so ther is no need to update
        if(poiId == imagePoiId)return;

        PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(poiId,
                new PoiDataManager.OnPoiLoadedCallBack() {
            @Override
            public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
                setImage(poiData);
            }
        });

        if(poiData != null) {
            setImage(poiData);
        }
    }

    /*
    Updates track image. It selects first image from poiData and set it as track image
     */
    public void setImage(PoiData poiData){
        if(poiData == null)throw new NullPointerException("Cant update image. PoiData is null!");

        imagePoiId = poiData.id;
        if(poiData.galleryImages != null && poiData.galleryImages.size() > 0) {
            imageUrl = poiData.galleryImages.get(0).getImage();
        }else{
            imageUrl = poiData.gallery.image_medium;
        }
    }

    /*
    if track does not have poi but still uses his image it changes image of track with different poi
     */
    private void updateImage(){
        if(!this.selectedPoiIds.contains(imagePoiId) && selectedPoiIds.size() > 0){
            int poiId = this.selectedPoiIds.get(0);
            setImage(poiId);
        }
    }

    public void setSelectedPois(Integer[] selectedPoiIds) {
        this.selectedPoiIds = new ArrayList<>(Arrays.asList(selectedPoiIds));

        updateImage();
    }


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Track)) return super.equals(o);

        // every track has distinctive id
        return ((Track) o).id == this.id;
    }

    public static void addPoiToTrack(Track track, int poiId){
        addPoiToTrack(track, poiId, true);
    }

    public static void removePoiFromTrack(Track track, int poiId) {
        addPoiToTrack(track, poiId, false);
    }

    public static void addPoiToTrack(Track track, int poiId, boolean add){
        if(add){
            track.addSelectedPoi(poiId);
        }else{
            track.removeSelectedPoi(poiId);
        }

        Settings settings = Settings.getInstance();

        // check if currently changed track is used for navigation
        if(settings.getNavigationMode() == NavigationMode.TRIP &&
                settings.getTrack().equals(track)){

            if(Settings.isTrackValid(track)) {
                // if changed track is still valid than update track
                settings.setTrack(track);
            } else {
                // current track is no longer valid. turn off Trip mode and set Discovering mode
                settings.setNavigationMode(NavigationMode.DISCOVERING);
            }
        }

        //saves current track in settings
        Settings.saveSettings();

        //saves track in tracks
        Tracks tracks = ApiCaller.getInstance().getUser().tracks;
        tracks.setSetting(track.id, track, true);
        tracks.saveSettings();
    }
}