package app.tripko.bertoncelj1.onlineAPI.user;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import app.tripko.bertoncelj1.DataStorage;
import app.tripko.bertoncelj1.MyApplication;
import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.onlineAPI.user.settings.FavouriteSetting;
import app.tripko.bertoncelj1.onlineAPI.user.settings.RecentSetting;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Tracks;
import app.tripko.bertoncelj1.onlineAPI.user.settings.VisitedSetting;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by openstackmaser on 13.2.2016.
 */

public class User{
    public static final String DEBUG_TAG = "User";
    public static final User SUPER_USER = new User("superUser", "650c6277f76c4d5dd6d0de87f10f73fa");

    public FavouriteSetting favourites;
    public VisitedSetting visited;
    public RecentSetting recent;
    public Tracks tracks;
    public int userTrackId = -1;

    private String name;
    private String apiKey;
    private String saveFolder;

    private Destination mDestination;
    private int mHotelPoiId = -1;

    public static User getUserLogin(Context context){
        //TODO add strings "user" and "user_api_key" to constants!
        SharedPreferences sharedPref = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        String userApiKey = sharedPref.getString("user_api_key", null);
        if(userApiKey != null) return new User("", userApiKey);
        return null;
    }

    public void setUserLogin(Context context){
        //TODO add strings "user" and "user_api_key" to constants!
        SharedPreferences sharedPref = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("user_api_key", this.getApiKey());
        editor.commit();
    }

    public void setUserLogout(Context context){
        //TODO add strings "user" and "user_api_key" to constants!
        SharedPreferences sharedPref = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("user_api_key", null);
        editor.commit();
    }

    private static Destination loadDestination() {
        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) return null;

        Object object = dataStorage.loadFileObject("userDestination");

        return object instanceof Destination? (Destination) object : null;
    }

    public void saveDestination(){
        if(mDestination == null) return;

        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) return;

        dataStorage.save("userDestination", mDestination);
    }

    public User(String name, String apiKey) {
        this.name = name;
        this.apiKey = apiKey;
        this.saveFolder = apiKey;
    }

    public void initUserSettings(){
        favourites = new FavouriteSetting(saveFolder);
        favourites.loadOnline(null);

        visited = new VisitedSetting(saveFolder);
        visited.loadOnline(null);

        recent = new RecentSetting(saveFolder);

        tracks = new Tracks(saveFolder);

        mDestination = loadDestination();
    }


    public String getName() {
        return name;
    }
    public String getApiKey() {
        return apiKey;
    }

    public boolean equals(User user) {
        return user.name.equals(this.name);
    }

    public void setDestination(Destination mDestination) {
        this.mDestination = mDestination;
    }

    public Destination getDestination() {
        return mDestination;
    }

    public void setHotelPoiId(int hotelPoiId){
        this.mHotelPoiId = hotelPoiId;
    }

    public int getHotelPoiId(){
        return mHotelPoiId;
    }
}
