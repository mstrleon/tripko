package app.tripko.bertoncelj1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by bertoncelj1 on 10/19/15.
 */
public class DataStorage {

    private static final String DEBUG_TAG = "DataStorage";

    public enum Type{
        STRING, OBJECT, BITMAP
    }

    File mCacheDir;
    private static DataStorage INSTANCE;

    public static void init(final File pCacheDir){
        INSTANCE = new DataStorage(pCacheDir);
    }

    public static DataStorage getInstance(){
        return INSTANCE;
    }

    public DataStorage(final File pCacheDir){
        mCacheDir = pCacheDir;
    }

    public boolean fileExists(String filePath){
        File file = new File(mCacheDir, filePath);
        if(file == null)return false;
        return file.exists();
    }

    public boolean save(String filePath, String data){
        return save(filePath, data, Type.STRING);
    }

    public boolean save(String filePath, Bitmap data){
        return save(filePath, data, Type.BITMAP);
    }

    public boolean save(String filePath, Object data){
        return save(filePath, data, Type.OBJECT);
    }

    private boolean save(String filePath, Object data, Type type){
        boolean operationComplete = false;
        //MLog.d(DEBUG_TAG, "writing in file: " + data);
        FileOutputStream fos = null;
        try {

            //fos = context.openFileOutput(file, Context.MODE_PRIVATE);
            File file = new File(mCacheDir,  filePath);
            file.getParentFile().mkdirs();

            fos = new FileOutputStream(file);

            switch(type){
                case BITMAP:
                    operationComplete = saveBitmap(fos, (Bitmap)data);
                    break;

                case OBJECT:
                    operationComplete = saveObject(fos, data);
                    break;

                case STRING:
                    operationComplete = saveString(fos, (String)data);
                    break;
            }

            fos.close();

        } catch (FileNotFoundException e) {
            Dbg.e(DEBUG_TAG, "File not found! :" + e.toString());
            operationComplete = false;
        } catch (IOException e) {
            Dbg.e(DEBUG_TAG, "IO exception :" + e.toString());
            operationComplete = false;
        }

        return operationComplete;
    }

    private boolean saveBitmap(FileOutputStream fos, Bitmap bitmapImage) throws IOException{
        bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        return true;
    }

    private boolean saveString(FileOutputStream fos, String string) throws IOException{
        fos.write(string.getBytes());
        return true;
    }

    private boolean saveObject(FileOutputStream fos, Object object) throws IOException{
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(object);
        os.close();
        return true;
    }

    public String loadFileString(String filePath){
        return (String) loadFile(filePath, Type.STRING);
    }

    public Bitmap loadFileBitmap(String filePath){
        return (Bitmap) loadFile(filePath, Type.BITMAP);
    }

    public Object loadFileObject(String filePath){
        return loadFile(filePath, Type.OBJECT);
    }

    private Object loadFile(String filePath, Type type)
    {
        Object data = null;
        try {
            File file = new File(mCacheDir, filePath);
            FileInputStream fis = new FileInputStream(file);

            switch(type){
                case BITMAP:
                    data = loadBitmap(fis);
                    break;

                case OBJECT:
                    data = loadObject(fis, file);
                    break;

                case STRING:
                    data = loadString(fis);
                    break;
            }

            fis.close();
        } catch (FileNotFoundException e) {
            Dbg.e(DEBUG_TAG, "File not found! :" + e.toString());
        } catch (IOException e) {
            Dbg.e(DEBUG_TAG, "IO exception :" + e.toString());
        }

        return data;

    }

    private Bitmap loadBitmap(FileInputStream fis) throws IOException{
        return BitmapFactory.decodeStream(fis);
    }

    private Object loadObject(FileInputStream fis, File file) throws IOException{
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object data = null;
        try {
            data = ois.readObject();
        } catch (ClassNotFoundException e) {
            Dbg.e(DEBUG_TAG, "Load object class not found: " + e.toString());
        } catch (InvalidClassException e) {
            Dbg.e(DEBUG_TAG, "Invalid class this class will be delated: " + e.toString());

            ois.close();
            file.delete();
            return null;
        }
        ois.close();

        return data;
    }

    private String loadString(FileInputStream fis) throws IOException{
        InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
        BufferedReader bufferedReader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        return sb.toString();
    }

    public LinkedList<Object> loadAllFiles(String filePath, Type type)
    {
        LinkedList<Object> allData = null;

        try {
            File[] files = new File(mCacheDir, filePath).listFiles();

            allData = new LinkedList<>();

            if(files == null){
                return allData;
            }

            for(File file: files){
                if(file.isFile()) {
                    final FileInputStream fis = new FileInputStream(file);
                    if (fis == null) continue;

                    Object data = null;
                    switch(type){
                        case BITMAP:
                            data = loadBitmap(fis);
                            break;

                        case OBJECT:
                            data = loadObject(fis, file);
                            break;

                        case STRING:
                            data = loadString(fis);
                            break;
                    }

                    if(data != null) allData.add(data);
                    fis.close();
                }
            }

        } catch (FileNotFoundException e) {
            Dbg.e(DEBUG_TAG, "File not found! :" + e.toString());
        } catch (IOException e) {
            Dbg.e(DEBUG_TAG, "IO exception :" + e.toString());
        }

        return allData;
    }

}

