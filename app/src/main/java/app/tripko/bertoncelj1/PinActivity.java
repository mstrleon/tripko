package app.tripko.bertoncelj1;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import app.tripko.bertoncelj1.customViews.PinInput;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.onlineAPI.user.UserAuthenticator;

public class PinActivity extends AppCompatActivity implements PinInputFragment.OnPinChangedListener {

    private boolean mIsCheckingPin = false;

    private PinInputFragment mPinFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        setTitle("");

        mPinFragment = (PinInputFragment) getFragmentManager()
                .findFragmentById(R.id.pinInputFragment);

        mPinFragment.setOnPinChangedListener(this);
        mPinFragment.setInfo(false, "enter your pin");
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            killApp();
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    private void killApp(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
        finish();
        System.exit(0);
    }

    private void tryUserLogin(final String pin){
        UserAuthenticator authenticator = new UserAuthenticator();

        mPinFragment.setInfo(true, "checking pin");
        mPinFragment.disableInput();

        authenticator.authenticateUser(pin, new UserAuthenticator.UserAuthenticateResponse() {
            @Override
            public void onReturn(DataReceivedReport pReport, String userName, String apiKey) {
                mPinFragment.enableInput();

                if(pReport.isSuccessful()){
                    Toast.makeText(PinActivity.this, "Successful login", Toast.LENGTH_SHORT).show();

                    logIn(userName, apiKey);

                }else {
                    mPinFragment.clearPin();
                    mPinFragment.setInfo(false, "enter your pin");
                    Toast.makeText(PinActivity.this, "There was problem accessing server", Toast.LENGTH_SHORT).show();
                }

                /*
                if(pin.startsWith("1")){
                    logIn("superUser", "2acbca19f64a9205c5e78ced12b3a959");
                }
                */
            }
        });
    }


    @Override
    public void onPinChanged(char[] pinCode, int pinCodeLength) {
        if(pinCodeLength == PinInputFragment.PIN_LENGTH){
            final String pin = new String(pinCode);
            tryUserLogin(pin);
        }
    }

    private void logIn(final String userName, final String apiKey){
        mPinFragment.setInfo(true, "logging in");

        User user = new User(userName, apiKey);
        ApiCaller.login(user);
        user.setUserLogin(this);
        user.initUserSettings();

        SplashScreenActivity.startActivityAfterUserLogin(this);
        finish();
    }

}
