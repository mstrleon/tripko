package app.tripko.bertoncelj1.trips;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.SwitchTripFragment;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.customViews.SwitchChooseButton;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Tracks;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.poiListView.PoiListViewActivity;
import app.tripko.bertoncelj1.poiListView.PoiListViewAdapter;
import app.tripko.bertoncelj1.poiListView.listManager.ListManager;
import app.tripko.bertoncelj1.poiListView.listManager.StaticList;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by anze on 6/11/16.
 *
 * Shows the trip. All the pois that are in trip
 * and enables to modify the trip. It's name, duration ...
 *
 * If the trip is not specified in bundle, it automatically closes
 */
public class TripShowActivity extends PoiListViewActivity implements
        PoiListViewAdapter.OnClickListener,
        View.OnClickListener,
        SwitchChooseButton.SwitchChooseButtonChanged {

    public static final int RESULT_CODE_TRIP_CHANGED = 10;
    public static final int RESULT_CODE_TRIP_NOT_CHANGED = 11;
    public static final int RESULT_CODE_TRIP_DELETED = 12;

    PoiListViewAdapter mListAdapter;
    Button mRenameTrip;
    Button mDeleteTrip;
    TextView mAddNewPois;
    TextView mStartTrip;
    TextView mTripName;

    SwitchTripFragment mSwitchTripFragment;

    private int mTrackId;
    private boolean mTripChanged = false;

    /*
     saves the number of pois before pause so it can compare it
     in resume function to see if any pois have been added
     */
    int numberOfPoisBeforePause = -1;
    /*
     So thet it know if track needs to be saved
     */
    private boolean mNumberOfPoisChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            mTrackId = extras.getInt("selectedTrackId", -1);
            User user = ApiCaller.getInstance().getUser();
            mSelectedTrack = user.tracks.getSetting(mTrackId);
        }

        if(mSelectedTrack == null) {
            Dbg.e("Cannot get selected track!");
            finish();
            return;
        }

        ListManager listManager = super.getListManger();
        if(!(listManager instanceof StaticList)){
            throw new IllegalStateException("List manager can only be StaticList.");
        }

        StaticList list = (StaticList) listManager;
        list.setTrackName(mSelectedTrack.name);

        mListAdapter = super.getPoiListViewAdapter();
        mListAdapter.setOnButtonClickListener(this);


        super.addButtons(R.layout.segment_button_start_trip_or_add_new_pois,
                R.id.segment_bottom_buttons);
        addDeleteTripButton();
        initViews();
    }

    @Override
    public void onResume(){
        super.onResume();

        addPoisToList();
        super.refreshPoiList();
        refreshDeleteTripButtonVisibility();

        if(numberOfPoisBeforePause != mSelectedTrack.getSize()){
            //beside the track name also the number of pois are displayed, and the number
            //needs to be upgraded if the number of pois changed
            updateTrackName();
            mNumberOfPoisChanged = true;
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        numberOfPoisBeforePause = mSelectedTrack.getSize();
    }

    /*
    Reload track from settings and adds all pois to list
     */
    public void addPoisToList(){
        //reloads track from setting
        User user = ApiCaller.getInstance().getUser();
        mSelectedTrack = user.tracks.getSetting(mTrackId);

        Integer[] selectedPoiIds = mSelectedTrack.getSelectedPoisArray();

        StaticList list = (StaticList) super.getListManger();
        list.setPoiIds(selectedPoiIds);

        mListAdapter.setSelectedPois(selectedPoiIds);
    }

    /*
    This button has to be added manually since this class only extends poiListView and has no own layout
     */
    private void addDeleteTripButton(){
        ViewGroup contentWrapper = (ViewGroup) findViewById(R.id.content_frame_wrapper);
        View view = LayoutInflater.from(contentWrapper.getContext())
                .inflate(R.layout.segment_delete_trip_button, contentWrapper, true);

        mDeleteTrip = (Button) view.findViewById(R.id.deleteTrip);
        mDeleteTrip.setOnClickListener(this);
    }

    /*
    Show delete trip button only if there are no pois left
     */
    private void refreshDeleteTripButtonVisibility(){
        mDeleteTrip.setVisibility(mListAdapter.getItemCount() == 0? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    protected int getRightDrawerLayoutId(){
        return R.layout.drawer_right_trip_show;
    }

    /*
    Initializes all views and sets default values and sets listeners
     */
    private void initViews(){
        mSwitchTripFragment = (SwitchTripFragment) getSupportFragmentManager()
                .findFragmentById(R.id.switchTripFragment);
        mSwitchTripFragment.setTripSwitchOnClickListener(this);
        mSwitchTripFragment.setShowEditTripButton(false);

        mAddNewPois = (TextView) findViewById(R.id.addNewPois);
        mAddNewPois.setOnClickListener(this);

        mStartTrip = (TextView) findViewById(R.id.startTrip);
        mStartTrip.setOnClickListener(this);

        mRenameTrip = (Button) findViewById(R.id.renameTrip);
        mRenameTrip.setOnClickListener(this);

        mTripName = (TextView) findViewById(R.id.tripName);
        updateTrackName();
    }

    @Override
    protected ListManager getListManagerFromId(int listManagerId){
        return new StaticList();
    }

    @Override
    public void onBackPressed() {

        mTripChanged |= updateSelectedPois();

        if(mTripChanged) {
            Intent intent = new Intent();
            intent.putExtra("track", mSelectedTrack);
            setResult(RESULT_CODE_TRIP_CHANGED, intent);
        }
        super.onBackPressed();
    }

    /*
     returns true if any changes has been made to selectedPoiList
     if user deleted or added any new pois
     */
    private boolean updateSelectedPois(){
        final Integer[] selectedPoiIds = mListAdapter.getSelectedPoiIds();

        //checks if user deleted or added any of pois in the track
        if(mNumberOfPoisChanged || selectedPoiIds.length != mSelectedTrack.getSize()){
            mSelectedTrack.setSelectedPois(selectedPoiIds);

            //saves track
            Tracks tracks = ApiCaller.getInstance().getUser().tracks;
            tracks.setSetting(mSelectedTrack.id, mSelectedTrack, true);
            tracks.saveSettings();
            return true;
        }

        return false;
    }


    private void updateTrackName(){
        super.setToolbarTitle(mSelectedTrack.name);

        // TODO set correct locale
        final String tripName = String.format("%s (%d poi)", mSelectedTrack.name,
                mSelectedTrack.getSize());
        mTripName.setText(tripName);
    }


    @Override
    public void onAddButtonClicked(View viewClicked,
                                   PoiData poiData, int position, boolean isSelected) {

        mTripChanged = true;
        this.removePoiWithUndo(position);
    }

    public void removePoiWithUndo(final int position){

        final RecyclerView recyclerView = mListAdapter.getRecyclerView();

        // remove poi from list and save ints date in case we need to re add it
        final PoiData poidata = mListAdapter.removePoi(position);

        // also remove poi from track
        super.removePoiFromTrack(mSelectedTrack, poidata.id);

        refreshDeleteTripButtonVisibility();

        Snackbar snackbar = Snackbar
                .make(recyclerView, "POI Removed", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // if user chooses to undo the delete action that readd poi back to track
                        TripShowActivity.super.addPoiToTrack(mSelectedTrack, poidata.id);

                        // and back to list
                        mListAdapter.addPoi(position, poidata);
                        recyclerView.scrollToPosition(position);
                        refreshDeleteTripButtonVisibility();
                    }
                });
        snackbar.show();
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.addNewPois:
                this.startActivity(
                        new Intent(this, AddPoisToTripActivity.class)
                                .putExtra("selectedTrackId", mSelectedTrack.id));
                break;

            case R.id.startTrip:
                // check if track is valid. track can be empty on not
                // set so there is no way to start a trip
                if(!Settings.isTrackValid(mSelectedTrack)) return;

                Settings setting = Settings.getInstance();
                setting.setTrack(mSelectedTrack);
                setting.setNavigationMode(NavigationMode.TRIP);

                Intent intent = new Intent(TripShowActivity.this, ArrowNavigation.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.renameTrip:

                RenameTripDialog dialog = new RenameTripDialog(this, mSelectedTrack.name);
                dialog.setOnTripNameChangedListener(new RenameTripDialog.OnTripNameChangedListener() {
                    @Override
                    public void onTripNameChanged(String newTripName) {
                        mSelectedTrack.name = newTripName;
                        mTripChanged = true;

                        //update toolbar title
                        updateTrackName();

                        //saves track
                        Tracks tracks = ApiCaller.getInstance().getUser().tracks;
                        tracks.setSetting(mSelectedTrack.id, mSelectedTrack, true);
                        tracks.saveSettings();
                    }
                });

                dialog.show();
                break;

            case R.id.deleteTrip:
                intent = new Intent();
                intent.putExtra("track", mSelectedTrack);
                setResult(RESULT_CODE_TRIP_DELETED, intent);

                super.onBackPressed();
                break;
        }
    }


    @Override
    public boolean onSwitchChooseButtonChanged(SwitchChooseButton.Option pOptionChosen) {
        Settings settings = Settings.getInstance();

        NavigationMode navigationMode = Settings.stateToNavigationMode(pOptionChosen);

        if(navigationMode == NavigationMode.TRIP){
            settings.setNavigationMode(NavigationMode.TRIP);

            //TODO should I set the track again if it changes?
            settings.setTrack(mSelectedTrack);
            settings.setTrackFirstPosition();
        }else{
            settings.setNavigationMode(NavigationMode.DISCOVERING);
        }

        mSwitchTripFragment.updateViewState();

        return true;
    }
}
