package app.tripko.bertoncelj1.trips;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.customViews.CustomSlider;
import app.tripko.bertoncelj1.customViews.TimeBar;
import app.tripko.bertoncelj1.customViews.clearableEditText.ClearableEditText;
import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.destination.DestinationsManager;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.poiListView.PoiListViewActivity;
import app.tripko.bertoncelj1.poiListView.PoiListViewAdapter;
import app.tripko.bertoncelj1.poiListView.listManager.ListManager;
import app.tripko.bertoncelj1.poiListView.listManager.DestinationList;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

/*
    Plans new trip with pois from destination.
 */
public class TripPlannerActivity extends PoiListViewActivity implements
        View.OnClickListener,
        PoiListViewAdapter.OnClickListener,
        CustomSlider.OnPositionChangedListener,
        LocationManager.ILocationListener, PoiListViewAdapter.OnDestinationClickListener {

    private static final String DEBUG_TAG = "TripPlannerActivity";
    private CustomSlider mHowMuchTime;
    private CustomSlider mVisitLength;
    private TimeBar mTimeBar;
    private TextView mExceededText;

    private ClearableEditText mSearchBox;
    private ImageButton mSearchButton;
    private String mLastSearchPhrase = "";
    private Destination mDestination;

    private DestinationList mDesinationsLists[];

    public static final String[] HOW_MUCH_TIME_ELEMENTS = new String[]
            {"Less then an hour", "1 hour", "2 hours",
            "4 hours", "6 hours", "12 hours", "Whole day"};

    // values are in hours
    public static final float[] HOW_MUCH_TIME_VALUES = new float[]
            {0.5f, 1, 2, 4, 6, 12, 18};

    public static final String[] VISIT_LENGTH_ELEMENTS = new String[]
            {"Shorter", "I can do both", "Long"};

    // values are in hours
    public static final float[] VISIT_LENGTH_TIME_VALUES = new float[]
            {0.2f, 1, 2};



    @Override
    protected int getLayoutId(){
        return R.layout.activity_trip_planner;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // requests location update so that application knows the user
        // position an can properly sort destinations
        LocationManager.setLocationListener(this, this, this);

        initDestination();

        //creates new track
        //the track is saved when user finishes with selecting pois
        super.mSelectedTrack =  new Track();
        //sets no pois selected
        super.getPoiListViewAdapter().setSelectedPois(new Integer[0]);

        super.addButtons(R.layout.segment_buttons_start_or_save_trip, R.id.segment_bottom_buttons);

        findViewById(R.id.saveTrip).setOnClickListener(this);
        findViewById(R.id.startTrip).setOnClickListener(this);

        mHowMuchTime = (CustomSlider) findViewById(R.id.sliderHowMuchTime);
        mVisitLength = (CustomSlider) findViewById(R.id.sliderShortOrLongVisits);
        mExceededText = (TextView) findViewById(R.id.exceededText);

        mHowMuchTime.setOnPositionChangedListener(this);
        mVisitLength.setOnPositionChangedListener(this);

        mHowMuchTime.setElements(HOW_MUCH_TIME_ELEMENTS);
        mVisitLength.setElements(VISIT_LENGTH_ELEMENTS);

        mTimeBar = (TimeBar) findViewById(R.id.timeBar);

        setSlidersPositions(mHowMuchTime, mVisitLength);

        int position = mHowMuchTime.getPosition();
        setTimeBarMax(position);
        calculateTripTime();

        toggleButtonsVisibility();

        getPoiListViewAdapter().setOnButtonClickListener(this);
        getPoiListViewAdapter().setOnDestinationClickListener(this);

        initSearchBox();

        // TODO only temporarily disabled order by switch
        mOrderBySwitch.setVisibility(View.INVISIBLE);
    }

    @Override
    public void refreshPoiList() {
        if(mDestination != null){
            final PoiListViewAdapter listAdapter = super.getPoiListViewAdapter();

            listAdapter.removeAll();

            // add all destination to the list but leave the parent out
            // parent is "Slovenia" and should not be visible
            // starts with second level
            if(mDestination.children != null) {
                for (Destination child : mDestination.children) {
                    addDestinationToList(child);
                }
            }
        }
    }

    public void addDestinationToList(Destination destination){
        if(destination == null) return;
        final PoiListViewAdapter listAdapter = super.getPoiListViewAdapter();

        listAdapter.addDestination(destination);

        // check if destination is expended
        if(!destination.isSelected()) return;

        // add all of its children to the list
        addPoisToList(destination.poiIDs);

        // recursively add all children to the list
        if(destination.children == null) return;
        for(Destination child: destination.children){
            addDestinationToList(child);
        }
    }

    private int addPoisToList(Integer[] poiIDs){
        return addPoisToList(poiIDs, -1);
    }

    /*
    Adds poi to the list
    if positon is -1 than it adds them to the last place
     */
    private int addPoisToList(Integer[] poiIDs, int position){
        if(poiIDs == null) return 0;

        final PoiListViewAdapter listAdapter = super.getPoiListViewAdapter();

        for (Integer poiId : poiIDs) {
            PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(poiId, null);
            if (poiData == null) continue; // TODO

            // check if poi is disabled
            if(!Settings.getInstance().isDisabled(poiData.poiType)) {
                if(position == -1){
                    // add them to the last place
                    listAdapter.addPoi(poiData);
                }else{
                    listAdapter.addPoi(position++, poiData);
                }
            }
        }

        return position;
    }

    @Override
    public void onDestinationClicked(Destination destination, int position) {
        if(destination.isSelected()){
            collapseDestination(destination, position);
        }else{
            expandDestination(destination, position);
        }
    }

    private void collapseDestination(Destination destination, int position){
        Dbg.d(DEBUG_TAG, "Collapsing destination:" + destination.name + " pos:" + position);
        final PoiListViewAdapter listAdapter = super.getPoiListViewAdapter();

        destination.setSelected(false);
        listAdapter.notifyItemChanged(position);
        listAdapter.notifyItemRemoved(-1);

        // position is showing on current element we need it to show to next
        position ++;


        // check every List element that is bellow this destentation
        for(int i=position; i<listAdapter.getListData().size(); i++){
            PoiListViewAdapter.ListData listData = listAdapter.getListData().get(i);

            // if list element is poiData than check if this destination has child like it
            // if it has than remove it
            if(listData.isPoiDataType()){
                PoiData poiData = listData.getPoiData();

                if(Util.isInsideArray(destination.poiIDs, poiData.id)){
                    listAdapter.removeListData(i);
                    i--;
                    continue;
                }
            }

            // if list element is destination check if it is child or sibling.
            if(listData.isDestinationDataType()) {

                // If it is child than remove.
                if(destination.children != null) {
                    for (Destination child : destination.children) {
                        if (child.id == listData.getDestinatnion().id) {

                            // recursively collapse destination
                            collapseDestination(listData.getDestinatnion(), i);


                            listAdapter.removeListData(i);
                            i--;
                            continue;
                        }
                    }
                }

                // If it is sibling than stop since thwer should be no more elements from this destination
                if(destination.parent != null) {
                    for (Destination sibling : destination.parent.children) {
                        if (listData.getDestinatnion().id == sibling.id) break;
                    }
                }
            }
        }
    }

    /*
    finds views and sets all listeners for search box
     */
    public void initSearchBox(){
        mSearchButton = (ImageButton) findViewById(R.id.search_button);
        mSearchBox = (ClearableEditText) findViewById(R.id.search_box);

        mSearchBox.setListener(new ClearableEditText.Listener() {
            @Override
            public void didClearText() {
                TripPlannerActivity.this.refreshPoiList();
            }
        });

        mSearchBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //moves searchBox to the top of the screen
                    scrollTo(mRecyclerView.getHeight());
                }
            }
        });

        mSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().equals("")) {
                    // do not show search if search is empty
                    TripPlannerActivity.this.refreshPoiList();
                }else{
                    TripPlannerActivity.this.refreshPoiList(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripPlannerActivity.this.refreshPoiList(mSearchBox.getText().toString());
            }
        });
    }

    private void scrollTo(int height) {
        mRecyclerView.startNestedScroll(View.SCROLL_AXIS_VERTICAL);
        mRecyclerView.dispatchNestedPreScroll(0, height, null, null);
        mRecyclerView.dispatchNestedScroll(0, 0, 0, 0, new int[]{0, -height});
        mRecyclerView.scrollTo(0, 0);
    }

    private void expandDestination(Destination destination, int position){
        Dbg.d(DEBUG_TAG, "Expanding destination:" + destination.name + " pos:" + position);
        final PoiListViewAdapter listAdapter = super.getPoiListViewAdapter();

        destination.setSelected(true);
        listAdapter.notifyItemChanged(position);

        position ++;
        position = addPoisToList(destination.poiIDs, position);

        if(destination.children == null) return;



        for(Destination child: destination.children){
            listAdapter.addDestination(position ++, child);
        }
    }


    /*
    Reads destination from extras or downloads new
     */
    public void initDestination(){
        final DestinationsManager destManager = new DestinationsManager();

        destManager.loadDestinations();
        mDestination = destManager.getTopDestination();

        if(mDestination == null){
            destManager.downloadNewData(ApiCaller.getInstance(), new DestinationsManager.CallBackFunction() {
                @Override
                public void onNewDataLoaded() {
                    mDestination = destManager.getTopDestination();
                    refreshPoiList();
                }
            });
        }else{
            ((DestinationList) mListManager).setDestination(mDestination);
            refreshPoiList();
        }
    }


    /*
    Sets positions of both sliders

    the thing is that the value in settings might not be the same as
    value set in _VALUES array so that is way it has to find the closest to that value se in settings

    This function is static so it can also be called from other classes.
     */
    public static void setSlidersPositions(CustomSlider mHowMuchTime, CustomSlider mVisitLength) {
        Settings settings = Settings.getInstance();

        // reads value from settings
        float howMuchTime = Util.minToHours(settings.getHowMuchTime());
        // tries to find the closes value to this value from the list of values
        int index = Util.getClosestIndex(HOW_MUCH_TIME_VALUES, howMuchTime);
        // sets the new value to settings
        settings.setHowMuchTime(Util.hoursToMin(HOW_MUCH_TIME_VALUES[index]));
        // moves slider to the right position
        mHowMuchTime.setPosition(index);

        // does same as above
        float visitLength = Util.minToHours(settings.getVisitTime());
        index = Util.getClosestIndex(VISIT_LENGTH_TIME_VALUES, visitLength);
        settings.setVisitTime(Util.hoursToMin(VISIT_LENGTH_TIME_VALUES[index]));
        mVisitLength.setPosition(index);
    }

    @Override
    protected int getRightDrawerLayoutId(){
        return R.layout.drawer_plan_trip;
    }

    @Override
    protected ListManager getListManagerFromId(int listManagerId){
        return new DestinationList(Destination.createUnknown());
    }

    /*
    Sets mSelectedTrack with data
    "equips" track with necessary data
     */
    private void setSelectedTrackData(Integer[] selectedPoiIds){
        mSelectedTrack.name = "Trip in " + mDestination.name;
        mSelectedTrack.timeCreated = System.currentTimeMillis();
        mSelectedTrack.setSelectedPois(selectedPoiIds);
        mSelectedTrack.timePlaned = -1;
    }

    @Override
    public void onClick(View v) {
        Integer[] selectedPoiIds = TripPlannerActivity.super.
                getPoiListViewAdapter().getSelectedPoiIds();

        //"equips" track with necessary data
        setSelectedTrackData(selectedPoiIds);

        switch(v.getId()){
            case R.id.saveTrip:
                if(selectedPoiIds.length == 0) {
                    Toast.makeText(TripPlannerActivity.this, "No POIs selected.", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                //"equips" track with necessary data
                setSelectedTrackData(selectedPoiIds);

                //track is saved later in ChooseDataActivity class

                Intent myIntent = new Intent(TripPlannerActivity.this, ChooseDateActivity.class);
                myIntent.putExtra("selectedTrack", mSelectedTrack);
                TripPlannerActivity.this.startActivity(myIntent);
                break;


            case R.id.startTrip:
                if(selectedPoiIds.length != 0) {
                    //"equips" track with necessary data
                    setSelectedTrackData(selectedPoiIds);

                    //saves track
                    User user = ApiCaller.getInstance().getUser();
                    user.tracks.setSetting(mSelectedTrack.id, mSelectedTrack, true);
                    user.tracks.saveSettings();

                    //set this track as user selected track
                    Settings setting = Settings.getInstance();
                    setting.setTrack(mSelectedTrack);

                    setting.setNavigationMode(NavigationMode.TRIP);
                }

                Intent intent = new Intent(TripPlannerActivity.this, ArrowNavigation.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

    // hides save trip button and other views if user has not selected any pois
    protected void toggleButtonsVisibility(){
        int selectedPoisSize = TripPlannerActivity.super.
                getPoiListViewAdapter().selectedPoisSize();

        int visibility = selectedPoisSize > 0 ? View.GONE : View.GONE;

        findViewById(R.id.or).setVisibility(visibility);
        // mTimeBar.setVisibility(visibility);
        findViewById(R.id.saveTrip).setVisibility(visibility);
    }


    @Override
    public void onAddButtonClicked(View viewClicked, PoiData poiData, int position, boolean isSelected) {
        calculateTripTime();
        toggleButtonsVisibility();
    }

    @Override
    public void onPause(){
        super.onPause();

        // write values of both sliders into settings
        setInSettingsBothSliders(mHowMuchTime.getPosition(), mVisitLength.getPosition());
    }

    /*
    Writes into settings "how much time" and "visit length" value
    This function is static so it can also be called from other classes.
     */
    public static void setInSettingsBothSliders(int howMuchTimeIndex, int visitLenthIndex){
        Settings settings = Settings.getInstance();

        float howMuchTime = TripPlannerActivity.HOW_MUCH_TIME_VALUES[howMuchTimeIndex];
        settings.setHowMuchTime(Util.hoursToMin(howMuchTime));

        float visitLength = TripPlannerActivity.VISIT_LENGTH_TIME_VALUES[visitLenthIndex];
        settings.setVisitTime(Util.hoursToMin(visitLength));
    }


    /*
    Calculates trip time depending on how many and which pois are selected
    and displays it on trip bar
     */
    protected void calculateTripTime(){
        Integer[] selectedPoiIds = TripPlannerActivity.super.
                getPoiListViewAdapter().getSelectedPoiIds();

        HashMap<Integer, PoiData> poiDatas = PoiDataManager.getInstance().getPoiData();

        int tripTime = 0;

        for(int poiId: selectedPoiIds){
            PoiData poiData = poiDatas.get(poiId);

            if(poiData != null) {
                tripTime += poiData.visitDuration;
            }else {
                // TODO At this point poi should not be null since it needed poiData to display this poi
                // this is just a temporary fix
                tripTime += PoiData.DEF_VISIT_DURATION;
            }

            // adds walking time between pois
            tripTime += 10;
        }

        // increase total time by 20 %
        tripTime += (tripTime * 20) / 100;

        // sets the calculate time to the time bar
        mTimeBar.setTimeMinutes(tripTime);

        showExceeded(mTimeBar.getTime() > mTimeBar.getMaxTime());
    }

    public void showExceeded(boolean show){
        if(show){
            mExceededText.setVisibility(View.VISIBLE);
            mTimeBar.setTextColor(Color.RED);
            mTimeBar.setTimeBarColor(Color.RED);
        } else {
            mExceededText.setVisibility(View.INVISIBLE);
            mTimeBar.setTextColor(PoiType.Colors.GREEN);
            mTimeBar.setTimeBarColor(PoiType.Colors.GREEN);
        }
    }

    /*
    Sets time bar max value relative to "how much time do you have" slider's position
     */
    private void setTimeBarMax(int position){
        final int max = Util.hoursToSec(HOW_MUCH_TIME_VALUES[position]);
        mTimeBar.setMinMaxTime(0, max);
    }


    @Override
    public void onSliderPositionChanged(CustomSlider slider, int position, String element) {
        Settings settings = Settings.getInstance();

        if(slider == mHowMuchTime){
            setTimeBarMax(position);
            mTimeBar.refreshTime();
            showExceeded(mTimeBar.getTime() > mTimeBar.getMaxTime());
        } else if(slider == mVisitLength){
            // TODO what to do with this slider?
        }


    }

    @Override
    public void onLocationChanged(LatLng location, float distance) {
        /*
        // no reason to update if position hasn't changed for more than x meters
        if(lastUpdateLocation != null &&
                lastUpdateLocation.getDistance(location) < 50) return;

        // onLocationChanged can be called before mDestination is set;
        if(mDestination == null) return;

        lastUpdateLocation = location;

        ArrayList<Destination> destinationsSorted =
                sortDestinationByDistance(mDestination.children);

        if(hasOrderChanged(destinationsSorted, mAdapter.getList())){
            mAdapter.setList(destinationsSorted);
        }
        */
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


}
