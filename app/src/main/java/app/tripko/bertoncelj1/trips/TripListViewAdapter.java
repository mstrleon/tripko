package app.tripko.bertoncelj1.trips;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Tracks;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 5/30/16.
 *
 * Adapter for trips view list
 */
public class TripListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    public static final int TRIP_SHOW_REQUEST_CODE = 1;
    RecyclerView mRecyclerView;
    private Activity mActivity;
    private ArrayList<Track> mTracks;

    public TripListViewAdapter(Activity pActivity, Track[] pTracks, RecyclerView pRecyclerView) {
        this.mRecyclerView = pRecyclerView;
        this.mActivity = pActivity;
        this.mTracks = new ArrayList<Track>(Arrays.asList(pTracks));
    }

    public void updateList(Track[] newTracks){
        int i;
        for(i=0; i < newTracks.length; i++){
            Track newTrack = newTracks[i];

            // find if this poi is already in list
            int oldPosition = getTrackPosition(newTrack) - 1;

            if(oldPosition != -2){
                if(i != oldPosition) {
                    // if already in list remove it and move it to the new position
                    Track track = mTracks.remove(oldPosition);
                    mTracks.add(i, track);
                    notifyItemMoved(oldPosition, i);
                }
            } else {
                // this poi is not in list so create new
                mTracks.add(i, newTrack);
                notifyItemInserted(i);
            }
        }

        // remove all the remaining data
        // the old pois were pushed down when new tracks were inserted
        int start = i;
        int range = mTracks.size() - start;
        for(i=mTracks.size() - 1; i >= start; i--){
            mTracks.remove(i);
        }

        notifyItemRangeRemoved(start, range);
    }

    public void addTrack(Track pTrack, int listPosition){
        mTracks.add(listPosition - 1, pTrack);
        super.notifyItemInserted(listPosition);
    }

    public void addTrack(Track pTrack){
        addTrack(pTrack, 1);
    }

    public Track getItem(int listPosition){
        return mTracks.get(listPosition - 1);
    }

    public void removeTrack(int listPosition){
        mTracks.remove(listPosition - 1);
        this.notifyItemRemoved(listPosition);
    }

    public int getTrackPosition(Track pTrack){
        for(int i=0; i<mTracks.size(); i++){
            if(mTracks.get(i).id == pTrack.id) return i + 1;
        }

        return -1;
    }

    @Override
    public void onClick(View v) {
        int itemPosition = mRecyclerView.getChildAdapterPosition(v);

        Track track = mTracks.get(itemPosition - 1);
        Intent myIntent = new Intent(mActivity, TripShowActivity.class);
        myIntent.putExtra("selectedTrackId", track.id);
        mActivity.startActivityForResult(myIntent, TRIP_SHOW_REQUEST_CODE);
    }

    //finds track with specified id and updates it's data
    public void updateTrackData(Track track) {

        for(int i=0; i<mTracks.size(); i++){
            if(mTracks.get(i).id == track.id){
                mTracks.set(i, track);
                this.notifyItemChanged(i + 1);
                return;
            }
        }

        Dbg.e("Cannot update track with id " + track.id + ". Track not found.");
    }


    public class TripViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subTitle, distance, trackLengthBubble;
        public ImageView image, favourite, close;
        public ImageButton addButton;


        public TripViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subTitle = (TextView) view.findViewById(R.id.subTitle);
            trackLengthBubble = (TextView) view.findViewById(R.id.numberOfPoisBubble);
            image = (ImageView) view.findViewById(R.id.image);
            addButton = (ImageButton) view.findViewById(R.id.add_button);
        }
    }

    //empty view on the top of the list, if we add element to place 0 it wont
    //animate other elements (move down animation), this empty element enables
    //to add element in second position so that animation will occur
    public class EmptyViewHolder extends RecyclerView.ViewHolder{

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }


    public int getItemViewType(int position) {
        return position == 0? 0 : 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == 0)return new EmptyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.empty, parent, false));

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_trip, parent, false);

        itemView.setOnClickListener(this);
        return new TripViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof TripViewHolder) {
            TripViewHolder tripViewHolder = (TripViewHolder) holder;
            //-1 since there is empty element in beginning
            Track track = mTracks.get(position - 1);

            tripViewHolder.title.setText(track.name);
            String subTitle = track.timePlaned == -1? "Trip date not set." : Util.getDate(track.timePlaned);
            tripViewHolder.subTitle.setText(subTitle);
            tripViewHolder.trackLengthBubble.setText(track.getSize() + " poi");

            final ImageView imageView = tripViewHolder.image;
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            ImageLoader.getInstance().displayImage(ApiCaller.API_URL + track.getImageUrl(), imageView);
        }

    }

    @Override
    public int getItemCount() {
        //+1 for empty element in beginning
        return mTracks.size() + 1;
    }

}
