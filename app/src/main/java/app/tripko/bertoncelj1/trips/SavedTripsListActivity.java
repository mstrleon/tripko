package app.tripko.bertoncelj1.trips;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.poiListView.listManager.ListManager;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Util;
import jp.wasabeef.recyclerview.animators.LandingAnimator;

/**
 * Created by anze on ?
 *
 * Displays all saved trips in list
 */
public class SavedTripsListActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;

    private TextView mNoDataMessage;
    private TextView mCreateNewTrip;
    private TripListViewAdapter mListAdapter;
    private SearchView mSearchBox;

    /*
    Track is not immediately deleted when user swipes but is rather
    deleted when there is no way for user to undo the delete
     */
    private Track mLastDeletedTrack = null;
    private int mLastDeletedTrackPosition;
    Track[] tracks;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_trips_list);

        mNoDataMessage = (TextView) findViewById(R.id.no_data_message_text);
        mCreateNewTrip = (TextView) findViewById(R.id.createNewTrack);
        mCreateNewTrip.setOnClickListener(this);

        //animates the first element into the list
        boolean animateFirstElement = false;
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            animateFirstElement = extras.getBoolean("animateFirstElement", false);
        }

        mSearchBox = (SearchView) findViewById(R.id.searchView);
        if(mSearchBox != null) {
            mSearchBox.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    refreshTracks();
                }
            });

            mSearchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    refreshTracks();
                    return false;
                }
            });
        }

        initList(animateFirstElement);
        initSwipeDelete();
        initToolBar();
    }


    private String getSearchQuerry(){
        return mSearchBox != null ? mSearchBox.getQuery().toString() : "";
    }

    private boolean searchTracks(){
        String searchQuery = getSearchQuerry();
        return mSearchBox != null && mSearchBox.hasFocus() || searchQuery.length() > 0;
    }

    private void refreshTracks(){
        if(searchTracks()){
            Track[] searchedTracks = searchTracks(getSearchQuerry());
            mListAdapter.updateList(searchedTracks);

        } else{
            mListAdapter.updateList(tracks);
        }

        showNoDataMessage();
    }

    private void showNoDataMessage(){
        int size = mListAdapter.getItemCount();
        if(size == 0){
            if(searchTracks()) {
                String searchQuery = getSearchQuerry();

                // display different message if no pois match search query
                mNoDataMessage.setText(searchQuery.equals("") ?  "Type something to search" :
                        "Nothing matches '" + searchQuery + "'");
            } else {
                mNoDataMessage.setText("N tracks ");
            }
            mNoDataMessage.setVisibility(View.VISIBLE);
        }else{
            mNoDataMessage.setVisibility(View.GONE);
        }
    }

    /*
    Returns tracks that match search query
     */
    private Track[] searchTracks(String searchQuerry){
        List<Track> tracks = new ArrayList<>();

        searchQuerry = searchQuerry.toLowerCase();
        for(Track t: this.tracks){
            if(t.name.toLowerCase().contains(searchQuerry)){
                tracks.add(t);
            }
        }

        return tracks.toArray(new Track[tracks.size()]);
    }



    private void initToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.icon_arrow_left_36db);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    private void initList(boolean animateFirstElement){
        mRecyclerView = (RecyclerView) findViewById(R.id.list);

        tracks = ApiCaller.getInstance().getUser().tracks.getSettings(Track.class);
        Arrays.sort(tracks, new Comparator<Track>() {
            @Override
            public int compare(Track lhs, Track rhs) {
                return Util.longCompare(rhs.timeCreated, lhs.timeCreated);
            }
        });

        Track[] track = null;
        if(animateFirstElement) {
            //rewrite list without the first element since it's added later. with delay for animation
            track = new Track[tracks.length - 1];
            for (int i = 0; i < track.length; i++) {
                track[i] = tracks[i + 1];
            }
        }

        if(tracks.length == 0) {
            mNoDataMessage.setVisibility(View.VISIBLE);

        }else {
            mNoDataMessage.setVisibility(View.INVISIBLE);

            mListAdapter = new TripListViewAdapter(this, animateFirstElement? track : tracks, mRecyclerView);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            LandingAnimator animator = new LandingAnimator(){
                @Override
                public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                    return true;
                }
            };
            animator.setAddDuration(200);
            animator.setRemoveDuration(0);
            mRecyclerView.setItemAnimator(animator);
            mRecyclerView.setAdapter(mListAdapter);

            if(animateFirstElement) {
                //add first element with delay, if it is added immediately it wont animate
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRecyclerView.scrollTo(0, 0);
                        mListAdapter.addTrack(tracks[0]);
                    }
                }, 600);
            }
        }
    }

    private void initSwipeDelete(){

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                final int trackPosition = viewHolder.getAdapterPosition();
                removeTrack(trackPosition);
            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    public void removeTrack(int trackPosition) {

        // delete last deleted track if not null, this means that user swiped
        // two track fast and snackBar hasn't hide from the first swipe
        removeLastDeletedTrack();

        mLastDeletedTrack = mListAdapter.getItem(trackPosition);
        mLastDeletedTrackPosition = trackPosition;
        mListAdapter.removeTrack(trackPosition);

        final Snackbar snackbar = Snackbar
                .make(mRecyclerView, "Trip Removed", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reAddLastDeletedTrack();
                    }
                });

        snackbar.setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                super.onDismissed(snackbar, event);
                removeLastDeletedTrack();
            }
        });

        SavedTripsListActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                snackbar.show();
            }
        });

        showNoDataMessage();
    }

    private void removeLastDeletedTrack(){
        if(mLastDeletedTrack != null){
            ApiCaller.getInstance().getUser().tracks.unsetSetting(mLastDeletedTrack.id);
            mLastDeletedTrack = null;
        }
    }

    /*
    Adds track back to the list in case user decided to undo delete action
     */
    private void reAddLastDeletedTrack(){
        if(mLastDeletedTrack != null){
            mListAdapter.addTrack(mLastDeletedTrack, mLastDeletedTrackPosition);
            mLastDeletedTrack = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TripListViewAdapter.TRIP_SHOW_REQUEST_CODE) {
            if(resultCode == TripShowActivity.RESULT_CODE_TRIP_CHANGED && data != null) {

                Track track = (Track) data.getSerializableExtra("track");
                if(track != null) {
                    mListAdapter.updateTrackData(track);
                }else{
                    Dbg.e("Could not acquire track");
                }
            } else if (resultCode == TripShowActivity.RESULT_CODE_TRIP_DELETED){
                Track track = (Track) data.getSerializableExtra("track");
                if(track != null) {
                    final int position = mListAdapter.getTrackPosition(track);
                    if (position != -1) {
                        // delay deletion so that animation is visible
                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                removeTrack(position);
                            }
                        }, 500);
                    } else Dbg.e("Cannot find track with id " + track.id);
                }else{
                    Dbg.e("Could not acquire track");
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Dbg.e("IMPORTANT", "Low memory!");
    }

    @Override
    public void onStop(){
        super.onStop();

        ImageLoader.getInstance().clearMemoryCache();
    }

    @Override
    public void onPause(){
        super.onPause();
        removeLastDeletedTrack();
        ApiCaller.getInstance().getUser().tracks.saveSettings();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.createNewTrack) {
            // if user misclicks undo button, the track wont be deleted,
            // but it will be added back to track list
            reAddLastDeletedTrack();

            /*
            Sends user to trip planner screen so he can create new trip
             */
            startActivity(new Intent(SavedTripsListActivity.this,
                    TripPlannerActivity.class));
        }
    }
}
