package app.tripko.bertoncelj1.trips;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;

/**
 * Created by anze on 6/11/16.
 *
 * Simple dialog for renaming trip
 */
public class RenameTripDialog extends Dialog implements View.OnClickListener{

    interface OnTripNameChangedListener{
        void onTripNameChanged(final String newTripName);
    }

    private OnTripNameChangedListener mListener;

    private TextView mTripName;
    private String mCurrentTripName;

    protected RenameTripDialog(Context context, String pCurrentTripName) {
        super(context);
        setContentView(R.layout.dialog_rename_trip);

        mCurrentTripName = pCurrentTripName;
        mTripName = (TextView) findViewById(R.id.tripName);
        mTripName.setText(pCurrentTripName);

        (findViewById(R.id.renameTrip)).setOnClickListener(this);
    }

    public void setOnTripNameChangedListener(OnTripNameChangedListener pListener){
        mListener = pListener;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.renameTrip:
                final String newTrackName = mTripName.getText().toString().trim();

                if(mListener != null && !newTrackName.equals(mCurrentTripName)){
                    mListener.onTripNameChanged(mTripName.getText().toString());
                }
                super.dismiss();
                break;

        }
    }
}

