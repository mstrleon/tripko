package app.tripko.bertoncelj1.trips;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.poiListView.PoiListViewActivity;
import app.tripko.bertoncelj1.poiListView.PoiListViewAdapter;
import app.tripko.bertoncelj1.poiListView.listManager.DestinationList;
import app.tripko.bertoncelj1.poiListView.listManager.ListManager;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by anze on 7/29/16.
 *
 * This is special activity used only when user wants to add more POIs to already created trip
 */
public class AddPoisToTripActivity extends TripPlannerActivity implements
        PoiListViewAdapter.OnClickListener {

    Destination mDestination;

    public AddPoisToTripActivity(){
        mDestination = ApiCaller.getInstance().getUser().getDestination();

        //creates unknown destination if it does't exist
        if (mDestination == null) mDestination = Destination.createUnknown();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            int trackId = extras.getInt("selectedTrackId", -1);
            User user = ApiCaller.getInstance().getUser();
            mSelectedTrack = user.tracks.getSetting(trackId);
        }
        if(mSelectedTrack == null) {
            Dbg.e("Cannot get selected track!");
            finish();
            return;
        }

        super.addButtons(R.layout.segment_buttons_start_trip, R.id.segment_bottom_buttons);

        findViewById(R.id.startTrip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set this track as user selected track
                Settings setting = Settings.getInstance();
                setting.setTrack(mSelectedTrack);

                setting.setNavigationMode(NavigationMode.TRIP);

                Intent intent = new Intent(AddPoisToTripActivity.this, ArrowNavigation.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        super.getPoiListViewAdapter().setSelectedPois(mSelectedTrack.getSelectedPoisArray());

        getPoiListViewAdapter().setOnButtonClickListener(this);

        super.calculateTripTime();
    }


    /*
    Just hide the buttons
     */
    @Override
    protected void toggleButtonsVisibility() {
        findViewById(R.id.or).setVisibility(View.GONE);
        findViewById(R.id.saveTrip).setVisibility(View.GONE);
    }

    @Override
    protected ListManager getListManagerFromId(int listManagerId){
        return new DestinationList(mDestination);
    }

    @Override
    public void onAddButtonClicked(View viewClicked, PoiData poiData, int position, boolean isSelected) {
        super.onAddButtonClicked(viewClicked, poiData, position, isSelected);

        // adds or removes poi to track without asking user to which one since the track is already known.
        if(isSelected){
            super.addPoiToTrack(mSelectedTrack, poiData.id);
        } else {
            super.removePoiFromTrack(mSelectedTrack, poiData.id);
        }
    }

}
