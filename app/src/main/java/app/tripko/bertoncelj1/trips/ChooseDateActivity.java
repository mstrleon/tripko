package app.tripko.bertoncelj1.trips;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Toast;

import java.util.GregorianCalendar;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Tracks;
import app.tripko.bertoncelj1.onlineAPI.user.User;

/**
 * Created by anze on ?
 *
 * Simple dialog on the bottom of the screen for choosing date when trip is created
 */
public class ChooseDateActivity extends AppCompatActivity implements View.OnClickListener, CalendarView.OnDateChangeListener {

    private long mTimePlaned;
    private Track mTrack;

    private CalendarView mCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date);

        Bundle extras = getIntent().getExtras();
        if(extras == null) throw new IllegalArgumentException("Expecting selected poi ids list");
        else {
            mTrack = (Track) extras.getSerializable("selectedTrack");
            if(mTrack == null) throw new IllegalArgumentException("Expecting track"); //TODO #ONLY_DEBUG finish() and return
        }

        mCalendar = (CalendarView) findViewById(R.id.calendar);
        mCalendar.setMinDate(System.currentTimeMillis() - 1000);

        // the only way to get selected date from calendar is to listen to every time data changes
        mCalendar.setOnDateChangeListener(this);

        findViewById(R.id.saveTrip).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.saveTrip){

            //saves track
            User user = ApiCaller.getInstance().getUser();
            user.tracks.setSetting(mTrack.id, mTrack, true);
            user.tracks.saveSettings();

            Toast.makeText(ChooseDateActivity.this, "Trip saved as: '" + mTrack.name + "'.",
                    Toast.LENGTH_SHORT).show();

            Intent myIntent = new Intent(ChooseDateActivity.this, SavedTripsListActivity.class);
            myIntent.putExtra("animateFirstElement", true);
            ChooseDateActivity.this.startActivity(myIntent);
            finish();
        }
    }

    // the only way to get selected date from calendar is to listen to every time data changes
    @Override
    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
        mTrack.timePlaned = new GregorianCalendar( year, month, dayOfMonth ).getTime().getTime();
    }
}
