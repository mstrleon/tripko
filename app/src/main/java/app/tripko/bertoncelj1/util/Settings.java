package app.tripko.bertoncelj1.util;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.tripko.bertoncelj1.DataStorage;
import app.tripko.bertoncelj1.MyApplication;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.customViews.SwitchChooseButton;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;

/**
 * Created by anze on 4/20/16.
 */
public class Settings implements Serializable, PoiDataManager.OnPoiLoadedCallBack {

    public interface OnSettingsChangedListener extends Serializable {
        void onTripChanged(Settings settings);
        /*
        If isActive is false that means that request is no more active and has benn canceled
         */
        void nextTripPositionRequest(Settings settings, int tripPosition, boolean isActive);

        void onTripPositionChanged(Settings settings, int tripPosition);

        //returns true if navigation mode should changed false otherwise
        boolean onNavigationModeChanged(Settings settings, NavigationMode pNavigationMode);
    }

    private ArrayList<OnSettingsChangedListener> mOnSettingsChangedListeners = new ArrayList<>(5);

    public static final String DEBUG_TAG = "Settings";
    private static final String SETTINGS_PATH = "settings/user";

    private NavigationMode mNavigationMode = NavigationMode.DISCOVERING;

    private static boolean distanceMode = false;

    private Track mTrack = null;

    // location is stored for every poi so we can sort them
    private Map<Integer, LatLng> mPoiPositions = new HashMap<>();

    private int mHowMuchTime = 4 * 60; // 4h
    private int mVisitTime = 10; // 10 minutes
    private ArrayList<PoiType> disabledTypes = new ArrayList<>();
    private int disabledTypesHash = 0;
    private int currentTrackPosition = 0;
    private boolean mNextTrackPositionRequest = false;

    private static Settings sInstance;

    public static Settings getInstance(){
        if(sInstance == null) {
            sInstance = loadSettings();
            if(sInstance == null){
                Dbg.d(DEBUG_TAG, "Couldn't load settings from file creating new settings");
                new Settings();
            }
        }
        return sInstance;
    }

    private Settings(){
        sInstance = this;
    }

    public boolean toggleDistanceMode() {
        return distanceMode = !distanceMode;
    }

    public void setOnSettingsChangedListener(OnSettingsChangedListener listener){
        if(listener == null) throw new NullPointerException("Listener is null!");
        mOnSettingsChangedListeners.add(listener);
    }

    public boolean removeOnSettingsChangedListener(OnSettingsChangedListener listener){
        if(listener == null) throw new NullPointerException("Listener is null!");
        return mOnSettingsChangedListeners.remove(listener);
    }

    public boolean areAnyPoisDisabled(){
        return (disabledTypes != null && disabledTypes.size() != 0);
    }

    public boolean isNextTrackPositionRequest(){
        return mNextTrackPositionRequest;
    }

    public void cancelNextTrackPositionRequest(){
        if(mNextTrackPositionRequest == false)return;
        mNextTrackPositionRequest = false;
        onNextTripPositionRequest(currentTrackPosition, false);
    }

    public void setNextTrackPositionRequest(){
        if(mNextTrackPositionRequest == true)return;
        mNextTrackPositionRequest = true;
        onNextTripPositionRequest(currentTrackPosition, true);
    }


    public int getDisabledTypesHash(){
        return disabledTypesHash;
    }

    public int getHowMuchTime(){
        return mHowMuchTime;
    }

    public int getVisitTime(){
        return mVisitTime;
    }

    public void setHowMuchTime(int minutes){
        mHowMuchTime = minutes;
    }

    public void setVisitTime(int minutes){
        mVisitTime = minutes;
    }


    public ArrayList<PoiType> getDisabledTypes(){
        return disabledTypes;
    }

    public void setTrack(Track track){
        if(!isTrackValid(track)) throw new IllegalArgumentException("Track is not valid");
        mTrack = track;
        updatePoiPositions();

        // make sure that currentTrackPosition is not greater that track size
        currentTrackPosition = currentTrackPosition % mTrack.getSize();

        if(mNavigationMode == NavigationMode.TRIP){
            onTripChanged();
        }
    }

    // goes through pois in track and saves poi position if not yet saved
    private void updatePoiPositions(){
        PoiDataManager manager = PoiDataManager.getInstance();

        for(int poiId: mTrack.getSelectedPois()){
            if(!mPoiPositions.containsKey(poiId)){
                // if poiData with id poiId is not set onPoiDataLoaded is asy called when data is ready
                PoiData poiData = manager.getPoiDataFromId(poiId, this);
                addPoiPosition(poiData);
            }
        }
    }

    @Override
    public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
        addPoiPosition(poiData);
    }

    // extracts position from poi data and saves it
    private void addPoiPosition(PoiData poiData){
        if(poiData == null) return;

        mPoiPositions.put(poiData.id, poiData.latLng.copy());
    }


    public void clearTrack(){
        //if track is invalid(set to null) navigation mode must be discovering
        this.setNavigationMode(NavigationMode.DISCOVERING);
        mTrack = null;
    }

    public NavigationMode getNavigationMode(){
        return mNavigationMode;
    }

    // returns true if navigation mode has changed false otherwise
    public boolean setNavigationMode(NavigationMode pNavigationMode){
        if(mNavigationMode == pNavigationMode) return  false;
        if(pNavigationMode == NavigationMode.TRIP && !isTrackValid(mTrack))return false;

        final NavigationMode oldNavigationMode = mNavigationMode;
        mNavigationMode = pNavigationMode;

        //TODO 15 should we save track position for every track?
        setTrackFirstPosition();

        boolean shouldChange =  onNavigationModeChanged(oldNavigationMode);

        /*
        Start poiTrackService if TRIP mode is set
         */
        if(shouldChange && mNavigationMode == NavigationMode.TRIP){
            PoiTrackService.start(MyApplication.getAppContext());

        }

        return shouldChange;
    }

    private boolean onNavigationModeChanged(NavigationMode oldNavigationMode){
        if(mOnSettingsChangedListeners != null){
            boolean shouldChange = true;

            for(int i=0; i<mOnSettingsChangedListeners.size(); i++){

                // listener returns if navigation mode should change if it shouldn't
                // it sets navigation mode to old value,
                // and all the other listeners are notified that navigation mode is changed to old value

                shouldChange = mOnSettingsChangedListeners.get(i).onNavigationModeChanged(this, mNavigationMode);

                if(!shouldChange) {
                    //notify other listener that navigation mode is not changing
                    for (int j = 0; j < i; j++) {
                        mOnSettingsChangedListeners.get(i).onNavigationModeChanged(this, oldNavigationMode);
                    }

                    //sets to old value
                    mNavigationMode = oldNavigationMode;
                    break;
                }
            }

            return shouldChange;
        }

        return true;
    }

    private void onTripChanged() {
        for (OnSettingsChangedListener listener : mOnSettingsChangedListeners) {
            listener.onTripChanged(this);
        }
    }


    private void onTripPositionChanged(int tripPosition){
        for(OnSettingsChangedListener listener: mOnSettingsChangedListeners){
            listener.onTripPositionChanged(this, tripPosition);
        }
    }

    private void onNextTripPositionRequest(int tripPosition, boolean isActive){
        for(OnSettingsChangedListener listener: mOnSettingsChangedListeners){
            listener.nextTripPositionRequest(this, tripPosition, isActive);
        }
    }

    /*
    Check if track is valid that means that it has some pois in it
     */
    public static boolean isTrackValid(Track track){
        return track != null &&
                track.getSelectedPois() != null &&
                track.getSize() != 0;
    }


    public static NavigationMode stateToNavigationMode(SwitchChooseButton.Option state){
        return state == SwitchChooseButton.Option.RIGHT?
                NavigationMode.DISCOVERING : NavigationMode.TRIP;
    }

    public static SwitchChooseButton.Option navigationModeToState(NavigationMode navigationMode){
        return navigationMode == NavigationMode.DISCOVERING?
                SwitchChooseButton.Option.RIGHT : SwitchChooseButton.Option.LEFT;
    }

    public Track getTrack(){
        return mTrack;
    }

    public int getCurrentTrackPosition(){
        return currentTrackPosition;
    }

    public boolean isLastTrackPosition(){
        return currentTrackPosition == mTrack.getSize() - 1;
    }

    public int getCurrentTrackPositionPoiId(){
        return mTrack.getSelectedPois().get(currentTrackPosition);
    }

    public void setTrackFirstPosition(){
        currentTrackPosition = 0;
    }

    public boolean setNextTrackPosition(){
        mNextTrackPositionRequest = false;

        if(mTrack == null || mTrack.getSelectedPois() == null ||
                mTrack.getSelectedPois().size() <= currentTrackPosition) return false;

        currentTrackPosition ++;

        if(currentTrackPosition >= mTrack.getSize()){
            currentTrackPosition = mTrack.getSize() - 1;

            // finishes trip if there are no more pois
            setNavigationMode(NavigationMode.DISCOVERING);
        }else {
            onTripPositionChanged(currentTrackPosition);
        }
        return true;
    }

    public boolean setPreviousTrackPosition(){
        mNextTrackPositionRequest = false;

        if(mTrack == null || mTrack.getSelectedPois() == null ||
                mTrack.getSelectedPois().size() <= currentTrackPosition) return false;

        currentTrackPosition --;
        if(currentTrackPosition < 0) currentTrackPosition = 0;

        onTripPositionChanged(currentTrackPosition);

        return true;
    }

    // returns true if any poi was removed otherwise returns true
    public boolean removePoiFromTrip(int poiId){
        if(!isTrackValid(mTrack)) {
            Log.e(DEBUG_TAG, "removePoiFromTrip("+poiId+"), track is not valid");
            return false;
        }

        int index = mTrack.getSelectedPois().indexOf((Integer) poiId);

        if(index == -1){
            Log.e(DEBUG_TAG, "Cannot remove poi with id " + poiId + ". Poi doesn't exits");
            return false;
        }

        mTrack.getSelectedPois().remove(index);

        onTripPositionChanged(this.currentTrackPosition);

        return true;
    }

    public void addPoiToTrip(int poiId){
        throw new IllegalAccessError("Function not yet supported");
    }


    //TODO 7 This could be faster with HashMap
    public boolean isDisabled(PoiType t) {
        for(PoiType disabled: disabledTypes){
            if(disabled.equals(t))return true;
        }
        return false;
    }

    public void setType(PoiType type, boolean state) {
        if(state) {
            if(disabledTypes.remove(type))
                disabledTypesHash -= type.hash;
        } else {
            if(disabledTypes.add(type))
                disabledTypesHash += type.hash;
        }
    }

    public boolean isPoiHighlighted(int poiDataId){
        // if track is not selected than poi is not highlighted
        if(mTrack == null || mTrack.getSize() <= currentTrackPosition) return false;

        // if track is ongoing, than check if poi is in that track
        return mTrack.getSelectedPois().get(currentTrackPosition) == poiDataId;
    }

    public static void saveSettings(){
        //TODO check is setting have changed!
        if(sInstance == null) return;
        Dbg.i(DEBUG_TAG, "Saving...");

        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) {
            Dbg.e(DEBUG_TAG, "Storage not available. No data saved.");
            return;
        }

        final String filePath = SETTINGS_PATH;

        //listener cannot be saved since it contains "this" value from the class where it was set
        ArrayList<OnSettingsChangedListener> listeners = sInstance.mOnSettingsChangedListeners;

        //it is not set to null, so that when the settings are loaded this varable is inicialized
        //if we set it to null we would have to init the variable on loading object
        sInstance.mOnSettingsChangedListeners = new ArrayList<>(5);

        if(!dataStorage.save(filePath, sInstance)){
            Dbg.e(DEBUG_TAG, "Error while saving settings");
        }

        sInstance.mOnSettingsChangedListeners = listeners;
    }

    private static Settings loadSettings() {
        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) {
            Dbg.e(DEBUG_TAG, "Storage not available. No settings loaded.");
            return null;
        }

        Object object = dataStorage.loadFileObject(SETTINGS_PATH);

        return object instanceof Settings? (Settings) object : null;
    }

}
