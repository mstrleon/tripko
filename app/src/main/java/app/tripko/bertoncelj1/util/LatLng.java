package app.tripko.bertoncelj1.util;

import android.location.Address;

import java.io.Serializable;
import java.util.List;

/**
 * Created by binbong on 12/23/15.
 */
public class LatLng implements Serializable {
    private double mlatitude;
    private double mlongitude;


    public LatLng(final LatLng latLng) {
        if(latLng == null)throw new NullPointerException("latLng must not be null!");
        set(latLng.mlatitude, latLng.mlongitude);
    }

    public LatLng(double latitude, double longitude) {
        set(latitude, longitude);
    }

    public void set(double latitude, double longitude){
        if(-180.0D <= longitude && longitude < 180.0D) {
            this.mlongitude = longitude;
        } else {
            this.mlongitude = ((longitude - 180.0D) % 360.0D + 360.0D) % 360.0D - 180.0D;
        }

        this.mlatitude = Math.max(-90.0D, Math.min(90.0D, latitude));
    }

    public void set(LatLng latLng){
        set(latLng.mlatitude, latLng.mlongitude);
    }

    public double getLatitude(){
        return mlatitude;
    }

    public com.mapbox.mapboxsdk.geometry.LatLng getMapBoxLatLng() {
        return new com.mapbox.mapboxsdk.geometry.LatLng(this.mlatitude, this.mlongitude);
    }

    public LatLng copy() {
        return new LatLng(this.mlatitude, this.mlongitude);
    }

    public double getLongitude(){
        return mlongitude;
    }

    public LatLng setRandomPosition(double latitudeTop, double longitudeTop, double latitudeBottom, double longitudeBottom){
        this.mlatitude = Util.rand.nextDouble() * (latitudeTop - latitudeBottom) + latitudeBottom;
        this.mlongitude = Util.rand.nextDouble() * (longitudeTop - longitudeBottom) + longitudeBottom;
        return this;
    }

    //sets random position inside top and bottom bounds
    public LatLng setRandomPosition(LatLng top, LatLng bottom){
        this.mlatitude = Util.rand.nextDouble() * (top.mlatitude - bottom.mlatitude) + bottom.mlatitude;
        this.mlongitude = Util.rand.nextDouble() * (top.mlongitude - bottom.mlongitude) + bottom.mlongitude;
        return this;
    }

    //sets random position somewhere around center with cube boundaries
    public LatLng setRandomPosition(LatLng center, int rectSide){
        double rectDistDeg = mToDeg(rectSide);
        setRandomPosition(center.mlatitude + rectDistDeg,
                center.mlongitude + rectDistDeg,
                center.mlatitude - rectDistDeg,
                center.mlongitude - rectDistDeg);
        return this;
    }

    private static LatLng randomPosition = new LatLng(0,0);
    public static LatLng getRandomPosition(LatLng center, int rectSide){
        return randomPosition.setRandomPosition(center, rectSide);
    }

    private static double mToDeg(int meters){
        return meters / 111111;

    }

    //calculates distance between two coordinates on earth
    public float getDistance(LatLng pos) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(this.mlatitude - pos.mlatitude);
        double dLng = Math.toRadians(this.mlongitude - pos.mlongitude);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(pos.mlatitude)) * Math.cos(Math.toRadians(this.mlatitude)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (float) (earthRadius * c);
    }

    public LatLng getMidPoint(LatLng pos){

        //convert to radians
        final double lat1 = Math.toRadians(this.mlatitude);
        final double lat2 = Math.toRadians(pos.mlatitude);
        final double lon1 = Math.toRadians(this.mlongitude);
        final double dLon = Math.toRadians(pos.mlongitude - this.mlongitude);

        double Bx = Math.cos(lat2) * Math.cos(dLon);
        double By = Math.cos(lat2) * Math.sin(dLon);
        double lat = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
        double lon = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

        return new LatLng(Math.toDegrees(lat), Math.toDegrees(lon));
    }

    //get angle between two points
    public float getAngle(LatLng pos){
        double longitude1 = pos.mlongitude;
        double longitude2 = this.mlongitude;
        double latitude1 = Math.toRadians(pos.mlatitude);
        double latitude2 = Math.toRadians(this.mlatitude);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);

        return (float) Math.atan2(y, x);
    }

    @Override
    public String toString(){
        return String.format("lat:%f lon:%f", mlatitude, mlongitude);
    }

    public boolean equals(final LatLng pCompare) {
        return pCompare.mlatitude == this.mlatitude &&
                pCompare.mlongitude == this.mlongitude;
    }

    public boolean equals(final double pLatitude, final double pLongitude) {
        return pLatitude == this.mlatitude &&
                pLongitude == this.mlongitude;
    }

}
