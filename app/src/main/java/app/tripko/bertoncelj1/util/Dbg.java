package app.tripko.bertoncelj1.util;

import android.util.Log;

/**
 * Created by anze on 4/10/16.
 */
public class Dbg {

    public static final String APP_DEBUG_TAG = "Guido";
    private static final String DEFAULT_TAG= "/";

    private Dbg() {

    }

    public static void v(final String pMessage) {
        Dbg.v(DEFAULT_TAG, pMessage, null);
    }

    public static void v(final String pMessage, final Throwable pThrowable) {
        Dbg.v(DEFAULT_TAG, pMessage, pThrowable);
    }

    public static void v(final String pTag, final String pMessage) {
        Dbg.v(pTag, pMessage, null);
    }

    public static void v(final Object pObject, final String pMessage) {
        Dbg.v(pObject.getClass().getSimpleName(), pMessage, null);
    }

    public static void v(String pTag, final String pMessage, final Throwable pThrowable) {
        if (pThrowable == null) {
            Log.v(APP_DEBUG_TAG, pTag + ": " + pMessage);
        } else {
            Log.v(APP_DEBUG_TAG, pTag + ": ", pThrowable);
        }
    }


    public static void d(final String pMessage) {
        Dbg.d(DEFAULT_TAG, pMessage, null);
    }

    public static void d(final String pMessage, final Throwable pThrowable) {
        Dbg.d(DEFAULT_TAG, pMessage, pThrowable);
    }

    public static void d(final String pTag, final String pMessage) {
        Dbg.d(pTag, pMessage, null);
    }

    public static void d(final Object pObject, final String pMessage) {
        Dbg.d(pObject.getClass().getSimpleName(), pMessage, null);
    }

    public static void d(final String pTag, final String pMessage, final Throwable pThrowable) {
        if (pThrowable == null) {
            Log.d(APP_DEBUG_TAG, pTag + ": " + pMessage);
        } else {
            Log.d(APP_DEBUG_TAG, pTag + ": ", pThrowable);
        }
    }

    public static void i(final String pMessage) {
        Dbg.i(DEFAULT_TAG, pMessage, null);
    }

    public static void i(final String pMessage, final Throwable pThrowable) {
        Dbg.i(DEFAULT_TAG, pMessage, pThrowable);
    }

    public static void i(final String pTag, final String pMessage) {
        Dbg.i(pTag, pMessage, null);
    }

    public static void i(final Object pObject, final String pMessage) {
        Dbg.i(pObject.getClass().getSimpleName(), pMessage, null);
    }

    public static void i(final String pTag, final String pMessage, final Throwable pThrowable) {
        if (pThrowable == null) {
            Log.i(APP_DEBUG_TAG, pTag + ": " + pMessage);
        } else {
            Log.i(APP_DEBUG_TAG, pTag + ": ", pThrowable);
        }
    }

    public static void w(final String pMessage) {
        Dbg.w(DEFAULT_TAG, pMessage, null);
    }

    public static void w(final String pMessage, final Throwable pThrowable) {
        Dbg.w(DEFAULT_TAG, pMessage, pThrowable);
    }

    public static void w(final String pTag, final String pMessage) {
        Dbg.w(pTag, pMessage, null);
    }

    public static void w(final Object pObject, final String pMessage) {
        Dbg.w(pObject.getClass().getSimpleName(), pMessage, null);
    }

    public static void w(final String pTag, final String pMessage, final Throwable pThrowable) {
        if (pThrowable == null) {
            Log.w(APP_DEBUG_TAG, pTag + ": " + pMessage);
        } else {
            Log.w(APP_DEBUG_TAG, pTag + ": ", pThrowable);
        }
    }

    public static void e(final String pMessage) {
        Dbg.e(DEFAULT_TAG, pMessage, null);
    }

    public static void e(final String pMessage, final Throwable pThrowable) {
        Dbg.e(DEFAULT_TAG, pMessage, pThrowable);
    }

    public static void e(final String pTag, final String pMessage) {
        Dbg.e(pTag, pMessage, null);
    }

    public static void e(final Object pObject, final String pMessage) {
        Dbg.e(pObject.getClass().getSimpleName(), pMessage, null);
    }

    public static void e(final String pTag, final String pMessage, final Throwable pThrowable) {
        if (pThrowable == null) {
            Log.e(APP_DEBUG_TAG, pTag + ": " + pMessage);
        } else {
            Log.e(APP_DEBUG_TAG, pTag + ": ", pThrowable);
        }
    }
}
