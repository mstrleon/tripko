package app.tripko.bertoncelj1.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import app.tripko.bertoncelj1.MyApplication;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.onlineAPI.user.settings.VisitedSetting;

/**
 * Created by anze on 7/20/16.
 */
public class PoiTrackService extends Service implements
        LocationManager.ILocationListener,
        PoiDataManager.OnPoiLoadedCallBack {

    private static String DEBUG_TAG = "PoiTrackService";

    private boolean mGettingPoiData = false;
    private PoiData mPoiData;

    private static boolean isServiceStarted = false;

    public static void start(Context context){
        Dbg.d(DEBUG_TAG, "starting PoiTrackService, isServiceStarted:" + isServiceStarted);

        if(!isServiceStarted){
            context.startService(new Intent(context, PoiTrackService.class));
        }
    }

    /*
    Gets currently selected poiId from settings and loads poiData for that id
    If PoiData does not exist it loads it

    This function is called every time that poiId in settings changes
     */
    private synchronized void refreshPoiData(Settings settings){
        if(!Settings.isTrackValid(settings.getTrack())) {
            Dbg.e(DEBUG_TAG, "Track is not valid. Cannot refresh poi data");
            return;
        }

        int poiId = settings.getCurrentTrackPositionPoiId();

        PoiDataManager manager = PoiDataManager.getInstance();
        mPoiData = manager.getPoiDataFromId(poiId, this);

        //mPoiData does not yet exist, waiting for onPoiDataLoaded to be called
        if(mPoiData == null && manager.isIsLoadingSinglePoiData()){
            Dbg.i(DEBUG_TAG, "Waiting for PoiDataManager to load poiData");
            mGettingPoiData = true;
        }else {
            throwIfPoiDataIsNotValid(mPoiData);
        }
    }

    /*
    PoiData has been loaded, mGettingPoiData can be set to false
     */
    @Override
    public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
        Dbg.i(this, "New poiData loaded");
        mGettingPoiData = false;
        mPoiData = poiData;
        throwIfPoiDataIsNotValid(mPoiData);
    }

    /*
    Checks if poiData is valid. If it is not this service can not operate.
     */
    private void throwIfPoiDataIsNotValid(PoiData poiData){
        String reason = null;

        if(poiData == null) reason = "PoiData is null";
        if(poiData.latLng == null) reason = "location is null";
        if(poiData.name == null) reason = "name is null";

        if(reason != null) {
            throw new IllegalArgumentException("mPoiData is not valid: " + reason);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Dbg.i(DEBUG_TAG, "service onStartCommand");
        super.onStartCommand(intent, flags, startId);

        isServiceStarted = true;

        // try to set location listener
        boolean ableToAttachListener = setLocationListener();
        if(!ableToAttachListener) {
            // unable to set location listener. This service is of no use, stop it
            this.stopSelf(startId);
            return START_NOT_STICKY;
        }

        return START_STICKY;
    }

    private boolean setLocationListener(){

        if(LocationManager.isInit()){
            // location manager is already init and running just attach listener
            LocationManager.setLocationListener(this);
            return true;
        }else{
            // location manager is not yet available try to start it with app context
            Context context = this.getApplicationContext();

            if(context == null){
                Dbg.d(DEBUG_TAG, "Cannot start PoiTrackService since context is null");
                return false;
            }

            if(!LocationManager.hasLocationPermission(context)){
                Dbg.d(DEBUG_TAG, "App does not have location permission");
                return false;
            }

            // try starting with app context
            try {
                LocationManager.setLocationListener(null, context, this);
            } catch (Exception e){
                Dbg.d(DEBUG_TAG, "Unable to start PoiTrackService since: " + e.getMessage());
                return false;
            }
        }

        return true;
    }

    @Override
    public void onCreate() {
        Dbg.i(DEBUG_TAG, "service onCreate");

        Settings.getInstance().setOnSettingsChangedListener(new Settings.OnSettingsChangedListener() {
            @Override
            public void onTripChanged(Settings settings) {
                refreshPoiData(settings);
            }

            @Override
            public void nextTripPositionRequest(Settings settings, int tripPosition, boolean isActive) {

            }

            @Override
            public void onTripPositionChanged(Settings settings, int tripPosition) {
                refreshPoiData(settings);
            };

            @Override
            public boolean onNavigationModeChanged(Settings settings, NavigationMode pNavigationMode) {
                if(pNavigationMode != NavigationMode.TRIP){
                    PoiTrackService.this.stopSelf();
                }
                return true;
            }
        });

        refreshPoiData(Settings.getInstance());
    }

    @Override
    public void onDestroy() {
        Dbg.i(DEBUG_TAG, "service onDestroy");
        super.onDestroy();

        isServiceStarted = false;
        LocationManager.removeLocationListener(this);
    }

    @Override
    public void onLocationChanged(LatLng location, float distance) {
        if(mGettingPoiData)return;

        Settings settings = Settings.getInstance();

        float distanceToPoi = location.getDistance(mPoiData.latLng);

        //if poi is already pending to be switched then soft inside is used (distance is extended)
        boolean isInRange = settings.isNextTrackPositionRequest()?
                mPoiData.isInsideSoft(distanceToPoi) : mPoiData.isInside(distanceToPoi);

        //set next track position request or cancel it if poi is no longer in range
        if(settings.getNavigationMode() == NavigationMode.TRIP && isInRange){
            settings.setNextTrackPositionRequest();

            setPoiVisited(mPoiData);
        }else{
            settings.cancelNextTrackPositionRequest();
        }
    }

    private void setPoiVisited(PoiData poiData){
        User user = ApiCaller.getInstance().getUser();

        // check if pois is already set as visited
        if(!user.visited.isSettingSet(mPoiData.id)) {
            VisitedSetting.Setting setting = VisitedSetting.createSetting(mPoiData.id);

            user.visited.setSetting(mPoiData.id, setting, true);
            user.visited.saveOnline(null);
        }
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
