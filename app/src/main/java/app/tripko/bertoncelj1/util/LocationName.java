package app.tripko.bertoncelj1.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigationActivity;

/**
 * Created by openstackmaser on 19.2.2016.
 */
public class LocationName {
    private static final String DEBUG_TAG = "LocationName";

    private static LocationName sInstance;

    public interface LocationAddressListener{
        void gotLocationAddress(final LocationAddress pLocationAddress);
    }

    private static final int GET_ADDRESSES_TRIES = 1;

    private Context mContext;
    private Geocoder mGeocoder;

    private static final LocationAddress LOCATION_ADDRESS_FAILED = new LocationAddress(LocationAddressStatus.FAILED);
    private static final LocationAddress LOCATION_ADDRESS_ACQUIRING = new LocationAddress(LocationAddressStatus.ACQUIRING);

    public enum LocationAddressStatus{
        ACQUIRING,
        FAILED,
        SUCCESS,
    }

    public static class LocationAddress{
        public final LocationAddressStatus status;

        public LocationAddress(LocationAddressStatus status){
            this.status = status;
        }

        public String city;
        public String country;
        public String state;

        /*
        Returns the most accurate precise location address available
        the one that describes location the best
         */
        public String getMostPrecise(){
            if(city != null)return city;
            if(country != null)return country;
            return state;
        }
    }

    private void initParams(final Context pContext){
        mContext = pContext;
        mGeocoder = new Geocoder(mContext, Locale.getDefault());
    }


    public static void initialize(final Context pContext){
        if(pContext == null)throw new IllegalArgumentException("Context cannot be null!");
        if(sInstance == null)sInstance = new LocationName();

        sInstance.initParams(pContext);
    }

    public static LocationName instance(){
        //if(sInstance == null)throw new IllegalStateException("Init the instance first by calling initialize().");
        if(sInstance == null)LocationName.initialize(ArrowNavigationActivity.sContext); //self initialization
        return sInstance;
    }

    /*
    Gets location name/address for given location
    Listener is called only if GoogleApi is used. otherwise locationAddress is returned immediately
     */
    @Nullable
    public static LocationAddress getAddress(final LatLng pLocation, LocationAddressListener listener){
        if(listener == null) return LOCATION_ADDRESS_FAILED;

        if(sInstance == null){
            if(ArrowNavigationActivity.sContext == null)return LOCATION_ADDRESS_FAILED;
            initialize(ArrowNavigationActivity.sContext);
        }

        return sInstance._getAddress(pLocation, listener);
    }

    /*
    Trues ti get location address from Geocoder, if that fails,
    it than tries to get it from googleAPI which rarely fails
     */
    @Nullable
    private  LocationAddress _getAddress(final LatLng pLocation, LocationAddressListener listener){
        try {
            List<Address> addresses = mGeocoder.getFromLocation(pLocation.getLatitude(), pLocation.getLongitude(), 1);

            /*
            Geocoder seems to be a bit buggie so it need more tries to get addresses from location
             */
            int tries = GET_ADDRESSES_TRIES;
            try {
                while (addresses.size() == 0 && tries-- > 0) {
                    addresses = mGeocoder.getFromLocation(pLocation.getLatitude(), pLocation.getLongitude(), 1);
                }
            }catch(Exception e){
                Dbg.e(DEBUG_TAG, "network is unavailable or any other I/O problem occurred:" + e.getMessage());
            }

            if (addresses.size() > 0) {
                Log.i(DEBUG_TAG, "Got addresses after " + (GET_ADDRESSES_TRIES - tries) + " tries");

                LocationAddress address = new LocationAddress(LocationAddressStatus.SUCCESS);
                address.country = addresses.get(0).getCountryName();
                address.state = addresses.get(0).getLocality();
                address.city = addresses.get(0).getLocality();

                return address;
            }else {
                Dbg.e(DEBUG_TAG, "Could not get addresses after " + GET_ADDRESSES_TRIES + " tries");

                //tries to get location name from GoogleApi if Geocoder failed to do so
                return _getAddressesGoogleAPI(pLocation, listener);
            }

        } catch (IOException e) {
            e.printStackTrace();
            return new LocationAddress(LocationAddressStatus.FAILED);
        }
    }

    private LocationAddress _getAddressesGoogleAPI(final LatLng pLocation, LocationAddressListener listener){
        String urlString = "http://maps.google.com/maps/api/geocode/json?latlng=" +
                pLocation.getLatitude() + "," +
                pLocation.getLongitude() + "&sensor=true";

        new GetJSON(listener).execute(urlString);
        return LOCATION_ADDRESS_ACQUIRING;
    }

    class GetJSON extends AsyncTask<String, Void, JSONObject> {

        private Exception exception;
        private LocationAddressListener mListener;

        public GetJSON(LocationAddressListener listener){
            mListener = listener;
        }

        protected JSONObject doInBackground(String... urls) {
            try{
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                connection.setRequestProperty("User-Agent", "");
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                connection.connect();

                InputStream inputStream = connection.getInputStream();

                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder("");
                String line;
                while ((line = rd.readLine()) != null) {
                    stringBuilder.append(line);
                }

                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(stringBuilder.toString());
                } catch (JSONException e) {
                    exception = e;
                    return null;
                }

                return jsonObject;

            } catch (IOException e) {
                exception = e;
                return null;
            }
        }

        protected void onPostExecute(JSONObject locationJSON) {
            if(locationJSON == null){
                Dbg.e(DEBUG_TAG, "Could not get location from google API");
                if(exception != null)exception.printStackTrace();
                mListener.gotLocationAddress(new LocationAddress(LocationAddressStatus.FAILED));
                return;
            }

            LocationAddress address = getAddressFromJSON(locationJSON);
            if(address == null){
                Dbg.e(DEBUG_TAG, "Could extract info form google API JSON.");
                mListener.gotLocationAddress(new LocationAddress(LocationAddressStatus.FAILED));
                return;
            }

            mListener.gotLocationAddress(address);
        }
    }

    @Nullable
    private LocationAddress getAddressFromJSON(JSONObject jsonObj){
        LocationAddress locationAddress;
        try {
            String Status = jsonObj.getString("status");
            if(!Status.equalsIgnoreCase("OK")) {
                Dbg.e(DEBUG_TAG, "google api returned " + Status);
                return null;
            }

            locationAddress = new LocationAddress(LocationAddressStatus.SUCCESS);
            JSONArray Results = jsonObj.getJSONArray("results");
            JSONObject zero = Results.getJSONObject(0);
            JSONArray address_components = zero.getJSONArray("address_components");

            for (int i = 0; i < address_components.length(); i++) {
                JSONObject zero2 = address_components.getJSONObject(i);
                String long_name = zero2.getString("long_name");
                JSONArray mtypes = zero2.getJSONArray("types");
                String Type = mtypes.getString(0);

                if (Type.equalsIgnoreCase("locality")) {
                    locationAddress.city = long_name;
                } else {
                    if (Type.equalsIgnoreCase("administrative_area_level_2")) {
                        locationAddress.country = long_name;
                    } else if (Type.equalsIgnoreCase("administrative_area_level_1")) {
                        locationAddress.state = long_name;
                    } else if (Type.equalsIgnoreCase("country")) {
                        locationAddress.country = long_name;
                    }
                }
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
            return LOCATION_ADDRESS_FAILED;
        }

        return locationAddress;
    }



}
