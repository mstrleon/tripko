package app.tripko.bertoncelj1.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;

/**
 * Created by openstackmaser on 3.2.2016.
 */
public class LocationManager implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult>
                                        {

    private static final int REFRESH_TIME = 5000;
    private static final int REFRESH_TIME_FASTEST = 1000;
    private static final int REFRESH_DISTANCE = 2;
    private static final int GPS_PERMISSION_CODE = 8; //something random less then 255

    private static final String DEBUG_TAG = "LocationManager";
    public static LatLng lastGPSPosition = new LatLng(0, 0);
    public static boolean lastGPSPositionAvailable = false;

    private Activity mActivity;
    private Context mContext;
    private ArrayList<ILocationListener> mListeners = new ArrayList<>();

    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static LocationManager sInstance;

    private static final int NOT_NOTIFIED = -1;
    private static final int PROVIDER_ENABLED = 0;
    private static final int PROVIDER_DISABLED = 1;
    private int mLastNotify = NOT_NOTIFIED;

    private LocationManager(Activity pActivity, Context pContext){
        this.mContext = pContext;
        this.mActivity = pActivity;

        mGoogleApiClient = new GoogleApiClient.Builder(pContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public static boolean isInit(){
        return sInstance != null;
    }

    /*
    Just requests location update without attaching listener.
    This can be used if position is read from lastGPSPosition, without a need of a listener
    pActivity parameter can be null if view already has permissions
     */
    public static void requestLocationUpdate(Activity pActivity, Context pContext){
        if(pContext == null && !hasLocationPermission(pContext)){
            throw new IllegalStateException("Activity parameter cannot be null is location service has no permission. It needs activity in order to get the permission.");
        }
        Dbg.d(DEBUG_TAG, "requestLocationUpdate(activity, context)");

        if(sInstance == null) sInstance = new LocationManager(pActivity, pContext);

        // connect google location API
        sInstance.connectAPI();
    }


    public static void setLocationListener(Activity pActivity, Context pContext, ILocationListener listener){
        if(listener == null) throw new NullPointerException("listener == null");
        Dbg.d(DEBUG_TAG, "setLocationListener(activity, context, listener)");


        requestLocationUpdate(pActivity, pContext);

        setLocationListener(listener);
    }

    /*
    Set Location listener without requesting location updates
     */
    public static void setLocationListener(ILocationListener listener){
        if(!isInit()) {
            throw new IllegalStateException("LocationManager is not available " +
                    "you need to init it with " +
                    "requestLocationUpdate or setLocationListener function");
        }

        // add listener
        Dbg.d(DEBUG_TAG, "listener added, num of listeners: " + sInstance.mListeners.size());
        sInstance.mListeners.add(listener);

        // set to not notify so that everyone will get notified when first notify request is send
        // otherwise you are only notified when certain criteria is meet. See where mLastNotify is used
        sInstance.mLastNotify = NOT_NOTIFIED;
    }

    public static void removeLocationListener(ILocationListener listener){
        if(listener == null)throw new NullPointerException("listener == null");
        if(!isInit()) return;
        sInstance.mListeners.remove(listener);

        Dbg.i(DEBUG_TAG, "listener removed, num of listeners: " + sInstance.mListeners.size());

        if(sInstance.mListeners.size() == 0){
            Dbg.d(DEBUG_TAG, "disconnect API");
            sInstance.mGoogleApiClient.disconnect();
        }
    }

    public static boolean isLocationReliable(){
        return lastGPSPositionAvailable &&
                lastGPSPosition != null &&
                lastGPSPosition.getLatitude() != 0 &&
                lastGPSPosition.getLongitude() != 0;
    }

    private void connectAPI(){
        // if already connected no need to reconnect
        if(!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()){
            Dbg.d(DEBUG_TAG, "connect API");
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        this.updateLocation(location);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Dbg.d(DEBUG_TAG, "onConnected");
        if(mLocationRequest == null) {
            setup();
        }

        startLocationUpdates();
    }

    protected void startLocationUpdates() throws SecurityException{
        if(mLocationRequest != null) {
            Dbg.d(DEBUG_TAG, "Requesting location updates");
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Dbg.d(DEBUG_TAG, "onConnectionSuspended:" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Dbg.d(DEBUG_TAG, "onConnectionFailed:" + connectionResult.toString());

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        Status status = locationSettingsResult.getStatus();

        //clear location reques shuld not be used if it is not usful? or shuld we TODO
        if(status.getStatusCode() != LocationSettingsStatusCodes.SUCCESS){
            mLocationRequest = null;
        }

        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                startLocationUpdates();
                // we no longer need activity since we are already connected
                LocationManager.this.mContext = null;
                notifyAllListenerProviderEnabled("GPS");
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // cannot ask user for permission, return
                    if(mActivity == null) return;

                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    status.startResolutionForResult(
                            mActivity,
                            REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Dbg.e(DEBUG_TAG, "Unable to startResolutionForResult() error:" + e.getMessage());
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way
                // to fix the settings so we won't show the dialog.
                notifyAllListenerProviderDisabled("GPS");
                break;
        }
    }



    public interface ILocationListener {
        void onLocationChanged(LatLng location, float distance);
        void onProviderEnabled(String provider);
        void onProviderDisabled(String provider);
    }


    private void setup(){
        Dbg.d(DEBUG_TAG, "GPS setting up...");

        try{
            if (hasLocationPermission(mContext)) {
                Dbg.d(DEBUG_TAG, "Has GSP permission");

                Dbg.d(DEBUG_TAG, "Last known location");
                final Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                updateLocation(lastLocation);

                createLocationRequest();
            } else {
                Dbg.d(DEBUG_TAG, "GPS permission not given");

                showRationaleForGPS();

                ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION_CODE);
            }


        } catch (SecurityException e) {
            Dbg.d(DEBUG_TAG, "GPS permission not granted!");
        }
    }

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    protected void createLocationRequest() {
        Dbg.d(DEBUG_TAG, "Requesting location updates " +
                "provider:" + android.location.LocationManager.GPS_PROVIDER + ", " +
                "refresh time:" + REFRESH_TIME + ", " +
                "minDistance:" + REFRESH_DISTANCE);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(REFRESH_TIME);
        mLocationRequest.setFastestInterval(REFRESH_TIME_FASTEST);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(this);
    }

    public static boolean hasLocationPermission(Context pContext){
        if(pContext == null)throw new NullPointerException("Need activity to check permission");

        return ContextCompat.checkSelfPermission(pContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(pContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    /*
    When an Activity gets onRequestPermissionsResult it must call this function to handle it.
    Returns true if request was for LocationService, false otherwise
     */
    public static boolean onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(sInstance == null)throw new IllegalStateException("You need to first call setLocationListener before resolving permission result");

        switch (requestCode) {
            case GPS_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Dbg.d(DEBUG_TAG, "User granted GPS permission");
                    sInstance.setup();
                } else {
                    Dbg.d(DEBUG_TAG, "User did not grant GPS permission");
                    sInstance.setup();
                }
                return true;
            }
        }

        return false;
    }

    public void updateLocation(Location location){
        //Dbg.d(DEBUG_TAG, "GPS location changed");
        lastGPSPositionAvailable = location != null;

        if(location == null){
            Dbg.e(DEBUG_TAG, "GPS location is null");
            notifyAllListenerProviderDisabled("GPS");
            return;
        }

        final LatLng newLocation = new LatLng(location.getLatitude(), location.getLongitude());

        // if location is 0, 0 that means that location is probably not right.
        if(newLocation.equals(0, 0)){
            notifyAllListenerProviderDisabled("GPS");
        }else {
            notifyAllListenerProviderEnabled("GPS");
        }

        final float distance = lastGPSPosition.getDistance(newLocation);
        if(distance > REFRESH_DISTANCE){
            lastGPSPosition.set(newLocation);

            Dbg.d(DEBUG_TAG, "GPS " + lastGPSPosition + ", distance to last:" + distance);
            notifyAllListenerPositionHasChanged(lastGPSPosition, distance);
        }
    }

    public void notifyAllListenerPositionHasChanged(LatLng newLocation, float distance){
        for(ILocationListener listener: mListeners){
            listener.onLocationChanged(newLocation, distance);
        }
    }


    public void notifyAllListenerProviderEnabled(String provider){
        // notify provider enabled only after provider disabled was notified
        if(mLastNotify != PROVIDER_DISABLED) return;
        mLastNotify = PROVIDER_ENABLED;

        for(ILocationListener listener: mListeners){
            listener.onProviderEnabled(provider);
        }
    }

    public void notifyAllListenerProviderDisabled(String provider){
        if(mLastNotify == PROVIDER_DISABLED) return;
        mLastNotify = PROVIDER_DISABLED;

        for(ILocationListener listener: mListeners){
            listener.onProviderDisabled(provider);
        }
    }

    private void showRationaleForGPS(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mActivity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Explain to the user why we need to use the GPS
                Dbg.d(DEBUG_TAG, "User requested RationaleForGPS");
                //TODO
            }
        }
    }
}
