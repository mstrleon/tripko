package app.tripko.bertoncelj1.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import app.tripko.bertoncelj1.BuildConfig;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigationActivity;

/**
 * Created by binbong on 12/23/15.
 */
public class Util {
    public static final Random rand = new Random();

    private static float sDensity = Resources.getSystem().getDisplayMetrics().density;

    public static float getDensity(){
        return sDensity;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * sDensity);
    }

    public static float dpToPx(float dp)
    {
        return (dp * sDensity);
    }

    public static int spToPx(int sp) {
        return (int) (sp * Resources.getSystem().getDisplayMetrics().scaledDensity);
    }

    public static float pxToSp(float sp){
        return sp / Resources.getSystem().getDisplayMetrics().scaledDensity;
    }

    public static int longCompare(long lhs, long rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }

    public static float limit(float bottom, float x, float top ) {
        if(x > top)return top;
        if(x < bottom)return bottom;
        return x;
    }

    public static int limit(int bottom, int x, int top ) {
        if(x > top)return top;
        if(x < bottom)return bottom;
        return x;
    }

    // converts seconds to hours
    public static float secToHours(int seconds){
        return seconds / 3600f;
    }

    public static int hoursToSec(float hours){
        return (int) (hours * 3600);
    }

    // converts minutes to seconds
    public static float secToMin(int seconds){
        return seconds / 60f;
    }

    public static int minToSec(float minutes){
        return (int) (minutes * 60);
    }

    // converts minutes to hours
    public static float minToHours(int minutes){
        return minutes / 60f;
    }

    public static int hoursToMin(float hours){
        return (int) (hours * 60);
    }


    private static final int WRITE_KM_DISTANCE = 1000; //at which point it should be written km instead fo m, in meters
    private static final int WRITE_KM_ROUND = 10000; //at which point should start rounding numbers
    public static final String METER = " m";
    public static final String KILO_METER = " km";

    public static String distanceToString(float distance){
        if (distance < WRITE_KM_DISTANCE) return (int)(distance) + METER;

        // rounds distance to 1 decimal point accuracy
        if (distance < WRITE_KM_ROUND) return BigDecimal.valueOf(distance / 1000d)
                .setScale(1, BigDecimal.ROUND_HALF_UP) + KILO_METER;

        return (int)(distance/1000) + KILO_METER;
    }

    public static String extractStringValue(final String keyName, AbstractMap<String,Object> map){
        Object value = map.get(keyName);
        if(value == null){
            //Dbg.e("No value for key " + keyName);
        }else {
            if (value instanceof String) {
                return (String) value;
            } else {
                Dbg.e("Key " + keyName + " is not a String " + keyName.getClass());
            }
        }
        return "";
    }

    public static int extractIntegerValue(final String keyName, AbstractMap<String,Object> map){
        return extractIntegerValue(keyName, map, -1);
    }

    public static int extractIntegerValue(final String keyName, AbstractMap<String,Object> map, int defValue){
        Object value = map.get(keyName);
        if(value == null){
            //Dbg.e("No value for key " + keyName);
        }else {
            if (value instanceof Integer) {
                return (Integer) value;
            } else {
                Dbg.e("Key " + keyName + " is not an Integer, " + keyName.getClass());
            }
        }
        return defValue;
    }

    public static double extractDoubleValue(final String keyName, AbstractMap<String,Object> map){
        Object value = map.get(keyName);
        if(value == null){
            //Dbg.e("No value for key " + keyName);
        }else {
            if (value instanceof Double) {
                return (Double) value;
            } else if (value instanceof String) {
                return Double.parseDouble((String) value);
            } else {
                Dbg.e("Key " + keyName + " is not a Double " + keyName.getClass());
            }
        }
        return -1;
    }

    public static boolean isInsideArray(Integer[] array, int value){
        for(Integer i: array){
            if(i == value) return true;
        }

        return false;
    }

    public static void check(boolean assertion){
        if(BuildConfig.DEBUG && !assertion){
            throw new RuntimeException("Assertion failed !");
        }
    }

    public static String getDate(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("dd. MM. yyyy");
        return format.format(date);
    }

    /**** Method for Setting the Height of the ListView dynamically.
     **** Hack to fix the issue of not showing all the items of the ListView
     **** when placed inside a ScrollView  ****/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    /*
    converts color from int (#RRGGBB) to andEngine Color class
     */
    public static org.andengine.util.adt.color.Color convertColor(int color){
        return new org.andengine.util.adt.color.Color(
                Color.red(color) / 255f,
                Color.green(color) / 255f,
                Color.blue(color) / 255f
        );
    }

    /*
    Returns how many hours would someone need to cover that distance
     */
    public static float getHoursDurationFromDistance(float distance) {
        final float walkingSpeed = 5; // km/h
        final float actualDistance = 1.4f; // how many time is actual distance longer than straight distance

        distance /= 1000; // convert to km

        return (distance * actualDistance) / walkingSpeed;
    }

    /*
    Returns index of the element in array that is closest to the given value
     */
    public static int getClosestIndex(float[] array, float value) {
        int closestIndex = 0;
        float closestDistance = Float.MAX_VALUE;


        for(int i=0; i < array.length; i++){
            float distance = Math.abs(array[i] - value);
            if(closestDistance > distance){
                closestDistance = distance;
                closestIndex = i;
            }
        }

        return closestIndex;
    }

    /*
     Returns score for how much is searchWord found in array
    */
    public static int getScore(String searchWord, String[] strs){
        int score = 0;

        if(searchWord.equals("")) return 0;

        // check if whole word matches
        if(Arrays.asList(strs).contains(searchWord))score += 100;

        for(String namePart: strs) {
            int containsIndex = namePart.indexOf(searchWord);
            // if the word starts with search word give higher
            // score than if it is only contained
            if(containsIndex == 0)score += 40;
            else if(containsIndex > 0)score += 20;
        }

        return score / strs.length;
    }

    /*
    Opens mail app with
     */
    public static void openMailApp(Context context, String mailTo, String subject, String text){
        if(context == null) throw new NullPointerException("Cant open mail app without context!");

        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        if(mailTo != null) emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{mailTo});
        if(subject != null) emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
        if(text != null) emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

        /* Send it off to the Activity-Chooser */
        context.startActivity(Intent.createChooser(emailIntent, "Send mail ..."));
    }

    /**
     * Converts from mapbox's LatLng location to my LatLng.
     * Both classes use the same name and it si a bit ugly to call the whole
     * com.mapbox.mapboxsdk.geometry.LatLng when trying to convert fom one to the other.
     * @param latLng MapBox LatLng
     * @return My LatLng
     */
    public static LatLng getMyLatLang(com.mapbox.mapboxsdk.geometry.LatLng latLng){
        return new LatLng(latLng.getLatitude(), latLng.getLongitude());
    }

}
