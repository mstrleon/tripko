package app.tripko.bertoncelj1.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 4/28/16.
 */
public class HttpCalls {
    private static final String DEBUG_TAG = "HttpCalls";

    public interface ResponseFunction {
        void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report);
    }

    public static void getCall(final User pUser, String url, ResponseFunction pFun){
        if(pUser == null)throw new IllegalArgumentException("User is null!");

        new HttpGetCall(pFun, pUser).execute(url);
    }

    public static void postCall(final User pUser, String url,
                                final String pJsonData, ResponseFunction pFun){
        if(pUser == null)throw new IllegalArgumentException("User is null!");

        new HttpPostCall(pFun, pUser, pJsonData).execute(url);
    }

    //first post call for initialising user
    public static void postCallNoUser(String url, final String pJsonData, ResponseFunction pFun){
        new HttpPostCall(pFun, null, pJsonData).execute(url);
    }


    private static class HttpGetCall extends AsyncTask<String, Void, DataReceivedReport> {
        private ResponseFunction mResponseFunction;
        private Object receivedData;
        private Class<?> receivedDataType;
        private String requestedUrl;
        private User mUser;

        public HttpGetCall(ResponseFunction fun, final User pUser){
            this.mUser = pUser;
            this.mResponseFunction = fun;
        }

        @Override
        protected DataReceivedReport doInBackground(String... urls) {
            InputStream is = null;
            try {
                requestedUrl = urls[0];
                URL url = new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                initConnection(conn, mUser);
                conn.setRequestMethod("GET");

                // Starts the query
                conn.connect();
                int responseCode = conn.getResponseCode();
                if(responseCode != 200){
                    return new DataReceivedReport(false, "Wrong response code " + responseCode);
                }

                //gets output and evaluates it
                is = conn.getInputStream();

                String content_type = conn.getHeaderField("Content-Type");

                //type := "application" / "audio" / "image" / "message" / "multipart" / "text" / "video" / x-token
                if(content_type.startsWith("text") || content_type.startsWith("application/json")){
                    receivedDataType = String.class;
                    receivedData = readStringFromInputStream(is);
                    //TODO odvisno od časovne zahtevnosti se tuki lahko prejeto besedlio direkt pretvori v hashmap tako da ga potem ni potrebno pretvarjati v PoiManagerju
                }

                else if(content_type.startsWith("image")){
                    receivedDataType = Bitmap.class;
                    receivedData = BitmapFactory.decodeStream(is);
                }

                else{
                    return new DataReceivedReport(false, "Unrecognized content type " + responseCode);
                }

                is.close();

                //storage.writeFile(requestedUrl, receivedData);
                return new DataReceivedReport(true, "Received something with code " + responseCode);


            } catch (IOException e) {
                Dbg.e(DEBUG_TAG, e.getMessage());
                return new DataReceivedReport(false,
                        "Unable to retrieve web page. URL may be invalid.");
            }
        }

        @Override
        protected void onPostExecute(DataReceivedReport report) {
            if(report.isSuccessful() == false){
                Dbg.w("Guido", requestedUrl + " error because " + report.getMessage());
            }

            if(mResponseFunction != null) {
                mResponseFunction.onDataReceived(receivedData, receivedDataType, requestedUrl, report);
            }
        }
    }

    private static class HttpPostCall extends AsyncTask<String, Void, DataReceivedReport> {
        private final ResponseFunction mResponseFunction;
        private final User mUser;
        private final String mJsonData;

        private Object receivedData;
        private Class<?> receivedDataType;
        private String requestedUrl;

        public HttpPostCall(ResponseFunction fun, final User pUser, final String pJsonData){
            this.mUser = pUser;
            this.mResponseFunction = fun;
            this.mJsonData = pJsonData;
        }

        @Override
        protected DataReceivedReport doInBackground(String... urls) {
            InputStream is = null;
            try {
                try {
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                requestedUrl = urls[0];
                URL url = new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                initConnection(conn, mUser);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);

                //set out data
                OutputStream os = conn.getOutputStream();
                byte[] outputInBytes = mJsonData.getBytes("UTF-8");
                os.write(outputInBytes);
                os.close();

                // Starts the query
                conn.connect();
                int responseCode = conn.getResponseCode();
                if(responseCode != 200){
                    return new DataReceivedReport(false, "Wrong response code " + responseCode);
                }

                //gets output and evaluates it
                is = conn.getInputStream();

                String content_type = conn.getHeaderField("Content-Type");

                //type := "application" / "audio" / "image" / "message" / "multipart" / "text" / "video" / x-token
                if(content_type.startsWith("text") || content_type.startsWith("application/json")){
                    receivedDataType = String.class;
                    receivedData = readStringFromInputStream(is);
                }

                else if(content_type.startsWith("image")){
                    receivedDataType = Bitmap.class;
                    receivedData = BitmapFactory.decodeStream(is);
                }

                else{
                    return new DataReceivedReport(false, "Unrecognized content type " + responseCode);
                }
                is.close();
                return new DataReceivedReport(true, "Received something with code " + responseCode);


            } catch (IOException e) {
                //e.printStackTrace();
                Dbg.e(DEBUG_TAG, e.getMessage());
                return new DataReceivedReport(false,
                        "Unable to retrieve web page. URL may be invalid.");
            }
        }


        @Override
        protected void onPostExecute(DataReceivedReport report) {
            if(report.isSuccessful() == false){
                Dbg.w(DEBUG_TAG, requestedUrl + " error because " + report.getMessage());
            }

            if(mResponseFunction != null) {
                mResponseFunction.onDataReceived(receivedData, receivedDataType, requestedUrl, report);
            }
        }
    }

    private static String readStringFromInputStream(InputStream is) throws  IOException{
        //reads inputStream into StringBuilder
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder(is.available());
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }

    private static void initConnection(final HttpURLConnection conn, final User pUser){
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestProperty("Accept", "application/json");
        //conn.setRequestProperty("Accept-Encoding:", "gzip,deflate");
        //conn.setRequestProperty("Accept-Language:", "sl,en-GB;q=0.8,en;q=0.6,de;q=0.4");
        conn.setRequestProperty("Content-Type", "application/json");
        if(pUser != null) {
            conn.setRequestProperty("Authorization", "Token token=\"" + pUser.getApiKey() + "\"");
        }
        //conn.setRequestProperty("Cookie", "Cookie cookie=\"" + currentUser.cookie + "\"");
    }

    private static String readHeaders(HttpURLConnection conn){
        String headers = "\nHeaders:\n";

        Map<String, List<String>> headerFields = conn.getHeaderFields();
        for(Map.Entry<String, List<String>> header : headerFields.entrySet()){
            headers += header.getKey() + ":";

            List<String> headerValues = header.getValue();
            for(int i=0; i<headerValues.size(); i++){
                headers += " " + headerValues.get(i) + ",";
            }

            headers += "\n";

        }
        return headers;
    }

    // Reads an InputStream and converts it to a Map
    public static HashMap<String, Object> getHashMap(final String input){
        //converts the read string into Map
        try {
            return new ObjectMapper()
                    .readValue(input, new TypeReference<HashMap<String, Object>>() {});
        }catch (IOException e){
            Dbg.e(DEBUG_TAG, "Error while reading HashMap:\n" + e.getMessage());
            return null;
        }
    }
}