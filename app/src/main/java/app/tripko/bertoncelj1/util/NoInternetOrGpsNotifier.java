package app.tripko.bertoncelj1.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.customViews.notificationBar.NotificationBar;
import app.tripko.bertoncelj1.customViews.notificationBar.NotificationBarManager;


/**
 * Created by anze on 9/20/16.
 */
public class NoInternetOrGpsNotifier {

    private static final int NONE = -1;
    public static final int AVAILABLE = 0;
    public static final int UNAVAILABLE = 1;

    private Context mContext;
    private ViewGroup mViewGroup;

    private static final int COLOR_AVAILABLE = PoiType.Colors.GREEN;
    private static final int COLOR_UNAVAILABLE = PoiType.Colors.RED;

    private NotificationBar mNoInternet;
    private int mInternetState = NONE;

    NotificationBar mNoGps;
    private int mGpsState = NONE;

    private Handler mHandler;

    public NoInternetOrGpsNotifier(Context context, ViewGroup viewGroup, int width){
        mContext = context;
        mViewGroup = viewGroup;
        mHandler = new Handler();

        setWidth(width);
    }

    public NoInternetOrGpsNotifier(Context context, ViewGroup viewGroup){
        this(context, viewGroup, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public void setWidth(int width){
        NotificationBarManager.getInstance().setWidth(width);
    }

    private Runnable closeInternetCallback = new Runnable() {
        @Override
        public void run() {
            if(mNoInternet != null && mInternetState == AVAILABLE) {
                mNoInternet.dismiss();
                mNoInternet = null;
            }
        }
    };

    private View.OnClickListener mInternetClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mContext.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
        }
    };

    public void showInternetActivity(int newState){
        if(mNoInternet == null) {
            mNoInternet = createNotificationBar(android.R.drawable.ic_delete);
            mNoInternet.show();
        }

        if(newState == AVAILABLE){
            mNoInternet.setMessage("Got internet connection");
            mNoInternet.setBackgroundColor(COLOR_AVAILABLE);
            mNoInternet.setActionButton(null, null);
            mNoInternet.notifyChanged();

            mHandler.removeCallbacks(closeInternetCallback);

            // hides after 2s
            mHandler.postDelayed(closeInternetCallback, 2000);

        }else if(newState == UNAVAILABLE){
            mNoInternet.setMessage("No internet connection");
            mNoInternet.setBackgroundColor(COLOR_UNAVAILABLE);
            mNoInternet.setActionButton("Settings", mInternetClickListener);
            mNoInternet.notifyChanged();
        }

        mInternetState = newState;
    }

    private Runnable closeGPSCallback = new Runnable() {
        @Override
        public void run() {
            if(mNoGps != null && mGpsState == AVAILABLE) {
                mNoGps.dismiss();
                mNoGps = null;
            }
        }
    };

    private View.OnClickListener mGpsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mContext.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    };

    public void showGpsActivity(int newState){
        if(mNoGps == null) {
            mNoGps = createNotificationBar(android.R.drawable.ic_input_add);
            mNoGps.setActionButton("Settings", mGpsClickListener);
            mNoGps.show();
        }

        if(newState == AVAILABLE){
            mNoGps.setMessage("Got GPS signal");
            mNoGps.setBackgroundColor(COLOR_AVAILABLE);
            mNoGps.setActionButton(null, null);
            mNoGps.notifyChanged();

            mHandler.removeCallbacks(closeGPSCallback);

            // hides after 2s
            mHandler.postDelayed(closeGPSCallback, 2000);

        }else if(newState == UNAVAILABLE){
            mNoGps.setMessage("No GPS");
            mNoGps.setBackgroundColor(COLOR_UNAVAILABLE);
            mNoGps.setActionButton("Settings", mGpsClickListener);
            mNoGps.notifyChanged();
        }

        mGpsState = newState;
    }

    private NotificationBar createNotificationBar(int iconDrawableId){
        return NotificationBar.make(mViewGroup,
                "No message",
                null);
    }
}
