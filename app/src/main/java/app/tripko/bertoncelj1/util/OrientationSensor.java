package app.tripko.bertoncelj1.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Display;
import android.view.Surface;

import java.util.ArrayDeque;

/**
 * Created by anze on 18.5.2016.
 */
public class OrientationSensor implements SensorEventListener {

    private static final String DEBUG_TAG = "OrientationSensor";

    static final float LOW_PASS_FILER_ALPHA = 0.15f;
    private final int AVERAGE_FILTER_LENGTH = 100;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;

    private Display mDisplay; // used to detect screen rotation

    private Float mAzimuth;  // View to draw a compass
    private float[] mGravity;
    private float[] mGeomagnetic;
    AngleLowPassAverageFilter mAzimuthFilter = new AngleLowPassAverageFilter();
    private OnOrientationListener mListener;
    private float mAverageAccuracy = SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM;

    public interface OnOrientationListener{
        void onOrientationChanged(float azimuth);
        void onOrientationAccuracyChanged(int accuracy, boolean isReliable);
    }


    public OrientationSensor(final Context pContext){
        mSensorManager = (SensorManager)pContext.getSystemService(pContext.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public float getAzimuth(){
        return mAzimuthFilter.average();
    }

    public void enable(final Display pDisplay, final OnOrientationListener pListener){
        if(pListener == null) throw new IllegalArgumentException("Listener is null");

        mDisplay = pDisplay;
        mListener = pListener;
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_GAME);
    }

    public void disable(){
        mSensorManager.unregisterListener(this);
        mDisplay = null;
        mListener = null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = lowPass(event.values, mGravity);
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            mGeomagnetic = lowPass(event.values, mGeomagnetic);

            //low pass filter, in order to avoid false triggering,
            //if accuracies are 3, 3, 3, 1, 3, 3, it wont trigger, it has to be low for couple of cycles
            mAverageAccuracy = mAverageAccuracy + 0.2f * (event.accuracy - mAverageAccuracy);
            if(mAverageAccuracy < SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM + 0.001f){
                mListener.onOrientationAccuracyChanged(event.accuracy, false);
            }
        }

        if (mGravity != null && mGeomagnetic != null) {

            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float[] rotationMatrix = rotateCoordinateSystem(R);

                float orientation[] = new float[3];
                SensorManager.getOrientation(rotationMatrix, orientation);

                mAzimuth = orientation[0]; // orientation contains: azimut, pitch and roll

                mAzimuthFilter.add(mAzimuth);
                mListener.onOrientationChanged(getAzimuth());
            }
        }
    }

    private float[] rotateCoordinateSystem(final float R[]){
        int axisX = SensorManager.AXIS_X;
        int axisY = SensorManager.AXIS_Y;
        switch (mDisplay.getRotation()) {
            case Surface.ROTATION_0:
                axisX = SensorManager.AXIS_X;// is this valid?
                axisY = SensorManager.AXIS_Y;// is this valid?
                break;

            case Surface.ROTATION_90:
                axisX = SensorManager.AXIS_Y;
                axisY = SensorManager.AXIS_MINUS_X;
                break;

            case Surface.ROTATION_180:
                axisX = SensorManager.AXIS_MINUS_X;
                axisY = SensorManager.AXIS_MINUS_Y;
                break;

            case Surface.ROTATION_270:
                axisX = SensorManager.AXIS_MINUS_Y;
                axisY = SensorManager.AXIS_X;
                break;
        }

        float[] rotationMatrix = new float[9];
        SensorManager.remapCoordinateSystem(R, axisX, axisY, rotationMatrix);
        return rotationMatrix;
    }

    private String orientationToString(int deviceOrientation) {
        switch (deviceOrientation) {
            case Surface.ROTATION_0: return "0";
            case Surface.ROTATION_90: return "90";
            case Surface.ROTATION_180: return "180";
            case Surface.ROTATION_270: return "270";
        }
        return "?";
    }

    public String accuracyToString(int accuracy){
        switch (accuracy){
            case SensorManager.SENSOR_STATUS_ACCURACY_HIGH: return "high";
            case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM: return "medium";
            case SensorManager.SENSOR_STATUS_ACCURACY_LOW: return "low";
            case SensorManager.SENSOR_STATUS_UNRELIABLE: return "unreliable";
            case SensorManager.SENSOR_STATUS_NO_CONTACT: return "no contact";
        }

        return accuracy + "";
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if(sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            boolean isAccuracyReliable = accuracy > 1;
            mListener.onOrientationAccuracyChanged(accuracy, isAccuracyReliable);
        }
        //Dbg.d(DEBUG_TAG, sensor.toString() + ", accuracy:" + accuracy);
    }

    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) return input;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + LOW_PASS_FILER_ALPHA * (input[i] - output[i]);
        }
        return output;
    }

    private class AngleLowPassAverageFilter {
        private float sumSin, sumCos;
        private ArrayDeque<Float> queue = new ArrayDeque<>();

        public void add(float radians){
            sumSin += (float) Math.sin(radians);
            sumCos += (float) Math.cos(radians);
            queue.add(radians);

            //AngleLowpassFilter approximates the last LENGTH values
            if(queue.size() > AVERAGE_FILTER_LENGTH){
                float old = queue.poll();
                sumSin -= Math.sin(old);
                sumCos -= Math.cos(old);
            }
        }

        public float average(){
            int size = queue.size();
            return (float) Math.atan2(sumSin / size, sumCos / size);
        }
    }
}
