package app.tripko.bertoncelj1.customViews;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigationActivity;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;


/**
 * Created by openstackmaser on 1.2.2016.
 */
public class SwitchChooseButton extends LinearLayout implements CompoundButton.OnCheckedChangeListener {

    public enum Option {
        LEFT, RIGHT;

        public static final int LEFT_INT = 0;
        public static final int RIGHT_INT = 1;
        public static Option fromInteger(int x) {
            switch(x) {
                case LEFT_INT:return LEFT;
                case RIGHT_INT:return RIGHT;
            }
            return null;
        }

    }

    public interface SwitchChooseButtonChanged{
        boolean onSwitchChooseButtonChanged(Option pOptionChosen);
    }

    SwitchChooseButtonChanged mOnChooseButtonChangeListener;

    private static final int COLOR_HIGHLIGHTED = Color.argb(255, 255,255,255);
    private static final int COLOR_NOT_HIGHLIGHTED = Color.argb(150, 255,255,255);
    private static final int ANIMATION_DURATION = 200;
    private Switch mSwitch;
    private TextView mRightText;
    private TextView mLeftText;

    public SwitchChooseButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SwitchChooseButton, 0, 0);
        final String rightTextString = a.getString(R.styleable.SwitchChooseButton_rightText);
        final String leftTextString = a.getString(R.styleable.SwitchChooseButton_leftText);
        final int selectedInt = a.getInt(R.styleable.SwitchChooseButton_selected, Option.LEFT_INT);
        final Option selected = Option.fromInteger(selectedInt);
        a.recycle();

        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.switch_choose_button, this, true);

        mRightText = (TextView) findViewById(R.id.tvRightText);
        mRightText.setText(rightTextString);

        mSwitch = (Switch) findViewById(R.id.sSwitch);
        mSwitch.setOnCheckedChangeListener(this);

        mLeftText = (TextView) findViewById(R.id.tvLeftText);
        mLeftText.setText(leftTextString);

        setState(selected);
    }

    private Option checkedToState(boolean checked){
        return checked? Option.RIGHT : Option.LEFT;
    }

    private boolean stateToChecked(Option state){
        //if (state == Option.LEFT) return false;
        //else if (state == Option.RIGHT) return true;
        return state == Option.RIGHT;
    }

    public Option getState(){
        return checkedToState(mSwitch.isChecked());
    }

    public void setState(final Option state, Activity activity){
        if(activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    setState(state);
                }
            });
        }else{
            setState(state);
        }
    }

    public void setState(final Option state){
        mSwitch.setOnCheckedChangeListener(null);

        final boolean isChecked = stateToChecked(state);
        mSwitch.setChecked(isChecked);
        setTextHighlightColor(isChecked);

        mSwitch.setOnCheckedChangeListener(this);
    }

    public void setOnSwitchChooseButtonChangedListener(SwitchChooseButtonChanged pListener){
        mOnChooseButtonChangeListener = pListener;
    }

    public SwitchChooseButton(Context context) {
        this(context, null);
    }

    public void setRightText(final CharSequence pText){
        mRightText.setText(pText);
    }

    public void setLeftText(final CharSequence pText){
        mLeftText.setText(pText);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        boolean shouldChange = true;
        if(mOnChooseButtonChangeListener != null) {
            shouldChange = mOnChooseButtonChangeListener.onSwitchChooseButtonChanged(
                    checkedToState(isChecked));
        }

        //if it should change just change text to right color otherwise set switch button to old sate
        if(shouldChange){
            setTextHighlightColor(isChecked);
        }else{
            setState(checkedToState(!isChecked));
        }

    }

    private void setTextHighlightColor(boolean isChecked){
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setTextHighlightColorAnimate(isChecked);
        }else{
            setTextHighlightColorImmediate(isChecked);
        }
    }

    private void setTextHighlightColorAnimate(boolean isChecked){
        ObjectAnimator.ofObject(isChecked? mRightText : mLeftText, "textColor", new ArgbEvaluator(), COLOR_NOT_HIGHLIGHTED, COLOR_HIGHLIGHTED)
                .setDuration(ANIMATION_DURATION)
                .start();

        ObjectAnimator.ofObject(isChecked? mLeftText : mRightText, "textColor", new ArgbEvaluator(), COLOR_HIGHLIGHTED, COLOR_NOT_HIGHLIGHTED)
                .setDuration(ANIMATION_DURATION)
                .start();


    }

    private void setTextHighlightColorImmediate(boolean isChecked){
        mRightText.setTextColor(isChecked? COLOR_HIGHLIGHTED : COLOR_NOT_HIGHLIGHTED);
        mLeftText.setTextColor(isChecked? COLOR_NOT_HIGHLIGHTED : COLOR_HIGHLIGHTED);
    }
}