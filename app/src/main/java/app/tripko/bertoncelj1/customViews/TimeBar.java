package app.tripko.bertoncelj1.customViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 8/27/16.
 */
public class TimeBar extends LinearLayout implements ViewTreeObserver.OnGlobalLayoutListener {

    private TextView mTimeText;
    private View mTimeBar;
    private ResizeAnimation mResizeAnimation = new ResizeAnimation();


    private int mMinTime = 0;
    private int mMaxTime = 10 * 3600; // 10 h

    // all the time is represented in seconds
    // time the animation is moving towards
    private int mTargetTime = 0;
    // current time displayed on screen, if animation is not running it should be the same as mTargetTime
    private int mCurrentTime = 0;

    // if view has been drawn
    boolean init = false;

    public TimeBar(Context context) {
        super(context);
    }

    public TimeBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.time_bar, this, true);

        mTimeText = (TextView) findViewById(R.id.timeText);
        mTimeBar = findViewById(R.id.timeBarView);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TimeBar, 0, 0);
        mMinTime = a.getInt(R.styleable.TimeBar_minTime, 0);
        mMaxTime = a.getInt(R.styleable.TimeBar_maxTime, mMaxTime);
        final int animDuration = a.getInt(R.styleable.TimeBar_animationDuration, 500);
        final float textSize = a.getDimension(R.styleable.TimeBar_textSize, mTimeText.getTextSize());
        final int textColor = a.getColor(R.styleable.TimeBar_textColor, mTimeText.getCurrentTextColor());
        final float barHeight = a.getDimension(R.styleable.TimeBar_barHeight, 4);
        a.recycle();

        setAnimationDuration(animDuration);

        mTimeText.setTextSize(Util.pxToSp(textSize));
        mTimeText.setTextColor(textColor);

        // set time bar height
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, (int) barHeight);
        mTimeBar.setLayoutParams(lp);

        // sets background of a parent view to the timeBar background
        //mTimeBar.setBackgroundColor(((ColorDrawable) this.getBackground()).getColor());
        //this.setBackgroundColor(Color.TRANSPARENT);


        ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(this);
    }

    public TimeBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TimeBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setAnimationDuration(long durationMillis){
        mResizeAnimation.setDuration(durationMillis);
    }

    public void setTimeBarColor(int color){
        mTimeBar.setBackgroundColor(color);
    }

    public void setTextColor(int color){
        mTimeText.setTextColor(color);
    }

    public int getMinTime(){
        return mMinTime;
    }

    public int getMaxTime(){
        return mMaxTime;
    }

    public void setMinTime(int minTime){
        mMinTime = minTime;
    }

    public void setMaxTime(int maxTime){
        mMaxTime = maxTime;
    }

    public void setMinMaxTime(int minTime, int maxTime){
        setMinTime(minTime);
        setMaxTime(maxTime);
    }

    public void setTimeHours(float time){
        setTimeSeconds((int)(time * 3600));
    }

    public void setTimeMinutes(float time){
        setTimeSeconds((int)(time * 60));
    }

    public void setTimeSeconds(int time){
        mTargetTime =  time;

        // checks if view have be initiated
        if(!init) return;

        // starts animation
        if(mResizeAnimation.hasStarted()) mResizeAnimation.reset();
        mResizeAnimation.setTargets(mCurrentTime, mTargetTime);
        mTimeBar.startAnimation(mResizeAnimation);
    }

    public void refreshTime(){
        setTimeSeconds(mTargetTime);
    }

    public float getTime(){
        return mTargetTime;
    }


    public static float limit(float bottom, float x, float top ) {
        if(x > top)return top;
        if(x < bottom)return bottom;
        return x;
    }

    public static int limit(int bottom, int x, int top ) {
        if(x > top)return top;
        if(x < bottom)return bottom;
        return x;
    }


    private void setTimeProgress(int time){
        mCurrentTime = time;

        // calculates thw width of the progress bar relative to current time
        int width = this.getWidth() * time / (mMaxTime - mMinTime);

        // due to unknown bug, width must not be 0
        width = Util.limit(1, width, this.getWidth());


        // sets left margin to text so that it is positioned on the center of the end of the time bar
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mTimeText.getWidth(), LinearLayout.LayoutParams.WRAP_CONTENT);
        int left = (width - mTimeText.getWidth() / 2);
        left = limit(0, left, this.getWidth() - mTimeText.getWidth());
        lp.setMargins(left, 0, 0, 0);
        mTimeText.setLayoutParams(lp);

        // updates text
        mTimeText.setText(getTimeText(time));

        // set width of the time bar
        mTimeBar.getLayoutParams().width = width;
        mTimeBar.requestLayout();
    }

    private String getTimeText(int time){
        int hoursInt = time / 3600;
        int minutesInt =  (time / 60) % 60;

        if (hoursInt > 0) return hoursInt + " h";
        else return minutesInt + " min";
    }

    @Override
    public void onGlobalLayout() {
        if(init) return;
        init = true;

        // sets time progress for the first time
        setTimeProgress(mTargetTime);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    public class ResizeAnimation extends Animation {
        int target;
        int start;

        public void setTargets(int start, int target){
            this.start = start;
            this.target = target;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int newTime = (int) (start + (target - start) * interpolatedTime);

            setTimeProgress(newTime);
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }

}
