package app.tripko.bertoncelj1.customViews.notificationBar;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import app.tripko.bertoncelj1.R;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;

/**
 * Created by anze on 9/20/16.
 */
public class NotificationBarManager {

    private static final String DEBUG_TAG = "NotificationBarManager";

    private WeakReference<ViewGroup> mCurrentParentLayout;
    private WeakReference<RecyclerView> mCurrentListLayout;
    private ListAdapter mAdapter;

    private static NotificationBarManager sInstance;

    private int mWidth;

    private NotificationBarManager() {
        sInstance = this;
    }

    public static NotificationBarManager getInstance() {
        return sInstance == null ? new NotificationBarManager() : sInstance;
    }

    private void createHolderLayout(View parent, Context context) {
        if (currentHolderLayoutExists() && currentTopLayoutExists()) {
            RecyclerView pl = mCurrentListLayout.get();
            ViewGroup topLayout = mCurrentParentLayout.get();

            topLayout.removeView(pl);
        }

        mCurrentParentLayout = new WeakReference<>(findTopLayout(parent));

        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup holdView = (ViewGroup) inflater.inflate(
                R.layout.notification_bar_holder_layout, mCurrentParentLayout.get(), false);

        RecyclerView recyclerView = (RecyclerView) holdView.findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        SlideInDownAnimator animator = new SlideInDownAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                return false;
            }
        };
        animator.setAddDuration(200);
        animator.setRemoveDuration(200);
        animator.setChangeDuration(0);
        recyclerView.setItemAnimator(animator);

        mAdapter = new ListAdapter();
        recyclerView.setAdapter(mAdapter);

        initSwipeDelete(recyclerView);

        mCurrentParentLayout.get().addView(holdView);

        mCurrentListLayout = new WeakReference<>(recyclerView);

        updateWidth();
    }

    private void initSwipeDelete(RecyclerView recyclerView){

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public void setWidth(int width){
        mWidth = width;

        if(currentHolderLayoutExists()) updateWidth();
    }

    private void updateWidth(){
        RecyclerView view = mCurrentListLayout.get();
        view.setLayoutParams(
                new LinearLayout.LayoutParams(mWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
    }

    private boolean currentHolderLayoutExists(){
        return mCurrentListLayout != null && mCurrentListLayout.get() != null;
    }

    private boolean currentTopLayoutExists(){
        return mCurrentParentLayout != null && mCurrentParentLayout.get() != null;
    }

    public void show(NotificationBar data, View parent, Context context){
        if(!currentHolderLayoutExists() ||
                !currentTopLayoutExists() ||
                findTopLayout(parent) != mCurrentParentLayout.get()){
            createHolderLayout(parent, context);
        }

        mAdapter.addData(data);
    }

    public void dismiss(NotificationBar data){
        mAdapter.removeData(data);
    }

    public void notify(NotificationBar data){
        mAdapter.notify(data);
    }

    private ViewGroup findTopLayout(View view){
        ViewGroup fallback = null;

        if(true) return (ViewGroup) view;

        do {
            if (view instanceof CoordinatorLayout) {
                // We've found a CoordinatorLayout, use it
                return (ViewGroup) view;
            } else if (view instanceof ViewGroup) {
                if (view.getId() == android.R.id.content) {
                    // If we've hit the decor content view, then we didn't find a CoL in the
                    // hierarchy, so use it.
                    return (ViewGroup) view;
                } else {
                    // It's not the content view but we'll use it as our fallback
                    fallback = (ViewGroup) view;
                }
            }

            if (view != null) {
                // Else, we will loop and crawl up the view hierarchy and try to find a parent
                final ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
            }
        } while (view != null);

        return fallback;
    }

    private static class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        public static final int VIEW_TYPE_EMPTY = 0;
        public static final int VIEW_TYPE_NORMAL = 1;


        ArrayList<NotificationBar> mDataset = new ArrayList<>();

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public static class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            private NotificationBarLayout notificationBar;
            private boolean firstTime = true;

            public ViewHolder(NotificationBarLayout v) {
                super(v);
                notificationBar = v;
            }
        }

        public static class EmptyViewHolder extends RecyclerView.ViewHolder {

            public EmptyViewHolder(View v) {
                super(v);
            }
        }

        public void addData(NotificationBar data){
            // check if data is already added
            if(mDataset.contains(data)) {
                Log.e(DEBUG_TAG, "Cannot add data:" + data);
                return;
            }

            mDataset.add(0, data);
            notifyItemInserted(getItemPosition(0));
        }

        public void removeData(NotificationBar data){
            int indexRemoved = mDataset.indexOf(data);
            if(indexRemoved == -1) {
                Log.e(DEBUG_TAG, "Cannot remove data:" + data);
                return;
            }

            mDataset.remove(indexRemoved);
            notifyItemRemoved(getItemPosition(indexRemoved));
        }

        public void notify(NotificationBar data){
            int index = mDataset.indexOf(data);
            if(index == -1) {
                Log.e(DEBUG_TAG, "Cannot notify data:" + data);
                return;
            }

            notifyItemChanged(getItemPosition(index));
        }

        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            switch (viewType){
                case VIEW_TYPE_EMPTY:
                    return new EmptyViewHolder(LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.empty, parent, false));

                case VIEW_TYPE_NORMAL:
                    return new ViewHolder((NotificationBarLayout) LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.notification_bar_layout, parent, false));

            }

            Log.e(DEBUG_TAG, "Unable to find view with view type:" + viewType);
            return null;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            if(holder instanceof  ViewHolder) {
                final int itemId = (int) this.getItemId(position);
                final NotificationBar data = mDataset.get(itemId);
                final ViewHolder viewHolder = (ViewHolder) holder;

                data.setView(((ViewHolder) holder).notificationBar);

                if(viewHolder.firstTime){
                    viewHolder.firstTime = false;

                    data.updateMessage();
                    data.updateBackgroundColor();
                    data.updateIcon();
                    data.updateActionButton();
                } else {

                    if (data.mMessageNeedsUpdate) data.updateMessage();
                    if (data.mBackgroundColorNeedsUpdate) data.updateBackgroundColor();
                    if (data.mIconImageNeedsUpdate) data.updateIcon();
                    if (data.mAnimationNeedsToRun) data.animateBackgroundColor();
                    if (data.mActionButtonNeedsUpdate) data.updateActionButton();
                }
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            //+1 for empty element at the beginning
            return mDataset.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0? VIEW_TYPE_EMPTY : VIEW_TYPE_NORMAL;
        }

        @Override
        public long getItemId(int position) {
            return position == 0? -1 : position - 1;
        }

        public int getItemPosition(int itemId) {
            return itemId + 1;
        }


    }
}
