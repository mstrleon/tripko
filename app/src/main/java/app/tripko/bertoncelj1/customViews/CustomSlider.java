package app.tripko.bertoncelj1.customViews;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import io.apptik.widget.MultiSlider;

/**
 * Created by anze on 8/24/16.
 */
public class CustomSlider extends LinearLayout implements MultiSlider.OnThumbValueChangeListener, View.OnTouchListener {

    public interface OnPositionChangedListener {
        void onSliderPositionChanged(CustomSlider slider, int position, String element);
    }

    private OnPositionChangedListener mOnPositionChangedListener;

    SnapToPlaceAnimation mSnapToPlaceAnimation = new SnapToPlaceAnimation();

    private TextView mMiddleText;
    private TextView mLeftText;
    private TextView mRightText;
    private MultiSlider mSlider;
    private String[] mElements;

    public CustomSlider(Context context) {
        super(context);
    }

    boolean init = false;
    public CustomSlider(Context context, AttributeSet attrs) {
        super(context, attrs);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_slider, this, true);

        mSlider = (MultiSlider) findViewById(R.id.slider);

        mMiddleText = (TextView) findViewById(R.id.middleText);
        mLeftText = (TextView) findViewById(R.id.leftText);
        mRightText = (TextView) findViewById(R.id.rightText);

        mSlider.setOnThumbValueChangeListener(this);

        mSlider.setOnTouchListener(this);

        /*
        Middle text is always selected. If it is not selected it is invisible
         */
        setSelected(mMiddleText, true);

        this.setElements(new String[]
                        {"Less then an hour", "1 hour", "2 hours",
                        "4 hours", "6 hours", "12 hours", "Whole day"});

        ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(init) return;
                init = true;
                /*
                Refreshes position of the text when layout is set
                 */
                MultiSlider.Thumb thumb = mSlider.getThumb(0);
                onValueChanged(mSlider, thumb, 0, thumb.getValue());

                setTextPosition(mLeftText, mSlider, thumb, 0, false);
                setTextPosition(mRightText, mSlider, thumb, thumb.getMax(), false);
            }
        });
    }

    public void setElements(String[] elements){
        if(elements == null) throw new IllegalArgumentException("Elements are null!");
        if(elements.length < 2) throw new IllegalArgumentException("Need at least two elements, given " + elements.length);

        mElements = elements;

        mSlider.setMin(0);
        mSlider.setStep(1);
        mSlider.setMax((mElements.length - 1) * 64);

        mLeftText.setText(mElements[0]);
        mRightText.setText(mElements[mElements.length - 1]);
    }

    public CustomSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomSlider(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, 0, 0);
    }

    public void setOnPositionChangedListener(OnPositionChangedListener listener){
        mOnPositionChangedListener = listener;
    }

    private void setSelected(TextView textView, boolean selected){
        textView.setTextColor(selected? Color.WHITE : Color.GRAY);
    }

    private void setTextPosition(TextView textView, MultiSlider multiSlider, MultiSlider.Thumb thumb, int value, boolean wrapWidth){
        int left = value  * (multiSlider.getWidth() - thumb.getThumb().getBounds().width()) / (thumb.getMax()-thumb.getMin());
        //set text in the middle
        left += (thumb.getThumb().getBounds().width() - textView.getWidth()) / 2;
        //upostevaj margin
        left += ((LayoutParams) multiSlider.getLayoutParams()).rightMargin;
        left = Math.max(0, left);

        // TODO this should be margins....
        int top = textView.getPaddingTop();
        int right = textView.getPaddingRight();
        int bottom = textView.getPaddingBottom();

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(wrapWidth? ViewGroup.LayoutParams.WRAP_CONTENT : textView.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(left, top, right, bottom);
        textView.setLayoutParams(lp);
    }

    /*
    Gets position of an element form thumb value
     */
    private int getPosition(int thumbValue){
        return (thumbValue + 64 / 2) / 64;
    }

    public int getPosition(){
        return getPosition(mSlider.getThumb(0).getValue());
    }

    public void setPosition(int position){
        if(position < 0 || position > mElements.length - 1) {
            throw new IllegalArgumentException("Position is out of range");
        }

        mSlider.getThumb(0).setValue(this.getValue(position));
    }

    /*
    Calculates thumb's value from element position
     */
    private int getValue(int position){
        return position * 64;
    }

    @Override
    public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
        int position = getPosition(value);

        setSelected(mLeftText, position == 0);
        setSelected(mRightText, position == mElements.length -1);

        if(position == 0 || position == mElements.length - 1){
            mMiddleText.setVisibility(View.INVISIBLE);
        }else{
            mMiddleText.setVisibility(View.VISIBLE);

            setTextPosition(mMiddleText, multiSlider, thumb, value, true);
            mMiddleText.setText(mElements[position]);
        }

        if(mOnPositionChangedListener != null) {
            mOnPositionChangedListener.onSliderPositionChanged(this, position, mElements[position]);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        /*
        Snaps to place when user releases the thumb
         */
        if(event.getAction() == MotionEvent.ACTION_UP ||
                event.getAction() == MotionEvent.ACTION_POINTER_UP) {
            snapToPlace();
        }

        return false;
    }

    /*
    Snaps thumb back to place so it can not be somewhere in between values
     */
    private void snapToPlace(){
        MultiSlider.Thumb thumb = mSlider.getThumb(0);

        int startValue = thumb.getValue();
        int endValue = getValue(this.getPosition());

        // start animation
        if(mSnapToPlaceAnimation.hasStarted()) mSnapToPlaceAnimation.reset();
        mSnapToPlaceAnimation.setTargets(startValue, endValue);
        this.startAnimation(mSnapToPlaceAnimation);
    }

    public class SnapToPlaceAnimation extends Animation {
        int target;
        int start;

        public SnapToPlaceAnimation(){
            this.setDuration(100);
            this.setInterpolator(new FastOutLinearInInterpolator());
        }

        public void setTargets(int start, int target){
            this.start = start;
            this.target = target;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int newValue = (int) (start + (target - start) * interpolatedTime);

            mSlider.getThumb(0).setValue(newValue);
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }
}
