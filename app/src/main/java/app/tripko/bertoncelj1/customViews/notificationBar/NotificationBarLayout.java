package app.tripko.bertoncelj1.customViews.notificationBar;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.customViews.BoundedLinearLayout;


/**
 * Created by anze on 9/20/16.
 */
public class NotificationBarLayout extends LinearLayout {

    private TextView mMessageView;
    private ImageView mIconView;
    private Button mActionButton;

    private OnLayoutChangeListener mOnLayoutChangeListener;

    public NotificationBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        setClickable(true);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mMessageView = (TextView) findViewById(R.id.notificationBar_message);
        mIconView = (ImageView) findViewById(R.id.notificationBar_icon);
        mActionButton = (Button) findViewById(R.id.notificationBar_actionButton);
    }

    public TextView getMessageView() {
        return mMessageView;
    }

    public ImageView getIconView() {
        return mIconView;
    }

    public Button getActionButton() {
        return mActionButton;
    }


    public void animateColorChange(int colorTo){
        if(this.getBackground() instanceof ColorDrawable) {

            int colorFrom = ((ColorDrawable) this.getBackground()).getColor();
            ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
            colorAnimation.setDuration(10250); // milliseconds
            colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    NotificationBarLayout.this.setBackgroundColor((int) animator.getAnimatedValue());
                }

            });
            colorAnimation.start();
        }else {
            // background is not a single color so there is no need to animate it
            this.setBackgroundColor(colorTo);
        }
    }

    void animateChildrenIn(int delay, int duration) {
        ViewCompat.setAlpha(mMessageView, 0f);
        ViewCompat.animate(mMessageView).alpha(1f).setDuration(duration)
                .setStartDelay(delay).start();

        if (mIconView.getVisibility() == VISIBLE) {
            ViewCompat.setAlpha(mIconView, 0f);
            ViewCompat.animate(mIconView).alpha(1f).setDuration(duration)
                    .setStartDelay(delay).start();
        }
    }

    void animateChildrenOut(int delay, int duration) {
        ViewCompat.setAlpha(mMessageView, 1f);
        ViewCompat.animate(mMessageView).alpha(0f).setDuration(duration)
                .setStartDelay(delay).start();

        if (mIconView.getVisibility() == VISIBLE) {
            ViewCompat.setAlpha(mIconView, 1f);
            ViewCompat.animate(mIconView).alpha(0f).setDuration(duration)
                    .setStartDelay(delay).start();
        }
    }


}
