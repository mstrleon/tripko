package app.tripko.bertoncelj1.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 13.5.2016.
 */
public class CustomFontButton extends Button {
    private static final String DEBUG_TAG = "CustomFontButton";

    private static final String DEF_FONT = "fonts/CallunaSansRegular.ttf";

    private static Map<String, Typeface> mTypefaces = new HashMap<>(5);

    public CustomFontButton(Context context) {
        super(context);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }


    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFont);
        String customFont = a.getString(R.styleable.CustomFont_font);
        if(customFont == null) customFont = DEF_FONT;

        setCustomFont(context, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context context, String typefaceAssetPath) {
        Typeface typeface = null;

        if (mTypefaces.containsKey(typefaceAssetPath)) {
            typeface = mTypefaces.get(typefaceAssetPath);
        } else {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), typefaceAssetPath);
            } catch (RuntimeException e) {
                Dbg.e(DEBUG_TAG, "Could not createFromAsset: " + e.getMessage());
                return false;
            }
        }

        setTypeface(typeface);
        return true;
    }

}
