package app.tripko.bertoncelj1.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 11/12/16.
 */

public class OneButtonSpinner extends LinearLayout implements View.OnClickListener {

    private static final int DEFAULT_NUMBER = 0;
    private static final int DEFAULT_MIN_NUMBER = 0;
    private static final int DEFAULT_MAX_NUMBER = 100000;

    public interface OnSpinnerNumberChangedListener {
        void onSpinnerNumberChanged(int newNumber);
    }

    // current number displayed in spinner
    private int mCurrentNumber;

    // number can be limited to min and max
    private int mMaxNumber;
    private int mMinNumber;

    private OnSpinnerNumberChangedListener mListener;

    private TextView mNumber;
    private ImageView mSubtractButton;

    public OneButtonSpinner(Context context) {
        this(context, null);
    }

    public OneButtonSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        // get attributes
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.OneButtonSpinner);
        mCurrentNumber = a.getInteger(R.styleable.OneButtonSpinner_number, DEFAULT_NUMBER);
        mMinNumber = a.getInteger(R.styleable.OneButtonSpinner_min, DEFAULT_MIN_NUMBER);
        mMaxNumber = a.getInteger(R.styleable.OneButtonSpinner_max, DEFAULT_MAX_NUMBER);
        a.recycle();

        // inflate layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.one_button_spinner_layout, this, true);

        // find views
        mNumber = (TextView) findViewById(R.id.number);
        mSubtractButton = (ImageView) findViewById(R.id.subtract_button);

        mNumber.setOnClickListener(this);
        mSubtractButton.setOnClickListener(this);


        // update number for the first time
        updateNumber();
    }


    /**
     * Set the number of the spinner
     * @param number new number to be set
     */
    public void setNumber(int number){
        updateNumber(number);
    }

    /**
     * Return currently visible number
     * @return number that is currently set
     */
    public int getNumber(){
        return mCurrentNumber;
    }

    /**
     * Set listener that is called every time number has changed.
     * Listener wont be called if user has pressed the button but the
     * number hasn't changed due to minimum and maximum
     * @param listener .
     */
    public void setOnNumberChangedListener(OnSpinnerNumberChangedListener listener){
        mListener = listener;
    }


    /**
     * Number on spinner will not go lower than this minimum specified.
     * Make sure that minimum > maximum or IllegalArgumentException will be thrown
     * @param minimum .
     */
    public void setMinumum(int minimum){
        if(minimum > mMaxNumber) {
            throw new IllegalArgumentException("Minimum cannot be greater than maximum! " +
                    "Min:" + minimum + ", Max:" + mMaxNumber);
        }

        mMinNumber = minimum;
        updateNumber();
    }

    public int getMinimum(){
        return mMinNumber;
    }

    /**
     * Number on spinner will exceed this number
     * Make sure that minimum > maximum or IllegalArgumentException will be thrown
     * @param maximum .
     */
    public void setMaximum(int maximum){
        if(maximum < mMinNumber) {
            throw new IllegalArgumentException("Maximum cannot be smaller than minimum! " +
                    "Min:" + mMinNumber + ", Max:" + maximum);
        }

        mMaxNumber = maximum;
        updateNumber();
    }

    public int getMaximum(){
        return mMaxNumber;
    }

    /**
     * SetMinimum and setMaximum combined into one function
     * Make sure that minimum > maximum or IllegalArgumentException will be thrown
     * @param minimum .
     * @param maximum .
     */
    public void setMinMax(int minimum, int maximum){
        if(minimum > maximum) {
            throw new IllegalArgumentException("Minimum cannot be greater than maximum! " +
                    "Min:" + minimum + ", Max:" + maximum);
        }


        mMinNumber = minimum;
        mMaxNumber = maximum;
        updateNumber();
    }

    private void updateNumber(){
        updateNumber(mCurrentNumber);
    }

    private void updateNumber(int number){
        // check if the current number is within specified limits
        if(number < mMinNumber) number = mMinNumber;
        if(number > mMaxNumber) number = mMaxNumber;

        if(number == mCurrentNumber) return;
        mCurrentNumber = number;

        if(mListener != null) {
            mListener.onSpinnerNumberChanged(mCurrentNumber);
        }

        mNumber.setText(String.valueOf(mCurrentNumber));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.number:
                setNumber(mCurrentNumber + 1);
                break;

            case R.id.subtract_button:
                setNumber(mCurrentNumber - 1);
                break;

        }
    }
}
