package app.tripko.bertoncelj1.customViews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 3/14/16.
 */
public class BubbleText extends CustomFontTextView {

    private static Rect sPadding = new Rect();
    static{
        sPadding.left = Util.dpToPx(15);
        sPadding.top = Util.dpToPx(5);
        sPadding.right = Util.dpToPx(15);
        sPadding.bottom = Util.dpToPx(5);
    }

    private static int sHeight = Util.dpToPx(40);
    private static int sCornerRadius = Util.dpToPx(40);

    private static final int STROKE_WIDTH = Util.dpToPx(1);
    private static final int UNSELECTED_COLOR = PoiType.Colors.BACKGROUND;
    private static final int UNSELECTED_STROKE_OPACITY = 80; // from 0 to 256 0->color, 255->white color

    private GradientDrawable mUnselectedDrawable ;

    private static final int UNDEFINED_COLOR = Color.GRAY;

    private boolean mIsSelected = true;
    private int mBackgroundColor = UNDEFINED_COLOR;
    private GradientDrawable mBackgroundDrawable;
    private PoiType mPoiType;
    private boolean mAlwaysSelected = false;

    public BubbleText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BubbleText);
        int color = a.getColor(R.styleable.BubbleText_backgroundColor, UNDEFINED_COLOR);
        setBackgroundColor(color);

        this.setPadding(sPadding.left, sPadding.top, sPadding.right, sPadding.bottom);
    }

    @Override
    public boolean performClick() {
        setSelected(!mIsSelected);
        return super.performClick();
    }

    /**
     * Bubble text is always selected.
     * It will not toggle when clicked and setSelected wont have any effect
     * @param alwaysSelected .
     */
    public void setAlwaysSelected(boolean alwaysSelected){
        this.mAlwaysSelected = alwaysSelected;

        if(alwaysSelected) setSelected(true);
    }

    public void setSelected(boolean pIsSelected){
        if(mAlwaysSelected) pIsSelected = true;

        mIsSelected = pIsSelected;
        super.setBackground(mIsSelected? mBackgroundDrawable : mUnselectedDrawable);
    }

    public boolean isSelected(){
        return mIsSelected;
    }

    /**
     * Set background color when BubbleText is selected
     * @param color .
     */
    public void setBackgroundColor(int color){
        if(mBackgroundColor != color) {
            mBackgroundDrawable = createBackgroundColor(color);

            int strokeColor = combineColors(Color.WHITE, color, UNSELECTED_STROKE_OPACITY);
            mUnselectedDrawable = createBackgroundColor(UNSELECTED_COLOR, strokeColor);
        }

        setSelected(mIsSelected);
    }

    private int combineColors(int color1, int color2, int opacity){
        if(opacity > 256)throw new IllegalArgumentException("opacity > 256, opacity:" + opacity);

        int red = Color.red(color2) + (opacity * (Color.red(color1) - Color.red(color2))) / 256;
        int green = Color.green(color2) + (opacity * (Color.green(color1) - Color.green(color2))) / 256;
        int blue = Color.blue(color2) + (opacity * (Color.blue(color1) - Color.blue(color2))) / 256;

        return Color.argb(255, red, green, blue);
    }

    private static GradientDrawable createBackgroundColor(int color){
        GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP,
                new int[] {color, color});
        gradient.setShape(GradientDrawable.RECTANGLE);
        gradient.setCornerRadius(sCornerRadius);
        gradient.setSize(0, sHeight);

        return gradient;
    }

    private static GradientDrawable createBackgroundColor(int color, int strokeColor){
        GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP,
                new int[] {color, color});
        gradient.setShape(GradientDrawable.RECTANGLE);
        gradient.setCornerRadius(sCornerRadius);
        gradient.setSize(-1, sHeight);
        gradient.setStroke(STROKE_WIDTH, strokeColor);

        return gradient;
    }

    public void setType(final PoiType pPoiType) {
        this.mPoiType = pPoiType;
        this.setText(mPoiType.name);
        this.setBackgroundColor(mPoiType.color);
    }
}
