package app.tripko.bertoncelj1.customViews;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 3/28/16.
 */
public class BubbleTextArranger extends LinearLayout {

    private static int RIGHT_MARGIN = 12;
    private static int BOTTOM_MARGIN = 14;

    private Context mContext;

    //holds bubbles before the width is known,
    // once width is know it moves all bubbles to rows
    private LinearLayout mPreHolder;

    private ArrayList<LinearLayout> mRows = new ArrayList<>();
    private int mLowestRowWidth;
    private LinearLayout.LayoutParams llp;

    public BubbleTextArranger(Context context) {
        super(context);
    }

    public BubbleTextArranger(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        final int right = Util.dpToPx(RIGHT_MARGIN);
        final int bottom = Util.dpToPx(BOTTOM_MARGIN);

        llp = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        llp.setMargins(0, 0, right, bottom);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.bubble_text_arranger, this, true);

        mPreHolder = (LinearLayout) findViewById(R.id.pre_holder);

        //addDummyBubbles(context, 7);
    }

    public int getRowsCount(){
        return mRows.size();
    }

    public void addDummyBubbles(Context context, int n){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.bubble_text_arranger, this, true);

        String[] strs = {"square", "bridge", "jože plečnik", "church", "Italian food", "cathedral", "restaurant", "castle", "botanical garden", "tivoli"};

        for(int i=0; i<n; i++){
            final PoiType type = new PoiType(strs[i%strs.length], PoiType.TYPES[i%PoiType.TYPES.length].color);
            final BubbleText bubbleText = (BubbleText) inflater.inflate(R.layout.bubble_text, null, false);
            bubbleText.setType(type);
            bubbleText.setClickable(true);
            bubbleText.setSelected(true);
            addBubbleText(bubbleText);
        }
    }

    //once layout is draw it moves all bubbles to rows
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        //height is ready
        moveAllFromPreHolder();
    }

    //adds buuble to row if layout is not yet drawn ait adds to preHolder
    public void addBubbleText(final BubbleText pBubbleText){
        if(ViewCompat.isLaidOut(this)){
            addBubbleTextLaidOut(pBubbleText);
        }else{
            mPreHolder.addView(pBubbleText);
        }
    }

    //moves all bubbles from preHolder to rows
    private void moveAllFromPreHolder(){
        while(mPreHolder.getChildCount() > 0){
            View child = mPreHolder.getChildAt(0);

            mPreHolder.removeView(child);
            child.setLayoutParams(llp);
            addBubbleTextLaidOut((BubbleText) child);
        }
    }

    //add new bubble to row, if row is full
    private void addBubbleTextLaidOut(BubbleText pBubbleText) {
        final int bubbleWidth = llp.leftMargin + pBubbleText.getWidth() + llp.rightMargin;

        final LinearLayout row;
        if(mLowestRowWidth - bubbleWidth <= 0){
            row = createNewRow();
            mLowestRowWidth = this.getWidth();
        }else{
            row = mRows.get(mRows.size() - 1);
        }

        pBubbleText.setClickable(true);
        mLowestRowWidth -= bubbleWidth;
        row.addView(pBubbleText);
    }

    private LinearLayout createNewRow(){
        final LayoutParams llp = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        final LinearLayout row = new LinearLayout(mContext);
        row.setLayoutParams(llp);

        this.addView(row);
        mRows.add(row);
        return row;
    }
}
