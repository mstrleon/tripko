package app.tripko.bertoncelj1.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 10/24/16.
 */
public class RateStars extends LinearLayout implements View.OnTouchListener {

    public static final int DEFAULT_NUMBER_OF_STARS = 5;
    public static final int DEFAULT_MARGIN = Util.dpToPx(2);

    private Drawable mRatingStarEmpty;
    private Drawable mRatingStarFull;

    private ImageView mStars[];

    OnRatingChangedListener mLister;

    private int mMargin = DEFAULT_MARGIN;
    private int mNumberOfStars = DEFAULT_NUMBER_OF_STARS;

    public interface OnRatingChangedListener{
        /**
         * This function is called when rating is changed
         * @param rating how many stars are "lit"
         * @param ratingFloat actual rating in float
         */
        void onRatingChanged(int rating, float ratingFloat);
    }

    public RateStars(Context context) {
        this(context, null);

        mMargin = DEFAULT_MARGIN;
        mNumberOfStars = DEFAULT_NUMBER_OF_STARS;
    }

    public RateStars(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RateStar);
        mMargin = a.getDimensionPixelSize(R.styleable.RateStar_margin, DEFAULT_MARGIN);
        mNumberOfStars = a.getInteger(R.styleable.RateStar_numberOfStart, DEFAULT_NUMBER_OF_STARS);
        a.recycle();

        if(mNumberOfStars != 5) throw new IllegalArgumentException("Only 5 stars are currently supported!");

        // loads stars backgrounds
        mRatingStarFull = ResourcesCompat.getDrawable(getResources(), R.drawable.rating_star_64dp, null);
        mRatingStarEmpty = ResourcesCompat.getDrawable(getResources(), R.drawable.rating_star_empty_64dp, null);

        // inflates view
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.rate_stars, this, true);

        main.setOnTouchListener(this);


        // init starts
        mStars = new ImageView[mNumberOfStars];
        mStars[0] = (ImageView) findViewById(R.id.ratingStar1);
        mStars[1] = (ImageView) findViewById(R.id.ratingStar2);
        mStars[2] = (ImageView) findViewById(R.id.ratingStar3);
        mStars[3] = (ImageView) findViewById(R.id.ratingStar4);
        mStars[4] = (ImageView) findViewById(R.id.ratingStar5);

        // Margins cannot not be set in xml layout since margin must change depending on the size of stars
        setMargins(mMargin, mMargin, mMargin, mMargin);

        setRating(1);
    }

    /*
    Set margins to the stars
     */
    private void setMargins(int left, int top, int right, int bottom){
        for(ImageView star: mStars){
            LinearLayout.LayoutParams lp = (LayoutParams) star.getLayoutParams();
            lp.setMargins(left, top, right, bottom);
            star.setLayoutParams(lp);
        }
    }

    public void setOnRatingChangedListener(OnRatingChangedListener listener){
        this.mLister = listener;
    }

    /**
     * Handles click on a star in a row
     * @param index of star clicked
     */
    public void onStarClicked(int index){
        setRating((index + 1) / (float) mNumberOfStars);
    }

    /**
     * Set rating
     * @param rating between 0 and 1
     */
    public void setRating(float rating){
        int ratingInt = (int) (rating * mNumberOfStars);

        if(mLister != null) mLister.onRatingChanged(ratingInt, rating);

        for(int i=0; i < mNumberOfStars; i++){
            setStarHighlighted(i, i < ratingInt);
        }
    }

    /**
     * set appropriate background depending on if star is highlighted or not
     * @param index of a star
     * @param isHighlighted is highlighted or not
     */
    private void setStarHighlighted(int index, boolean isHighlighted){
        if(index < 0 || index >= mNumberOfStars) throw new IllegalArgumentException("Index is out of range!");

        final ImageView star = mStars[index];
        final Drawable newBackground = isHighlighted? mRatingStarFull : mRatingStarEmpty;

        if(star.getBackground() != newBackground) {
            star.setBackground(newBackground);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float singleStarWidth = v.getWidth() / mNumberOfStars;

        // makes sure that at least 1 star is selected
        float x = event.getX() < singleStarWidth? singleStarWidth / 2 : event.getX();

        // calculate rating
        float rating = (x + singleStarWidth) / v.getWidth();
        setRating(rating);

        return true;
    }

    /**
     * Returns star index depending on viewId provided
     * @param id view id of star
     * @return return star with provided id. if star doesn't exits it returns -1
     */
    private int getStarIndex(int id){
        for (int i = 0; i < mStars.length; i++) {
            if (mStars[i].getId() == id) {
                return i;
            }
        }

        return -1;
    }
}
