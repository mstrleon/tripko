package app.tripko.bertoncelj1.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;

/**
 * Created by anze on 13.5.2016.
 */
public class PinInput extends LinearLayout {

    private static final String DOT = "●";
    private static final String BLANK = " ";

    private TextView mInputs[];

    public PinInput(Context context) {
        this(context, null);
    }

    public PinInput(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.pin_input, this, true);

        mInputs = new TextView[4];
        mInputs[0] = (TextView) findViewById(R.id.pinInput1);
        mInputs[1] = (TextView) findViewById(R.id.pinInput2);
        mInputs[2] = (TextView) findViewById(R.id.pinInput3);
        mInputs[3] = (TextView) findViewById(R.id.pinInput4);

        clear();
    }

    public void setDots(int length){
        for(int i=0; i<mInputs.length; i++){
            mInputs[i].setText(i < length? DOT : BLANK);
        }
    }

    public void setCharacters(char[] chars){
        for(int i=0; i<mInputs.length; i++){
            mInputs[i].setText(i < chars.length? chars[i]+"" : BLANK);
        }
    }

    public void clear(){
        for(int i=0; i<mInputs.length; i++){
            mInputs[i].setText(BLANK);
        }
    }

}
