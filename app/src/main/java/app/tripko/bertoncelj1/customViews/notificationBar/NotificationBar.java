package app.tripko.bertoncelj1.customViews.notificationBar;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import app.tripko.bertoncelj1.R;

/**
 * Created by anze on 9/20/16.
 */
public class NotificationBar {

    private static final String DEBUG_TAG = "NotificationBar";
    private WeakReference<NotificationBarLayout> mView;
    private final ViewGroup mTargetParent;
    private Context mContext;

    private CharSequence mMessage = "";
    boolean mMessageNeedsUpdate = true;

    private Drawable mIconImage;
    boolean mIconImageNeedsUpdate = true;

    private CharSequence mActionButtonText = "";

    private int mBackgroundColor = Color.WHITE;
    boolean mBackgroundColorNeedsUpdate = true;

    boolean mAnimationNeedsToRun = false;
    boolean mActionButtonNeedsUpdate = true;

    private View.OnClickListener mListener;


    @NonNull
    public static NotificationBar make(@NonNull ViewGroup parent, @NonNull CharSequence text,
                                       Drawable iconImage) {
        NotificationBar notBar = new NotificationBar(parent, text, iconImage);

        return notBar;
    }

    NotificationBar(ViewGroup parent, CharSequence text, Drawable iconImage) {
        mTargetParent = parent;
        if(parent != null) mContext = parent.getContext();
        mMessage = text;
        mIconImage = iconImage;
    }

    public void show() {
        NotificationBarManager.getInstance().show(this, mTargetParent, mContext);
    }

    public void dismiss(){
        NotificationBarManager.getInstance().dismiss(this);
    }

    public void notifyChanged(){
        NotificationBarManager.getInstance().notify(this);
    }

    public NotificationBarLayout getView(){
        if(mView == null) return null;
        return mView.get();
    }

    public void setView(NotificationBarLayout view){
        mView = new WeakReference<>(view);
    }

    void updateMessage(){
        throwExceptionIfViewNotSet();

        mMessageNeedsUpdate = false;
        final TextView tv = this.getView().getMessageView();
        tv.setText(mMessage);
    }

    public void setMessage(@NonNull CharSequence message) {
        mMessage = message;
        mMessageNeedsUpdate = true;
    }

    public void setActionButton(CharSequence text, View.OnClickListener listener){
        mActionButtonText = text;
        mListener = listener;

        mActionButtonNeedsUpdate = true;
    }

    public void updateActionButton(){
        throwExceptionIfViewNotSet();

        mActionButtonNeedsUpdate = false;

        mMessageNeedsUpdate = false;
        final Button button = this.getView().getActionButton();

        if(mListener == null || mActionButtonText == null){
            button.setVisibility(View.GONE);
        } else {
            button.setVisibility(View.VISIBLE);
            button.setText(mActionButtonText);
            button.setOnClickListener(mListener);
        }
    }

    void updateIcon(){
        throwExceptionIfViewNotSet();

        final ImageView iv = this.getView().getIconView();

        mIconImageNeedsUpdate = false;
        if(mIconImage == null) iv.setVisibility(View.GONE);
        else iv.setImageDrawable(mIconImage);
    }

    public void setIcon(Drawable iconImage) {
        mIconImage = iconImage;
        mIconImageNeedsUpdate = true;
    }

    void updateBackgroundColor(){
        throwExceptionIfViewNotSet();

        mBackgroundColorNeedsUpdate = false;
        this.getView().setBackgroundColor(mBackgroundColor);
    }

    public void setBackgroundColor(int color){
        mBackgroundColor = color;
        mBackgroundColorNeedsUpdate = true;
    }

    public void setBackgroundColorAnimate(int color) {
        mBackgroundColor = color;
        mAnimationNeedsToRun = true;
    }

    void animateBackgroundColor(){
        if(getView() == null) return;

        throwExceptionIfViewNotSet();

        mAnimationNeedsToRun = false;
        this.getView().animateColorChange(mBackgroundColor);
    }

    private void throwExceptionIfViewNotSet(){
        if(getView() == null) throw new NullPointerException("Cant get view");
    }
}
