package app.tripko.bertoncelj1.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import app.tripko.bertoncelj1.R;

/**
 * Created by anze on 7/12/16.
 */
public class SquareLinearLayout extends LinearLayout {

    public SquareLinearLayout(Context context) {
        super(context);
    }

    public SquareLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int size = Math.min(width, height);
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY));
        /*
        switch (mStaticDimencion){
            case NONE:
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                break;

            case WIDTH:
                super.onMeasure(widthMeasureSpec, widthMeasureSpec);
                break;

            case HEIGHT:
                super.onMeasure(heightMeasureSpec, heightMeasureSpec);
                break;

        }
        */
    }
}
