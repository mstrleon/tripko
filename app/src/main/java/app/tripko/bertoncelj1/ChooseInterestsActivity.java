package app.tripko.bertoncelj1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.customViews.BubbleText;
import app.tripko.bertoncelj1.customViews.BubbleTextArranger;
import app.tripko.bertoncelj1.customViews.CustomSlider;
import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.trips.TripPlannerActivity;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

public class ChooseInterestsActivity extends AppCompatActivity {

    private CustomSlider mHowMuchTime;
    private CustomSlider mVisitLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_interests);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.icon_arrow_left_36db);

        //noinspection ConstantConditions
        findViewById(R.id.or).bringToFront();

        //noinspection ConstantConditions
        findViewById(R.id.startUsinga).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = ChooseInterestsActivity.this;

                Intent mainIntent = new Intent(activity, ArrowNavigation.class);
                activity.startActivity(mainIntent);
                activity.finish();
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.startPlanning).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = ChooseInterestsActivity.this;

                Intent mainIntent = new Intent(activity, TripPlannerActivity.class);
                activity.startActivity(mainIntent);

                // if destination has no POIs this activity finishes so
                // that when user presses back it is not returned to this activity but is
                // rather returned to choose destination so that user can pick new destination
                Destination mDestination = ApiCaller.getInstance().getUser().getDestination();
                if(mDestination == null ||
                        mDestination.poiIDs == null ||
                        mDestination.poiIDs.length == 0) {
                    finish();
                }
            }
        });

        mHowMuchTime = (CustomSlider) findViewById(R.id.sliderHowMuchTime);
        mVisitLength = (CustomSlider) findViewById(R.id.sliderShortOrLongVisits);

        TripPlannerActivity.setSlidersPositions(mHowMuchTime, mVisitLength);

        mHowMuchTime.setElements(TripPlannerActivity.HOW_MUCH_TIME_ELEMENTS);
        mVisitLength.setElements(TripPlannerActivity.VISIT_LENGTH_ELEMENTS);

        initBubbles();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // write values of both sliders into settings
        TripPlannerActivity.setInSettingsBothSliders(mHowMuchTime.getPosition(), mVisitLength.getPosition());

        Settings.saveSettings();
    }

    private void initBubbles(){

        final LayoutInflater inflater = this.getLayoutInflater();

        BubbleTextArranger arranger = (BubbleTextArranger) findViewById(R.id.modes_holder);

        Settings settings = Settings.getInstance();
        for(PoiType t: PoiType.TYPES){
            final PoiType type = t;
            final BubbleText bubbleText = (BubbleText) inflater.inflate(R.layout.bubble_text, null, false);
            bubbleText.setType(type);
            bubbleText.setClickable(true);
            bubbleText.setSelected(!settings.isDisabled(t));

            bubbleText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInterestsChanged(type, bubbleText.isSelected());
                }
            });

            arranger.addBubbleText(bubbleText);
        }
    }

    public void onInterestsChanged(PoiType type, boolean state){
        if(state) Settings.getInstance().getDisabledTypes().remove(type);
        else Settings.getInstance().getDisabledTypes().add(type);
    }

}
