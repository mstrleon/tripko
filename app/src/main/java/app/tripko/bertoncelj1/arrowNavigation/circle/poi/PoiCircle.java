package app.tripko.bertoncelj1.arrowNavigation.circle.poi;

import android.content.res.AssetManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;

import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.arrowNavigation.circle.CompassCircle;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.textCloud.CloudPool;
import app.tripko.bertoncelj1.arrowNavigation.textCloud.TextCloud;
import app.tripko.bertoncelj1.arrowNavigation.toast.Toast;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.PoiTrackService;
import app.tripko.bertoncelj1.util.Util;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigationActivity;
import app.tripko.bertoncelj1.arrowNavigation.circle.CircleSprite;

/**
 * Created by openstackmaser on 23.1.2016.
 */
public class PoiCircle extends CircleSprite {


    /*CHAN*/private static final float NOT_CLICKABLE_SIZE = Util.dpToPx(40);

    /*CHAN*/public static final float CIRCLE_UNDER_DISTANCE = 20; //distance at which circle goes under the compass, in meters
    /*CHAN*/private static final int CIRCLE_UNDER_DEPTH = 500; //how deep does a circle go under a compass

    /*CHAN*/private static final float DISTANCE_TEXT_PADDING = 0.7f; //what portion of the circle should text take
    /*CHAN*/private static final float TEXT_CLOUD_SCALE = 3f; //scale size of the text cloud
    /*CHAN*/private static final int TEXT_CLOUD_MARGIN = 5;   //distance between circle and cloud arrow
    private static final String DEBUG_TAG = "PoiCircle";

    //collision parameters
    /*CHAN*/private static float FORCE_DAMPING = 0.5f;    //how much should the force be decreased before applied
    /*CHAN*/private static float VELOCITY_DAMPING = 0.8f; //ow much should the velocity be decreased after force is applied

    private LatLng loc = new LatLng(0, 0); //location on the map
    private final Vector2 primaryPosition = new Vector2(); //this is where the object is supposed to be on screen

    private final int MAX_TEXT_DISTANCE_LENGTH = 100;
    //public Text textBelow;

    private Text mTextDistance;

    private Sprite mSurface;

    private PoiData mPoiData;

    public static long textBelowStartTime; //time when was text below shown
    /*CHAN*/public static final int TIME_ON = 5000; //how long should text cloud be visible

    private int mLastZIndex;
    private float mLastMeasuredDistance;
    private float mLastAngle;
    public boolean isUnderCompass = false;

    private Shader mSurfaceShader;
    private Shader mIconShader;

    private Scene mSceneAttached;

    public static boolean sDistanceMode = false;

    private static int sTileIndex = 0;

    public PoiCircle(float pX, float pY, PoiData pInitData, VertexBufferObjectManager pVertexBufferObjectManager){
        super(pX, pY, PoiCircleMapTexture.getPoiIconsMap(), pVertexBufferObjectManager);

        mPoiData = pInitData;

        mIconShader = Shader.obtain();
        this.setShaderProgram(mIconShader);

        final TextureRegion surfaceTexture = PoiCircleMapTexture.getPoiSurfaceTexture();
        mSurface = new Sprite(0, 0, surfaceTexture, pVertexBufferObjectManager);
        mSurface.setVisible(true);
        mSurfaceShader = Shader.obtain();
        mSurfaceShader.setShadedIntensity(0.3f);
        mSurface.setShaderProgram(mSurfaceShader);

        this.setCurrentTileIndex(mPoiData.textureIndex);

        mTextDistance = new Text(0, 0, ArrowNavigation.mFontCallunaSans, "", MAX_TEXT_DISTANCE_LENGTH, this.getVertexBufferObjectManager());
        mTextDistance.setHorizontalAlign(HorizontalAlign.CENTER);
        mTextDistance.setText("??? m");
        mTextDistance.setVisible(false);
    }


    public float getLastMeasuredDistance(){
        return mLastMeasuredDistance;
    }

    public PoiData getPoiData(){
        return mPoiData;
    }

    public void setPoiData(final PoiData pPoiData) {
        this.mPoiData = pPoiData;
    }

    //only sets title index if you want to switch between distance modes  you have to call setCurrentTileIndexDistance
    @Override
    public void setCurrentTileIndex(int pCurrentTileIndex) {
        if(pCurrentTileIndex > PoiCircleMapTexture.getTileCount()){
            Dbg.e(DEBUG_TAG, "Incorrect tile index: " + pCurrentTileIndex);
        }
        final int color = PoiCircleMapTexture.getColorFromTextureIndex(pCurrentTileIndex);
        mSurfaceShader.setColor(color);

        super.setCurrentTileIndex(pCurrentTileIndex);
    }



    public void setPointName(final String pName){
        mPoiData.name = pName;
    }

    public String getPointName(){
        return mPoiData.name;
    }

    public void setDistanceMode(boolean pDistanceMode){
        mTextDistance.setVisible(pDistanceMode);
        this.setVisible(!pDistanceMode);
    }

    public void setShaded(boolean shaded){
        this.mSurfaceShader.setShaded(shaded);
        this.mIconShader.setShaded(shaded);
    }

    public boolean isShaded(){
        return this.mSurfaceShader.isShaded();
    }

    public void detachFromScene() {
        mSceneAttached.detachChild(this);
        mSceneAttached.detachChild(mTextDistance);
        mSceneAttached.detachChild(mSurface);
    }

    @Override
    public void dispose() {
        super.dispose();

        Shader.recycle(mSurfaceShader);
        Shader.recycle(mIconShader);
    }

    public void setScale(){
        final float distance = loc.getDistance(LocationManager.lastGPSPosition);
        mLastMeasuredDistance = distance;

        isUnderCompass = distance < CIRCLE_UNDER_DISTANCE;
        if(sDistanceMode){
            mTextDistance.setText(Util.distanceToString(distance));
        }

        //TODO krogci grejo pregloboko pod komaps...vrjetno je napacna distance fnkcija
        //if(distance <= CIRCLE_UNDER_DISTANCE) mContactDistance = -CIRCLE_UNDER_DEPTH;
        //else if(distance < 50) mContactDistance = DEFAULT_CONTACT_DISTANCE * 4;
        //else mContactDistance = DEFAULT_CONTACT_DISTANCE;

        final float newSize = distanceToSize(distance);
        super.setSize(newSize);

        this.resetRotationCenter();
    }

    public void hide(){
        mTextDistance.setVisible(false);
        mTextDistance.setIgnoreUpdate(true);

        mSurface.setVisible(false);
        mSurface.setIgnoreUpdate(true);

        this.setVisible(false);
        this.setIgnoreUpdate(true);
    }

    public void show(boolean shaded){
        super.setSizeDirect(0);
        mSurface.setScale(0);

        mTextDistance.setVisible(sDistanceMode);
        mTextDistance.setIgnoreUpdate(false);

        mSurface.setVisible(true);
        mSurface.setIgnoreUpdate(false);

        this.setVisible(!sDistanceMode);
        this.setShaded(shaded);
        this.setIgnoreUpdate(false);
    }

    private void moveToPrimaryPosition(){
        this.calculatePrimaryPosition(ArrowNavigation.compassAngle);

        final float x = primaryPosition.x;
        final float y = primaryPosition.y;

        Body body = (Body) this.getUserData();
        if(body != null) {
            body.setTransform(x / 32, y / 32, 0);
            body.setLinearVelocity(0, 0);
        }
        this.setPosition(x, y);
        mSurface.setPosition(x, y);
    }

    /*CHAN*/ private final float distanceToSizeTab[][] = {
            {5,110},   //1
            {10, 100}, //2
            {25, 90},  //3
            {50, 80},  //4
            {100, 70}, //5
            {150, 65}, //6
            {200, 60}, //7
            {350, 50}, //8
            {500, 40}, //9
            {1000, 30},//10
            {10000, 22}//11
    };

    int category = 0;
    // gets distance in meters, returns size
	/* CHAN */ private float distanceToSize(float distance) {
        category = distanceToSizeTab.length - 1;
        float ret = distanceToSizeTab[category][1];
        for (int i = 1; i < distanceToSizeTab.length-1; i++) {
            if (distance < distanceToSizeTab[i][0]) {
                category = i;
                ret = getValueOnLinearLine(distanceToSizeTab[i-1], distanceToSizeTab[i], distance);
                break;
            }
        }

        return Util.dpToPx(ret);
    }

    /*CHAN*/ private final float distanceToScreenTab[][] = {
            {50, 0.15f},
            {200, 0.3f},
            {500, 0.45f},
            {1000, 0.7f},
            {2000, 1f},
    };
    public float procentage;
    public float mdistance;
    public static float min = 0.25f;
    public static float max = 0.35f;
    //gets distance in meters returns distance from compass arrow
    /*CHAN*/private float distanceToScreenLocation(float  distance){
        float ret = distanceToScreenTab[distanceToScreenTab.length-1][1];
        for(int i=0; i < distanceToScreenTab.length-1; i++){
            if(distance < distanceToScreenTab[i][0]){
                ret = getValueOnLinearLine(distanceToScreenTab[i], distanceToScreenTab[i+1], distance);
                break;
            }
        }
        if(ret - 0.02 < min)min = ret - 0.02f;
        if(ret + 0.05 > max)max = ret + 0.05f;
        ret = (ret - min) / (max - min);
        procentage = ret;
        float centerSize = CompassCircle.staticThis != null? CompassCircle.staticThis.getSize()/2 : ArrowNavigation.CAMERA_WIDTH / 4;
        if(distance > CIRCLE_UNDER_DISTANCE)centerSize +=  this.getSize() / 2 * 0.7f;
        return mdistance = centerSize + ret * (ArrowNavigation.CAMERA_HEIGHT / 2 - centerSize);
    }

    private static float getValueOnLinearLine(float[] point1, float[] point2, float x) {
        // find linear function from given points and calculate y form it
        float k = (point2[1] - point1[1]) / (point2[0] - point1[0]);
        float n = point1[1] - k * point1[0];

        if (n - (point2[1] - k * point2[0]) > 0.01f)
            throw new Error();

        return k * x + n;
    }

    public void setLocation(LatLng loc){
        this.loc.set(loc);
        moveToPrimaryPosition();
    }

    public LatLng getLocation(){
        return loc;
    }

    public void calculatePrimaryPosition(float compassAngle){
        final float angle = mLastAngle = loc.getAngle(LocationManager.lastGPSPosition);
        float distance = loc.getDistance(LocationManager.lastGPSPosition);

        distance = distanceToScreenLocation(distance);
        if(distance > ArrowNavigation.MAX_DISTANCE)distance =  ArrowNavigation.MAX_DISTANCE;
        if(distance < ArrowNavigation.MIN_DISTANCE)distance =  ArrowNavigation.MIN_DISTANCE;

        float x = (float)(Math.sin(compassAngle + angle) * distance + ArrowNavigation.CENTER_X);
        float y = (float)(Math.cos(compassAngle + angle) * distance + ArrowNavigation.CENTER_Y);

        x = Util.limit(50, x, ArrowNavigation.CAMERA_WIDTH - 50);
        y = Util.limit(50, y, ArrowNavigation.CAMERA_HEIGHT - 50);

        primaryPosition.set(x, y);
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        //Util.log("circle click");
        if(pSceneTouchEvent.isActionDown()) {
            //final float zoomSize = ArrowNavigation.mCamera.getZoom() * this.getSize();
            //if (zoomSize < NOT_CLICKABLE_SIZE) return false;

            final TextCloud textCloud = TextCloud.instance();

            if (textCloud.getAttachedToPoiCircle() == this) {
                textCloud.detachPoiCircle();
                return false;
            }

            textCloud.attachPoiCircle(this);
            textBelowStartTime = System.currentTimeMillis();
            return true;
        }

        return false;
    }

    //problem je v temu da je ta kompass klik prej poklican kokr
    //pa surface click, kar pomen da ga ne morm sklopt, mogoce se da nekak sklopt da se otroci ne sprozjo ce majo ze starsi click action TODO?
    //kjer se hendla tist za pinch zoom pa slide in ker nocmo klika gumba
    //med temu ka slidajmo al pa pinch zoomamo ga pac mormo izklopt na daljavo
    public static boolean clickEnabled = true;
    public void attachToScene(Scene pScene) {
        if(this.getParent() != null && mSceneAttached != null){
            mSceneAttached.detachChild(this);
            mSceneAttached.detachChild(mTextDistance);
        }
        mSceneAttached = pScene;
        pScene.registerTouchArea(this);
        pScene.attachChild(this);
        pScene.attachChild(mTextDistance);
        pScene.attachChild(mSurface);
        //pScene.attachChild(textBelow);
        this.setVisible(true);

        setZIndex(ArrowNavigation.Z_INDEX_CIRCLE_START);
        sortChildren(false);
    }

    @Override
    public void setZIndex(final int pZIndex) {
        if(!isOnTop) {
            mSurface.setZIndex(pZIndex);
            super.setZIndex(pZIndex + 1);
            mTextDistance.setZIndex(pZIndex + 1);
        }
    }

    private boolean isOnTop = false;

    public void setZIndexOnTop(){
        if(isOnTop)return;

        mLastZIndex = mSurface.getZIndex();
        this.setZIndex(ArrowNavigation.Z_INDEX_CIRCLE_TOP);

        isOnTop = true;
        mSceneAttached.sortChildren(true);
        Entity e;
    }

    public void resetZIndex(){
        isOnTop = false;
        this.setZIndex(mLastZIndex);
        mSceneAttached.sortChildren(true);
    }


    public float distanceToCenter;
    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        if(this.getX() == this.getX() || this.getY() == this.getY()){
            distanceToCenter = 2;
        }
        setScale();
        showToast();
        super.onManagedUpdate(pSecondsElapsed);

        if(sDistanceMode == true){
            mTextDistance.setPosition(this.getX(), this.getY());
            final float textWidthScaled = this.getWidth() * this.getScaleX() / mTextDistance.getWidth();
            mTextDistance.setScale(textWidthScaled * DISTANCE_TEXT_PADDING);
        }

        handleTextCloud();

        for(int i=0; i<CircleSprite.collisionSprites.size(); i++){
            CircleSprite s = collisionSprites.get(i);
            if(!s.equals(this) && this.isContactWith(s)){
                if(!isUnderCompass){
                    handleCollision(s, 1);
                }
            }
        }

        calculatePrimaryPosition(ArrowNavigation.compassAngle);
        //this.setPosition(primaryPosition.x, primaryPosition.y);

        mSurface.setPosition(this.getX(), this.getY());
        mSurface.setScale(this.getWidth() * this.getScaleX() / mSurface.getWidth());

        final Body b = (Body) this.getUserData();
        final float mul = mLastMeasuredDistance < 110? Math.min(5, 110/2 / mLastMeasuredDistance) : 1;
        b.applyForce(new Vector2(primaryPosition.x - getX(), primaryPosition.y - getY()).mul(FORCE_DAMPING * b.getMass() * mul), b.getWorldCenter());
        b.setLinearVelocity(b.getLinearVelocity().mul(VELOCITY_DAMPING));

    }


    private void showToast() {
        if(mPoiData.isInside(mLastMeasuredDistance)){
            ArrowNavigationActivity.sToast.showToast(this.getPoiData(), false);
        }
    }


    public static void loadResources(final TextureManager pTextureManager, final AssetManager pAssetManager) {
        CloudPool.setTextureManager(pTextureManager);

        PoiCircleMapTexture.loadMap(pTextureManager, pAssetManager);
    }

    private void handleTextCloud(){
        final TextCloud textCloud = TextCloud.instance();
        if(textCloud.getAttachedToPoiCircle() == this){
            if(System.currentTimeMillis() - textBelowStartTime > TIME_ON){
                textCloud.detachPoiCircle();
            }else {
                textCloud.setScale(this.getScaleX() * TEXT_CLOUD_SCALE);
                final float belowCircle = this.getScaleX() * (this.getHeight() + TEXT_CLOUD_MARGIN) / 2; //y coordinate shifted below circle
                textCloud.setPosition(this.getX(), this.getY() - belowCircle);
            }
        }
    }


    public float getLastAngle() {
        return mLastAngle;
    }
}
