package app.tripko.bertoncelj1.arrowNavigation.textCloud;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.source.EmptyBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.bitmap.source.decorator.BaseBitmapTextureAtlasSourceDecorator;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircle;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircleMapTexture;
import app.tripko.bertoncelj1.poiCard.PoiCardActivity;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigationActivity;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.util.Dbg;


/**
 * Created by openstackmaser on 24.1.2016.
 */
public class TextCloud {

    private static final String DEBUG_TAG = "TextCloud";

    /*CHAN*/private static final int MAX_ROWS = 3;

    /*CHAN*/private static final int QUALITY = 3;                   //makes the image drawn bigger -> bigger means more pixels -> higher resolution -> better quality
    /*CHAN*/private static final float MAX_SCALE = 5f / QUALITY;
    //values are in proportion to each other
    /*CHAN*/private static final int ARROW_HEIGHT = 5 * QUALITY;
    /*CHAN*/private static final int ARROW_WIDTH = 15 * QUALITY;
    /*CHAN*/private static final int ROW_HEIGHT = 20 * QUALITY;
    /*CHAN*/public static final int MAX_CIRCLE_DIAMETER = 45 * QUALITY;
    /*CHAN*/public static final int CANVAS_WIDTH = 150 * QUALITY;
    /*CHAN*/private static final int MIN_WIDTH = ARROW_WIDTH + ROW_HEIGHT;
    /*CHAN*/private static final int MIN_HEIGHT = ROW_HEIGHT;
    /*CHAN*/public static final int CANVAS_HEIGHT = TextCloud.getHeight(MAX_ROWS);

    /*CHAN*/private static final int TEXT_MIN_WIDTH = 100;
    /*CHAN*/private static final int TEXT_MAX_LENGTH = 100;          //maximum character that text can have, additional characters will be ignored
    /*CHAN*/private static final String TEXT_CUT_APPEND = "...";    //string that is appended if text is to long

    /*CHAN*/private static final int DEFAULT_SURFACE_COLOR = Color.WHITE;
    /*CHAN*/private static final org.andengine.util.adt.color.Color DEFAULT_TEXT_COLOR = org.andengine.util.adt.color.Color.BLACK;



    final MediaPlayer mClickSound = MediaPlayer.create(ArrowNavigation.sContext, R.raw.subtle_button_sound); //TODO move media player into new util class

    private SurfaceSprite surface;
    private Text text;

    private BitmapTextureAtlas mBitmapTextureAtlas;
    private int mSurfaceColor = DEFAULT_SURFACE_COLOR;

    private float lastWidth = 0;
    private float lastHeight = 0;
    private boolean mForceRedraw = false;
    private PoiCircle mAttachedToPoiCircle;


    private TextCloud(VertexBufferObjectManager vbm, Font pFont) {
        this.mBitmapTextureAtlas = CloudPool.obtain();

        text = new Text(0, 0, pFont, "", TEXT_MAX_LENGTH, vbm);
        text.setVisible(false);
        text.setAutoWrapWidth(text.getHeight() * 8); //set the height of the text tis text will be eight time s weider than high
        text.setAutoWrap(AutoWrap.WORDS_ADVANCE);
        text.setHorizontalAlign(HorizontalAlign.CENTER);
        text.setColor(DEFAULT_TEXT_COLOR);

        final TextureRegion textureRegion = setText("");
        surface = new SurfaceSprite(0, 0, textureRegion, vbm);
        surface.setVisible(false);

        this.setPosition(0, 0);
        this.setScale(1);

        mClickSound.setVolume(0.5f, 0.5f);
    }

    private TextureRegion redrawSurface() {
        return redrawSurface(false);
    }

    private TextureRegion redrawSurface(boolean pForce) {

        float height;
        float width;

        float textWidth = 0;

        float rowHeight = text.getHeight() / text.getLines().size();

        float circleBorder = text.getHeight() / (text.getLines().size());
        circleBorder = Math.min(MAX_CIRCLE_DIAMETER, circleBorder - ARROW_HEIGHT);
        textWidth = Math.max(text.getLineWidthMaximum(), TEXT_MIN_WIDTH+45);
        width = ROW_HEIGHT/rowHeight * textWidth;
        if(width > CANVAS_WIDTH - circleBorder)width = CANVAS_WIDTH - circleBorder;
        height =  width / textWidth * text.getHeight();

        if(text.getLines().size() < 2)height += ROW_HEIGHT/2;
        else if(text.getLines().size() == 2)height += ROW_HEIGHT/5;
        else if(text.getLines().size() == 3)height -= ROW_HEIGHT/8;
        else height -=0;

        return redrawSurface(pForce, width, height, mBitmapTextureAtlas);
    }

    //drawes new surface (new cloud) on BitmapTextureAtlas for a given textWidth
    private TextureRegion redrawSurface(boolean pForce, float pWidth, float pHeight, BitmapTextureAtlas atlas){
        if(pWidth != pWidth)pWidth = MIN_WIDTH;
        if(pWidth <  MIN_WIDTH)pWidth = MIN_WIDTH;
        if(pWidth > CANVAS_WIDTH)pWidth = CANVAS_WIDTH;

        if(pHeight != pHeight)pHeight = MIN_HEIGHT;
        if(pHeight <  MIN_HEIGHT)pHeight = MIN_HEIGHT;
        if(pHeight > CANVAS_HEIGHT)pHeight = CANVAS_HEIGHT;

        //if width hasn't changed no redrawing is needed
        if(!pForce && pWidth == lastWidth && pHeight == lastHeight) return null;
        lastWidth = pWidth;
        lastHeight = pHeight;


        final float width = pWidth;
        final float height = pHeight;

        final IBitmapTextureAtlasSource baseTextureSource = new EmptyBitmapTextureAtlasSource(CANVAS_WIDTH, CANVAS_HEIGHT);
        final IBitmapTextureAtlasSource decoratedTextureAtlasSource = new BaseBitmapTextureAtlasSourceDecorator(baseTextureSource) {
            @Override
            protected void onDecorateBitmap(Canvas pCanvas) throws Exception {
                Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

                paint.setStyle(Paint.Style.FILL);
                paint.setColor(mSurfaceColor);


                final float left = (CANVAS_WIDTH - width) / 2;
                final float right = (CANVAS_WIDTH + width) / 2;
                final float top = ARROW_HEIGHT;
                final float bottom = height;

                final float circleDiameter = Math.min(MAX_CIRCLE_DIAMETER, height - ARROW_HEIGHT);

                //since i know circleDiameter is set to (TextCloud.getHeight(rows) - ARROW_HEIGHT) this is not necessary
                //final float maxCircleDiameter = TextCloud.getHeight(rows) - ARROW_HEIGHT;
                //circleDiameter = Math.min(maxCircleDiameter, circleDiameter);

                final float innerLeft = (CANVAS_WIDTH - width + circleDiameter) / 2;
                final float innerRight = (CANVAS_WIDTH + width  - circleDiameter) / 2;
                final float innerTop = top + circleDiameter / 2;
                final float innerBottom = bottom - circleDiameter / 2;



                //check if is necessary to draw 4 circles
                if(innerTop < innerBottom){
                    pCanvas.drawRect(left, innerTop, right, innerBottom, paint);

                    pCanvas.drawCircle(innerLeft, innerBottom, circleDiameter / 2, paint);
                    pCanvas.drawCircle(innerRight, innerBottom, circleDiameter / 2, paint);
                }

                //draws two circles on each side of the rectangle holding text
                pCanvas.drawCircle(innerLeft, innerTop, circleDiameter / 2, paint);
                pCanvas.drawCircle(innerRight, innerTop, circleDiameter / 2, paint);

                //draws rectangle holding text
                pCanvas.drawRect(innerLeft, top, innerRight, bottom, paint);

                //draws triangle arrow pointing up
                Point[] points = {
                        new Point(CANVAS_WIDTH /2, 0),
                        new Point(CANVAS_WIDTH /2+ARROW_WIDTH/2, ARROW_HEIGHT),
                        new Point(CANVAS_WIDTH /2-ARROW_WIDTH/2, ARROW_HEIGHT)
                };

                Path wallpath = new Path();
                wallpath.reset(); // only needed when reusing this path for a new build
                wallpath.moveTo(points[0].x, points[0].y); // used for first point
                wallpath.lineTo(points[1].x, points[1].y);
                wallpath.lineTo(points[2].x, points[2].y);
                wallpath.lineTo(points[0].x, points[0].y);

                pCanvas.drawPath(wallpath, paint);

                //TODO enable code below only in debug mode
                //draws rectangle around cloud
                //paint.setStyle(Paint.Style.STROKE);
                //paint.setColor(Color.RED);
                //pCanvas.drawRect(0,0,CANVAS_WIDTH, MAX_HEIGHT, paint);
            }

            @Override
            public BaseBitmapTextureAtlasSourceDecorator deepCopy() {
                throw new IModifier.DeepCopyNotSupportedException();
            }
        };

        atlas.clearTextureAtlasSources();
        return BitmapTextureAtlasTextureRegionFactory.createFromSource(atlas, decoratedTextureAtlasSource, 0, 0);
    }

    public void setVisible(final boolean pVisible){
        text.setVisible(pVisible);
        surface.setVisible(pVisible);
    }


    public static int getMaxHeight(){
        return TextCloud.getHeight(MAX_ROWS);
    }

    public static int getHeight(int rows){
        return ARROW_HEIGHT + ((rows > 1)? (int)(ROW_HEIGHT * (rows)) : ROW_HEIGHT);
    }


    public int getWidth(){
        return CANVAS_WIDTH;
    }

    public float getBoxHeight(){
        return this.getRowCount() * ROW_HEIGHT;
    }

    public int getRowCount() {
        return Math.max(1, text.getLines().size()); //at least one row must exist
    }

    public float getScale(){
        return surface.getScaleX();
    }



    public void setTextColor(int pColor){
        text.setColor(pColor);
    }

    public void setSurfaceColor(int pColor){
        mSurfaceColor = pColor;

        redrawSurface(true);
    }

    float lastX;
    float lastY;
    boolean positionOnArrow = true;

    private void updateSurfacePosition(){
        final float yShift = positionOnArrow ? this.CANVAS_HEIGHT * this.getScale() / 2 : 0;
        surface.setPosition(lastX, lastY - yShift);
    }

    public void setPosition(final float pX, final float pY){
        lastX = pX;
        lastY = pY;
        updatePosition();
    }

    public void updatePosition(){
        updateSurfacePosition();
        updateTextPosition();
    }

    private void updateTextPosition(){
        final float scale = surface.getScaleX();
        final float y = surface.getY() + scale * (TextCloud.getMaxHeight()/2 - lastHeight/2 - ARROW_HEIGHT/2);
        text.setPosition(surface.getX(), y);
    }

    public float lastScale;
    public void setScale(final float pScale){
        lastScale = pScale;
        updateScale();
    }

    private void updateScale(){
        final float scale = Math.min(MAX_SCALE, lastScale * animationScale / QUALITY);
        surface.setScale(scale);

        updateTextPosition();
        float circleDiameter = Math.min(MAX_CIRCLE_DIAMETER, lastHeight - ARROW_HEIGHT);
        float newScale = (lastWidth - circleDiameter) / Math.max(text.getLineWidthMaximum(), TEXT_MIN_WIDTH) * scale;
        text.setScale(newScale);
    }

    public TextureRegion setText(String txt){
        if(txt.length() > TEXT_MAX_LENGTH)txt = txt.substring(0, TEXT_MAX_LENGTH);

        text.setText(txt);

        shortenTextIfTooMuchLines(txt);

        return redrawSurface();
    }

    public String getText(){
        return (String) text.getText();
    }

    private void shortenTextIfTooMuchLines(String txt){
        if(text.getLines().size() > MAX_ROWS){
            final ArrayList<CharSequence> lines = text.getLines();
            int charCountInsideLimit = 0; //how many chars are still inside MAX_ROWS limit
            for(int i=0; i < MAX_ROWS; i++)charCountInsideLimit += lines.get(i).length() + 1; // + 1 for removed space
            final String wholeString = txt.substring(0, charCountInsideLimit - 1 //-1 since las line does not have space
                    - TEXT_CUT_APPEND.length()) + TEXT_CUT_APPEND;
            text.setText(wholeString);
        }
    }

    private static TextCloud INSTANCE;
    private static Scene sScene;
    public static void create(VertexBufferObjectManager vbm, Font pFont, Scene pScene){
        INSTANCE = new TextCloud(vbm, pFont);
        INSTANCE.attachToScene(pScene);
    }

    public static TextCloud instance(){
        return INSTANCE;
    }

    public void detachPoiCircle(){
        if(mAttachedToPoiCircle == null) return;
        this.setVisible(false);
        this.mAttachedToPoiCircle = null;
        stopOpeningAnimation();
    }

    public void attachPoiCircle(final PoiCircle pPoiCircle){
        this.mAttachedToPoiCircle = pPoiCircle;
        this.setText(pPoiCircle.getPointName());
        this.setTextColor(PoiCircleMapTexture.getColorFromTextureIndex(
                pPoiCircle.getCurrentTileIndex()));
        this.setVisible(true);
        startOpeningAnimation();
    }

    public static void dispose() {
        if(INSTANCE != null) {
            sScene.detachChild(INSTANCE.surface);
            sScene.detachChild(INSTANCE.text);
            INSTANCE.surface.dispose();
            INSTANCE.text.dispose();
        }

        TextCloud.INSTANCE = null;
    }


    public PoiCircle getAttachedToPoiCircle(){
        return mAttachedToPoiCircle;
    }

    public boolean isAnimating = false;
    public float animationScale = 1; //TODO think of a better name
    private static final float ANIMATION_SPEED = 7f;

    public void stopOpeningAnimation(){
        isAnimating = true;
        animationScale = 1;
    }

    public void startOpeningAnimation(){
        animationScale = 0;
        isAnimating = true;
    }



    private class SurfaceSprite extends Sprite{
        public SurfaceSprite(final float pX, final float pY, final ITextureRegion pTextureRegion, final VertexBufferObjectManager pVertexBufferObjectManager) {
            super(pX, pY, pTextureRegion.getWidth(), pTextureRegion.getHeight(), pTextureRegion, pVertexBufferObjectManager);
        }

        @Override
        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
            if(!surface.isVisible())return false;

            if(pSceneTouchEvent.isActionDown()) {
                if(ArrowNavigationActivity.sContext == null){
                    Dbg.e(DEBUG_TAG, "sContext is null, abort");

                }else if(isLocationInsideCloud(pTouchAreaLocalX, pTouchAreaLocalY)){

                    mClickSound.seekTo(0);
                    mClickSound.start();
                    Intent intent = new Intent(ArrowNavigationActivity.sContext, PoiCardActivity.class);
                    Bundle extras = new Bundle();

                    final PoiData poiData = TextCloud.this.mAttachedToPoiCircle.getPoiData();
                    extras.putSerializable("poiData", poiData);
                    intent.putExtras(extras);
                    ArrowNavigationActivity.sContext.startActivity(intent);

                    Dbg.w(DEBUG_TAG, "Starting activity PoiCard " + poiData.name);
                    return true;
                }
            }
            return false;
        }

        @Override
        protected void onManagedUpdate(float pSecondsElapsed) {
            if(isAnimating){
                animationScale += ANIMATION_SPEED * pSecondsElapsed;

                if(animationScale > 1){
                    animationScale = 1;
                    isAnimating = false;
                }
                updateScale();
                updatePosition();
            }
        }

    }

    private boolean isLocationInsideCloud(final float pX, final float pY){
        if(CANVAS_WIDTH/2 - lastWidth/2 <  pX && pX < CANVAS_WIDTH/2 + lastWidth /2){
            if(CANVAS_HEIGHT - lastHeight < pY && pY < CANVAS_HEIGHT - ARROW_HEIGHT)return true;
        }
        return false;
    }

    public void attachToScene(final Scene pScene){
        sScene = pScene;
        pScene.attachChild(surface);
        pScene.attachChild(text);
        pScene.registerTouchArea(surface);

        setZIndex(ArrowNavigation.Z_INDEX_TEXT_CLOUD_TOP);
    }

    public void setZIndex(final int pZIndex) {
        surface.setZIndex(pZIndex);
        text.setZIndex(pZIndex);

        sScene.sortChildren(true);
    }

}
