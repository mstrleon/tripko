package app.tripko.bertoncelj1.arrowNavigation;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import app.tripko.bertoncelj1.R;

/**
 * Created by anze on 3/8/16.
 */
public class NoActivityDialog extends Dialog implements View.OnClickListener{

    protected NoActivityDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_no_activity);

        configureWindow(super.getWindow());

        findViewById(R.id.dialog).setOnClickListener(this);
    }


    private void configureWindow(final Window window){
        //makes background transperant
        window.setBackgroundDrawable(new ColorDrawable(0));

        //sets dimming amount
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.dimAmount = 0.8f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        window.setAttributes(lp);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.dialog:
                super.dismiss();
                break;

        }
    }
}
