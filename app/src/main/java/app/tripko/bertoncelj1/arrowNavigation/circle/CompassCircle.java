package app.tripko.bertoncelj1.arrowNavigation.circle;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.media.MediaPlayer;

import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.math.MathConstants;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircle;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircleMapTexture;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.Shader;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by openstackmaser on 23.1.2016.
 */
public class CompassCircle extends CircleSprite {

    public interface OnCompassClickedListener {
        void onCompassClicked();
    }

    public static final int STATE_NOT_SET = -1;
    public static final int STATE_NAVIGATION = 0;
    public static final int STATE_DISCOVERING = 1;
    public static final int STATE_INFO_MESSAGE = 2;

    private static final int TILE_INDEX_COMPASS = 1;
    private static final int TILE_INDEX_ARROW = 0;

    private static final int SURFACE_TILE_INDEX_COMPASS = 0;
    private static final int SURFACE_TILE_INDEX_ARROW = 1;
    private static final int SURFACE_TILE_EMPTY = 2;

    private OnCompassClickedListener mOnCompassClickedListener;
    private final TiledSprite mSurfaceSprite;
    private final Shader mSurfaceShader;
    private static TiledTextureRegion sCompassArrow;
    private static TiledTextureRegion sSurfaces;

    boolean bouncing = false;
    float scaleStart;
    float scaleStartNeedle;
    float bounceVelocity = 0;
    private float mSize; //2 * radius of compass
    private int mState = STATE_NOT_SET;

    public static CompassCircle staticThis;

    private final int MAX_NEEDLE_SIZE = 200;
    //bouncing parameters
    final float BOUNCE_START_SIZE = 0.98f;      //show size of the spring. < 1 starts smaller, > 1 starts larger
    final float BOUNCE_SPRING_STIFFNESS = 900; //how fast spring accelerates
    final float BOUNCE_SPRING_DAMPING = 0.7f;  //how quickly bouncing stops, 1->never

    private PoiCircle mPointingTarget = null;
    private Text mInfoTextTop;
    private Text mInfoTextBottom;

    //info text position settings
    private float mInfoTextWidthHorizontal = 0.8f; //width of the text relative to circle size
    private float mInfoTextVerticalSeparation = 0; //how much space between top and bottom text, relative to the text height
    private float mInfoTextVerticalPosition = 0.45f; // 0.5 is in the middle of the circle 0 is bottom, 1 is top
    private static final int INFO_TEXT_MAX_LENGTH = 50;


    final MediaPlayer mClickSound = MediaPlayer.create(ArrowNavigation.sContext, R.raw.subtle_button_sound); //TODO move media player into new util class

    public CompassCircle(float pX, float pY, VertexBufferObjectManager pVertexBufferObjectManager, Font pFont) {
        //TODO check if load resources has been called
        super(pX, pY, sCompassArrow, pVertexBufferObjectManager);

        this.setCurrentTileIndex(TILE_INDEX_COMPASS);

        mInfoTextTop = new Text(0, 0, pFont, "", INFO_TEXT_MAX_LENGTH, pVertexBufferObjectManager);
        mInfoTextTop.setVisible(false);
        mInfoTextTop.setHorizontalAlign(HorizontalAlign.CENTER);

        mInfoTextBottom = new Text(0, 0, pFont, "", INFO_TEXT_MAX_LENGTH, pVertexBufferObjectManager);
        mInfoTextBottom.setVisible(false);
        mInfoTextBottom.setHorizontalAlign(HorizontalAlign.CENTER);

        setInfoText("Take me to the", "nextPoi");
        setInfoTextColorTop(Color.WHITE);
        setInfoTextColorBottom(PoiType.Colors.GREEN);

        mSurfaceSprite = new TiledSprite(pX, pY, sSurfaces, this.getVertexBufferObjectManager());
        mSurfaceShader = Shader.obtain(Color.WHITE);
        mSurfaceSprite.setShaderProgram(mSurfaceShader);
        staticThis = this;

        mClickSound.setVolume(0.5f, 0.5f);
    }

    public int getState(){
        return mState;
    }

    public String getStateString(){
        switch(mState){
            case STATE_DISCOVERING:return "Discovering";
            case STATE_NAVIGATION: return "Navigation";
            case STATE_INFO_MESSAGE: return "Info message";
        }
        return "Undefined state number: " + mState;
    }

    public void setPointingTarget(PoiCircle pTarget){
        this.mPointingTarget = pTarget;

        if(mState == STATE_NAVIGATION && mPointingTarget != null){
            setColorToPointingTarger();
        }
    }

    public boolean isPointingTargetSet(){
        return this.mPointingTarget != null;
    }

    public void setState(int compassState){
        if(mState == compassState)return;
        mState = compassState;

        Dbg.i(this, "Set compass state to" + getStateString());
        switch (compassState){
            case STATE_DISCOVERING:
                this.setVisible(true);
                this.setCurrentTileIndex(TILE_INDEX_COMPASS);
                mInfoTextTop.setVisible(false);
                mInfoTextBottom.setVisible(false);
                mSurfaceSprite.setCurrentTileIndex(SURFACE_TILE_INDEX_COMPASS);
                mSurfaceShader.setColor(Color.WHITE);
                break;

            case STATE_NAVIGATION:
                if(mPointingTarget == null)throw new NullPointerException("mPointingTarger is not set!");
                this.setVisible(true);
                this.setCurrentTileIndex(TILE_INDEX_ARROW);
                mInfoTextTop.setVisible(false);
                mInfoTextBottom.setVisible(false);
                mSurfaceSprite.setCurrentTileIndex(SURFACE_TILE_INDEX_ARROW);

                setColorToPointingTarger();
                break;

            case STATE_INFO_MESSAGE:
                this.setVisible(false);
                mInfoTextTop.setVisible(true);
                mInfoTextBottom.setVisible(true);
                mSurfaceSprite.setCurrentTileIndex(SURFACE_TILE_EMPTY);
                mSurfaceShader.setColor(Color.WHITE);
                break;

            case STATE_NOT_SET:
            default:
                throw new IllegalArgumentException("Illegal compass state " + compassState);
        }
    }

    /*
    Sets the same color as is the color of mPointingTarget POI
     */
    private void setColorToPointingTarger(){
        final int tileIndex = mPointingTarget.getCurrentTileIndex();
        final int color = PoiCircleMapTexture.getColorFromTextureIndex(tileIndex);
        mSurfaceShader.setColor(color);
    }

    public void setInfoMessageNextPoi(){
        mInfoTextVerticalPosition = 0.45f;
        mInfoTextVerticalSeparation = 0;
        mInfoTextWidthHorizontal = 0.8f;
        setInfoText("Take me to the", "next POI");
        setInfoTextColorTop(Color.WHITE);
        setInfoTextColorBottom(PoiType.Colors.GREEN);
    }

    public void setInfoMessageFinishTrip(){
        mInfoTextVerticalPosition = 0.50f;
        mInfoTextVerticalSeparation = 0;
        mInfoTextWidthHorizontal = 0.9f;

        setInfoText("This is the last POI", "press here to finish trip");
        setInfoTextColorTop(Color.WHITE);
        setInfoTextColorBottom(Color.WHITE);
    }


    public float getSize(){
        return mSize;
    }

    //set default size values relative to camera (screen size)
    public void configureSize(final float width){
        mSize = Math.min(width, Util.dpToPx(MAX_NEEDLE_SIZE)); //todo check if getWidth is right
        this.setScale(mSize / (this.getWidth()+100), mSize / (this.getHeight()+100));
        mSurfaceSprite.setScale(mSize / (mSurfaceSprite.getWidth()-100), mSize / (mSurfaceSprite.getHeight()-100));
        this.setSizeDirect(mSize);
        scaleStart = mSurfaceSprite.getScaleX();
        scaleStartNeedle = this.getScaleX();

        refreshInfoTextPosition();
    }

    //set default size values relative to camera (screen size)
    public void configurePosition(float x, float y){
        super.setPosition(x, y);
        mSurfaceSprite.setPosition(x, y);
        refreshInfoTextPosition();
    }

    private void refreshInfoTextPosition(){
        float x = this.getX();
        float y = this.getY();
        float size = this.getSize() * this.getScaleX();

        float infoTextWidthMax = Math.max(mInfoTextTop.getWidth(), mInfoTextBottom.getWidth());
        float scale = size / infoTextWidthMax * mInfoTextWidthHorizontal;
        mInfoTextTop.setScale(scale);
        mInfoTextBottom.setScale(scale);

        y = y - size/2 + size*mInfoTextVerticalPosition;
        mInfoTextTop.setPosition(x, y  + mInfoTextTop.getHeight()*scale / 2 +
                mInfoTextTop.getHeight()*mInfoTextVerticalSeparation/2);

        mInfoTextBottom.setPosition(x, y - mInfoTextBottom.getHeight()*scale / 2 -
                mInfoTextBottom.getHeight()*mInfoTextVerticalSeparation/2);
    }

    public void setInfoText(String topText, String bottomText){
        if(topText != null)mInfoTextTop.setText(topText);
        if(bottomText != null)mInfoTextBottom.setText(bottomText);

        refreshInfoTextPosition();
    }

    public void setInfoTextColorTop(int color){
        mInfoTextTop.setColor(Util.convertColor(color));
    }

    public void setInfoTextColorBottom(int color){
        mInfoTextBottom.setColor(Util.convertColor(color));
    }

    @Override
    public void attachToScene(final Scene pScene){
        pScene.attachChild(mInfoTextTop);
        pScene.attachChild(mInfoTextBottom);
        pScene.attachChild(mSurfaceSprite);
        pScene.registerTouchArea(this);
        pScene.setTouchAreaBindingOnActionDownEnabled(true);
        pScene.attachChild(this);


        this.setZIndex();
        pScene.sortChildren(false);
    }

    public void setZIndex() {
        super.setZIndex(ArrowNavigation.Z_INDEX_COMPASS);
        mSurfaceSprite.setZIndex(ArrowNavigation.Z_INDEX_COMPASS);
        mInfoTextTop.setZIndex(ArrowNavigation.Z_INDEX_COMPASS + 2);
        mInfoTextBottom.setZIndex(ArrowNavigation.Z_INDEX_COMPASS + 2);
    }


    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);
        final float rotationAngle;
        if(mPointingTarget == null){
            rotationAngle = ArrowNavigation.compassAngle * MathConstants.RAD_TO_DEG;
        }else{
            rotationAngle = (ArrowNavigation.compassAngle + mPointingTarget.getLastAngle()) * MathConstants.RAD_TO_DEG;
        }
        this.setRotation(rotationAngle);

        if(bouncing){
            //if pSecondsElapsed is too large change (acceleration) is not calculated right and compass "explodes"
            if(pSecondsElapsed > 0.01)pSecondsElapsed = 0.01f;

            float currScale = mSurfaceSprite.getScaleX();
            if(compare(currScale, scaleStart, 0.01f) || compare(bounceVelocity, 0, 0.01f)){
                final float change = (currScale - scaleStart) * pSecondsElapsed;
                bounceVelocity -= change * BOUNCE_SPRING_STIFFNESS;
                bounceVelocity *= BOUNCE_SPRING_DAMPING;

                currScale += bounceVelocity * pSecondsElapsed;
                mSurfaceSprite.setScale(currScale);
                this.setScale(scaleStartNeedle * currScale / scaleStart);
                refreshInfoTextPosition();
            }else{
                stopBounce();
            }
        }
    }

    private boolean compare(final float com1, final float com2, final float diff){
        return (Math.abs(com1 - com2) > diff);
    }

    //problem je v temu da je ta kompass klik prej poklican kokr
    //pa surface click, kar pomen da ga ne morm sklopt, mogoce se da nekak sklopt da se otroci ne sprozjo ce majo ze starsi click action TODO?
    //kjer se hendla tist za pinch zoom pa slide in ker nocmo klika gumba
    //med temu ka slidajmo al pa pinch zoomamo ga pac mormo izklopt na daljavo
    public static boolean clickEnabled = true;
    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if(!clickEnabled)return false;

        if(pSceneTouchEvent.isActionUp()) {
            this.setBounce();
            this.runBounce();

            if(mOnCompassClickedListener != null){
                mOnCompassClickedListener.onCompassClicked();
            }

        }

        return false;
    }

    private void stopBounce(){
        bounceVelocity = 0;
        bouncing = false;
        mSurfaceSprite.setScale(scaleStart);
        this.setScale(scaleStartNeedle);
    }

    public void setOnCompassClickedLister(OnCompassClickedListener listener){
        mOnCompassClickedListener = listener;
    }

    private void runBounce(){
        mSurfaceSprite.setScale(scaleStart * BOUNCE_START_SIZE);
        this.setScale(scaleStartNeedle * BOUNCE_START_SIZE);
        bounceVelocity = 0;
        bouncing = true;

        mClickSound.seekTo(0);
        mClickSound.start();
    }

    private void setBounce(){
        if(bouncing)stopBounce();
        mSurfaceSprite.setScale(scaleStart * BOUNCE_START_SIZE);
        this.setScale(scaleStartNeedle * BOUNCE_START_SIZE);
        bouncing = false;
    }


    private static TextureManager sTextureManager;
    private static BitmapTextureAtlas arrowAtlas;
    private static BitmapTextureAtlas texImage;

    public static void loadResources(final TextureManager pTextureManager, final AssetManager pAssetManager){
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        sTextureManager = pTextureManager;
        arrowAtlas = new BitmapTextureAtlas(pTextureManager, 1024, 512, TextureOptions.BILINEAR);
        sCompassArrow = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(arrowAtlas, pAssetManager, "compass_arrow_map.png", 0, 0, 2, 1);
        arrowAtlas.load();

        texImage = new BitmapTextureAtlas(pTextureManager, 1536, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        sSurfaces = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texImage, pAssetManager, "compass_surfaces.png", 0, 0, 3, 1);
        texImage.load();
    }

    public void detachFromScene() {
        IEntity parent = this.getParent();
        parent.detachChild(this);
        parent.detachChild(mSurfaceSprite);
        parent.detachChild(mInfoTextTop);
        parent.detachChild(mInfoTextBottom);
    }

    @Override
    public void dispose() {
        super.dispose();

        if(sTextureManager != null){
            if(arrowAtlas != null){
                sTextureManager.unloadTexture(arrowAtlas);
                arrowAtlas = null;
            }

            if(texImage != null){
                sTextureManager.unloadTexture(texImage);
                texImage = null;
            }

            if(sTextureManager.getTexturesLoadedCount() == 0){
                sTextureManager = null;
            }

        }
    }

}
