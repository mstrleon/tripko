package app.tripko.bertoncelj1.arrowNavigation.circle.poi;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.support.annotation.NonNull;


import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by openstackmaser on 2.2.2016.
 */
public class PoiCircleMapTexture {
    private static final String DEBUG_TAG = "PoiCircleMapTexture";

    //how much should shaded icon be shaded 0->no shade (transparent), 255->black (real color)
    private static final int SHADED_ICON_ALPHA = (int)(255f * 0.8f);
    private static final int SHADED_ICON_BACKGROUND_ALPHA = (int)(255f * 0.5f);
    private static final String ASSETS_PATH = "gfx/POI_ikone/";

    private static final String SURFACE_TEXTURE_NAME = "POI_surface.png";
    private static final int SURFACE_WIDTH = 128;
    private static final int SURFACE_HEIGHT = 128;

    private static final String ICON_MAP_NAME = "POI_icons_map.png";
    private static final int ICONS_X = 1;   //how many icons are in x direction
    private static final int ICONS_Y = 15;  //how many icons are in y direction
    private static final int ICON_COUNT = ICONS_X * ICONS_Y;
    private static final int ICON_WIDTH = 128;
    private static final int ICON_HEIGHT = 128;
    private static final int ICON_MAP_WIDTH = ICON_WIDTH;
    private static final int ICON_MAP_HEIGHT = ICON_HEIGHT * ICON_COUNT;

    public static final PoiType[] textures = {
            PoiType.ARCHITECTURE,
            PoiType.ART_MUSEUMS,
            PoiType.COOL_UNIQUE,
            PoiType.PUBLIC_TRANSPORT,
            PoiType.BICYCLE_TRANSPORT,
            PoiType.FOOD,
            PoiType.SERVICES,
            PoiType.SHOPPING,
            PoiType.INFORMATION,
            PoiType.NIGHTLIFE,
            PoiType.SPORT,
            PoiType.NATURE,
            PoiType.ENTERTAINMENT,
            PoiType.CAFE,
            PoiType.BEACH
    };


    public static int getTextureIndexFromName(String index_name){
        PoiType type = PoiType.getPoiTypeFromName(index_name);
        return getTextureIndexFromPoiType(type);
    }

    public static int getTextureIndexFromId(int id){
        PoiType type = PoiType.getPoiTypeFromId(id);
        return getTextureIndexFromPoiType(type);
    }

    public static int getTextureIndexFromPoiType(PoiType type){
        if(type == null)return 0;

        for(int i = 0; i< textures.length; i++){
            if(textures[i].equals(type))return i;
        }

        Dbg.e(DEBUG_TAG, "Can't find texture id for poiType " + type.name);
        return 0;
    }

    public static int getColorFromTextureIndex(int pTextureIndex){
        if(0 > pTextureIndex || pTextureIndex > textures.length){
            return PoiType.DEFAULT_COLOR;
        }
        return textures[pTextureIndex].color;
    }


    private static TiledTextureRegion mPoiIconsMap;
    private static TextureRegion mPoiSurfaceTexture;

    public static TiledTextureRegion getPoiIconsMap(){
        return mPoiIconsMap;
    }

    public static int getTileCount(){
        if(mPoiIconsMap == null){
            Dbg.e(DEBUG_TAG, "mPoiIconsMap is null, cannot access tile count");
        }
        return mPoiIconsMap.getTileCount();
    }

    public static TextureRegion getPoiSurfaceTexture(){
        return mPoiSurfaceTexture;
    }

    public static void loadMap(TextureManager pTextureManager, AssetManager pAssetManager) {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(ASSETS_PATH);

        //both textures (surface and icons) are on the same
        //bitmap atlas, hence ICON_MAP_HEIGHT + SURFACE_HEIGHT
        BitmapTextureAtlas bitmapTextureAtlas = new BitmapTextureAtlas(pTextureManager,
                ICON_MAP_WIDTH, ICON_MAP_HEIGHT + SURFACE_HEIGHT,
                TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        //loads surface texture
        mPoiSurfaceTexture = BitmapTextureAtlasTextureRegionFactory.
                createFromAsset(bitmapTextureAtlas, pAssetManager, SURFACE_TEXTURE_NAME, 0, 0);

        //loads icon map, moved down by surface texture for SURFACE_HEIGHT pixels
        mPoiIconsMap = BitmapTextureAtlasTextureRegionFactory.
                createTiledFromAsset(bitmapTextureAtlas, pAssetManager,
                        ICON_MAP_NAME, 0, SURFACE_HEIGHT, ICONS_X, ICONS_Y);

        bitmapTextureAtlas.load();
    }


    //returns bitmap icons read from icon_map.png
    //first half of icons are light the second half is shaded
    public static Bitmap[] getBitmapIcons(final Resources pResources){
        Bitmap iconBitmapMap = null;
        // load image
        try {
            // get input stream
            InputStream ims = pResources.getAssets().open("gfx/POI_ikone/POI_icons_map.png");
            BufferedInputStream bufferedInputStream = new BufferedInputStream(ims);
            iconBitmapMap = BitmapFactory.decodeStream(bufferedInputStream);
        }
        catch(IOException ex) {
            Dbg.d(DEBUG_TAG, "Unable to get bitmaps:" + ex.getMessage());
            return null;
        }

        if(iconBitmapMap == null){
            return null;
        }

        //scales icons according to screen size, width and height must me scaled
        //down by 3 because icon size was measured with screen with density 3
        final float bitmapWidth = Util.dpToPx(iconBitmapMap.getWidth() / 3f);
        final float bitmapHeight = Util.dpToPx(iconBitmapMap.getHeight() / 3f);
        
        Bitmap canvasBitmap = Bitmap.createBitmap((int) bitmapWidth, (int) bitmapHeight, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(canvasBitmap);


        Paint color = new Paint();
        color.setStyle(Paint.Style.FILL);
        color.setAntiAlias(true);

        int iconCount = textures.length;

        //draws circles under icon
        for(int i=0; i < iconCount; i++) {
            final float x = bitmapWidth / 2;
            final float y = (bitmapWidth / 2) + i * bitmapWidth;
            final float radius = bitmapWidth / 2;
            color.setColor(getColorFromTextureIndex(i));
            canvas.drawCircle(x, y, radius, color);
        }

        //draws icons over circles
        canvas.drawBitmap(iconBitmapMap, null, new RectF(0, 0, bitmapWidth, bitmapHeight), null);

        //cuts out non shaded icons
        Bitmap[] icons = new Bitmap[iconCount * 2];
        for(int i=0; i< iconCount; i++) {
            final int x = 0;
            final float y = i *  bitmapWidth;
            final float width =  bitmapWidth;
            final float height = bitmapWidth;
            final Bitmap bitmapIcon = Bitmap.createBitmap(canvasBitmap, (int) x, (int) y, (int) width, (int) height);
            icons[i] = bitmapIcon;
        }

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        //draws circles under icon
        for(int i=0; i < iconCount; i++) {
            final float x = bitmapWidth / 2;
            final float y = (bitmapWidth / 2) + i * bitmapWidth;
            final float radius = bitmapWidth / 2;
            color.setColor(getColorFromTextureIndex(i));
            color.setAlpha(SHADED_ICON_BACKGROUND_ALPHA);
            canvas.drawCircle(x, y, radius, color);
        }

        Paint paint = new Paint();
        paint.setAlpha(SHADED_ICON_ALPHA);
        canvas.drawBitmap(iconBitmapMap, null, new RectF(0, 0, bitmapWidth, bitmapHeight), null);

        /*
        //shades circles
        color.setColor(Color.BLACK);
        color.setAlpha(SHADED_ICON_ALPHA);
        for(int i=0; i < iconCount; i++) {
            final float x = bitmapWidth / 2;
            final float y = (bitmapWidth / 2) + i * bitmapWidth;
            final float radius = bitmapWidth / 2;
            canvas.drawCircle(x, y, radius, color);
        }
        */

        //cuts out shades icons
        for(int i=0; i< iconCount; i++){
            final int x = 0;
            final float y = i * bitmapWidth;
            final float width = bitmapWidth;
            final float height =  bitmapWidth;
            final Bitmap bitmapIcon = Bitmap.createBitmap(canvasBitmap, (int) x, (int) y, (int) width, (int) height);
            icons[i + iconCount] = bitmapIcon;
        }

        return icons;
    }


}
