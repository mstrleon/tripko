package app.tripko.bertoncelj1.arrowNavigation.circle.poi;

import android.graphics.Color;
import android.opengl.GLES20;

import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.shader.ShaderProgramManager;
import org.andengine.opengl.shader.constants.ShaderProgramConstants;
import org.andengine.opengl.shader.exception.ShaderProgramException;
import org.andengine.opengl.shader.exception.ShaderProgramLinkException;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.attribute.VertexBufferObjectAttributes;
import org.andengine.util.adt.pool.GenericPool;

/**
 * Created by openstackmaser on 24.2.2016.
 */
public class Shader extends ShaderProgram {
    public static final String UNIFORM_INTENSITY = "intensity";
    public static final String UNIFORM_COLOR = "uColor";
    public static final double START_ANGLE = 60.0;

    private static final float DEF_INTENSITY_FACTOR_CHANGE = 0.0005f;
    static final float DEF_INTENSITY_SHADED = 0.4f;
    private static final float DEF_INTENSITY_NOT_SHADED = 1f;

    private static int mModelViewProjectionMatrixLocation = ShaderProgramConstants.LOCATION_INVALID;
    private static int mTexture0Location = ShaderProgramConstants.LOCATION_INVALID;
    private static int mIntensityLocation = ShaderProgramConstants.LOCATION_INVALID;
    private static int mColorLocation = ShaderProgramConstants.LOCATION_INVALID;

    private float mIntensityShaded = DEF_INTENSITY_SHADED;
    private float mIntensityNotShaded = DEF_INTENSITY_NOT_SHADED;
    private float mMaxIntensityFactorChange = DEF_INTENSITY_FACTOR_CHANGE;
    private long mPrevChangeTime = 0;
    private boolean mShaded = false;
    private float mIntensity = mShaded ? mIntensityShaded : mIntensityNotShaded;
    private float mTargetIntensity = mIntensity;
    private boolean mIsChanging = false;

    private float mAlpha;
    private float mRed;
    private float mGreen;
    private float mBlue;

    private static final String VERTEXT_SHADER =
            "uniform mat4 " + ShaderProgramConstants.UNIFORM_MODELVIEWPROJECTIONMATRIX + ";\n" +
                    "attribute vec4 " + ShaderProgramConstants.ATTRIBUTE_POSITION + ";\n" +
                    "attribute vec2 " + ShaderProgramConstants.ATTRIBUTE_TEXTURECOORDINATES + ";\n" +
                    "attribute vec4 " + ShaderProgramConstants.ATTRIBUTE_COLOR + ";\n" +
                    "varying vec2 " + ShaderProgramConstants.VARYING_TEXTURECOORDINATES + ";\n" +
                    "varying vec4 " + ShaderProgramConstants.VARYING_COLOR + ";\n" +
                    "void main() {\n" +
                    "   " + ShaderProgramConstants.VARYING_COLOR + " = " + ShaderProgramConstants.ATTRIBUTE_COLOR + ";\n" +
                    "   " + ShaderProgramConstants.VARYING_TEXTURECOORDINATES + " = " + ShaderProgramConstants.ATTRIBUTE_TEXTURECOORDINATES + ";\n" +
                    "   gl_Position = " + ShaderProgramConstants.UNIFORM_MODELVIEWPROJECTIONMATRIX + "*" + ShaderProgramConstants.ATTRIBUTE_POSITION + ";\n" +
                    "}";

    private static final String FRAGMENT_SHADER =
            "precision lowp float;\n" +
                    "uniform float " + UNIFORM_INTENSITY + ";\n" +
                    "uniform vec4 " + UNIFORM_COLOR + ";\n" +
                    "uniform sampler2D " + ShaderProgramConstants.UNIFORM_TEXTURE_0 + ";\n" +
                    "varying mediump vec2 " + ShaderProgramConstants.VARYING_TEXTURECOORDINATES + ";\n" +
                    "varying lowp vec4 " + ShaderProgramConstants.VARYING_COLOR + ";\n" +
                    "void main() {\n" +
                    "   vec4 myColor = texture2D(" + ShaderProgramConstants.UNIFORM_TEXTURE_0 + ", " + ShaderProgramConstants.VARYING_TEXTURECOORDINATES + ");\n" +
                    "   vec4 inten = " + UNIFORM_INTENSITY + " * " + UNIFORM_COLOR + ";\n" +
                    "   inten.a = " + UNIFORM_COLOR + ".a;\n" + //alpha does not change with intensity
                    "   gl_FragColor = inten * " + ShaderProgramConstants.VARYING_COLOR + " * myColor;\n" +
                    "}";

    private static final GenericPool<Shader> POOL = new GenericPool<Shader>() {
        @Override
        protected Shader onAllocatePoolItem() {
            return new Shader();
        }
    };

    static ShaderProgramManager manger;

    public static void setShaderManager(final ShaderProgramManager pManager){
        manger = pManager;
    }

    public static Shader obtain(){
        return Shader.obtain(Color.WHITE);
    }

    public static Shader obtain(int pColor){
        final Shader shader = new Shader();//POOL.obtainPoolItem();
        manger.loadShaderProgram(shader);
        shader.setColor(pColor);
        return shader;
    }

    public static void recycle(final Shader pShader){
        POOL.recyclePoolItem(pShader);
    }

    private Shader(){
        this(Color.GRAY);
    }

    private Shader(int pColor) {
        super(VERTEXT_SHADER, FRAGMENT_SHADER);
        this.setColor(pColor);
    }

    public void toggleShaded(){
        setShaded(!mShaded);
    }

    public void setShaded(boolean pShaded) {
        if(pShaded == mShaded)return;

        mShaded = pShaded;
        mTargetIntensity = mShaded ? mIntensityShaded : mIntensityNotShaded;
    }

    public void setShadedIntensity(float intensityShaded){
        mIntensityShaded = intensityShaded;
    }

    public void setNotShadedIntensity(float intensityNotShaded){
        mIntensityNotShaded = intensityNotShaded;
    }

    public boolean isShaded(){
        return mShaded;
    }

    public boolean isChanging(){
        return mIsChanging;
    }

    public void setColor(final int pColor){
        this.setColor(
                1, //((pColor & 0xFF000000) >> 24) / 255f,
                ((pColor & 0xFF0000) >> 16) / 255f,
                ((pColor & 0xFF00) >> 8) / 255f,
                ((pColor & 0xFF) >> 0) / 255f
        );
    }
    public void setColor(float pAlpha, float pRed, float pGreen, float pBlue){
        this.mAlpha = pAlpha;
        this.mRed = pRed;
        this.mGreen = pGreen;
        this.mBlue = pBlue;
    }

    @Override
    protected void link(GLState pGLState) throws ShaderProgramLinkException {
        GLES20.glBindAttribLocation(mProgramID, ShaderProgramConstants.ATTRIBUTE_POSITION_LOCATION, ShaderProgramConstants.ATTRIBUTE_POSITION);
        GLES20.glBindAttribLocation(mProgramID, ShaderProgramConstants.ATTRIBUTE_TEXTURECOORDINATES_LOCATION, ShaderProgramConstants.ATTRIBUTE_TEXTURECOORDINATES);
        GLES20.glBindAttribLocation(mProgramID, ShaderProgramConstants.ATTRIBUTE_COLOR_LOCATION, ShaderProgramConstants.ATTRIBUTE_COLOR);

        super.link(pGLState);

        mModelViewProjectionMatrixLocation = getUniformLocation(ShaderProgramConstants.UNIFORM_MODELVIEWPROJECTIONMATRIX);
        mTexture0Location = getUniformLocation(ShaderProgramConstants.UNIFORM_TEXTURE_0);
        mIntensityLocation = getUniformLocation(UNIFORM_INTENSITY);
        mColorLocation = getUniformLocation(UNIFORM_COLOR);
    }

    @Override
    public void bind(GLState pGLState, VertexBufferObjectAttributes pVertexBufferObjectAttributes) throws ShaderProgramException {
        super.bind(pGLState, pVertexBufferObjectAttributes);
        GLES20.glUniformMatrix4fv(
                mModelViewProjectionMatrixLocation,
                1,
                false,
                pGLState.getModelViewProjectionGLMatrix(),
                0
        );
        GLES20.glUniform1i(mTexture0Location, 0);

        //if(mIntensity != mTargetIntensity){
        final long currentTime = System.currentTimeMillis();
        final long deltaTime = currentTime - mPrevChangeTime;
        mPrevChangeTime = currentTime;

        final float absoluteIntensityDifference = mTargetIntensity - mIntensity;
        final float intensityChange = this.limitToMaxIntensityFactorChange(absoluteIntensityDifference, deltaTime);
        mIntensity += intensityChange;

        this.mIsChanging = mIntensity != mTargetIntensity;
        if(!mIsChanging){
            //toggleShaded();
        }
        //}


        GLES20.glUniform1f(mIntensityLocation, mIntensity);
        GLES20.glUniform4f(mColorLocation, mRed, mGreen, mBlue, mAlpha);
    }

    private float limitToMaxIntensityFactorChange(final float pValue, final long pDeltaTime) {
        if (pValue > 0) {
            return Math.min(pValue, this.mMaxIntensityFactorChange * pDeltaTime);
        } else {
            return Math.max(pValue, -this.mMaxIntensityFactorChange * pDeltaTime);
        }
    }
}