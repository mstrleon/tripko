package app.tripko.bertoncelj1.arrowNavigation.circle.poi.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import app.tripko.bertoncelj1.DataStorage;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.util.HttpCalls;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 3/6/16.
 */
public class PoiDataManager {

    public interface  CallBackFunction {
        void onNewDataLoaded(LatLng pPosition);
    }

    private static final String DEBUG_TAG = "PoiDataManager";
    private static final String DATA_DIR = "poiData/";
    private static final String DATA_DIR_POIS = "poiData/pois/";

    private  HashMap<Integer, PoiData> mAllPoiData;
    private ArrayList<PoiData> mUnsavedPoiData;
    private DownloadDecider mDownloadDecider;
    private boolean mIsLoadingSinglePoiData;

    private static PoiDataManager INSTANCE;

    public static PoiDataManager getInstance(){
        if(INSTANCE == null){
            INSTANCE = new PoiDataManager();
        }
        return INSTANCE;
    }

    //TODO 2 Do Load poi data on creating manager it can sometimes take too long
    private PoiDataManager(){
        Dbg.i("PoiDataManager constructor");
        mDownloadDecider = new DownloadDecider();
        loadPoiData();
    }

    public static void destroy(){
        if(INSTANCE != null) {
            INSTANCE.saveUnsavedPoiData();
            INSTANCE = null;
        }
    }

    public HashMap<Integer, PoiData> getPoiData(){
        return mAllPoiData;
    }

    private void loadPoiData() {
        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) {
            Dbg.e(DEBUG_TAG, "Storage not available. No data loaded.");
            mAllPoiData = new HashMap<>();
            mUnsavedPoiData = new ArrayList<>();
            return;
        }

        LinkedList<Object> allData = dataStorage.loadAllFiles(DATA_DIR_POIS, DataStorage.Type.OBJECT);

        mAllPoiData = new HashMap<>(allData.size());
        mUnsavedPoiData = new ArrayList<>();

        Dbg.d(DEBUG_TAG, "Loading from cache ...");
        for(Object data: allData){
            final PoiData poiData = (PoiData) data; //TODO check instance of !?
            addNewPoiData(poiData, true);
        }
        Dbg.d(DEBUG_TAG, "Done loading from cache ...");
    }


    public void addNewPoiData(final PoiData pPoiData){
        addNewPoiData(pPoiData, false);
    }

    /**
     * @param pPoiData new poi data
     * @param isSaved if poiData is already saved
     */
    private void addNewPoiData(final PoiData pPoiData, boolean isSaved){
        mAllPoiData.put(pPoiData.id, pPoiData);

        if (!isSaved) mUnsavedPoiData.add(pPoiData);
    }

    public int addNewPoiData(ArrayList<PoiData> pPoiDataList){
        int newPoiDataAdded = 0;
        for(PoiData data: pPoiDataList){
            addNewPoiData(data, false);
            newPoiDataAdded ++;
        }

        saveUnsavedPoiData();
        return newPoiDataAdded;
    }

    private String getPoiDataFileName(final PoiData pPoiData){
        return pPoiData.id + ".PoiDataNew";
    }

    private void saveUnsavedPoiData(){
        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) {
            Dbg.e(DEBUG_TAG, "Storage not available. No data saved.");
            return;
        }

        for(int i = mUnsavedPoiData.size() - 1; i >= 0; i--){
            final PoiData poiData = mUnsavedPoiData.get(i);
            final String filePath = DATA_DIR_POIS + getPoiDataFileName(poiData);
            if(dataStorage.save(filePath, poiData)){
                mUnsavedPoiData.remove(i);
            }
        }
    }


    public boolean downloadNewData(final LatLng location, final int radius, ApiCaller pAPIManager, final CallBackFunction pCallBack){
        if(pAPIManager == null){
            Dbg.e(DEBUG_TAG, "downloadNewData(): Cannot download new data pAPIManger is null");
            return false;
        }
        if(location == null){
            Dbg.e(DEBUG_TAG, "downloadNewData(): Cannot download new data location is null");
            return false;
        }

        if(!mDownloadDecider.isReadyToDownload(location, radius)){
            Dbg.d(DEBUG_TAG, "downloadNewData(): not ready to download " + location.toString() + " r:" + radius);
            if(pCallBack != null)pCallBack.onNewDataLoaded(location);
            return false;
        }

        Dbg.d(DEBUG_TAG, "downloadNewData(): starting download " + location.toString() + " r:" + radius);

        pAPIManager.getPoisFromGpsAsync(location, radius, new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                if (report.isSuccessful() && dataType == String.class) {
                    ArrayList<PoiData> pois = PoiData.extractPoiData((String) data);
                    if(pois == null){
                        Dbg.e(DEBUG_TAG, "No POIs extracted from data:" + data);
                        return;
                    }

                    int newPoiDataAdded = 0;
                    if(pois.size() == 0){
                        Dbg.d(DEBUG_TAG, "No pois found in radius " + radius + " km from location " + location.toString());
                    }else {
                        newPoiDataAdded = PoiDataManager.this.addNewPoiData(pois);
                        Dbg.d(DEBUG_TAG, "New " + newPoiDataAdded + " pois added.");
                    }
                } else {
                    Dbg.e(DEBUG_TAG, "Error getting pois " + report.getMessage());
                }

                mDownloadDecider.registerDownloadComplete(location, radius, report.isSuccessful());

                if(pCallBack != null)pCallBack.onNewDataLoaded(location);
            }
        });

        return true;
    }

    public interface OnPoiLoadedCallBack{
        void onPoiDataLoaded(PoiData poiData, boolean isLast);
    }

    public ArrayList<PoiData> getPoiDataFromIid(Integer[] ids, OnPoiLoadedCallBack callBack){
        ArrayList<PoiData> poiDatas = new ArrayList<>();
        ArrayList<Integer> poiDataLoad = new ArrayList<>();

        for(int id: ids){
            PoiData data = mAllPoiData.get(id);
            if(data != null)poiDatas.add(data);
            else poiDataLoad.add(id);
        }

        return poiDatas;
    }

    public boolean isIsLoadingSinglePoiData(){
        return mIsLoadingSinglePoiData;
    }

    public PoiData getPoiDataFromId(int id, OnPoiLoadedCallBack callBack){
        PoiData poiData = mAllPoiData.get(id);
        if(poiData == null && callBack != null) loadSinglePoiData(id, callBack);
        return poiData;
    }

    private void loadSinglePoiData(final int id, final OnPoiLoadedCallBack callBack){
        // TODO lahko se bi zabeležilo katere poi id se že
        // downlovda tako da ko bi prišli novi requesti bi jih
        // lahko samo priključili k že aktivnim requestom za ta poi id

        mIsLoadingSinglePoiData = true;
        ApiCaller.getInstance().getPoiById(id, new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                mIsLoadingSinglePoiData = false;
                PoiData poiData = null;

                if(report.isSuccessful() && dataType == String.class) {
                    HashMap<String, Object> map = HttpCalls.getHashMap((String) data);
                    poiData = PoiData.extractPoiData(map);
                    PoiDataManager.getInstance().addNewPoiData(poiData);

                    if(id != poiData.id)throw new IllegalStateException(
                            "Asked for poi with id:" + id + " got instead id:" + id);

                    PoiDataManager.this.addNewPoiData(poiData, true);
                }
                if(callBack != null) callBack.onPoiDataLoaded(poiData, true);
            }
        });
    }

    class DataLoaded{
        int dataNotLoaded = 0;
        boolean doneLoading = false;
    }
    public void loadPoiDatas(final int[] ids, final OnPoiLoadedCallBack callBack){
        final DataLoaded dataLoaded = new DataLoaded();

        for(int i=0; i<ids.length; i++) {
            final int id = ids[i];

            dataLoaded.dataNotLoaded ++;
            if(i == ids.length - 1)dataLoaded.doneLoading = true;

            ApiCaller.getInstance().getPoiById(id, new HttpCalls.ResponseFunction() {
                @Override
                public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                    dataLoaded.dataNotLoaded --;
                    if (report.isSuccessful() && dataType == String.class) {
                        HashMap<String, Object> map = HttpCalls.getHashMap((String) data);
                        PoiData poiData = PoiData.extractPoiData(map);
                        PoiDataManager.getInstance().addNewPoiData(poiData);

                        if (id != poiData.id) throw new IllegalStateException(
                                "Asked for poi with id:" + id + " got instead id:" + id);

                        PoiDataManager.this.addNewPoiData(poiData, true);
                        if (callBack != null) {
                            boolean lastData = dataLoaded.doneLoading && dataLoaded.dataNotLoaded == 0;
                            callBack.onPoiDataLoaded(poiData, lastData);
                        }
                    }
                }
            });
        }


    }
}