package app.tripko.bertoncelj1.arrowNavigation.circle.poi.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static app.tripko.bertoncelj1.util.Util.extractIntegerValue;
import static app.tripko.bertoncelj1.util.Util.extractStringValue;

/**
 * Created by anze on 3/7/16.
 */
public class Gallery implements Serializable {
    final int id;
    final public String image_thumb;
    final public String image_medium;
    final public String image_large;
    final public String image_tower;
    final public String image_landscape;
    final public String image_square;

    public Gallery(int id, String thumb, String medium, String large,
                    String tower, String landscape, String square){
        this.id = id;
        this.image_thumb = thumb;
        this.image_medium = medium;
        this.image_large = large;
        this.image_tower = tower;
        this.image_landscape = landscape;
        this.image_square = square;
    }

    // extracts Gallery from HashMap
    public Gallery(HashMap<String,Object> map){
        id = extractIntegerValue("id", map, -1);
        image_thumb = extractStringValue("image_thumb", map);
        image_medium = extractStringValue("image_medium", map);
        image_large = extractStringValue("image_large", map);
        image_tower = extractStringValue("image_tower", map);
        image_landscape = extractStringValue("image_landscape", map);
        image_square = extractStringValue("image_square", map);
    }

    public String getImage(){
        if(image_medium != null)return image_medium;
        if(image_square != null)return image_square;
        if(image_thumb != null)return image_thumb;
        if(image_large != null)return image_large;
        if(image_landscape != null) return image_landscape;
        if(image_tower != null) return image_tower;

        return image_medium;
    }

    @Override
    public boolean equals(Object gallery){
        if(super.equals(gallery)) return true;

        if(!(gallery instanceof Gallery)) return false;

        Gallery g = (Gallery) gallery;
        if(!this.image_landscape.equals(g.image_landscape)) return false;
        if(!this.image_medium.equals(g.image_medium)) return false;
        if(!this.image_large.equals(g.image_large)) return false;
        if(!this.image_square.equals(g.image_square)) return false;
        if(!this.image_thumb.equals(g.image_thumb)) return false;
        if(!this.image_tower.equals(g.image_tower)) return false;

        return true;
    }

    @Override
    public String toString(){
        return String.format("id:%d, medium:%s", id, image_medium);
    }
}
