package app.tripko.bertoncelj1.arrowNavigation;

import java.io.Serializable;

/**
 * Created by openstackmaser on 24.2.2016.
 */
public enum NavigationMode implements Serializable {
    DISCOVERING, TRIP
}
