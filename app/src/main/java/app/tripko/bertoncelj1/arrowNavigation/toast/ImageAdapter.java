package app.tripko.bertoncelj1.arrowNavigation.toast;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.Gallery;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class ImageAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    private DisplayImageOptions options;

    private static Random sRandom = new Random();
    private static final int MAX_IMAGES = 1;
    private ArrayList<Gallery> mGallery;
    private PoiData mPoiData;
    private int mImagesCount;

    ImageAdapter(Context context, PoiData pPoiData) {
        this.mGallery = pPoiData.galleryImages;
        this.mPoiData = pPoiData;

        mImagesCount = 0;
        if(!mPoiData.gallery.image_medium.equals(""))mImagesCount ++;
        mImagesCount += (mGallery == null)? 0 : mGallery.size();
        if(mImagesCount > MAX_IMAGES)mImagesCount = MAX_IMAGES;

        inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.missing_image)
                .showImageForEmptyUri(R.drawable.missing_image)
                .showImageOnFail(R.drawable.missing_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();

    }

    @Override
    public int getCount() {
        return mImagesCount;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final RecyclerView.ViewHolder holder;
        final ImageView image;
        if (convertView == null) {
            view = inflater.inflate(R.layout.toast_gallery_item_list, parent, false);
            image = (ImageView) view.findViewById(R.id.image);
            view.setTag(image);
        } else {
            image = (ImageView) view.getTag();
        }

        final String imageUrl;
        if(mPoiData.gallery.image_medium != null) {
            if(position == 0) {
                imageUrl = mPoiData.gallery.image_medium;
            }else{
                imageUrl = mGallery.get(position - 1).image_medium;
            }
        } else {
            imageUrl = mGallery.get(position).image_medium;
        }


        ImageLoader.getInstance().displayImage(ApiCaller.API_URL + imageUrl, image, options, animateFirstListener);

        return view;
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
