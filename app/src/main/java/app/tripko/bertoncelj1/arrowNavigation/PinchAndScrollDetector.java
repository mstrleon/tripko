package app.tripko.bertoncelj1.arrowNavigation;

import android.view.MotionEvent;

import org.andengine.input.touch.TouchEvent;

import app.tripko.bertoncelj1.arrowNavigation.circle.CompassCircle;

/**
 * Created by openstackmaser on 13.1.2016.
 */
public class PinchAndScrollDetector  {

    private SmoothBoundCamera mSmoothBoundCamera;

    public PinchAndScrollDetector(final SmoothBoundCamera pZoomCamera){
        this.mSmoothBoundCamera = pZoomCamera;

        setSize(mSmoothBoundCamera.getWidth(), mSmoothBoundCamera.getHeight());
    }

    private MotionEvent mMotionEvent;

    float pinchDistanceStart;
    float zoom = 1;

    private float mWidth;
    private float mHeight;

    boolean oneDownInit = false;
    public static boolean first = false;
    boolean prepared = false;

    public final boolean onTouchEvent(final TouchEvent pSceneTouchEvent) {
        mMotionEvent = pSceneTouchEvent.getMotionEvent();

        mSmoothBoundCamera.markLastUserChange();

        int action = mMotionEvent.getAction() & MotionEvent.ACTION_MASK;
        switch(action){
            case MotionEvent.ACTION_DOWN:
                mSmoothBoundCamera.setFingersDown(true);

            case MotionEvent.ACTION_POINTER_DOWN:

                if(pSceneTouchEvent.getMotionEvent().getPointerCount() == 1 && first) {
                    prepareScroll();
                }

                if(pSceneTouchEvent.getMotionEvent().getPointerCount() >= 2) {
                    CompassCircle.clickEnabled = false;
                    prepareZoom();
                }
                break;

            case MotionEvent.ACTION_UP:
                prepared = false;
                mSmoothBoundCamera.setFingersDown(false);
                CompassCircle.clickEnabled = true;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                if(pSceneTouchEvent.getMotionEvent().getPointerCount() <= 2) {
                    mSmoothBoundCamera.setZoomSmoothResize(true);
                }
                break;

        }

        if(pSceneTouchEvent.getMotionEvent().getPointerCount() == 1) {
            if(!oneDownInit){
                first = true;
                oneDownInit = true;
                prepareScroll();
            }

            if(prepared) setScrollPosition();

        }

        if(pSceneTouchEvent.getMotionEvent().getPointerCount() >= 2) {
            setZoomPosition();
        }

        return pSceneTouchEvent.getMotionEvent().getPointerCount() >= 2;
    }

    public void setSize(final float pWidth, final float pHeight){
        mWidth = pWidth;
        mHeight = pHeight;
    }

    private void setScrollPosition(){
        float middleX = mMotionEvent.getX();
        float middleY = mHeight - mMotionEvent.getY();

        mSmoothBoundCamera.middleX = middleX;
        mSmoothBoundCamera.middleY = middleY;

        mSmoothBoundCamera.setNewPosition(middleX, middleY);
    }

    private void setZoomPosition(){
        oneDownInit = false;
        final float x1 = mMotionEvent.getX(0);
        final float y1 = mMotionEvent.getY(0);

        final float x2 = mMotionEvent.getX(1);
        final float y2 = mMotionEvent.getY(1);

        final float pinchDistance = (float)Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
        zoom = pinchDistance / pinchDistanceStart;

        //zoom = mSmoothBoundCamera.setZoom(zoom);
        //pinchDistanceStart *= pinchDistance / (pinchDistanceStart * zoom);

        final float middleX = (x1 + x2) / 2;
        final float middleY = mHeight - (y1 + y2) / 2;

        mSmoothBoundCamera.setNewPosition(middleX, middleY, zoom);
    }

    private void prepareScroll(){
        final float x = mMotionEvent.getX();
        final float y = mMotionEvent.getY();

        float relativeX = x / mWidth;
        mSmoothBoundCamera.middleXStart = mSmoothBoundCamera.getXMin() + relativeX * mSmoothBoundCamera.getWidth();

        final float relativeY = 1 - (y / mHeight);
        mSmoothBoundCamera.middleYStart = mSmoothBoundCamera.getYMin() + relativeY * mSmoothBoundCamera.getHeight();

        prepared = true;
    }

    private void prepareZoom(){
        mSmoothBoundCamera.setZoomSmoothResize(false);
        final float x1 = mMotionEvent.getX(0);
        final float y1 = mMotionEvent.getY(0);
        final float x2 = mMotionEvent.getX(1);
        final float y2 = mMotionEvent.getY(1);

        pinchDistanceStart = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        pinchDistanceStart /= mSmoothBoundCamera.getZoom();

        float middleX = (x1 + x2) / 2;
        float relativeX = middleX / mWidth;
        mSmoothBoundCamera.middleXStart = mSmoothBoundCamera.getXMin() + relativeX * mSmoothBoundCamera.getWidth();

        final float middleY = (y1 + y2) / 2;
        final float relativeY = 1 - (middleY / mHeight);
        mSmoothBoundCamera.middleYStart = mSmoothBoundCamera.getYMin() + relativeY * mSmoothBoundCamera.getHeight();
    }

}
