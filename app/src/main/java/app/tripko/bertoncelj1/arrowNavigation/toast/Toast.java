package app.tripko.bertoncelj1.arrowNavigation.toast;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.Random;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.customViews.BubbleText;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.VisitedSetting;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by openstackmaser on 4.2.2016.
 */
public class Toast {

    private SlidingUpPanelLayout mSlidingLayout;
    private View mCloseButtonClickArea;
    private TextView mTitle;
    private TextView mDescription;
    private TextView mNoImagesAvailable;
    private BubbleText mType;
    private LinearLayout mExternalNavigationLayout;
    private Button mExternalNavigationButton;
    private LinearLayout mPoiInfoLayout;
    private PoiData mShowingPoiData;
    private ListView mGalleryListView;
    private RelativeLayout mTopLayout;
    private TextView mToFarMessage;

    private long mTimeLastShown;
    private static final int TIME_BEFORE_SHOW_AGAIN = 5000; //time before toast can show again after closing
    private static final int TIME_BEFORE_SHOW_SAME_AGAIN = 60000; //time before toast can show again after closing

    public static final int STATE_EXTERNAL_NAVIGATION = 0;
    public static final int STATE_POI_INFO = 1;
    public static final int STATE_HIDDEN = 2;

    private int mState = STATE_HIDDEN;

    private Activity mActivity;


    public Toast(final Activity pActivity){
        mActivity = pActivity;

        mTopLayout = (RelativeLayout) pActivity.findViewById(R.id.ToastTopLayout);
        mSlidingLayout = (SlidingUpPanelLayout) pActivity.findViewById(R.id.sliding_layout);
        mCloseButtonClickArea = pActivity.findViewById(R.id.btn_close_click_area);
        mTitle = (TextView) pActivity.findViewById(R.id.toast_name);
        mDescription = (TextView) pActivity.findViewById(R.id.toast_description);
        mNoImagesAvailable = (TextView) pActivity.findViewById(R.id.toast_no_images_available);
        mType = (BubbleText) pActivity.findViewById(R.id.toast_type);
        mExternalNavigationLayout = (LinearLayout) pActivity.findViewById(R.id.externalNavigation);
        mPoiInfoLayout = (LinearLayout) pActivity.findViewById(R.id.poiInfo);
        mExternalNavigationButton = (Button) pActivity.findViewById(R.id.bExternalNavigation);
        mToFarMessage = (TextView) pActivity.findViewById(R.id.toFarMessage);
        mNoImagesAvailable.setVisibility(View.GONE);

        mTopLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                final int height = top - bottom;
                refreshHeight();
            }
        });

        mGalleryListView = (ListView) pActivity.findViewById(R.id.toast_gallery_list);

        mGalleryListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //disable sliding layout if galery is clicked
                if(event.getAction() == MotionEvent.ACTION_DOWN)mSlidingLayout.setEnabled(false);
                if(event.getAction() == MotionEvent.ACTION_UP)mSlidingLayout.setEnabled(true);
                return false;
            }
        });

        mExternalNavigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int poiId = Settings.getInstance().getCurrentTrackPositionPoiId();
                PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(poiId, null);
                if(poiData == null)return; // TODO

                LatLng position = LocationManager.lastGPSPosition;

                final Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?" +
                                "saddr=" + position.getLatitude() + "," + position.getLongitude() +
                                "&daddr=" + poiData.latLng.getLatitude() + "," + poiData.latLng.getLongitude()));
                intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
                pActivity.startActivity(intent);

                mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });


        mCloseButtonClickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                //mPoiCircleManager.highlightTarget(null, false);
            }
        });

        mSlidingLayout.setClipPanel(false);
        mSlidingLayout.setAnchorPoint(0);


        //slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
    }

    public int getState() {
        return mState;
    }

    public void refreshHeight(){
        mSlidingLayout.setPanelHeight(mSlidingLayout.getPanelHeight());
    }

    public void showExternalNavigation(final PoiData poiData){
        mState = STATE_EXTERNAL_NAVIGATION;
        mSlidingLayout.setTouchEnabled(false);

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mExternalNavigationLayout.setVisibility(View.VISIBLE);
                mPoiInfoLayout.setVisibility(View.INVISIBLE);
                mToFarMessage.setText(poiData.name + " " +
                        mActivity.getResources().getString(R.string.poi_to_far_toast));

                // waits until text is updated than it updates sliding layout height
                mExternalNavigationButton.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        mExternalNavigationButton.getViewTreeObserver().removeOnPreDrawListener(this);
                        mSlidingLayout.setPanelHeight(mExternalNavigationLayout.getHeight());
                        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        return false;
                    }
                });
            }
        });
    }

    public PoiData getCurrentShowingPoiData(){
        return mShowingPoiData;
    }

    public void hideToast() {
        if (mSlidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN) return;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                mState = STATE_HIDDEN;
            }
        });
        mShowingPoiData = null;
        mTimeLastShown = 0;
    }

    public void showToast(final int pPoiId, final boolean force){

        mSlidingLayout.setTouchEnabled(false);

        //loads poi data
        PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(pPoiId, new PoiDataManager.OnPoiLoadedCallBack() {
            @Override
            public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
                showToast(poiData, force);
            }
        });

        showToast(poiData, force);
    }

    //TODO to funkcijo bo ptrebno preurediti to da dobi poiCircle je definitivno narobe,
    public void showToast(final PoiData pPoiData, boolean force){
        if(pPoiData == null){
            throw new IllegalArgumentException("Cannot show toast. PoiData is null");
        }

        final Long currMillis = System.currentTimeMillis();

        if(!force && !canToastShow(currMillis, pPoiData))return;

        //Log poi visited //TODO move this to more reasonable place


        mSlidingLayout.setTouchEnabled(true);

        mState = STATE_POI_INFO;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mExternalNavigationLayout.setVisibility(View.GONE);
                mPoiInfoLayout.setVisibility(View.VISIBLE);

                switch(mSlidingLayout.getPanelState()){
                    //if it is already collapsed hide it first
                    case COLLAPSED:
                        mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

                    case HIDDEN:
                        mShowingPoiData = pPoiData;
                        mTimeLastShown = currMillis;

                        mType.setType(pPoiData.poiType);
                        mTitle.setText(pPoiData.name);
                        mTitle.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                            @Override
                            public boolean onPreDraw() {
                                mTitle.getViewTreeObserver().removeOnPreDrawListener(this);
                                mSlidingLayout.setPanelHeight(mTopLayout.getHeight());
                                mSlidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                                return false;
                            }
                        });
                        setDescription(pPoiData.description);
                        setGallery(pPoiData);
                        break;
                }
            }
        });
    }


    private boolean canToastShow(long currMillis, PoiData pPoiData){
        if(currMillis - mTimeLastShown < TIME_BEFORE_SHOW_AGAIN)return false;

        //if current Poi still in viewing distance don't do nothing
        if(mShowingPoiData != null &&
                pPoiData.isInside(mShowingPoiData.latLng.getDistance(LocationManager.lastGPSPosition)))
            return false;

        //ignore if triggered on same poi
        if(pPoiData == mShowingPoiData
                && currMillis - mTimeLastShown < TIME_BEFORE_SHOW_SAME_AGAIN &&
                mSlidingLayout.getPanelState() != SlidingUpPanelLayout.PanelState.HIDDEN)
            return false;

        return true;
    }

    private void setGallery(final PoiData pPoiData){
        final ImageAdapter imageAdapter = new ImageAdapter(mActivity, pPoiData);
        mGalleryListView.setAdapter(imageAdapter);
        mNoImagesAvailable.setVisibility(imageAdapter.getCount() == 0? View.VISIBLE : View.GONE);
    }

    private void setDescription(String pDescription){
        if(pDescription == null)pDescription = "No description";
        if(pDescription.length() > 100){
            int i;
            for(i=100; i>80; i--){
                if(pDescription.charAt(i) == ' ')break;
            }
            if(i == 80)i = 97;
            pDescription = pDescription.substring(0, i + 1) + "...";
        }
        mDescription.setText(pDescription);
    }
}
