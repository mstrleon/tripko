package app.tripko.bertoncelj1.arrowNavigation.circle.poi.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircleMapTexture;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.LocationName;
import app.tripko.bertoncelj1.util.Util;

import static app.tripko.bertoncelj1.util.Util.extractDoubleValue;
import static app.tripko.bertoncelj1.util.Util.extractIntegerValue;
import static app.tripko.bertoncelj1.util.Util.extractStringValue;

/**
 * Created by openstackmaser on 2.2.2016.
 */
public class PoiData  implements Serializable {

    public static final String DEBUG_TAG = "PoiData";

    //default value if radius from API is not set or it cannot be read
    public static final int DEF_RADIUS = 30;

    public int id;
    public String name = "no name";
    public LatLng latLng;
    public int textureIndex;
    public String description;

    public int radius;
    public int priority_index;
    public PoiType poiType;
    public int type_id;
    public String type_name;
    public int parent_type_id;
    public String openingHours;

    // how much time user spends on average at POI, in minutes
    public static final int DEF_VISIT_DURATION = 15;
    public int visitDuration = DEF_VISIT_DURATION;

    public String pin_small;
    public String pin_big;
    public String url;

    public Gallery gallery;

    public ArrayList<Gallery> galleryImages;

    public ArrayList<Tag> tags;
    public Address address;
    public Details details;
    public Contact contact;

    //tells where poi is location (in what city, town...)
    //It is not read from API but is rather acquired from LocationName class
    private static String LOCATION_NAME_UNKNOWN = "?";
    private static String LOCATION_NAME_LOADING = "...";
    private String locationName;

    private int mHash;
    private boolean hashGenerated = false;


    public static class Tag implements Serializable{
        public int id;
        public String name;

        public Tag(int pId, String pName){
            id = pId;
            name = pName;
        }
    }

    public static class Details implements Serializable{
        final public String wikipedia;
        final public String facebook;
        final public String twitter;
        final public String youtube;
        final public String instagram;
        final public String pintrest;

        public static Details getDetails(HashMap<String,Object> map){
            if(map != null) return new Details(map);
            return new Details();
        }

        private Details(){
            wikipedia = facebook = twitter = youtube = pintrest = instagram = "";
        }


        private Details(HashMap<String,Object> map){
            wikipedia = extractStringValue("wikipedia", map);
            facebook = extractStringValue("facebook", map);
            twitter = extractStringValue("twitter", map);
            youtube = extractStringValue("youtube", map);
            instagram = extractStringValue("instagram", map);
            pintrest = extractStringValue("pintrest", map);
        }
    }

    public static class Address implements Serializable{
        final public String address;
        final public String city;
        final public String post_code;
        final public String country;

        public static Address getAddress(HashMap<String,Object> map){
            if(map != null) return new Address(map);
            return new Address();
        }

        private Address(){
            address = city = post_code = country = "";
        }


        private Address(HashMap<String,Object> map){
            address = extractStringValue("address1", map);
            city = extractStringValue("city", map);
            post_code = extractStringValue("post_code", map);
            country = extractStringValue("country", map);
        }

        public boolean hasAddress(){
            return address != null && !address.equals("") && city != null && !city.equals("");
        }

        /*
        Returns formatted address like "Stari trg 9, Ljubljana"
         */
        public String getFormated(){
            if(hasAddress()) {
                return String.format("%s, %s", address, city);
            } else {
                return "No address";
            }
        }
    }

    public static class Contact implements Serializable{
        final public String name;
        final public String email;
        final public String telephone;
        final public String fax;
        final public String mobile;

        public static Contact getContact(HashMap<String,Object> map){
            if(map != null) return new Contact(map);
            return new Contact();
        }

        private Contact(){
            name = email = telephone = fax = mobile = "";
        }

        private Contact(HashMap<String,Object> map){
            name = extractStringValue("name", map);
            email = extractStringValue("email", map);
            telephone = extractStringValue("telephone", map);
            fax = extractStringValue("fax", map);
            mobile = extractStringValue("mobile", map);
        }
    }

    /**
     * Generates dummy poi data, just for testing purposes.
     */
    public static PoiData getDummyPoiData(int id){
        final PoiData poiData = new PoiData(null);
        poiData.id = id;
        poiData.name = "Lorem ipsum dolor sit as as da sd";
        poiData.description = "<p>Lorem ipsum dolor sit</p>"; // amet, consectetur adipiscing elit. Aenean laoreet et nibh vel posuere.<p/>
        // <// p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
        // Phasellus imperdiet augue in urna tristique tincidunt. Aliquam volutpat vitae ipsum sed mollis. Vestibulum
        // vel ligula justo. Mauris et eros vel erat dapibus consectetur. Aliquam id justo nec leo finibus maximus.
        // Integer hendrerit, diam convallis laoreet ultricies, massa lorem cursus elit, at aliquam tellus leo eget elit
        // . Morbi mollis erat leo.</p><p> Nulla vulputate, massa sed dictum maximus, nibh metus feugiat dolor, iaculis dapibus enim sem in mi. Vivamus a libero vitae nisi finibus vehicula nec vel nisl. Morbi pulvinar vel sem id suscipit. Integer elementum viverra molestie. Vestibulum turpis ex, tempus vitae lacinia eget, bibendum vitae metus.</p>";
        poiData.latLng = new LatLng(46.134170, 14.436035);
        poiData.textureIndex = 0;
        poiData.tags = new ArrayList<>();
        poiData.tags.add(new Tag(0, "square"));
        poiData.tags.add(new Tag(0, "cathedral"));
        poiData.galleryImages = null;

        return poiData;
    }

    /*
    Returns true if distance is inside the range of the poi. Each poi can have different "inside range"
     */
    public boolean isInside(float distanceToPoi){
        return distanceToPoi <= radius;
    }

    /*
    Same as isInside(float) with extended range for 1.75x min 20 and max 100
    Used for when users enters poi it cannot immediately leave it
     */
    public boolean isInsideSoft(float distanceToPoi){
        return distanceToPoi <= radius + Util.limit(15, distanceToPoi * 0.75f, 100);
    }

    @Override
    public String toString(){
        return String.format("id:%d, name:%s", id, name);
    }

    @Override
    public boolean equals(final Object object) {
        if(this == object)return true;

        if(object instanceof PoiData) {
            final PoiData pPoiData = (PoiData) object;
            return this.getHash() == pPoiData.getHash();
        }

        return false;
    }

    public interface GetLocationNameListener {
        void onGetLocationName(String location);
    }

    /*
    Returns locationName if already defined
    otherwise it gets it from LocationName class
     */
    public String getLocation(final GetLocationNameListener listener){
        if(locationName != null) return locationName;

        LocationName.LocationAddress address = LocationName.getAddress(latLng, new LocationName.LocationAddressListener() {
            @Override
            public void gotLocationAddress(LocationName.LocationAddress pLocationAddress) {
                locationName = getLocationNameFromLocationAddress(pLocationAddress);
                if(listener != null)listener.onGetLocationName(locationName);
            }
        });

        locationName = getLocationNameFromLocationAddress(address);

        //if no listener is set LOCATION_NAME_UNKNOWN is returned since
        // locationName is technically not loading
        return locationName;
    }

    /*
    Manual set location name
     */
    public void setLocationName(String locationName){
        this.locationName = locationName;
    }

    private String getLocationNameFromLocationAddress(LocationName.LocationAddress address){
        switch (address.status){
            case ACQUIRING:
                return LOCATION_NAME_LOADING;

            case SUCCESS:
                return address.getMostPrecise();

            case FAILED:
            default:
                return LOCATION_NAME_UNKNOWN;
        }
    }

    //TODO is this hash ok? Should I add parameters like gallery and images?
    private void generateHash(){
        if(name != null)mHash = name.hashCode();
        if(description != null)mHash += description.hashCode();
        if(latLng != null)mHash += (latLng.getLatitude() + latLng.getLongitude() + "").hashCode();
        mHash += textureIndex;

        hashGenerated = true;
    }

    public int getHash(){
        if(!hashGenerated)generateHash();
        return mHash;
    }

    public String galleryToString() {
        if(galleryImages == null || galleryImages.size() == 0) return "Gallery is empty";
        StringBuilder buf = new StringBuilder();

        buf.append("Gallery size: ")
                .append(galleryImages.size())
                .append("\n");

        for (Gallery g : this.galleryImages) {
            buf.append(g.toString())
                    .append("\n");
        }

        return buf.toString();
    }

    //creates new pois from json data
    public static ArrayList<PoiData> extractPoiData(String JsonData){
        HashMap<String,Object> map = null;
        try {
            map = new ObjectMapper().readValue(JsonData,HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            final ArrayList<LinkedHashMap> data = (ArrayList<LinkedHashMap>) map.get("data");

            if(data == null) return null;

            final ArrayList<PoiData> pois = new ArrayList<>(data.size());

            for(LinkedHashMap poiData: data){
                final PoiData newPoi = extractPoiData(poiData);
                if(newPoi != null){
                    pois.add(newPoi);
                }
            }

            return pois;

        } catch (Exception e){
            e.printStackTrace();
            Dbg.e(DEBUG_TAG, "Error occurred while extracting data: " + e.getMessage());
        }

        return null;
    }

    // read all gallery images from JSON
    private void setPoiGallery(ArrayList<LinkedHashMap> list){
        if(list == null)return;

        galleryImages = new ArrayList<>(list.size());

        for(LinkedHashMap element: list){
            HashMap map = (HashMap) element.get("image");

            Gallery gallery = new Gallery(map);

            if(!gallery.equals(gallery)) {
                galleryImages.add(gallery);
            }
        }

    }

    private void setPoiTags(ArrayList<LinkedHashMap> tagsList){
        if(tagsList == null)return;

        tags =  new ArrayList<>(tagsList.size());

        for(LinkedHashMap tag: tagsList) {
            final int id = extractIntegerValue("tag_id", tag);
            final String name = extractStringValue("name", tag);

            tags.add(new Tag(id, name));
        }
    }

    /*
    Gives rating of how much the phrase matches the destination. The score can be from 0 to inf.
    0 meaning that it does't match
     */
    public int getMatchingToQuerry(String querry){
        //splits search phrase to words
        String[] searchWords = querry.split("\\W+");

        //splits name and description to words
        String[] nameSplited = name.toLowerCase().split("\\W+");
        String[] typeSplited = poiType.name.toLowerCase().split("\\W+");

        int score = 0;
        for(String searchWord: searchWords){
            score += Util.getScore(searchWord, nameSplited) * 10;
            score += Util.getScore(searchWord, typeSplited);
        }

        return score;
    }

    private PoiData(){}

    private PoiData(HashMap<String,Object> map){
        if(map == null) return;

        id = extractIntegerValue("id", map);
        type_name = extractStringValue("type_name", map);
        type_id = extractIntegerValue("poi_type_id", map);
        poiType = PoiType.getPoiTypeFromId(this.type_id);
        radius = extractIntegerValue("radius", map, DEF_RADIUS);
        priority_index = extractIntegerValue("priority_index", map);
        visitDuration = extractIntegerValue("visit_duration", map, DEF_VISIT_DURATION);
        openingHours = extractStringValue("opening_hours", map);


        textureIndex = PoiCircleMapTexture.getTextureIndexFromPoiType(this.poiType);

        latLng = new LatLng(extractDoubleValue("lat", map), extractDoubleValue("lon", map)); //TODO get from pool?

        name = extractStringValue("name", map);
        url = extractStringValue("url", map);
        description = extractStringValue("description", map);

        gallery = new Gallery(map);
        address = Address.getAddress((HashMap<String, Object>) map.get("address"));
        contact = Contact.getContact((HashMap<String, Object>) map.get("contact"));
        details = Details.getDetails((HashMap<String, Object>) map.get("details"));


        setPoiGallery((ArrayList<LinkedHashMap>) map.get("gallery"));
        setPoiTags((ArrayList<LinkedHashMap>) map.get("tags"));

        generateHash();

    }

    //creates new pois from json data
    public static PoiData extractPoiData(HashMap<String,Object> map){
        if(map == null) return null;

        final PoiData newPoi = new PoiData(map);
        return newPoi;
    }
}