package app.tripko.bertoncelj1.arrowNavigation;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;

/**
 * Created by anze on 3/8/16.
 */
public class CalibrateCompassDialog extends Dialog implements View.OnClickListener{

    private CheckBox mDontShowAgainCheckBox;

    protected CalibrateCompassDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_calibrate_compass);

        configureWindow(super.getWindow());

        LinearLayout dontShowAgainLayout = (LinearLayout) findViewById(R.id.llDontShowAgain);
        mDontShowAgainCheckBox = (CheckBox) findViewById(R.id.cbDontShowAgain);

        dontShowAgainLayout.setOnClickListener(this);
        findViewById(R.id.calibrateImage).setOnClickListener(this);
    }


    private void configureWindow(final Window window){
        //makes background transperant
        window.setBackgroundDrawable(new ColorDrawable(0));

        //sets dimming amount
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.dimAmount = 0.8f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        window.setAttributes(lp);
    }

    Handler delayCloser;
    Runnable closeRunnable;
    boolean isClosing = false;

    public void closeWithDelay(long delay){
        if(closeRunnable != null || delayCloser != null)return;

        if(delay < 200){
            this.closeDialog();
        }

        closeRunnable = new Runnable() {
            @Override
            public void run() {
                if(!isClosing){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                           CalibrateCompassDialog.this.closeDialog();
                        }
                    });
                }
            }
        };

        delayCloser = new Handler();
        delayCloser.postDelayed(closeRunnable, delay);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.calibrateImage:
                closeDialog();
                break;

            case R.id.llDontShowAgain:
                mDontShowAgainCheckBox.setChecked(!mDontShowAgainCheckBox.isChecked());
                break;
        }
    }

    private void closeDialog(){
        isClosing = true;
        if(delayCloser != null && closeRunnable != null){
            delayCloser.removeCallbacks(closeRunnable);
        }
        saveDontShowDialog();

        super.dismiss();
    }

    // saves user's decision on showing this dialog
    private void saveDontShowDialog(){
        final Context context = CalibrateCompassDialog.this.getContext();
        SharedPreferences sharedPref = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("show_calibration_dialog", !mDontShowAgainCheckBox.isChecked());
        editor.commit();
    }

    // check if user decided to show this dialog
    public static boolean canShowDialog(final Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        return sharedPref.getBoolean("show_calibration_dialog", true);
    }
}
