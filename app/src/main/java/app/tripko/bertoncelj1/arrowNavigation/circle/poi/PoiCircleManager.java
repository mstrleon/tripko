package app.tripko.bertoncelj1.arrowNavigation.circle.poi;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import org.andengine.entity.scene.Scene;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.arrowNavigation.TextLocation;
import app.tripko.bertoncelj1.arrowNavigation.circle.CompassCircle;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Settings;


/**
 * Created by openstackmaser on 3.2.2016.
 */
public class PoiCircleManager {

    private static final String DEBUG_TAG = "PoiCircleManager";
    private final int MAX_POIS_ALLOWED = 20;
    private final int MAX_DRAW_DISTANCE = 50000; // in meters

    private VertexBufferObjectManager mVertexBufferObjectManager;
    private PhysicsWorld mPhysicsWorld;
    private Scene mScene;
    private Settings mSettings;

    private ArrayList<PoiCircle> mActivePoiSprites;
    private ArrayList<PoiData> mFilteredPoiData;
    private HashMap<Integer, PoiData> mNonReplaceablePoiData;

    private PoiCircle mHighlightedPoiCircle;

    public PoiCircleManager(PhysicsWorld pPhysicsWorld, VertexBufferObjectManager pVertexBufferObjectManager, Scene pScene) {
        if(pScene == null)throw new NullPointerException("pScene is null.");
        if(pVertexBufferObjectManager == null)throw new NullPointerException("pVertexBufferObjectManager is null.");
        if(pPhysicsWorld == null)throw new NullPointerException("pPhysicsWorld is null.");

        mScene = pScene;
        mVertexBufferObjectManager = pVertexBufferObjectManager;
        mPhysicsWorld = pPhysicsWorld;

        mActivePoiSprites = new ArrayList<>();
        mSettings = Settings.getInstance();
        mNonReplaceablePoiData = new HashMap<>();
    }

    private PoiCircle create(PoiData pInitData) {
        if(pInitData == null)throw new IllegalArgumentException("pInitData is null");
        final PoiCircle circle;
        final Body body;

        final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(1, 0.0f, 0f);

        circle = new PoiCircle(200, 200, pInitData, mVertexBufferObjectManager);
        circle.setLocation(pInitData.latLng);
        circle.setShaded(isPoiCircleHighlighted(circle));

        body = circle.setPhysicBody(mPhysicsWorld, objectFixtureDef, BodyDef.BodyType.DynamicBody, 0.1f);
        mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(circle, body, true, false));

        circle.attachToScene(mScene);

        mActivePoiSprites.add(circle);
        circle.addCollisionSprite();

        return circle;
    }

    public void setNonReplaceablePoiDatas(PoiData[] poiDatas){
        for(PoiData poiData: poiDatas) {
            setNonReplaceablePoiData(poiData);
        }
    }

    public PoiData setNonReplaceablePoiData(PoiData poiData){
        return mNonReplaceablePoiData.put(poiData.id, poiData);
    }

    public PoiData removeNonReplaceablePoiData(PoiData poiData){
        return mNonReplaceablePoiData.remove(poiData.id);
    }

    public void removeAllNonReplaceablePoiData(){
        mNonReplaceablePoiData.clear();
    }

    public ArrayList<PoiCircle> getActivePoiSprites(){
        return mActivePoiSprites;
    }

    ArrayList<PoiCircle> hiddenCircles = new ArrayList<>();

    private void hideCircle(final PoiCircle pPoiCircle){
        pPoiCircle.hide();
        mActivePoiSprites.remove(pPoiCircle);
        pPoiCircle.removeCollisionSprite();
        hiddenCircles.add(pPoiCircle);
    }

    private PoiCircle getHiddenCircle(){
        if(hiddenCircles.size() < 0)return null;
        PoiCircle hiddenCircle = hiddenCircles.remove(hiddenCircles.size() - 1);
        hiddenCircle.show(mHighlightedPoiCircle == hiddenCircle);
        mActivePoiSprites.add(hiddenCircle);
        hiddenCircle.addCollisionSprite();

        return hiddenCircle;
    }

    public void setInitData(final PoiCircle poiCircle, final PoiData pInitData){
        if(poiCircle == null || pInitData == null){
            throw new IllegalArgumentException("poiCircle or initData is null)");
        }
        Dbg.d(DEBUG_TAG, "Setting initData, Circle id:" + pInitData.id +  ", name:" + pInitData.name);

        poiCircle.setPoiData(pInitData);
        poiCircle.setPointName(pInitData.name);
        poiCircle.setLocation(pInitData.latLng);
        poiCircle.setCurrentTileIndex(pInitData.textureIndex);

        poiCircle.show(isPoiCircleHighlighted(poiCircle));

    }

    private boolean isPoiCircleHighlighted(PoiCircle poiCircle){
        if(mHighlightedPoiCircle == null)return false;
        return mHighlightedPoiCircle != poiCircle;
    }

    public void dimAllCircles(){
        if(mHighlightedPoiCircle != null){
            setHighlightedPoiCircle(mHighlightedPoiCircle, false);
        }
    }

    public PoiCircle setHighlightedPoiData(PoiData poiData, boolean highlight){
        if(poiData == null) {
            if (!highlight) {
                setHighlightedPoiCircle(null, false);
                return null;
            }

            throw new NullPointerException();
        }

        PoiCircle poiCircle = null;

        for(PoiCircle activePoiCircle: getActivePoiSprites()){
            if(activePoiCircle.getPoiData().id == poiData.id){
                poiCircle = activePoiCircle;
                break;
            }
        }

        if(highlight) {
            setNonReplaceablePoiData(poiData);
        }else{
            removeNonReplaceablePoiData(poiData);
        }

        if(poiCircle != null) {
            setHighlightedPoiCircle(poiCircle, highlight);
        }else {
            //if poiCircle does not exist it refreshes poiCircles and recursively calls this method
            if(highlight){
                refreshCircles();
                poiCircle = setHighlightedPoiData(poiData, true);
            }
        }

        return poiCircle;
    }

    //shades all circles except one
    private void setHighlightedPoiCircle(final PoiCircle pTargetPoi, boolean highlight){

        //dimm any pre highlighted poi circle
        if(mHighlightedPoiCircle != null && !highlight) {
            mHighlightedPoiCircle.resetZIndex();
        }

       mHighlightedPoiCircle = pTargetPoi;

        if(highlight) {
            pTargetPoi.setZIndexOnTop();
        }

        //shade all active circles
        for(PoiCircle poiCircle: mActivePoiSprites){
            poiCircle.setShaded(highlight && poiCircle != pTargetPoi);
        }

        if(!highlight){
            mHighlightedPoiCircle = null;
        }
    }


    //sets mode, displaying distance
    public void setDistanceMode(final boolean pDistanceMode){
        for(PoiCircle poiCircle: mActivePoiSprites){
            poiCircle.setDistanceMode(pDistanceMode);
        }
        PoiCircle.sDistanceMode = pDistanceMode;
    }

    private int lastEnforced = -1;
    public void enforceRules() {
        if(lastEnforced == mSettings.getDisabledTypesHash())return;

        lastEnforced = mSettings.getDisabledTypesHash();
        if(mSettings.areAnyPoisDisabled()){

            //gets array of all poiData available
            HashMap<Integer, PoiData> poiData = PoiDataManager.getInstance().getPoiData();
            final PoiData[] arrayPoiData = poiData.values().toArray(new PoiData[0]);

            //clears all previously set data
            if(mFilteredPoiData == null)mFilteredPoiData = new ArrayList<>();
            else if(mFilteredPoiData.size() > 0)mFilteredPoiData.clear();


            for(PoiData data: arrayPoiData){
                boolean isDisabled = mSettings.isDisabled(data.poiType);

                if(!isDisabled)mFilteredPoiData.add(data);
            }
        }else{
            mFilteredPoiData = null;
        }


    }

    public PoiData[] getFilteredPoiData(){
        if(mFilteredPoiData == null){
            HashMap<Integer, PoiData> poiData = PoiDataManager.getInstance().getPoiData();
            return poiData.values().toArray(new PoiData[0]);
        }else{
            return mFilteredPoiData.toArray(new PoiData[0]);
        }
    }

    public static class DistanceDataPair {
        public float distance;
        public PoiData poiData;

        public DistanceDataPair(float d, PoiData c){
            distance =  d;
            poiData = c;
        }
    }

    public static DistanceDataPair[] sortDataByDistance(PoiData[] arrayPoiData, LatLng location){
        DistanceDataPair[] distanceDataPairs = new DistanceDataPair[arrayPoiData.length];

        int i=0;
        for(PoiData poiCircle: arrayPoiData){
            float distance = poiCircle.latLng.getDistance(location);
            distanceDataPairs[i++] = new DistanceDataPair(distance, poiCircle);
        }

        Arrays.sort(distanceDataPairs, new Comparator<DistanceDataPair>() {
            @Override
            public int compare(DistanceDataPair lhs, DistanceDataPair rhs) {
                return Float.compare(lhs.distance, rhs.distance);
            }
        });

        return distanceDataPairs;
    }


    public void refreshCircles(){
        refreshCircles(LocationManager.lastGPSPosition);
    }

    /*
        refreshes currently visible circles.
        it makes sure that all circles that are supposed to be visible are visible
        It removes all circles that are to far away and adds all necessary circles ...
    */
    public void refreshCircles(final LatLng pUserLocation){
        if(mActivePoiSprites == null){
            Dbg.e(DEBUG_TAG, "Circle creator was is not initialized");
            return;
        }

        //list of currently active circles this list will be emptied and only circles that
        //are no longer visible remain
        PoiCircle[] circlesEmpty = mActivePoiSprites.toArray(new PoiCircle[0]);

        //from already visible poiCircles we remove all nonReplaceableCircles
        HashMap<Integer, PoiData> nonReplaceablePoiData = (HashMap<Integer, PoiData>) mNonReplaceablePoiData.clone();
        for(int i=0; i<circlesEmpty.length; i++){
            int poiDataId = circlesEmpty[i].getPoiData().id;
            if(mNonReplaceablePoiData.containsKey(poiDataId)) {
                circlesEmpty[i] = null;
                nonReplaceablePoiData.remove(poiDataId);
            }
        }
        int numEmptyCircles = moveDataDown(circlesEmpty);

        numEmptyCircles = attachesAllPoiDataToEmptyCircles(
                nonReplaceablePoiData.values().toArray(new PoiData[0]),
                nonReplaceablePoiData.size(),
                circlesEmpty, numEmptyCircles);

        enforceRules();

        //gets all poiData
        final PoiData[] arrayPoiData = getFilteredPoiData();

        //sorts data by distance
        DistanceDataPair[] distanceDataPairs = sortDataByDistance(arrayPoiData, pUserLocation);

        //decides how many circles should be displayed
        int numCirclesDisplayed = Math.min(
                //TODO non replaceable data should not exceed MAX_POIS_ALLOWED
                Math.max(0, MAX_POIS_ALLOWED - nonReplaceablePoiData.size()),
                distanceDataPairs.length);

        //list start empty and gets filled with poiData that is not yet attached to a circle
        ArrayList<PoiData> poiDataNotAttached = new ArrayList<>(numCirclesDisplayed);

        //goes through all circles that it must show, and compares them to already visible circles
        //it records every circles that is already drawn
        //it fills null in every circle that is already used
        for(int i = 0; i < numCirclesDisplayed && distanceDataPairs[i].distance < MAX_DRAW_DISTANCE; i++){
            boolean isDataAttached = false;
            final PoiData poiData = distanceDataPairs[i].poiData;

            //non replaceable dada is attached higher up
            if(!mNonReplaceablePoiData.containsKey(poiData.id)) {

                for (int j = 0; j < numEmptyCircles; j++) {

                    //checks if circle with this poiData
                    if (circlesEmpty[j] != null && circlesEmpty[j].getPoiData().id == poiData.id) {
                        isDataAttached = true;
                        circlesEmpty[j] = null;
                        break;
                    }
                }

                if(!isDataAttached){
                    poiDataNotAttached.add(distanceDataPairs[i].poiData);
                }
            }
        }

        //this is table has many "null" elements in it, it sorts the table so that all the non null
        //elements are in the beginning of the list
        numEmptyCircles =  moveDataDown(circlesEmpty);

        numEmptyCircles = attachesAllPoiDataToEmptyCircles(poiDataNotAttached.toArray(new PoiData[0]),
                poiDataNotAttached.size(), circlesEmpty, numEmptyCircles);

        //if not all empty circles are used it hides them
        for(int i=0; i < numEmptyCircles; i++){
            hideCircle(circlesEmpty[i]);
        }

        //sorts all poi circles
        setOverlapOrder(pUserLocation);
    }

    //Attaches all poi data to empty circles
    //if there are no more empty circles left it creates new circles or it revives hidden circle
    //returns number of empty circles left
    private int attachesAllPoiDataToEmptyCircles(PoiData[] poiDataNotAttached, int numPoiDataNotAttached, PoiCircle[] circlesEmpty, int emptyCirclesCount){
        int dataNotAttached = Math.min(poiDataNotAttached.length, numPoiDataNotAttached);

        //attaches all data to a poi circle that is not yet attached
        for(int i=0; i < dataNotAttached; i++){
            final PoiData data =  poiDataNotAttached[i];

            //if there are any empty circles left it just changes data in them
            if(emptyCirclesCount > 0) {
                emptyCirclesCount--;
                PoiCircle emptyCircle = circlesEmpty[emptyCirclesCount];
                circlesEmpty[emptyCirclesCount] = null;
                setInitData(emptyCircle, data);

                //if there is no more empty circles left it gets hidden circles
            }else if(hiddenCircles.size() > 0) {
                setInitData(getHiddenCircle(), data);

                //or else it creates new circle
            }else {
                create(data);
            }
        }

        return emptyCirclesCount;
    }

    //array that contains null elements reorders so that all non null elements are at the beginning
    //returns the number of non null elements
    //ecample inData: [1, 2, null, 4, null, null, 7, 8, null]
    // out:  [1, 2, 4, 7, 8, null, null, null, null] and returns 5
    private int moveDataDown(Object[] array){
        if(array.length == 0)return 0;

        int[] freePlaces = new int[array.length];
        int freePlacesIndexStart = 0;
        int freePlacesIndexEnd = 0;

        for(int i=0; i<array.length; i++){
            if(array[i] == null){
                freePlaces[freePlacesIndexEnd] = i;
                freePlacesIndexEnd ++;
            }else{
                if(freePlacesIndexStart != freePlacesIndexEnd) {
                    int freePlace = freePlaces[freePlacesIndexStart];
                    freePlacesIndexStart ++;

                    array[freePlace] = array[i];
                    array[i] = null;
                    freePlaces[freePlacesIndexEnd] = i;
                    freePlacesIndexEnd ++;
                }
            }
        }

        return array.length - (freePlacesIndexEnd - freePlacesIndexStart);
    }

    //displays message according to the numOfVisible circles
    private void displayMessage(int numCircles){
        final TextLocation textLocation = TextLocation.getInstance();
        if(numCircles == 0){
            final String message;
            if(mFilteredPoiData != null){
                message = "No POIs match your\n interests.";
            }else{
                message = "No POIs near you.\n Check GPS.";
            }
            if(textLocation == null)textLocation.setFirstText(message);
            else textLocation.setText(message);
        }else{
            if(textLocation != null)textLocation.resetLocationText();
        }
    }

    static class CircleDistancePair{
        float distance;
        PoiCircle poiCircle;

        public CircleDistancePair(float d, PoiCircle c){
            distance = d;
            poiCircle = c;
        }
    }

    //sets bigger circles over smaller
    public void setOverlapOrder(final LatLng pUserLocation){
        CircleDistancePair[] list = new CircleDistancePair[this.mActivePoiSprites.size()];

        int i=0;
        for(PoiCircle poiCircle: this.mActivePoiSprites){
            float distance = poiCircle.getLocation().getDistance(pUserLocation);
            list[i++] = new CircleDistancePair(distance, poiCircle);
        }

        Arrays.sort(list, new Comparator<CircleDistancePair>() {
            @Override
            public int compare(CircleDistancePair lhs, CircleDistancePair rhs) {
                return Float.compare(rhs.distance, lhs.distance);
            }
        });

        for(i=0; i<list.length; i++){
            list[i].poiCircle.setZIndex(ArrowNavigation.Z_INDEX_CIRCLE_START + i * ArrowNavigation.Z_INDEX_CIRCLE_STEP);
        }

        if(CompassCircle.staticThis != null)CompassCircle.staticThis.setZIndex();
        mScene.sortChildren();
    }




}
