package app.tripko.bertoncelj1.arrowNavigation;

import org.andengine.engine.camera.BoundCamera;

/**
 * Created by openstackmaser on 30.1.2016.
 */
public class SmoothBoundCamera extends BoundCamera {

    private float mZoom = 1;
    private boolean mZoomBoundsEnabled;
    /*CHAN*/private static final float MAX_ZOOM = 5;
    /*CHAN*/private static final float MIN_ZOOM = 1f;

    /*CHAN*/private static final float ZOOM_IN_SPEED = 0.8f;
    /*CHAN*/private static final float ZOOM_OUT_SPEED = 5f; //not used since max zoom is locked
    /*CHAN*/private static final int NO_ACTIVITY_TIMEOUT_SLOW = 2000;
    /*CHAN*/private static final int NO_ACTIVITY_TIMEOUT_FAST = 3000;
    /*CHAN*/private static final float NO_ACTIVITY_ZOOM_OUT_SPEED_SLOW = 0.5f;
    /*CHAN*/private static final float NO_ACTIVITY_ZOOM_OUT_SPEED_FAST = 1.5f;

    public float middleXStart = 0;
    public float middleYStart = 0;

    public float middleX = 0;
    public float middleY = 0;

    private float mWidth;
    private float mHeight;

    private boolean mZoomSmoothResize = true;
    private boolean mFingersDown;
    private boolean mBordersSmoothResize = true;

    public SmoothBoundCamera(float pX, float pY, float pWidth, float pHeight) {
        super(pX, pY, pWidth, pHeight);
    }

    public SmoothBoundCamera(float pX, float pY, float pWidth, float pHeight, float pBoundMinX, float pBoundMaxX, float pBoundMinY, float pBoundMaxY) {
        super(pX, pY, pWidth, pHeight, pBoundMinX, pBoundMaxX, pBoundMinY, pBoundMaxY);
    }

    @Override
    public void set(final float pXMin, final float pYMin, final float pXMax, final float pYMax) {
        super.set(pXMin, pYMin, pXMax, pYMax);

        mWidth = pXMax - pXMin;
        mHeight = pYMax - pYMin;
    }

    public void setZoomBoundsEnabled(boolean pEnable){
        mZoomBoundsEnabled = pEnable;
    }

    public boolean isZoomBoundsEnabled(){
        return mZoomBoundsEnabled;
    }

    public void ensureInBounds(){
        if(super.mBoundsEnabled){
            final float centerXPrev = getCenterX();
            final float centerYPrev = getCenterY();

            super.ensureInBounds();

            this.middleXStart -= centerXPrev - getCenterX();
            this.middleYStart -= centerYPrev - getCenterY();
        }
    }

    private long lastUserChange;
    public void markLastUserChange(){
        lastUserChange = System.currentTimeMillis();
    }

    public float setZoom(final float pZoom){
        return this.setZoom(pZoom, true);
    }

    private boolean boundsBigger = false;
    private float setZoom(final float pZoom, boolean limit){
        this.mZoom = pZoom;
        if(limit){
            if(mZoom > MAX_ZOOM)mZoom = MAX_ZOOM;
            else if(mZoom < MIN_ZOOM)mZoom = MIN_ZOOM - (MIN_ZOOM - mZoom)/10;
        }

        if(mZoom > 1.1f){
            if(!boundsBigger){
                super.setBounds(0 - 100, 0 - 100, mWidth + 100, mHeight + 100);
                boundsBigger = true;
            }
        }else{
            if(boundsBigger) {
                super.setBounds(0, 0, mWidth, mHeight);
                boundsBigger = false;
            }
        }
        return mZoom;
    }

    public void setFingersDown(boolean pFingersDown) {
        this.mFingersDown = pFingersDown;
    }



    public float getZoom(){
        return mZoom;
    }

    public void setZoomSmoothResize(boolean pState){
        this.mZoomSmoothResize = pState;
    }

    public boolean getZoomSmoothResize(){
        return mZoomSmoothResize;
    }

    public void setBordersSmoothResize(boolean pState){
        this.mBordersSmoothResize = pState;
    }

    public boolean getBordersSmoothResize(){
        return mBordersSmoothResize;
    }



    @Override
    public void onUpdate(final float pSecondsElapsed) {
        super.onUpdate(pSecondsElapsed);

		/* Update zoom. */
        final float currentZoom = this.getZoom();

        final long millis = System.currentTimeMillis();


        if(!mFingersDown && millis - lastUserChange > NO_ACTIVITY_TIMEOUT_SLOW){
            float absoluteZoomDifference = 0;
            if (currentZoom != MIN_ZOOM) {
                absoluteZoomDifference = MIN_ZOOM - currentZoom;
            }

            final float speed = (millis - lastUserChange < NO_ACTIVITY_TIMEOUT_FAST)?
                    NO_ACTIVITY_ZOOM_OUT_SPEED_SLOW : NO_ACTIVITY_ZOOM_OUT_SPEED_FAST;
            if(Math.abs(absoluteZoomDifference) > 0.01f) {
                final float zoomChange = Math.max(absoluteZoomDifference, -speed * pSecondsElapsed);
                this.setZoom(currentZoom + zoomChange, false);
                this.setNewPosition();
            }
        }

        if(mZoomSmoothResize) {
            float targetZoomFactor = currentZoom;
            if (currentZoom < MIN_ZOOM) targetZoomFactor = MIN_ZOOM;
            if (currentZoom > MAX_ZOOM) targetZoomFactor = MIN_ZOOM;

            if(currentZoom != targetZoomFactor) {
                final float absoluteZoomDifference = targetZoomFactor - currentZoom;
                final float zoomChange = this.limitToMaxZoomFactorChange(absoluteZoomDifference, pSecondsElapsed);

                this.setZoom(currentZoom + zoomChange, false);
                this.setNewPosition();
            }
        }
    }

    private float limitToMaxZoomFactorChange(final float pValue, final float pSecondsElapsed) {
        if (pValue > 0) {
            return Math.min(pValue, ZOOM_IN_SPEED * pSecondsElapsed);
        } else {
            return Math.max(pValue, -ZOOM_OUT_SPEED * pSecondsElapsed);
        }
    }

    public void setNewPosition(final float pMiddleX, final float pMiddleY){
        this.middleX = pMiddleX;
        this.middleY = pMiddleY;
        markLastUserChange();
        setNewPosition();
    }

    public void setNewPosition(final float pMiddleX, final float pMiddleY, final float pZoom){
        this.middleX = pMiddleX;
        this.middleY = pMiddleY;
        setNewPosition(pZoom);
    }

    public void setNewPosition(final float pZoom){
        setZoom(pZoom);
        markLastUserChange();
        setNewPosition();
    }

    private void setNewPosition() {

        float xMinNew = middleXStart - middleX / mZoom;
        //TODO finish this smooth bound boundaries stuff
        //if(xMinNew > mBoundsXMin)xMinNew = mBoundsXMin - (mBoundsXMin - xMinNew)/10;
        this.setXMin(xMinNew);

        this.setXMax(middleXStart - (middleX - mWidth) / mZoom);
        this.setYMin(middleYStart - middleY / mZoom);
        this.setYMax(middleYStart - (middleY - mHeight) / mZoom);

        this.ensureInBounds();

    }


}
