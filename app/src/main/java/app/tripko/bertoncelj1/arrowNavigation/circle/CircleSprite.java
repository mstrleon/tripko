package app.tripko.bertoncelj1.arrowNavigation.circle;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.ArrayList;
import java.util.List;

import app.tripko.bertoncelj1.util.Util;

/**
 * Created by binbong on 12/24/15.
 */

public abstract class CircleSprite extends TiledSprite {
    private float mTargetSize;
    private float mCurrentSize;
    /*CHAN*/ private static float MAX_SIZE_CHANGE = 80f; //maximum change when circle is smoothly resizing
    private Body mBody;

    /*CHAN*/public static int DEFAULT_CONTACT_DISTANCE = Util.dpToPx(10);
    public int mContactDistance = DEFAULT_CONTACT_DISTANCE;         //contact distance is calculated
    /*CHAN*/private static final float REPEL_FORCE_MUL = 0.08f;    //value multiplied when calculating repeal force
    /*CHAN*/private static final int REPEL_FORCE_ADD = 5;         //value added when calculating repel force



    protected static List<CircleSprite> collisionSprites = new ArrayList<>();

    public void addCollisionSprite(){
        collisionSprites.add(this);
    }

    public void removeCollisionSprite(){
        collisionSprites.remove(this);
    }

    //get distance between two sprites
    public float getDistance(CircleSprite s){
        return (s.mCurrentSize + this.mCurrentSize)/2;
    }


    public float getContactDistance(CircleSprite s){
        return (getDistance(s) + this.mContactDistance + s.mContactDistance);
    }


    public void setSizeDirect(final float pSize){
        mCurrentSize = pSize;
        mTargetSize = pSize;
        this.setScale(mCurrentSize/(this.getWidth()), mCurrentSize/(this.getHeight()));
    }
    public void setSize(final float pSize){
        mTargetSize = pSize;
    }

    public float getSize(){
        return mCurrentSize;
    }

    public CircleSprite(float pX, float pY, TiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
    }

    protected boolean isContactWith(CircleSprite s){
        double dist = Math.sqrt(Math.pow(s.getX() - this.getX(), 2) +
                Math.pow(s.getY() - this.getY(), 2));

        return dist < getContactDistance(s);
    }

    protected float handleCollision(CircleSprite s, float howHard){
        Vector2 f1 = new Vector2(this.getX() - s.getX(), this.getY() - s.getY());
        float len1 = f1.len();
        float mul = (getContactDistance(s) + REPEL_FORCE_ADD - len1) * REPEL_FORCE_MUL / len1;
        mul /= howHard;
        f1.mul(mul);
        Body b1 = (Body) this.getUserData();
        if(b1 != null) b1.applyForce(f1, b1.getWorldCenter());
        return mul;
    }


    public Body setPhysicBody(final PhysicsWorld pPhysicsWorld, final FixtureDef pObjectFixtureDef, final BodyDef.BodyType pBodyType, final float pBodySize){
        final float scaleX = this.getScaleX();
        final float scaleY = this.getScaleY();
        this.setScale(scaleX * pBodySize, scaleY * pBodySize);
        mBody = PhysicsFactory.createCircleBody(pPhysicsWorld, this, pBodyType, pObjectFixtureDef);
        this.setUserData(mBody);
        this.setScale(scaleX, scaleY);

        return mBody;
    }

    public abstract void attachToScene(final Scene pScene);

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        if(mTargetSize != mCurrentSize){
            final float absoluteDifference = mTargetSize - mCurrentSize;
            final float sizeChange = this.limitToMaxSizeFactorChange(absoluteDifference, pSecondsElapsed);
            mCurrentSize += sizeChange;
            this.setScale(mCurrentSize/(this.getWidth()), mCurrentSize/(this.getHeight()));
        }
    }

    private float limitToMaxSizeFactorChange(final float pValue, final float pSecondsElapsed) {
        if(pValue > 0) {
            return Math.min(pValue, this.MAX_SIZE_CHANGE * pSecondsElapsed);
        } else {
            return Math.max(pValue, -this.MAX_SIZE_CHANGE * pSecondsElapsed);
        }
    }


}
