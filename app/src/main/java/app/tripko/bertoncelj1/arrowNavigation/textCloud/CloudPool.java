package app.tripko.bertoncelj1.arrowNavigation.textCloud;


import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.adt.pool.GenericPool;

import app.tripko.bertoncelj1.arrowNavigation.textCloud.TextCloud;

/**
 * Created by openstackmaser on 25.1.2016.
 */
public class CloudPool {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    private static TextureManager mTextureManager; //TODO poglej ce se da to nardit da ne bo static
    private static BitmapTextureAtlas atlas;

    private static final GenericPool<BitmapTextureAtlas> POOL = new GenericPool<BitmapTextureAtlas>() {
        @Override
        protected BitmapTextureAtlas onAllocatePoolItem() {
            if(mTextureManager == null) {
                throw new Error("Texture manager not set. Set it with calling function setTextureManager");
            }

            atlas =  new BitmapTextureAtlas(mTextureManager, TextCloud.CANVAS_WIDTH, TextCloud.CANVAS_HEIGHT, TextureOptions.BILINEAR);
            atlas.load();
            return atlas;
        }
    };

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public static void setTextureManager(final TextureManager pTextureManager){
        mTextureManager = pTextureManager;
    }

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    public static BitmapTextureAtlas obtain() {
        return POOL.obtainPoolItem();
    }

    public static void destroy(){
        if(mTextureManager != null && atlas != null){
            mTextureManager.unloadTexture(atlas);

            mTextureManager = null;
            atlas = null;
        }
    }

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}

