package app.tripko.bertoncelj1.arrowNavigation.circle.poi.data;

import java.util.ArrayList;

import app.tripko.bertoncelj1.util.LatLng;

/**
 * Created by anze on 3/7/16.
 *
 * decides if it is necessary to download poi data
 */
public class DownloadDecider {

    //TODO SAVE previous downloaded places !

    private static final long DATA_EXPIRE_TIME_NORMAL = 1000 * 60 * 60 * 2; //2 hours
    private static final long DATA_EXPIRE_TIME_DOWNLOAD_FAIL = 1000 * 30; //30 seconds

    private ArrayList<DownloadPosition> previousDownloadPositions = new ArrayList<>();

    public DownloadDecider() {

        //set location 0, 0 as download complete so it wont try to download from this location
        previousDownloadPositions.add(new DownloadPosition(new LatLng(0, 0), 400, 1000 * 60 * 60 * 100)); //100h
    }

    public boolean isReadyToDownload(final LatLng location, int radiusNew){
        final Long curMillis = System.currentTimeMillis();

        for(int i=0; i < previousDownloadPositions.size(); i++){
            DownloadPosition prevPositions = previousDownloadPositions.get(i);

            final float distance = location.getDistance(prevPositions.location) / 1000;

            //current location access is inside previous prevPositionsDownload so it should not download new
            if(distance + radiusNew < prevPositions.radius + 2){
                //data also has to be valid
                if(prevPositions.isDataValid(curMillis)){
                    return false;
                }else{
                    previousDownloadPositions.remove(i);
                    i--; //TODO check if this is correct!
                }
            }
        }
        return true;
    }

    public void registerDownloadComplete(final LatLng location, int radius, boolean pDownloadOk){
        final long expireTime = pDownloadOk ? DATA_EXPIRE_TIME_NORMAL : DATA_EXPIRE_TIME_DOWNLOAD_FAIL;
        DownloadPosition downloadPosition = new DownloadPosition(location, radius, expireTime);
        previousDownloadPositions.add(downloadPosition);
    }

    private class DownloadPosition {
        LatLng location;
        int radius;
        long expireTime;
        long downloadTime;

        public DownloadPosition(final LatLng pLocation, final int pRadius, final long pExpireTime){
            this.location = pLocation.copy();
            this.radius = pRadius;
            this.downloadTime =  System.currentTimeMillis();;
            this.expireTime = pExpireTime;
        }

        public boolean isDataValid(long currentTime){
            return currentTime - this.downloadTime < this.expireTime;
        }
    }
}
