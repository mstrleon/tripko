package app.tripko.bertoncelj1.arrowNavigation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.acra.ACRA;
import org.andengine.ui.activity.SimpleLayoutGameActivity;

import app.tripko.bertoncelj1.MapsActivity;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.toast.Toast;
import app.tripko.bertoncelj1.customViews.BubbleText;
import app.tripko.bertoncelj1.customViews.BubbleTextArranger;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.poiListView.PoiListViewActivity;
import app.tripko.bertoncelj1.poiListView.listManager.ListManager;
import app.tripko.bertoncelj1.trips.SavedTripsListActivity;
import app.tripko.bertoncelj1.trips.TripPlannerActivity;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.NoInternetOrGpsNotifier;
import app.tripko.bertoncelj1.util.Settings;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by bertoncelj1 on 1/6/16.
 */
public abstract class ArrowNavigationActivity extends SimpleLayoutGameActivity implements PoiDataManager.OnPoiLoadedCallBack {

    private static final String DEBUG_TAG = "ArrowNavigationActivity";
    private SlidingUpPanelLayout slidingLayout;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private View mLeftDrawerView;
    private View mRightDrawerView;
    private View frame;
    private View frame2;
    private float lastTranslate = 0.0f;
    private ListView mDrawerList;

    protected UserActivity mUserActivity = new UserActivity(6 * 2).start(); //set on 2 minutes

    public boolean fingerCompass = false;

    public static Context sContext;
    public static Toast sToast;

    private PoiData mPoiDataHotel;
    private int mPoiDataHotelId = -1;
    private Button mTakeMeToHotel;

    protected NoInternetOrGpsNotifier mNotifier;

    int state = 0;
    @Override
    protected void onSetContentView() {
        super.onSetContentView();

        slidingLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        sToast = new Toast(this);

        initBubbles();

        //slidingLayout.setAnchorPoint(0.4f);
        slidingLayout.setClipPanel(true);

        mNotifier = new NoInternetOrGpsNotifier(this, (ViewGroup) findViewById(R.id.content_frame));

        findViewById(R.id.bShowMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArrowNavigationActivity.sContext, MapsActivity.class);
                ArrowNavigationActivity.sContext.startActivity(intent);
            }
        });


        findViewById(R.id.ibMenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        findViewById(R.id.ibSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);;
            }
        });

        initTakeMeToHotelButton();

        mDrawerList = (ListView) findViewById(R.id.left_list);
        final Integer names[] = {
                R.string.left_menu_list_create_trip,
                R.string.left_menu_list_saved_trips,
                R.string.left_menu_list_near_me,
                R.string.left_menu_list_recent,
                R.string.left_menu_list_favourites,
                R.string.left_menu_list_log_out
        };

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this,
                R.layout.left_menu_item_row, names);
        mDrawerList.setAdapter(adapter);
    }


    private boolean pendingTakeMeToHotel = false;
    private void initTakeMeToHotelButton(){
        mTakeMeToHotel = (Button) findViewById(R.id.takeMeToHotel);
        if(mTakeMeToHotel == null)throw new NullPointerException("Cannot find 'take me to hotel' button!");

        //get static id for hotel cubo
        mPoiDataHotelId = 55; //ApiCaller.getInstance().getUser().getHotelPoiId();

        /* Hides button if user has no hotel set */
        if(mPoiDataHotelId == -1){
            mTakeMeToHotel.setVisibility(View.GONE);
            return;
        }

        //get hotel poi from id. If it cannot get it it loads it and calls back on onPoiDataLoaded
        mPoiDataHotel = PoiDataManager.getInstance().getPoiDataFromId(mPoiDataHotelId, this);

        /* Disables button until poiData is available */
        if(mPoiDataHotel == null){
            //disables button so that not too much api request are send
            mTakeMeToHotel.setEnabled(false);
        }

        mTakeMeToHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTakeMeToHotelTrack();
            }
        });
    }

    @Override
    public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
        mTakeMeToHotel.setEnabled(true);
        mPoiDataHotel = poiData;

        if(pendingTakeMeToHotel) {
            pendingTakeMeToHotel = false;
            if (poiData == null) {
                android.widget.Toast.makeText(this, "Couldn't load hotel poi",
                        android.widget.Toast.LENGTH_SHORT).show();
            } else {
                startTakeMeToHotelTrack();
            }
        }

    }

    public void startTakeMeToHotelTrack(){
        /* if poiData for hotel is still not available it downloads it */
        if(mPoiDataHotel == null){

            pendingTakeMeToHotel = true;
            mPoiDataHotel = PoiDataManager.getInstance().getPoiDataFromId(mPoiDataHotelId,
                    ArrowNavigationActivity.this);

            /*
                mPoiDataHotel may not be null here.
                That would be a little unusual bot not impossible.
                That just means that it was added but other functions
            */
            if(mPoiDataHotel == null) {
                //disables button so that not too much api request are send
                mTakeMeToHotel.setEnabled(false);
                return;
            }
        }


        User user = ApiCaller.getInstance().getUser();
        Settings settings = Settings.getInstance();

        //Creates ne track with name 'take me to hotel'
        Track track =  new Track();
        track.name = "Take me to " + mPoiDataHotel.name;
        track.setImage(mPoiDataHotel);
        track.isVisible = false;

        user.tracks.setSetting(track.id, track, true);
        track.addSelectedPoi(0, mPoiDataHotel.id);

        settings.setTrack(track);
        user.tracks.saveSettings();
        settings.setNavigationMode(NavigationMode.TRIP);

        mDrawerLayout.closeDrawers();
    }




    private void displayNoActivityDialog(){
        /*
         If mEngine is null that means that something gone wrong and there is no
         need to display dialog this should never happen
         TODO 4 restart engine if fails to start
         */
        if(mEngine == null)return;

        mEngine.stop();
        final NoActivityDialog dialog = new NoActivityDialog(this);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mEngine.start();
                mUserActivity.resume();
            }
        });
        dialog.show();
    }

    private void initBubbles(){

        final LayoutInflater inflater = this.getLayoutInflater();

        BubbleTextArranger arranger = (BubbleTextArranger) findViewById(R.id.modes_holder);

        final Settings settings = Settings.getInstance();
        for(PoiType t: PoiType.TYPES){
            final PoiType type = t;
            final BubbleText bubbleText = (BubbleText) inflater.inflate(R.layout.bubble_text, arranger, false);
            bubbleText.setType(type);
            bubbleText.setClickable(true);
            bubbleText.setSelected(!settings.isDisabled(t));

            bubbleText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInterestsChanged(type, bubbleText.isSelected());
                }
            });

            arranger.addBubbleText(bubbleText);
        }

    }

    public void onInterestsChanged(PoiType type, boolean state){

    }

    private boolean mDrawersLocked;
    public void lockDrawers(final boolean pStatus){
        if(mDrawersLocked == pStatus)return;
        mDrawersLocked = pStatus;


        ArrowNavigationActivity.super.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mDrawersLocked) {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mRightDrawerView);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mLeftDrawerView);
                }else {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mRightDrawerView);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mLeftDrawerView);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Settings.saveSettings();
    }

    @Override
    protected void onStart() {
        super.onStart();
        sContext = this;

        if(mDrawerLayout == null || mLeftDrawerView == null || mRightDrawerView == null || mDrawerToggle == null) {
            // Configure navigation drawer
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);

            mLeftDrawerView = findViewById(R.id.left_drawer);
            mRightDrawerView = findViewById(R.id.right_drawer);
            frame = findViewById(R.id.dragView);
            frame2 = findViewById(R.id.content_frame);
            //toolbar = (Toolbar) findViewById(R.id.toolbar);
            //setSupportActionBar(toolbar);
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View drawerView) {
                    mDrawerToggle.syncState();

                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mRightDrawerView);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mLeftDrawerView);
                    mUserActivity.resume();
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    mDrawerToggle.syncState();
                    mUserActivity.stop();

                    if(R.id.right_drawer == drawerView.getId()) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mLeftDrawerView);
                    }

                    if(R.id.left_drawer == drawerView.getId()) {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mRightDrawerView);
                    }
                }

                @SuppressLint("NewApi")
                public void onDrawerSlide(View drawerView, float slideOffset)
                {
                    int width = R.id.right_drawer == drawerView.getId()? -mRightDrawerView.getWidth() : mLeftDrawerView.getWidth();

                    float moveFactor = (width * slideOffset);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    {
                        frame.setTranslationX(moveFactor);
                        frame2.setTranslationX(moveFactor);
                    }
                    else
                    {
                        TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                        anim.setDuration(0);
                        anim.setFillAfter(true);
                        frame.startAnimation(anim);
                        frame2.startAnimation(anim);

                        lastTranslate = moveFactor;
                    }
                }
            };

            mDrawerLayout.setDrawerListener(mDrawerToggle); // Set the drawer toggle as the DrawerListener
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        // If the nav drawer is open, hide action items related to the content view
        for(int i = 0; i< menu.size(); i++)
            menu.getItem(i).setVisible(!mDrawerLayout.isDrawerOpen(mLeftDrawerView));

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                mDrawerToggle.onOptionsItemSelected(item);

                if(mDrawerLayout.isDrawerOpen(mRightDrawerView))
                    mDrawerLayout.closeDrawer(mRightDrawerView);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Dbg.e(DEBUG_TAG, "Low memory!");
    }

    @Override
    public void onResume() {
        super.onResume();

        mDrawerLayout.closeDrawers();
    }

    public class DrawerItemCustomAdapter  extends ArrayAdapter<Integer> {

        Context mContext;
        int layoutResourceId;
        Integer nameIds[] = null;

        public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, Integer[] nameIds) {
            super(mContext, layoutResourceId, nameIds);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.nameIds = nameIds;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View listItem = convertView;

            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            listItem = inflater.inflate(layoutResourceId, parent, false);

            TextView textViewName = (TextView) listItem.findViewById(R.id.name);

            textViewName.setText(getString(nameIds[position]));

            textViewName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch(nameIds[position]) {
                        case R.string.left_menu_list_saved_trips:
                            startActivity(new Intent(ArrowNavigationActivity.this,
                                    SavedTripsListActivity.class));
                            break;

                        case R.string.left_menu_list_near_me:
                            startPoiListActivity(ListManager.NEAR_ME);
                            break;

                        case R.string.left_menu_list_recent:
                            startPoiListActivity(ListManager.RECENT);
                            break;

                        case R.string.left_menu_list_favourites:
                            startPoiListActivity(ListManager.FAVOURITE);
                            break;

                        // currently hidden
                        case R.string.left_menu_list_send_report:
                            ACRA.getErrorReporter().handleException(new Exception("Routine exception"));
                            break;

                        case R.string.left_menu_list_create_trip:
                            startActivity(new Intent(ArrowNavigationActivity.this,
                                    TripPlannerActivity.class));
                            break;

                        case R.string.left_menu_list_log_out:
                            ApiCaller.getInstance().logout(ArrowNavigationActivity.this);
                            break;
                    }
                }
            });


            return listItem;
        }

        private void startPoiListActivity(int listManagerId){
            Intent intent = new Intent(ArrowNavigationActivity.this, PoiListViewActivity.class);
            intent.putExtra("ListType", listManagerId);
            startActivity(intent);
        }
    }

    public class UserActivity extends TimerTask {
        private int mStartInterval;
        private int mInterval;
        boolean enabled = false;
        Timer T = new Timer();

        public UserActivity(int interval){
            mInterval = mStartInterval = interval;
        }

        public void refresh(){
            mInterval = mStartInterval + 1;
        }

        public UserActivity start(){
            enabled = true;
            T.scheduleAtFixedRate(UserActivity.this, 10000, 10000);
            return this;
        }

        @Override
        public void run() {
            if(!enabled)return;
            mInterval --;
            //Dbg.d("interval:" + mInterval);
            if(mInterval < 0){
                enabled = false;
                ArrowNavigationActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        displayNoActivityDialog();
                    }
                });
            }
        }

        public void stop() {
            enabled = false;
        }

        public void resume(){
            enabled = true;
            refresh();
        }
    }
}
