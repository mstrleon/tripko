package app.tripko.bertoncelj1.arrowNavigation.circle.poi;

import android.content.res.Resources;
import android.graphics.Color;

import java.io.Serializable;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 3/28/16.
 */
public class PoiType  implements Serializable {

    private static final String DEBUG_TAG = "PoiType";
    public static final int DEFAULT_COLOR = Color.GRAY;

    public static class Colors {
        public static int RED = DEFAULT_COLOR;
        public static int DARK_PURPLE = DEFAULT_COLOR;
        public static int LIGHT_PURPLE = DEFAULT_COLOR;
        public static int GREEN = DEFAULT_COLOR;
        public static int BROWN = DEFAULT_COLOR;
        public static int LIGHT_BLUE = DEFAULT_COLOR;
        public static int DARK_BLUE = DEFAULT_COLOR;
        public static int BACKGROUND = DEFAULT_COLOR;

        /**
         * Ignore deprecation on method getCollor, since substitute fot this method only works
         * in API > 21, and colors should not change with theme.
         * @param resources
         */
        @SuppressWarnings("deprecation")
        public static void init(Resources resources){
            try{
                RED = resources.getColor(R.color.app_red);
                LIGHT_BLUE = resources.getColor(R.color.app_light_blue);
                DARK_BLUE = resources.getColor(R.color.app_dark_blue);
                LIGHT_PURPLE = resources.getColor(R.color.app_light_purple);
                DARK_PURPLE = resources.getColor(R.color.app_dark_purple);
                GREEN = resources.getColor(R.color.app_green);
                BROWN = resources.getColor(R.color.app_brown);
                BACKGROUND = resources.getColor(R.color.def_background);

            }catch (Resources.NotFoundException e){

                Dbg.e(DEBUG_TAG, "Color not found in resources:" + e.getMessage());
            }
        }
    }

    public static final PoiType ARCHITECTURE = new PoiType("Architecture",           Colors.BROWN);
    public static final PoiType ART_MUSEUMS = new PoiType("Art & Museums",           Colors.DARK_PURPLE);
    public static final PoiType COOL_UNIQUE = new PoiType("Cool & Unique",           Colors.RED);
    public static final PoiType FOOD = new PoiType("Food",                           Colors.GREEN);
    public static final PoiType SERVICES = new PoiType("Services",                   Colors.DARK_BLUE);
    public static final PoiType PUBLIC_TRANSPORT = new PoiType("Public Transport",   Colors.LIGHT_PURPLE);
    public static final PoiType BICYCLE_TRANSPORT = new PoiType("Bicycle Transport", Colors.LIGHT_PURPLE);
    public static final PoiType SHOPPING = new PoiType("Shopping",                   Colors.LIGHT_BLUE);
    public static final PoiType INFORMATION = new PoiType("Information",             Colors.LIGHT_BLUE);
    public static final PoiType NIGHTLIFE = new PoiType("Nightlife",                 Colors.LIGHT_BLUE);
    public static final PoiType SPORT = new PoiType("Sport",                         Colors.LIGHT_BLUE);
    public static final PoiType NATURE = new PoiType("Nature",                       Colors.LIGHT_BLUE);
    public static final PoiType ENTERTAINMENT = new PoiType("Entertainment",         Colors.LIGHT_BLUE);
    public static final PoiType CAFE = new PoiType("Cafe",                           Colors.GREEN);
    public static final PoiType BEACH = new PoiType("Beach",                         Colors.LIGHT_BLUE);

    public static final PoiType[] TYPES = {
            ARCHITECTURE,
            ART_MUSEUMS,
            COOL_UNIQUE,
            FOOD,
            SERVICES,
            PUBLIC_TRANSPORT,
            BICYCLE_TRANSPORT,
            SHOPPING,
            INFORMATION,
            NIGHTLIFE,
            SPORT,
            NATURE,
            ENTERTAINMENT,
            CAFE,
            BEACH
    };

    public static final int MIN_ID = 1; //api starts with id 1
    public static final int MAX_ID = TYPES.length; //api starts with id 1

    public final String name;
    public final int  color;
    public final int hash;

    public PoiType(String pName, int pColor){
        name = pName;
        color = pColor;
        hash = name.hashCode() * pColor;
    }

    public static PoiType getPoiTypeFromId(int id) {
        if(MIN_ID > id || id > MAX_ID){
            Dbg.e(DEBUG_TAG, "invalid id:" + id);
            return null;
        }

        return TYPES[id - 1]; //api starts with id 1
    }

    public static PoiType getPoiTypeFromName(String name) {
        for(PoiType type: TYPES){
            if(type.name.equals(name))return type;
        }
        Dbg.e(DEBUG_TAG, "Unable to find poi type for: " + name);
        return null;
    }

    @Override
    public boolean equals(Object object){
        return object instanceof PoiType? this.hash == ((PoiType)object).hash : false;
    }





}
