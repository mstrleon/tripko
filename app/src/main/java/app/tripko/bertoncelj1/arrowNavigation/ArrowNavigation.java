package app.tripko.bertoncelj1.arrowNavigation;

import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.svg.opengl.texture.atlas.bitmap.SVGBitmapTextureAtlasTextureRegionFactory;
import org.andengine.input.sensor.location.LocationProviderStatus;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.util.debug.Debug;

import android.content.DialogInterface;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import java.io.IOException;
import java.util.HashMap;

import app.tripko.bertoncelj1.MyApplication;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.toast.Toast;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.arrowNavigation.circle.CompassCircle;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircleManager;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircle;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.Shader;
import app.tripko.bertoncelj1.arrowNavigation.textCloud.TextCloud;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.NoInternetOrGpsNotifier;
import app.tripko.bertoncelj1.util.OrientationSensor;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;


/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga
 *
 * @author Nicolas Gramlich
 * @since 21:18:08 - 27.06.2010
 */
public class ArrowNavigation extends ArrowNavigationActivity implements
        OrientationSensor.OnOrientationListener,
        IOnAreaTouchListener,
        IOnSceneTouchListener,
        LocationManager.ILocationListener,
        Settings.OnSettingsChangedListener {

    // ===========================================================
    // Constants
    // ===========================================================

    public static final int Z_INDEX_COMPASS = 100;
    public static final int Z_INDEX_CIRCLE_START = 10;
    public static final int Z_INDEX_CIRCLE_STEP = 2;
    public static final int Z_INDEX_CIRCLE_TOP = 500;       //START + MAX_CIRCLES * STEP < TOP !!
    public static final int Z_INDEX_TEXT_CLOUD_TOP = Z_INDEX_CIRCLE_TOP;
    public static final int Z_INDEX_TEXT_LOCATION_TOP = Z_INDEX_CIRCLE_TOP - 1;
    public static final int Z_INDEX_TEXT_LOCATION_BOTTOM = 0;
    private static final String DEBUG_TAG = "ArrowNavigation";

    public static int CAMERA_WIDTH = 540;
    public static int CAMERA_HEIGHT = 540;
    public static int CENTER_X = CAMERA_WIDTH / 2;
    public static int CENTER_Y = CAMERA_HEIGHT / 2;
    public static float MAX_DISTANCE = (float) Math.sqrt(CENTER_X * CENTER_X + CENTER_Y * CENTER_Y);
    public static float MIN_DISTANCE = CENTER_X / 8;

    private static final int TEXT_LOCATION_MARGIN_TOP = 1;
    private static final float TEXT_LOCATION_WIDTH_RATIO = 0.85f; //set as CAMERA_WIDTH * ratio

    private static final int FONT_CALLUNA_SANS_SIZE = 30;

    private static final int LINE_W = 6;        //border line WIDTH


    // ===========================================================
    // Fields
    // ===========================================================


    public static Font mFontCallunaSans;
    public static float compassAngle;
    public SmoothBoundCamera mCamera;


    private PhysicsWorld mPhysicsWorld;
    private Scene mScene;
    private ApiCaller mAPIManager = ApiCaller.getInstance();
    private PinchAndScrollDetector mPinchAndScrollDetector;
    private OrientationSensor mOrientationSensor;
    private PoiCircleManager mPoiCircleManager;
    // PoiData[] poiDataTrack;

    private CompassCircle mCompass;

    private void setWidthHeight(int w, int h) {
        CAMERA_WIDTH = w;
        CAMERA_HEIGHT = h;
        CENTER_X = CAMERA_WIDTH / 2;
        CENTER_Y = CAMERA_HEIGHT / 2;
        MAX_DISTANCE = (float) Math.sqrt(CENTER_X * CENTER_X + CENTER_Y * CENTER_Y);
        MIN_DISTANCE = 0;
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        Dbg.d(DEBUG_TAG, "Creating Engine options");
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        setWidthHeight(metrics.widthPixels, metrics.heightPixels);

        this.mCamera = new SmoothBoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
        this.mCamera.setBoundsEnabled(true);
        this.mCamera.setBounds(0,0,CAMERA_WIDTH,CAMERA_HEIGHT);

        mOrientationSensor = new OrientationSensor(this);

        return new EngineOptions(false, ScreenOrientation.PORTRAIT_FIXED, new FillResolutionPolicy(), mCamera);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        //let the location service handle the result
        LocationManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onCreateResources() {
        BuildableBitmapTextureAtlas mBuildableBitmapTextureAtlas = new BuildableBitmapTextureAtlas(this.getTextureManager(), 128, 2048, TextureOptions.NEAREST);

        CompassCircle.loadResources(this.getTextureManager(), this.getAssets());

        SVGBitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/poi_ikone/");
        PoiCircle.loadResources(this.getTextureManager(), this.getAssets());

        try {
            mBuildableBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 1, 0));
            mBuildableBitmapTextureAtlas.load();
        } catch (final ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            Debug.e(e);
        }

        loadFonts();
    }

    public void loadFonts() {
        FontFactory.setAssetBasePath("fonts/");
        ITexture fontTexture = new BitmapTextureAtlas(this.getTextureManager(), 512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        mFontCallunaSans = FontFactory.createFromAsset(getFontManager(), fontTexture, getAssets(), "CallunaSansRegular.ttf", Util.dpToPx(FONT_CALLUNA_SANS_SIZE), true, Color.WHITE);
        mFontCallunaSans.load();
    }

    private void resizeCamera(final int width, final int height){
        mCamera.set(0,0,width, height);
        mCamera.setBounds(0,0, width, height);
        mPinchAndScrollDetector.setSize(width, height);
        this.setWidthHeight(width, height);
    }

    @Override
    public Scene onCreateScene() {
        Dbg.d(DEBUG_TAG, "Create scene");
        mRenderSurfaceView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                resizeCamera(right - left, bottom - top);

                //TODO this might be the last time change listener is called
                if(mCompass != null) {
                    mCompass.configureSize(mCamera.getWidth() / 2);
                    mCompass.configurePosition(CENTER_X, CENTER_Y);
                }
                if (sToast != null) sToast.refreshHeight();
            }
        });

        this.mEngine.registerUpdateHandler(new FPSLogger());

        this.mPhysicsWorld = new PhysicsWorld(new Vector2(0, SensorManager.GRAVITY_EARTH), false);

        this.mScene = new Scene();
        this.mScene.setBackground(new Background(0, 0, 0));

        this.mPinchAndScrollDetector = new PinchAndScrollDetector(mCamera);

        createWalls();

        Shader.setShaderManager(mEngine.getShaderProgramManager());

        mPoiCircleManager = new PoiCircleManager(mPhysicsWorld, this.getVertexBufferObjectManager(), mScene);
        createInfoText();
        TextCloud.create(this.getVertexBufferObjectManager(), mFontCallunaSans, mScene);
        createCompassNeedle();


        initCircleManagerNavigationMode();
        createCircles();

        this.mScene.registerUpdateHandler(this.mPhysicsWorld);

        updateNotifierWidth();

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        this.mScene.setOnAreaTouchListener(this);
        this.mScene.setOnSceneTouchListener(this);
        this.mScene.setTouchAreaBindingOnActionDownEnabled(true);
        return this.mScene;
    }

    // no gsp or internet notifier's width is adjusted so that it does not cover menu buttons
    private void updateNotifierWidth(){
       /* View iconSettings = findViewById(R.id.ibSettings);
        View iconMenu = findViewById(R.id.ibMenu);
        int[] location = new int[2];

        iconSettings.getLocationOnScreen(location);
        int end = location[0];

        iconMenu.getLocationOnScreen(location);
        int start =  location[0] + iconMenu.getWidth();

        mNotifier.setWidth((int) ((end - start) * 0.90));
        */

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mNotifier.setWidth(Math.min(Util.dpToPx(600), metrics.widthPixels - Util.dpToPx(140)));
    }

    //TODO 3 check if this here is necessary isn't it enugh to check navigation mode onResume?
    private void initCircleManagerNavigationMode(){
        Settings setting = Settings.getInstance();
        if(setting.getNavigationMode() == NavigationMode.TRIP){
            Track track = setting.getTrack();
            if(!Settings.isTrackValid(track)){
                //Track is not valid, switching to discovery mode
                setting.setNavigationMode(NavigationMode.DISCOVERING);
            }else {
                setPoiDataTrack(setting.getTrack());
            }
        }

    }

    private void setPoiDataTrack(Track track){
        PoiData[] poiDataTrack = new PoiData[track.getSize()];
        HashMap<Integer, PoiData> poiDatas = PoiDataManager.getInstance().getPoiData();

        int i=0;
        for(Integer poiDataId: track.getSelectedPois()){
            poiDataTrack[i] = poiDatas.get(poiDataId);
            if(poiDataTrack[i] == null) {
                Settings.getInstance().setNavigationMode(NavigationMode.DISCOVERING);
                return;

                //TODO 3 is this really necessary ?
                //throw new NullPointerException("can not load track data");
            }
            i++;
        }

        mPoiCircleManager.setNonReplaceablePoiDatas(poiDataTrack);
        mPoiCircleManager.refreshCircles();
    }

    private void clearPoiDataTrack(){
        mPoiCircleManager.removeAllNonReplaceablePoiData();
        mPoiCircleManager.refreshCircles();
    }

    private void createWalls(){
        final Rectangle ground = new Rectangle(0, CAMERA_HEIGHT - LINE_W, CAMERA_WIDTH*2, LINE_W, this.getVertexBufferObjectManager());
        final Rectangle left = new Rectangle(LINE_W, 0, LINE_W, CAMERA_HEIGHT*2, this.getVertexBufferObjectManager());
        final Rectangle roof = new Rectangle(0, 20, CAMERA_WIDTH*2, LINE_W+20, this.getVertexBufferObjectManager());
        final Rectangle right = new Rectangle(CAMERA_WIDTH - LINE_W, 0, LINE_W, CAMERA_HEIGHT*2, this.getVertexBufferObjectManager());

        int[] location = new int[2];
        View v = findViewById(R.id.ibMenu);
        if (v == null) throw new NullPointerException("Cant find view ibMenu");
        v.getLocationOnScreen(location);
        final Rectangle menuWall = new Rectangle(
                (location[0] + v.getWidth())/2,
                CAMERA_HEIGHT-(location[1] + v.getHeight())/2,
                location[0] + v.getWidth()*2,
                location[1] + v.getHeight()*2,
                this.getVertexBufferObjectManager());

        v = findViewById(R.id.ibSettings);
        if (v == null) throw new NullPointerException("Cant find view ibSettings");
        v.getLocationOnScreen(location);
        final Rectangle menuSettings = new Rectangle(
                location[0] + (CAMERA_WIDTH - location[0])/2,
                CAMERA_HEIGHT - (location[1] + v.getHeight())/2,
                CAMERA_WIDTH - location[0] + v.getWidth()/2,
                (location[1] + v.getHeight()*2),
                this.getVertexBufferObjectManager());

        ground.setColor(Color.BLACK);
        left.setColor(Color.BLACK);
        roof.setColor(Color.BLACK);
        right.setColor(Color.BLACK);
        menuWall.setColor(Color.BLACK);
        menuSettings.setColor(Color.BLACK);

        final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0, 0.0f, 0.0f);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, left, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, right, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, menuWall, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(this.mPhysicsWorld, menuSettings, BodyType.StaticBody, wallFixtureDef);

        this.mScene.attachChild(ground);
        this.mScene.attachChild(roof);
        this.mScene.attachChild(left);
        this.mScene.attachChild(right);
        this.mScene.attachChild(menuWall);
        this.mScene.attachChild(menuSettings);
    }

    @Override
    public void onInterestsChanged(PoiType type, boolean state){
        Settings.getInstance().setType(type, state);

        mPoiCircleManager.refreshCircles();
    }

    private void createInfoText() {
        final float x = CAMERA_WIDTH / 2;
        final float y = CAMERA_HEIGHT / 4 + Util.dpToPx(TEXT_LOCATION_MARGIN_TOP);
        final float width = CAMERA_WIDTH  * TEXT_LOCATION_WIDTH_RATIO;
        final TextLocation textLocation = TextLocation.createInstance(x, y, width, mFontCallunaSans, this.getVertexBufferObjectManager());

        mScene.attachChild(textLocation);
    }

    @Override
    public void onDestroyResources() throws IOException {
        super.onDestroyResources();
    }

    @Override
    public void onResumeGame() {
        super.onResumeGame();
        mUserActivity.resume();
        Dbg.d(DEBUG_TAG, "Resume");
        mOrientationSensor.enable(getWindowManager().getDefaultDisplay(), this);

        //update navigation mode status every time activity resumes
        //it is called in onResumeGame instead of onResume activity because it it connected with mPoiCircleManager
        Settings settings = Settings.getInstance();
        NavigationMode navMode = Settings.getInstance().getNavigationMode();
        trySetNavigationMode(navMode, settings);
    }

    @Override
    public void onPauseGame() {
        super.onPauseGame();
        mUserActivity.stop();
        Dbg.d(DEBUG_TAG, "Pause");

        this.disableOrientationSensor();
    }

    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        mUserActivity.refresh();
        this.mPinchAndScrollDetector.onTouchEvent(pSceneTouchEvent);

        if(pSceneTouchEvent.getMotionEvent().getPointerCount() >= 2 || mCamera.getZoom() > 1.1f){
            super.lockDrawers(true);
        }else{
            super.lockDrawers(false);
        }

        if(fingerCompass) {
            float x = pSceneTouchEvent.getX();
            float y = pSceneTouchEvent.getY();

            float dx = x - CAMERA_WIDTH / 2;
            float dy = y - CAMERA_HEIGHT / 2;


            compassAngle = (float) Math.atan(dx / dy);
            if (dy < 0) compassAngle -= Math.PI;
        }


        return true;
    }


    /*
        Tries to set navigation mode, if successful returns true otherwise false.
        Setting navigation mode can fail if it can not get all poi data necessary
        or track is null or track is empty
     */
    private boolean trySetNavigationMode(final NavigationMode pNavigationMode, Settings pSettings){
        if(pNavigationMode == NavigationMode.TRIP) {
            Track track = pSettings.getTrack();
            if(track == null)return false;
            setPoiDataTrack(track);
        }else if(pNavigationMode == NavigationMode.DISCOVERING){
            clearPoiDataTrack();
        }

        setNavigationMode(pNavigationMode);
        return true;
    }

    private void setNavigationMode(final NavigationMode pNavigationMode){
        Dbg.d(DEBUG_TAG, "Navigation mode set:" + pNavigationMode.toString());

        /*
        text location set z index should always be calledv
        before highlightTarget() since sorting
        occurs in in highlightTarget() function
         */
        switch (pNavigationMode){
            case DISCOVERING:
                TextLocation.getInstance().resetLocationText();
                TextLocation.getInstance().setZIndex(Z_INDEX_TEXT_LOCATION_BOTTOM);
                mPoiCircleManager.dimAllCircles();
                mCompass.setPointingTarget(null);
                mCompass.setState(CompassCircle.STATE_DISCOVERING);
                break;

            case TRIP:
                Settings settings = Settings.getInstance();
                int poiId = settings.getCurrentTrackPositionPoiId();
                markTripPoi(poiId);
                break;
        }

        mScene.sortChildren(true);
    }



    @Override
    public boolean onAreaTouched( final TouchEvent pSceneTouchEvent, final ITouchArea pTouchArea,final float x, final float y) {
        mUserActivity.refresh();
        return false;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_arrow_navigation;
    }

    @Override
    protected int getRenderSurfaceViewID() {
        return R.id.rendersurfaceview;
    }

    @Override
    public void onLocationChanged(LatLng location, float distance) {
        if(mPoiCircleManager != null) mPoiCircleManager.refreshCircles();

        PoiDataManager.getInstance().downloadNewData(location, 20, mAPIManager, new PoiDataManager.CallBackFunction() {
            @Override
            public void onNewDataLoaded(LatLng pPosition) {
                if(mPoiCircleManager != null) mPoiCircleManager.refreshCircles(pPosition);
            }
        });

        Settings settings = Settings.getInstance();
        final PoiData poiData;
        if(settings.getNavigationMode() == NavigationMode.TRIP) {
            poiData = PoiDataManager.getInstance().getPoiDataFromId(
                        Settings.getInstance().getCurrentTrackPositionPoiId(), null);
        }else {
            poiData = null;
        }

        refreshTextLocation(poiData);
        refreshToastExternalNavigation(poiData, false);
    }

    // marks that toast was already show, so that it wont show again every time user changes location
    private boolean booleanExternalNavigationShow  = false;

    // distances at which external navigation toast shows/hides
    private static final int DISTANCE_SHOW_EXT_NAV_TOAST = 5000;
    private static final int DISTANCE_HIDE_EXT_NAV_TOAST = 4000;

    /*
    Shows or hides showExternalNavigation toast depending on distance to next poi
    Toast will be only show if it hasn't been shown before aka. booleanExternalNavigationShow = false
    */
    private void refreshToastExternalNavigation(PoiData poiData, final boolean force){
        if(sToast == null){
            Dbg.e(DEBUG_TAG, "Toast is not yet set cannot refresh toast");
            return;
        }

        if(Settings.getInstance().getNavigationMode() == NavigationMode.TRIP) {

            if(poiData == null){
                Dbg.d(DEBUG_TAG, "PoiData is null cannot refresh toast");
                return;
            }

            float distanceToPoi = poiData.latLng.getDistance(LocationManager.lastGPSPosition);

            if(distanceToPoi > DISTANCE_SHOW_EXT_NAV_TOAST){
                if (force || !booleanExternalNavigationShow) {
                    sToast.showExternalNavigation(poiData);
                    booleanExternalNavigationShow = true;
                }
            } else if (distanceToPoi < DISTANCE_HIDE_EXT_NAV_TOAST){
                // if user get's close to the poi automatically hide the toast and
                // booleanExternalNavigationShow to false so that the toast can be shown again
                // if user get distanced from poi
                if (sToast.getState() == Toast.STATE_EXTERNAL_NAVIGATION) {
                    sToast.hideToast();
                }
                booleanExternalNavigationShow = false;
            }

        } else {
            sToast.hideToast();
        }
    }

    private void refreshTextLocation(PoiData poiData){
        TextLocation textLocation = TextLocation.getInstance();
        if(textLocation == null) return;

        LatLng location = LocationManager.lastGPSPosition;
        Settings settings = Settings.getInstance();

        if(Settings.getInstance().getNavigationMode() == NavigationMode.DISCOVERING){
                // check if current TrackPosition is out of range
            textLocation.updateText(location);
            textLocation.setAutoWrap(AutoWrap.WORDS);
        } else if (poiData != null){
            float distance = poiData.latLng.getDistance(location);
            textLocation.setAutoWrap(AutoWrap.WORDS_OR_NEW_LINE);

            String durationStr = getDurationFromDistance(distance);
            // don't display ',' if durationStr is empty
            if(!durationStr.equals("")) durationStr = ", " + durationStr;

            textLocation.setText(poiData.name + "\n" +
                    Util.distanceToString(distance) + durationStr);
        }

    }

    private String getDurationFromDistance(float distance) {
        final float hoursFloat = Util.getHoursDurationFromDistance(distance);
        final int days = (int) (hoursFloat / 24);
        final int hours = (int) (hoursFloat % 24);
        final int minutes = (int) (hoursFloat * 60 % 60);
        final int seconds = (int) (hoursFloat * 360 % 60);

        final String daysStr = days == 0? "" : days + " day" + (days == 1? "" : "s");
        final String hoursStr = hours == 0? "" : hours + " h";
        final String minutesStr = minutes == 0? "" : minutes + " min";

        if (days > 0) return daysStr + " " +  hoursStr;
        if (hours > 0) return hoursStr + " " + minutesStr;
        if (minutes > 0) return minutesStr;
        else return "";
    }

    private void markTripPoi(int poiId){
        // get poi from poi data manager
        PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(poiId,
                new PoiDataManager.OnPoiLoadedCallBack() {
            @Override
            public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
                markTripPoi(poiData);
            }
        });

        // if poi not loaded than wait for onPoiDataLoaded function
        if(poiData != null){
            markTripPoi(poiData);
        }
    }

    private void markTripPoi(PoiData poiData){
        refreshTextLocation(poiData);
        refreshToastExternalNavigation(poiData, true);
        TextLocation.getInstance().setZIndex(Z_INDEX_TEXT_LOCATION_TOP);
        mScene.sortChildren();

        PoiCircle poiCircle = mPoiCircleManager.setHighlightedPoiData(poiData, true);
        mCompass.setPointingTarget(poiCircle);
        mCompass.setState(CompassCircle.STATE_NAVIGATION);
    }

    @Override
    public void onResume() {
        super.onResume();

        updateNotifierWidth();

        LocationManager.setLocationListener(this, this, this);

        Settings.getInstance().setOnSettingsChangedListener(this);
    }

    @Override
    public void onTripChanged(Settings settings) {
        setPoiDataTrack(settings.getTrack());
    }

    //saves the last state before setting next poi state
    int lastStateBeforeNextPoi = CompassCircle.STATE_NOT_SET;
    @Override
    public void nextTripPositionRequest(Settings settings, int tripPosition, boolean isActive) {
        //TODO 16 Check is this message is ever reported. #ONLY_DEBUG
        if(mCompass.getState() == CompassCircle.STATE_DISCOVERING)
            Dbg.e(DEBUG_TAG, "Illegal state in this function!");

        if(isActive) {
            if(mCompass.getState() != CompassCircle.STATE_INFO_MESSAGE) {
                //saves the last state before setting it to STATE_NEXT_POI
                lastStateBeforeNextPoi = mCompass.getState();

                if(settings.isLastTrackPosition()) {
                    mCompass.setInfoMessageFinishTrip();
                }else{
                    mCompass.setInfoMessageNextPoi();
                }

                mCompass.setState(CompassCircle.STATE_INFO_MESSAGE);

                // Shows toast for this next track poi,
                // if toast is already visible for this poiId just ignore it
                int trackPoiId = settings.getCurrentTrackPositionPoiId();
                PoiData toastPoiData = sToast.getCurrentShowingPoiData();
                if(toastPoiData == null || toastPoiData.id != trackPoiId) {
                    sToast.showToast(trackPoiId, true);
                }
            }
        }else{
            /*
            If pointing target is not set the compass circle state cannot be set to navigation.
            And if compass state was previously in navigation state than set it back to navigation state
            otherwise set it to discovering
             */
            if(mCompass.isPointingTargetSet()) {
                mCompass.setState(CompassCircle.STATE_NAVIGATION);
            }else{
                mCompass.setState(CompassCircle.STATE_DISCOVERING);
            }

        }
    }

    @Override
    public void onTripPositionChanged(Settings settings, int tripPosition) {
        if(settings.getNavigationMode() == NavigationMode.TRIP) {
            markTripPoi(settings.getCurrentTrackPositionPoiId());
        }
    }

    @Override
    public boolean onNavigationModeChanged(Settings settings, NavigationMode pNavigationMode){
        return trySetNavigationMode(pNavigationMode, settings);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationManager.removeLocationListener(this);
        Settings.getInstance().removeOnSettingsChangedListener(this);
    }

    @Override
    public void onProviderEnabled(String provider) {
        super.mNotifier.showGpsActivity(NoInternetOrGpsNotifier.AVAILABLE);
    }

    @Override
    public void onProviderDisabled(String provider) {
        super.mNotifier.showGpsActivity(NoInternetOrGpsNotifier.UNAVAILABLE);
    }

    private void createCompassNeedle() {
        final Body body;

        final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(0, 0.0f, 0.0f);

        mCompass = new CompassCircle(CENTER_X, CENTER_Y, this.getVertexBufferObjectManager(), mFontCallunaSans);
        mCompass.setOnCompassClickedLister(new CompassCircle.OnCompassClickedListener() {
            @Override
            public void onCompassClicked() {
                final Settings settings = Settings.getInstance();

                if(settings.isNextTrackPositionRequest()){
                    ArrowNavigation.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            settings.setNextTrackPosition();
                        }//public void run() {
                    });

                }else {
                    boolean distanceMode = settings.toggleDistanceMode();
                    mPoiCircleManager.setDistanceMode(distanceMode);
                }
            }
        });

        mCompass.configureSize(mCamera.getWidth() / 2);
        mCompass.configurePosition(CENTER_X, CENTER_Y);
        body = mCompass.setPhysicBody(this.mPhysicsWorld, objectFixtureDef, BodyType.StaticBody, 1.1f);
        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(mCompass, body, false, false));

        mCompass.attachToScene(mScene);
        //CircleSprite.addCollisionSprite(needle);
    }

    private void createCircles(){

        final LatLng position = LocationManager.lastGPSPosition;
        final int radius = 100;

        final PoiCircleManager creator = ArrowNavigation.this.mPoiCircleManager;
        final PoiDataManager poiDataManager = PoiDataManager.getInstance();
        if(poiDataManager.getPoiData().size() > 0){
            creator.refreshCircles(position);
        }

        poiDataManager.downloadNewData(position, radius, mAPIManager, new PoiDataManager.CallBackFunction() {
            @Override
            public void onNewDataLoaded(LatLng pPosition) {
                creator.refreshCircles(pPosition);
            }
        });

    }

    @Override
    public void onOrientationChanged(float azimuth) {
        compassAngle = -azimuth;
    }

    long lastTimeShown = 0;

    CalibrateCompassDialog visibleDialog;
    private static int LAST_SHOW_DELAY = 1000 * 60 * 5; // dialog can show only every 5 minutes

    @Override
    public void onOrientationAccuracyChanged(int accuracy, boolean isReliable) {
        long currTime = System.currentTimeMillis();

        // if dialog is visible and accuracy fixes it closes dialog, but only after 5s it was shown
        if(visibleDialog != null && isReliable){
            visibleDialog.closeWithDelay(5000 - (currTime - lastTimeShown));
        }

        if(visibleDialog == null &&
                !isReliable &&
                currTime - lastTimeShown > LAST_SHOW_DELAY){

            // this is inside if so that canShowDialog is called as least as possible
            // if onOrientationAccuracyChanged is constantly called canShowDialog is
            // called at max once every LAST_SHOW_DELAY
            lastTimeShown = currTime;
            if(!CalibrateCompassDialog.canShowDialog(this)) return;

            mEngine.stop();
            CalibrateCompassDialog dialog = new CalibrateCompassDialog(this);
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mEngine.start();
                    visibleDialog = null;
                }
            });
            visibleDialog = dialog;
            dialog.show();
        }
    }
}
