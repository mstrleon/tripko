package app.tripko.bertoncelj1.arrowNavigation;

import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.exception.OutOfCharactersException;
import org.andengine.opengl.font.IFont;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.align.HorizontalAlign;

import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.LocationName;

/**
 * Created by openstackmaser on 19.2.2016.
 */
public class TextLocation extends Text {

    private static final int MAX_TEXT_LENGTH = 100;
    private static final int MIN_DISTANCE = 300; //do not update text while close to previous update
    private static final int MIN_DISTANCE_HIDE = 1000; //hides current text if it is 1km from previous update and cannot get new text

    private static final String STR_FAILED = "";
    private static final String STR_ACQUIRING = " locating ";
    private static final String STR_UNKNOWN = "";
    private static final String[] STR_ACQUIRING_ANIMATION = {"", ".", "..", "...", "..", "."};

    private static final int ACQUIRING_ANIMATION_DELAY = 1000;
    private static final int ACQUIRING_ANIMATION_STEP = 500;

    private boolean mIsAcquiring = false;
    private long mAcquiringStart;
    private long mAcquiringLastStep;
    private int mAcquiringStepCount;
    private String mLastLocationName;
    private final LatLng mLastUpdateLocation = new LatLng(0,0);
    private CharSequence mLastNotManualLocationName = STR_UNKNOWN;
    private static String sFirstText = null;

    /* if user decides to change text manually, the location text
     * will not update with updateText, but will only save it, the update
     * occurs when user calls resetLocationText
     */
    private boolean manualSetText = false;

    private static TextLocation INSTANCE;

    public static TextLocation getInstance(){
        return INSTANCE;
    }

    public static void setFirstText(final String pFirstText){
        sFirstText = pFirstText;
    }

    public static TextLocation createInstance(final float pX, final float pY, final float AutoWrapWidth, final IFont pFont, final VertexBufferObjectManager pVertexBufferObjectManager){
        return INSTANCE = new TextLocation(pX, pY, AutoWrapWidth, pFont, pVertexBufferObjectManager);
    }

    private TextLocation(final float pX, final float pY, final float AutoWrapWidth, final IFont pFont, final VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pFont, "", MAX_TEXT_LENGTH, pVertexBufferObjectManager, DrawType.STATIC);

        this.setHorizontalAlign(HorizontalAlign.CENTER);
        this.setText(STR_UNKNOWN);
        this.setAutoWrapWidth(AutoWrapWidth);
        this.setAutoWrap(AutoWrap.NONE);


        if(sFirstText == null) {
            manualSetText = false;
        }else {
            this.setText(sFirstText);
            sFirstText = null;
        }
        if(LocationManager.lastGPSPositionAvailable) this.updateText(LocationManager.lastGPSPosition);

        INSTANCE = this;
    }


    public void updateText(final LatLng pLocation){
        if(mLastUpdateLocation.getDistance(pLocation) < MIN_DISTANCE)return;

        LocationName.LocationAddress locationAddress =
                LocationName.getAddress(pLocation, new LocationName.LocationAddressListener() {
                    @Override
                    public void gotLocationAddress(LocationName.LocationAddress pLocationAddress) {
                        updateText(pLocation, pLocationAddress, pLocationAddress.status);
                    }
                });

        updateText(pLocation, locationAddress, locationAddress.status);
    }

    public void updateText(final LatLng pLocation, final LocationName.LocationAddress pLocationAddress, LocationName.LocationAddressStatus status){
        switch(status){
            case SUCCESS:
                final String locationName = getLocationName(pLocationAddress);
                this.setTextLocation(locationName);
                mLastLocationName = locationName;
                mLastUpdateLocation.set(pLocation);
                break;

            case ACQUIRING:
                startAcquiringAnimation();
                break;

            case FAILED:
            default:
                if(mLastUpdateLocation.getDistance(pLocation) > MIN_DISTANCE_HIDE)
                    this.setTextLocation(STR_FAILED);
                else
                    this.setTextLocation(mLastLocationName);
                break;
        }
    }

    private String getLocationName(final LocationName.LocationAddress pLocationAddress){
        String locationName = STR_UNKNOWN;

        if(pLocationAddress == null) locationName = STR_UNKNOWN;
        else if(pLocationAddress.city != null) locationName = pLocationAddress.city;
        else if(pLocationAddress.state != null) locationName = pLocationAddress.state;
        else if(pLocationAddress.country != null) locationName = pLocationAddress.country;

        return locationName;
    }


    public void startAcquiringAnimation(){
        if(mIsAcquiring || manualSetText)return;
        mIsAcquiring = true;
        mAcquiringStart = System.currentTimeMillis();
        mAcquiringStepCount = 0;
    }

    public void stopAcquiringAnimation(){
        mIsAcquiring = false;
    }

    private void setTextLocation(final CharSequence pText){
        mLastNotManualLocationName = pText;
        if(!manualSetText){
            this.setText(pText);
            manualSetText = false;
        }
    }

    public void resetLocationText(){
        this.setText(mLastNotManualLocationName);
        manualSetText = false;
    }

    @Override
    public void setText(final CharSequence pText) throws OutOfCharactersException {
        stopAcquiringAnimation();
        super.setText(pText);
        manualSetText = true;
    }

    @Override
    protected void onManagedUpdate(final float pSecondsElapsed) {
        if(mIsAcquiring){
            final long millis = System.currentTimeMillis();
            if(millis - mAcquiringStart  > ACQUIRING_ANIMATION_DELAY &&
                    millis - mAcquiringLastStep  > ACQUIRING_ANIMATION_STEP) {
                mAcquiringLastStep = millis;

                final int step = mAcquiringStepCount++ % STR_ACQUIRING_ANIMATION.length;
                super.setText(STR_ACQUIRING_ANIMATION[step] + STR_ACQUIRING + STR_ACQUIRING_ANIMATION[step]);
            }
        }
    }

    public static void destroy() {
        if(INSTANCE != null) {
            INSTANCE.getParent().detachChild(INSTANCE);

            INSTANCE.dispose();
            INSTANCE = null;
        }
    }

}
