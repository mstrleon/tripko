package app.tripko.bertoncelj1;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

public class EmptyActivity extends AppCompatActivity {

    Bitmap bitmap[] = new Bitmap[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bitmap_images);

        bitmap[0] =  BitmapFactory.decodeResource(getResources(), R.drawable.missing_image);
        bitmap[1] = getBitmapFromAsset(this, "tank.bmp");
        bitmap[2] = getBitmapFromAsset(this, "tank.bmp");
        bitmap[3] = getBitmapFromAsset(this, "tank.bmp");
        bitmap[4] = getBitmapFromAsset(this, "tank.bmp");
        bitmap[5] = getBitmapFromAsset(this, "motor.bmp");

        ((ImageView) findViewById(R.id.slika1)).setImageBitmap(bitmap[0]);
        ((ImageView) findViewById(R.id.slika2)).setImageBitmap(bitmap[1]);
        ((ImageView) findViewById(R.id.slika3)).setImageBitmap(bitmap[2]);
        ((ImageView) findViewById(R.id.slika4)).setImageBitmap(bitmap[5]);
    }

    public static Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EmptyActivity.this, BitmapTest.class);
        startActivity(intent);
        finish();
    }

}
