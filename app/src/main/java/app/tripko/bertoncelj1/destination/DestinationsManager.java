package app.tripko.bertoncelj1.destination;

import java.util.ArrayList;

import app.tripko.bertoncelj1.DataStorage;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.HttpCalls;

/**
 * Created by anze on 5/27/16.
 */
public class DestinationsManager {
    private final static String DEBUG_TAG = "DestinationsManager";
    private static final String SAVE_PATH = "destinations";;

    public interface  CallBackFunction {
        void onNewDataLoaded();
    }

    //top destination in destination tree hierarchy
    private Destination mTopDestination;

    public Destination getTopDestination(){
        return mTopDestination;
    }

    public void downloadNewData(ApiCaller pAPIManager, final CallBackFunction pCallBack){
        if(pAPIManager == null){
            Dbg.e(DEBUG_TAG, "downloadNewData(): Cannot download new data pAPIManger is null");
            return;
        }

        pAPIManager.getNavigationTypesTree(new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {

                if (report.isSuccessful() && dataType == String.class) {

                    Destination topDestination = Destination.extractDestinations((String) data);
                    if(topDestination == null){
                        Dbg.e(DEBUG_TAG, "No destinations extracted from data:" + data);
                        return;
                    }

                    if(!topDestination.equals(mTopDestination)) {
                        Dbg.i(DEBUG_TAG, "Destination data has changed");
                        DestinationsManager.this.mTopDestination = topDestination;

                        if (pCallBack != null) pCallBack.onNewDataLoaded();
                        saveDestinations();
                    }

                } else {
                    Dbg.e(DEBUG_TAG, "Error getting destinations " + report.getMessage());
                }
            }
        });

    }

    public void saveDestinations(){
        Dbg.i(DEBUG_TAG, "Saving...");

        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) {
            Dbg.e(DEBUG_TAG, "Storage not available. No data saved.");
            return;
        }

        final String filePath = SAVE_PATH;

        if(!dataStorage.save(filePath, mTopDestination)){
            Dbg.e(DEBUG_TAG, "Error while saving desinations");
        }
    }

    public boolean loadDestinations(){
        final DataStorage dataStorage = DataStorage.getInstance();
        if(dataStorage == null) {
            Dbg.e(DEBUG_TAG, "Storage not available. No destinations loaded.");
            return false;
        }

        Object object = dataStorage.loadFileObject(SAVE_PATH);

        if (object instanceof Destination) {
            mTopDestination = (Destination) object;
            return true;
        } else {
            return false;
        }
    }

}
