package app.tripko.bertoncelj1.destination;

import android.os.Build;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import app.tripko.bertoncelj1.customViews.clearableEditText.ClearableEditText;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.Util;

import static app.tripko.bertoncelj1.util.Util.extractDoubleValue;
import static app.tripko.bertoncelj1.util.Util.extractIntegerValue;
import static app.tripko.bertoncelj1.util.Util.extractStringValue;

/**
 * Created by anze on 5/27/16.
 */
public class Destination implements Serializable {
    private final static String DEBUG_TAG = "Destination";

    final public int id;
    final public String name;
    final public String description;
    final public LatLng location;
    final public int priorityIndex;
    final public Integer[] poiIDs;
    final public Destination parent;
    final public ArrayList<Destination> children;
    private boolean isSelected = false;

    private String imageThumbUrl;
    private String imageThumbSmallUrl;
    private String imageMediumUrl;
    private String imageUrl;

    private int hash;

    public int getHash(){
        return hash;
    }

    private void generateHash(){
        int childHash = 1;
        if(children != null){
            for(Destination child: children){
                childHash = 31 * childHash + child.hash;
            }
        }

        int locationHash = location != null? hashCode(location.getLatitude(), location.getLongitude()) : 0;
        hash = hashCode(id,
                name,
                description,
                locationHash,
                priorityIndex,
                Arrays.hashCode(poiIDs),
                imageMediumUrl, imageThumbSmallUrl, imageThumbUrl, imageUrl,
                childHash);
    }

    //just helper method
    public int hashCode(Object... values) {
        return Arrays.hashCode(values);
    }


    /*
    Creates unknown destination.
    This cen be used for classes that need destination even if it is unknown.
     */
    public static Destination createUnknown(){
        return new Destination("Unknown",
                new Integer[]{1, 2, 3, 4, 5},
                LocationManager.lastGPSPosition);
    }

    public Destination(String name, Integer[] poiIDs, LatLng location) {
        this.name = name;
        this.poiIDs = poiIDs;
        this.location = location;

        this.id = -1;
        this.description = null;
        this.priorityIndex = -1;
        this.parent = null;
        this.children = null;

        this.isSelected = false;

        generateHash();
    }


    /*
    Gives rating of how much the phrase matches the destination. The score can be from 0 to inf.
    0 meaning that it does't match
     */
    public int getMatchingPhraseScore(String searchPhrase){
        //splits search pharase to words
        String[] searchWords = searchPhrase.split("\\W+");

        //splits name and description to words
        String[] nameSplited = name.toLowerCase().split("\\W+");
        String[] descriptionSplited = description.toLowerCase().split("\\W+");

        int score = 0;
        for(String searchWord: searchWords){
            score += Util.getScore(searchWord, nameSplited) * 10;
            score += Util.getScore(searchWord, descriptionSplited);
        }

        return score;
    }

    public String getBestQualityImageUrl(){
        if(imageUrl != null)return imageUrl;
        if(imageMediumUrl != null)return imageMediumUrl;
        if(imageThumbUrl != null) return imageThumbUrl;
        else return imageThumbSmallUrl;
    }

    public String getImageUrl() {
        final float density = Util.getDensity();

        //hdpi
        if (density < 4) {
            return imageMediumUrl;
        } else {
            return imageUrl;
        }
    }


    //extracted destinations from JsonData
    public static Destination extractDestinations(String JsonData){
        HashMap<String,Object> map = null;
        try {
            map = new ObjectMapper().readValue(JsonData,HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            //returns top destination
            return new Destination(map, null);

        } catch (Exception e){
            e.printStackTrace();
            Dbg.e(DEBUG_TAG, "Error occurred while extracting data: " + e.getMessage());
        }

        return null;
    }

    private Destination(HashMap<String,Object> map, Destination parent){
        this.parent = parent;

        id = extractIntegerValue("id", map);
        name = extractStringValue("name", map);
        description = extractStringValue("headline", map);
        priorityIndex = extractIntegerValue("priority_index", map);


        imageThumbUrl = extractStringValue("image_thumb", map);
        imageThumbSmallUrl = extractStringValue("image_thumb_small", map);
        imageMediumUrl = extractStringValue("image_medium", map);
        imageUrl = extractStringValue("image", map);

        //extract poiIDs
        ArrayList<HashMap> poiIds = ((ArrayList<HashMap>) map.get("pois"));
        if(poiIds == null) poiIds = new ArrayList<>(0);

        poiIDs = new Integer[poiIds.size()];
        for(int i=0; i<poiIds.size(); i++){
            poiIDs[i] = extractIntegerValue("poi_id", poiIds.get(i));
        }

        double lat = extractDoubleValue("latitude", map);
        double lon = extractDoubleValue("longitude", map);
        if(lat != -1 && lon != -1) {
            location = new LatLng(lat, lon);
        }else{
            location = null;
        }

        final ArrayList<LinkedHashMap> data = (ArrayList<LinkedHashMap>) map.get("children");
        if(data != null) {
            children = new ArrayList<>(data.size());

            for (LinkedHashMap poiData : data) {

                if(poiData != null){
                    final Destination newDestination = new Destination(poiData, this);

                    // empty destinations should not be shown. They are irrelevant
                    if(!newDestination.isEmpty()) {
                        children.add(newDestination);
                    }
                }
            }
        }else {
            children = null;
        }

        generateHash();

        isSelected = false;
    }

    /*
    Checks if destination has any children or POIs
     */
    public boolean isEmpty(){
        return ((poiIDs == null || poiIDs.length == 0) &&
                    (children == null || children.size() == 0));
    }

    public boolean isSelected(){
        return isSelected;
    }

    public void setSelected(boolean selected){
        isSelected = selected;
    }

    @Override
    public String toString(){
        return name + " " + children;
    }

    public String getString(int shiftRight){
        String s = "";

        for(int i=0; i<shiftRight; i++)s += ".  .  .";

        s += id + ", name:" + name;

        if(children != null){
            s +=  "children:" + children.size() + "\n";
            for(Destination child: children) {
                s += child.getString(shiftRight + 1) + "\n";
            }
        }

        return s;
    }

    public boolean equals(Destination destination){
        if(this.equals((Object) destination))return true;
        if(destination != null) {
            return destination != null && this.hash == destination.hash;
        }

        return false;
    }
}
