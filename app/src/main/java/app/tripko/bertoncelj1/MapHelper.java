package app.tripko.bertoncelj1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircleManager;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiCircleMapTexture;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 3/8/16.
 */
@SuppressWarnings("WeakerAccess")
public class MapHelper {

    private static final String DEBUG_TAG = "MapCreator";

    /*
    How many pois should be visible when zooming in user location
     */
    private static final int SHOW_NUMBER_CLOSEST_POI = 15;

    /*
    How close to the user we zoom when showing only x pois around him
     */
    private static final int MAX_ZOOM_DISTANCE = 2000;

    /*
    How close to the user we zoom when showing only x pois around him
     */
    private static final int MIN_ZOOM_DISTANCE = 100;

    /*
    When zooming into user location how many padding should there be
    around Pois that are surrounding user
     */
    private static final int USER_ZOOM_IN_PADDING = 150;

    /*
    How much padding should there be when zooming into the track
     */
    private static final int TRACK_ZOOM_IN_PADDING = 100;

    private Icon[] mMarkerIcons;
    private Icon mBlankIcon;
    private boolean mInitialized = false;
    private DirectionPlaner mDirectionPlanner = new DirectionPlaner();
    private MapboxMap mMap;

    public HashSet<Integer> alwaysVisiblePoiIds = new HashSet<>();

    public MapboxMap getMap() {
        return mMap;
    }

    private class DataMarker {
        PoiData poiData;
        Marker marker;

        public Marker getMarker() {
            return marker;
        }

        private void setMarkerVisibility(Settings settings, double zoomLevel) {
            // determine if poi is visible depending on zoom level
            boolean isPoiVisible = isPoiVisible(settings, poiData, zoomLevel);
            boolean isHighlighted = isPoiHighlighted(settings, poiData);

            if (!isPoiVisible) {
                setMarkerIcon(mBlankIcon);
            } else {
                setHighlighted(isHighlighted);
            }
        }

        private void setHighlighted(boolean isHighlighted) {
            int bitmapIconIndex = getTextureIndex(poiData, isHighlighted);
            Icon newIcon = mMarkerIcons[bitmapIconIndex];

            // change marker icon depending on if it is highlighted or not
            setMarkerIcon(newIcon);
        }

        public void setMarkerIcon(Icon newIcon) {
            // check if icon has changed
            if (marker.getIcon() != newIcon) {
                marker.setIcon(newIcon);
            }
        }


        private boolean isPoiVisible(Settings settings, PoiData poiData, double zoomLevel) {
            return isPoiInsideZoomLevel(poiData, zoomLevel) &&
                    !settings.isDisabled(poiData.poiType) ||
                    alwaysVisiblePoiIds.contains(poiData.id);
        }
    }

    private boolean isPoiInsideZoomLevel(PoiData poiData, double zoomLevel) {
        return getPriorityIndexFromZoom(zoomLevel) < poiData.priority_index;
    }

    private double getPriorityIndexFromZoom(double x) {
        return 9.5194650186606058e+000 * Math.pow(x, 0)
                + -1.3928124661410164e+000 * Math.pow(x, 1)
                + 1.7042155909568968e-001 * Math.pow(x, 2)
                + -6.7701202124147569e-003 * Math.pow(x, 3);
    }


    private HashMap<Marker, PoiData> mHashMap = new HashMap<>();

    private ArrayList<DataMarker> mMarkers = new ArrayList<>();

    public static MapboxMapOptions getDfaultMapOptions() {
        // Build mapboxMap
        MapboxMapOptions options = new MapboxMapOptions();
        options.styleUrl(Style.MAPBOX_STREETS);

        return options;
    }


    public void initMap(@NonNull MapboxMap map, Activity activity) {
        if (map == null) throw new NullPointerException("Cannot init MapHelper if map is null!");

        mMap = map;

        // TODO set info window
        map.setInfoWindowAdapter(new CustomInfoWindowAdapter(activity));

        map.setOnCameraChangeListener(new MapboxMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                refreshMarkers(cameraPosition.zoom);
            }
        });

        map.getMyLocationViewSettings().setPadding(0, 500, 0, 0);
        map.getMyLocationViewSettings().setForegroundTintColor(Color.parseColor("#56B881"));
        map.getMyLocationViewSettings().setAccuracyTintColor(Color.parseColor("#FBB03B"));

        if (LocationManager.hasLocationPermission(activity)) {
            map.setMyLocationEnabled(true);
        }

        final HashMap<Integer, PoiData> poiData = PoiDataManager.getInstance().getPoiData();
        PoiData[] poiDatas = poiData.values().toArray(new PoiData[0]);

        zoomIntoUserLocation(poiDatas, LocationManager.lastGPSPosition.getMapBoxLatLng());
    }

    /**
     * Load markers and display them ot the map
     *
     * @param context is needed to load marker icons
     */
    public void loadMarkers(@NonNull Context context) {
        if (mMap == null) throw new NullPointerException("Cannot load markers if map is null!");
        if (context == null)
            throw new NullPointerException("Cannot load markers if context is null!");

        final HashMap<Integer, PoiData> poiData = PoiDataManager.getInstance().getPoiData();
        PoiData[] poiDatas = poiData.values().toArray(new PoiData[0]);

        new LoadMarkers(mMap, context).execute(poiDatas);
    }

    /**
     * zooms into user location with some pois visible around him
     *
     * @param allPoiData   all the post that are inclided into calculation
     * @param userLocation current user location
     */
    public void zoomIntoUserLocation(PoiData[] allPoiData, LatLng userLocation) {
        // calculates witch points have to be visible on the map
        List<LatLng> zoomedInPoints = getZoomedPoisLocations(allPoiData, userLocation);
        setZoom(zoomedInPoints, USER_ZOOM_IN_PADDING);

    }

    private class LoadMarkers extends AsyncTask<PoiData[], Object, String> {

        MapboxMap mMap;
        Context mContext;
        Settings settings = Settings.getInstance();

        private LoadMarkers(MapboxMap map, Context context) {
            mContext = context;
            mMap = map;
        }

        // creates marker icons from bitmap icons
        // returns false if it was unable to load icons
        private boolean loadMarkerIcons() {
            // get bitmap icons
            final Bitmap[] bitmapIcons = PoiCircleMapTexture.getBitmapIcons(mContext.getResources());
            if (bitmapIcons == null) {
                Dbg.e(DEBUG_TAG, "Unable to load icons");
                return false;
            }

            final IconFactory iconFactory = IconFactory.getInstance(mContext);

            mMarkerIcons = new Icon[bitmapIcons.length];
            mBlankIcon = iconFactory.fromAsset("map_empty_marker.png");
            for (int i = 0; i < mMarkerIcons.length; i++) {
                final Bitmap bitmapIconHighlighted = bitmapIcons[i];
                mMarkerIcons[i] = iconFactory.fromBitmap(bitmapIconHighlighted);
            }

            return true;
        }

        @Override
        protected String doInBackground(PoiData[]... params) {
            PoiData[] poiDatas = params[0];

            if (!loadMarkerIcons()) {
                // unable to load marker icons.
                return null;
            }

            try {
                for (int i = poiDatas.length - 1; i >= 0; i--) {
                    PoiData data = poiDatas[i];

                    LatLng position = data.latLng.getMapBoxLatLng();
                    String name = data.name;

                    //creates icon for highlighted marker
                    int bitmapIconIndex = getTextureIndex(data, true);
                    Icon iconHighlighted = mMarkerIcons[bitmapIconIndex];


                    MarkerOptions marker = new MarkerOptions()
                            .position(position)
                            .icon(iconHighlighted)
                            .title(name)
                            .snippet(data.type_name);

                    publishProgress(marker, data);
                }
            } catch (Exception ex) {
                //To catch Could not allocate java pixel ref. exception
                ex.printStackTrace();
                Dbg.e(ex.getMessage());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Object... markerOptions) {
            super.onProgressUpdate(markerOptions);
            //draw marker on the map
            Marker marker = mMap.addMarker((MarkerOptions) markerOptions[0]);
            PoiData poiData = (PoiData) markerOptions[1];

            DataMarker dataMarker = new DataMarker();
            dataMarker.marker = marker;
            dataMarker.poiData = poiData;
            mMarkers.add(dataMarker);

            mHashMap.put(marker, poiData);

            dataMarker.setMarkerVisibility(settings, mMap.getCameraPosition().zoom);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    @SuppressWarnings("SimplifiableIfStatement")
    private boolean isPoiHighlighted(Settings settings, PoiData data) {
        if (settings.getNavigationMode() == NavigationMode.DISCOVERING) {
            // if in discovery mode than poi is highlighted
            return true;
        } else {
            return settings.isPoiHighlighted(data.id);
        }
    }

    // returns index of marker icon depending on what type the data is and if is highlighted or not
    private int getTextureIndex(PoiData data, boolean isHighlighted) {
        if (isHighlighted) {
            return data.textureIndex;
        } else {
            return mMarkerIcons.length / 2 + data.textureIndex;
        }
    }

    public void refreshMarkers() {
        refreshMarkers(mMap.getCameraPosition().zoom);
    }

    private void refreshMarkers(double zoomLevel) {
        final Settings settings = Settings.getInstance();

        for (DataMarker dataMarker : mMarkers) {
            dataMarker.setMarkerVisibility(settings, zoomLevel);
        }
    }

    /*
    Elevates poi so it is visible over other pois. It is no more covered.
    This is achieved by calling showInfoWindow.
     */
    public void elevatePoi(int poiId) {
        //finds marker with right poiId
        for (DataMarker dataMarker : mMarkers) {
            if (dataMarker.poiData.id == poiId) {
                Marker marker = dataMarker.getMarker();

                mMap.selectMarker(marker);
                return;
            }
        }
    }


    class CustomInfoWindowAdapter implements MapboxMap.InfoWindowAdapter {
        private final Context mContext;
        private final View mInfoWindowView;

        CustomInfoWindowAdapter(@NonNull Context context) {
            if (context == null)
                throw new NullPointerException("Cannot create custom info window without context");

            mContext = context;

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mInfoWindowView = inflater.inflate(R.layout.marker_info_window, null);
        }

        @Override
        public View getInfoWindow(@NonNull Marker marker) {
            render(marker, mInfoWindowView);
            return mInfoWindowView;
        }

        private void render(Marker marker, View view) {
            ((TextView) view.findViewById(R.id.title)).setText(marker.getTitle());
            TextView snippet = ((TextView) view.findViewById(R.id.snippet));
            snippet.setText(marker.getSnippet());

            PoiData data = mHashMap.get(marker);
            if (data != null) {
                snippet.setTextColor(data.poiType.color);
            }
        }
    }

    public void setOnDrawTripStateChangedListener(DirectionPlaner.DirectionPlanerStateListener pListener) {
        mDirectionPlanner.setOnStateChangedListener(pListener);
    }


    /*
    Returns if draw trip was successful
     */
    public boolean drawTrip() {

        Settings settings = Settings.getInstance();
        final int poiId = settings.getCurrentTrackPositionPoiId();
        final PoiData poiData = PoiDataManager.getInstance().getPoiData().get(poiId);

        if (poiData == null) {
            Dbg.d("Cannot draw trip. poiData == null");
            return false;
        }

        ArrayList<LatLng> points = new ArrayList<>();
        points.add(LocationManager.lastGPSPosition.getMapBoxLatLng());
        points.add(poiData.latLng.getMapBoxLatLng());

        mDirectionPlanner.setPathColor(poiData.poiType.color);
        mDirectionPlanner.clearPath();
        mDirectionPlanner.drawDirections(mMap, points);

        return true;
    }

    public void updatePathToLocation(LatLng location) {
        mDirectionPlanner.updatePathPointsToNewLocation(location);
    }

    public void clearTrip() {
        mDirectionPlanner.clearPath();
    }

    /**
     * Get location of pois that user sees around him
     *
     * @param poiDatas .
     * @param center   Where user is standing. This poi is also added to the list
     * @return SHOW_NUMBER_CLOSEST_POI that are around user within limits of
     * MAX_ZOOM_DISTANCE and MIN_ZOOM_DISTANCE
     */
    private List<LatLng> getZoomedPoisLocations(PoiData[] poiDatas, LatLng center) {
        List<LatLng> poisLatLng = new ArrayList<>();

        // adds center since it has to be visible
        poisLatLng.add(center);

        if (poiDatas != null && poiDatas.length != 0) {
            // sorts pois by distance to the center
            PoiCircleManager.DistanceDataPair[] distanceDataPairs =
                    PoiCircleManager.sortDataByDistance(poiDatas, Util.getMyLatLang(center));

            // get the most distant poi that will still be shown
            int mostDistantPoi = Math.min(SHOW_NUMBER_CLOSEST_POI, distanceDataPairs.length - 1);

            float maxDistance = distanceDataPairs[mostDistantPoi].distance;

            // limit the maxDistance so it is in rage
            maxDistance = Util.limit(MIN_ZOOM_DISTANCE, maxDistance, MAX_ZOOM_DISTANCE);

            // add all the pois that are < maxDistance to the list
            for (int i = 0; distanceDataPairs[i].distance <= maxDistance; i++) {
                poisLatLng.add(distanceDataPairs[i].poiData.latLng.getMapBoxLatLng());
            }

        }

        return poisLatLng;
    }

    /**
     * Zooms so that entire track is visible
     */
    public void zoomOnTrack() {
        ArrayList<LatLng> pathPoints = mDirectionPlanner.getPathPoints();
        setZoom(pathPoints, TRACK_ZOOM_IN_PADDING);
    }

    /**
     * Set zoom so that all the points are visible
     *
     * @param points  Points that have to be visible
     * @param padding Padding added to the points
     */
    public void setZoom(List<LatLng> points, int padding) {
        final CameraUpdate camera;

        if (points.size() <= 0) {
            throw new IllegalArgumentException("Cannot zoom into points. No points provided!");
        } else if (points.size() == 1) {
            // only one point provided. just move camera there
            CameraPosition position = new CameraPosition.Builder()
                    .target(points.get(0)) // Sets the new camera position
                    .zoom(14) // Sets the zoom
                    .build(); // Creates a CameraPosition from the builder

            camera = CameraUpdateFactory.newCameraPosition(position);

        } else {
            // add all pathPoints to the latLngBounds builder
            LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();

            for (LatLng point : points) {
                latLngBounds.include(point);
            }

            // create camera update from these points
            camera = CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), padding);
        }

        mMap.moveCamera(camera);
    }
}
