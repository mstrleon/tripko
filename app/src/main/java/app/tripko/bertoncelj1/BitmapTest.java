package app.tripko.bertoncelj1;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.poiCard.RatePoiDialog;

//TODO delete me! Just for testing!
public class BitmapTest extends AppCompatActivity {


    private static final String DEF_FONT = "fonts/CallunaSansRegular_liningNumbers.ttf";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bitmap_test);


        /*
        PoiData poiData = PoiData.getDummyPoiData(12);
        poiData.name = "Restautrant Garsa";
        poiData.setLocationName("Ljubljana");
        final RatePoiDialog dialog = new RatePoiDialog(this, poiData);


        findViewById(R.id.openDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });


        final ChooseTripDialog dialog = new ChooseTripDialog(this, true,
                ChooseTripDialog.ADD_THIS_POI_TO_TRIP);

        findViewById(R.id.openDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

        SwitchTripFragment fragment = (SwitchTripFragment) getSupportFragmentManager().findFragmentById(R.id.switchTripFragment);



        findViewById(R.id.openPoiCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BitmapTest.this, ChooseInterestsActivity.class);
                startActivity(intent);
            }
        });
        /*
        /*
        LinearLayout fontList = (LinearLayout) findViewById(R.id.fontList);

        String[] features = {"liga", "dlig", "onum", "lnum", "tnum",
                "zero", "frac", "sups", "subs", "smcp", "c2sc", "case", "hlig",
                "calt", "swsh", "hist", "ss**", "kern", "locl",
                "rlig", "medi", "init", "isol", "fina", "mark", "mkmk"};

        Typeface typeface = Typeface.createFromAsset(getAssets(), DEF_FONT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (int i = 0; i < features.length; i++) {
                final TextView myTextView = new TextView(this);

                myTextView.setText("1234567890 ačćdeĐ: " + features[i]);
                myTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                myTextView.setTypeface(typeface);
                myTextView.setFontFeatureSettings(features[i]);

                fontList.addView(myTextView);
            }
        }
        */
/*
        User user = User.SUPER_USER;
        final UserPoiSetting favourites = user.favourites;

        favourites.loadOnline(new UserPoiSetting.OnSettingsLoadedCallBack() {
            @Override
            public void settingsLoaded() {
                PoiData poiData = null;
                for(PoiData data: favourites.get()){
                    System.out.println("id:"  + data.id + "set:" + data.favourite);
                    poiData = data;
                }

                if(poiData != null){
                    favourites.set(poiData, false);
                    favourites.set(poiData, false);
                    favourites.set(poiData, false);

                    favourites.set(poiData, true);
                    favourites.set(poiData, false);
                    favourites.set(poiData, true);
                    favourites.set(poiData, false);
                    favourites.set(poiData, false);

                    favourites.saveOnline();
                }
            }
        });

    */    /*
        HashMap<Integer, PoiData> data = PoiDataManager.getInstance().getPoiData();
        data.get(0);



        LatLng location = new LatLng(46.056947, 14.505751);
        PoiDataManager.getInstance().downloadNewData(location, 100, ApiManager.getInstance(), new PoiDataManager.CallBackFunction() {
            @Override
            public void onNewDataLoaded(LatLng pPosition) {
                HashMap<Integer, PoiData> data = PoiDataManager.getInstance().getPoiData();
                data.get(0);
            }
        });

        */


        /*show all bitmaps*/

/*
        LinearLayout imagesHolder = (LinearLayout) findViewById(R.id.imagesHolder);

        Bitmap[] bitmaps = PoiCircleMapTexture.getBitmapIcons(this.getResources());

        for(int i=0; i<5; i++){
            for(Bitmap bitmap: bitmaps){
                ImageView imageView = new ImageView(this);
                imageView.setImageBitmap(bitmap);

                imagesHolder.addView(imageView);
            }
        }
*/

    }
}
