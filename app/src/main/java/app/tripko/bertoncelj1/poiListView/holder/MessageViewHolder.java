package app.tripko.bertoncelj1.poiListView.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.poiListView.PoiListViewAdapter;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 7/1/16.
 */
public class MessageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public Button button;
    public TextView message;
    public ProgressBar loading;
    private MyOnMessageClickListener mListener;

    public MessageViewHolder(View view, MyOnMessageClickListener listener) {
        super(view);
        button = (Button) view.findViewById(R.id.button);
        message = (TextView) view.findViewById(R.id.message);
        loading = (ProgressBar) view.findViewById(R.id.progressBar);

        mListener = listener;
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mListener != null) mListener.onMessageButtonClicked(v);
    }

    public interface MyOnMessageClickListener {
        void onMessageButtonClicked(View viewClicked);
    }
}
