package app.tripko.bertoncelj1.poiListView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import app.tripko.bertoncelj1.ChooseTripDialog;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.customViews.BubbleText;
import app.tripko.bertoncelj1.customViews.BubbleTextArranger;
import app.tripko.bertoncelj1.customViews.SwitchChooseButton;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Tracks;
import app.tripko.bertoncelj1.poiListView.listManager.ListManager;
import app.tripko.bertoncelj1.trips.SavedTripsListActivity;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;
import jp.wasabeef.recyclerview.animators.BaseItemAnimator;
import jp.wasabeef.recyclerview.animators.FadeInDownAnimator;
import jp.wasabeef.recyclerview.animators.LandingAnimator;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;

public class PoiListViewActivity extends AppCompatActivity implements
        PoiListViewAdapter.OnClickListener,
        SwitchChooseButton.SwitchChooseButtonChanged {

    protected RecyclerView mRecyclerView;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private View mRightDrawerView;
    private View mContentFrame;
    private float lastTranslate = 0.0f;

    private PoiListViewAdapter mListAdapter;
    private TextView mNoDataMessage;
    private TextView mToolbarTitle;
    protected SwitchChooseButton mOrderBySwitch; // TODO make private
    private SearchView mSearchBox;

    protected ListManager mListManager;

    protected Track mSelectedTrack = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // disable adjusting so that the keyboard does not push up the buttons
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        setContentView(getLayoutId());
        initRightDrawerLayout();

        Bundle bundle = getIntent().getExtras();
        int listManagerId = ListManager.NEAR_ME;
        if(bundle != null) {
            listManagerId = bundle.getInt("ListType", ListManager.NEAR_ME);
        }
        mListManager = getListManagerFromId(listManagerId);

        mNoDataMessage = (TextView) findViewById(R.id.no_data_message_text);
        mNoDataMessage.setVisibility(View.INVISIBLE);

        mSearchBox = (SearchView) findViewById(R.id.searchView);
        if(mSearchBox != null) {
            mSearchBox.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    refreshPoiList();
                }
            });

            mSearchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    refreshPoiList();
                    return false;
                }
            });
        }

        mOrderBySwitch = (SwitchChooseButton) findViewById(R.id.scbOrderBy);
        // some layout do not have this switch check if this layout has it
        if(mOrderBySwitch != null) mOrderBySwitch.setOnSwitchChooseButtonChangedListener(this);

        /*
        Sets this track as default track, usually the class extending this class will change it
        if no track is set in settings all pois will be unchecked
        */
        mSelectedTrack = Settings.getInstance().getTrack();

        initList();
        initToolBar();
        initBubbles();

        refreshPoiList();
    }

    protected int getLayoutId(){
        return R.layout.activity_poi_list_view;
    }

    protected ListManager getListManagerFromId(int listManagerId){
        return ListManager.getListManager(listManagerId);
    }

    /*
    Every class extending this class can set its own right drawer layout, for flexibility reasons
     */
    protected int getRightDrawerLayoutId(){
        return R.layout.drawer_right_chose_interests;
    }

    /*
    Sets right drawer dynamically so that every class that extends this class can set its own right drawer
     */
    private void initRightDrawerLayout(){
        int layoutId = getRightDrawerLayoutId();
        mRightDrawerView = findViewById(R.id.right_drawer);

        LayoutInflater.from(mRightDrawerView.getContext())
                .inflate(layoutId, (ViewGroup) mRightDrawerView, true);
    }

    public ListManager getListManger(){
        return mListManager;
    }

    public PoiListViewAdapter getPoiListViewAdapter(){
        return mListAdapter;
    }

    private void initToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // display back button only if activity is not the first one
        // so that user cannot exit application when pressing it
        if(!isTaskRoot()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.icon_arrow_left_36db);
        }

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolbarTitle.setText(mListManager.getTitle());
    }

    public void setToolbarTitle(String title){
        mToolbarTitle.setText(title);
    }

    private void initList(){
        mRecyclerView = (RecyclerView) findViewById(R.id.list);

        PoiListViewAdapter.Options options = new PoiListViewAdapter.Options();
        options.closeButtonEnabled = false;
        options.showDistance = false;

        mListAdapter = new PoiListViewAdapter(this,  mRecyclerView, options);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        BaseItemAnimator animator = new FadeInDownAnimator(){
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                return false;
            }
        };
        animator.setChangeDuration(200);
        animator.setAddDuration(200);
        animator.setRemoveDuration(200);
        mRecyclerView.setItemAnimator(animator);
        mRecyclerView.setAdapter(mListAdapter);

        /*
        if list is extendable add listener so when the list reaches bottom it loads new data
         */
        if(mListManager.isListExtendable()) {
            mRecyclerView.addOnScrollListener(createEndlessScrollListener(mLayoutManager));
        }else{
            mListAdapter.setMessageState(PoiListViewAdapter.MessageState.HIDDEN);
        }

        /*
        Sets all pois that needed to be selected(checked) if track is set
         */
        if(mSelectedTrack != null && mSelectedTrack.getSelectedPois() != null){
            mListAdapter.setSelectedPois(mSelectedTrack.getSelectedPoisArray());
        }

        /*
        handles all clicks on "add" button. Sub classes can override to handle action themselves
         */
        mListAdapter.setOnButtonClickListener(this);
    }

    /*
    Adds Buttons on the bottom of the list
     */
    protected void addButtons(int layoutId, int topLayoutId){
        RelativeLayout contentWrapper = (RelativeLayout) findViewById(R.id.content_frame_wrapper);
        LayoutInflater.from(contentWrapper.getContext())
                .inflate(layoutId, contentWrapper, true);

        // sets contentFrame above newly added segment
        View contentFrame = findViewById(R.id.content_frame);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) contentFrame.getLayoutParams();
        params.addRule(RelativeLayout.ABOVE, topLayoutId);
    }

    /*
    Reads sort order from order by switch choose button
     */
    protected int getSortOrderFromChooseButton() {
        // if there is not switch set sort_by_distance order
        if(mOrderBySwitch == null) return ListManager.SORT_BY_DISTANCE;

        return mOrderBySwitch.getState() == SwitchChooseButton.Option.LEFT?
                ListManager.SORT_BY_DISTANCE : ListManager.SORT_BY_NAME;
    }

    @Override
    public boolean onSwitchChooseButtonChanged(SwitchChooseButton.Option pOptionChosen) {
        refreshPoiList();
        return true;
    }

    private void initBubbles(){

        final LayoutInflater inflater = this.getLayoutInflater();

        BubbleTextArranger arranger = (BubbleTextArranger) findViewById(R.id.modes_holder);

        //if there is no arranger to draw bubble text into it returns,
        // for example if the drawer didn't hat interests enabled
        if(arranger == null)return;

        Settings settings = Settings.getInstance();
        for(PoiType t: PoiType.TYPES){
            final PoiType type = t;
            final BubbleText bubbleText = (BubbleText) inflater.inflate(R.layout.bubble_text, null, false);
            bubbleText.setType(type);
            bubbleText.setClickable(true);
            bubbleText.setSelected(!settings.isDisabled(t));

            bubbleText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInterestsChanged(type, bubbleText.isSelected());
                }
            });

            arranger.addBubbleText(bubbleText);
        }

    }

    public void onInterestsChanged(PoiType type, boolean state){
        if(state) Settings.getInstance().getDisabledTypes().remove(type);
        else Settings.getInstance().getDisabledTypes().add(type);

        refreshPoiList();
    }

    /*
    refreshes poiData in poiList this function should be called every time sort order is changed or query is changed
    */
    public void refreshPoiList(){
        String searchQuery = mSearchBox != null ? mSearchBox.getQuery().toString() : "";
        refreshPoiList(searchQuery);
    }

    public void refreshPoiList(String searchQuery){
        int sortOrder;

        if(mSearchBox != null && mSearchBox.hasFocus() || searchQuery.length() > 0){
            sortOrder = ListManager.SORT_BY_QUERY;
            mListManager.setSearchQuery(searchQuery);
        } else{
            sortOrder = getSortOrderFromChooseButton();
        }

        PoiData[] poiData = mListManager.getPoiDataSorted(sortOrder, false);

        mListAdapter.updateList(poiData);

        showNoDataMessage(mListAdapter.getItemCount(), sortOrder, searchQuery);
    }

    private void showNoDataMessage(int size, int sortOrder, String searchQuery){
        if(size == 0){
            if(sortOrder == ListManager.SORT_BY_QUERY) {
                // display different message if no pois match search query
                mNoDataMessage.setText(searchQuery.equals("") ?  "Type something to search" :
                        "Nothing matches '" + searchQuery + "'");
            } else {
                mNoDataMessage.setText(mListManager.getNoDataMessage());
            }
            mNoDataMessage.setVisibility(View.VISIBLE);
        }else{
            mNoDataMessage.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_item:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PoiListViewAdapter.POI_CARD_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK && data != null) {

                int poiId = data.getIntExtra("poiId", -1);
                Boolean isAdded = data.getBooleanExtra("poiIsAdded", false);
                if(poiId != -1) {
                    if(isAdded)mListAdapter.setSelectedPoi(poiId);
                    else mListAdapter.removeSelectedPoi(poiId);


                    mListAdapter.updatePoi(poiId);
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();


        if(mDrawerLayout == null || mRightDrawerView == null || mDrawerToggle == null) {
            // Configure navigation drawer
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);

            mContentFrame = findViewById(R.id.content_frame_wrapper);
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View drawerView) {
                    supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    mDrawerToggle.syncState();

                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mRightDrawerView);
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    mDrawerToggle.syncState();
                }

                @SuppressLint("NewApi")
                public void onDrawerSlide(View drawerView, float slideOffset)
                {
                    int width = -mRightDrawerView.getWidth();

                    float moveFactor = (width * slideOffset);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    {
                        mContentFrame.setTranslationX(moveFactor);
                    }
                    else
                    {
                        TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                        anim.setDuration(0);
                        anim.setFillAfter(true);
                        mContentFrame.startAnimation(anim);

                        lastTranslate = moveFactor;
                    }
                }
            };

            mDrawerToggle.setDrawerIndicatorEnabled(false);

            mDrawerLayout.addDrawerListener(mDrawerToggle); // Set the drawer toggle as the DrawerListener
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Dbg.e("IMPORTANT", "Low memory!");
    }

    @Override
    public void onStop(){
        super.onStop();

        ImageLoader.getInstance().clearMemoryCache();
    }

    /*
    This function is just for convenience for less cluttered code
     */
    private EndlessRecyclerViewScroll createEndlessScrollListener(LinearLayoutManager mLayoutManager){
        return new EndlessRecyclerViewScroll(mLayoutManager) {
            /*
            If list reaches bottom it can request to load more data. This function is called only when listener is set ... obvious
             */
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // fetch data here
                //customLoadMoreDataFromApi(page);

                if (!mListManager.isLoading() &&
                        mListAdapter.getMessageState() != PoiListViewAdapter.MessageState.NO_MORE_DATA) {

                    boolean isOk = mListManager.loadMoreData(new ListManager.OnLoadedListener() {
                        @Override
                        public void onMoreDataLoaded(PoiData[] pPoiData) {
                            //if (mListAdapter.extendData(pPoiData))
                            //    mListAdapter.setMessageState(PoiListViewAdapter.MessageState.HIDDEN);
                            //else
                                mListAdapter.setMessageState(PoiListViewAdapter.MessageState.NO_MORE_DATA);

                        }
                    });

                    if (isOk)
                        mListAdapter.setMessageState(PoiListViewAdapter.MessageState.LOADING);
                    else
                        mListAdapter.setMessageState(PoiListViewAdapter.MessageState.NO_MORE_DATA);
                }
            }
        };
    }

    @Override
    public void onAddButtonClicked(View viewClicked, PoiData poiData, int position, final boolean isSelected) {
        //if no track is selected no poi should be marked as selected!
        Util.check(!(mSelectedTrack == null && !isSelected));

        /*
        No track is currently selected, ask user to select a track with dialog
         */
        if(mSelectedTrack == null){
            askUserWhichTrip(poiData.id, false);
        }else {
            addPoiToTrack(mSelectedTrack, poiData.id, isSelected);

            // dismisses currently visible snackBar
            if(mCurrentSnackBar != null) mCurrentSnackBar.dismiss();

            // notify user (show snackBar) only if poi is added
            if(isSelected) {
                notifyUserPoiAdded(mSelectedTrack, poiData.id);
            }
        }
    }

    public void addPoiToTrack(Track track, int poiId){
        addPoiToTrack(track, poiId, true);
    }

    public void removePoiFromTrack(Track track, int poiId) {
        addPoiToTrack(track, poiId, false);
    }

    private void addPoiToTrack(Track track, int poiId, boolean add){
        if(add){
            track.addSelectedPoi(poiId);
        }else{
            track.removeSelectedPoi(poiId);
        }

        Settings settings = Settings.getInstance();

        // check if currently changed track is used for navigation
        if(settings.getNavigationMode() == NavigationMode.TRIP &&
                settings.getTrack().equals(track)){

            if(Settings.isTrackValid(track)) {
                // if changed track is still valid than update track
                settings.setTrack(track);
            } else {
                // current track is no longer valid. turn off Trip mode and set Discovering mode
                settings.setNavigationMode(NavigationMode.DISCOVERING);
            }
        }

        //saves current track in settings
        Settings.saveSettings();

        //saves track in tracks
        Tracks tracks = ApiCaller.getInstance().getUser().tracks;
        tracks.setSetting(track.id, track, true);
        tracks.saveSettings();
    }

    /*
    Shows dialog "ChooseTripDialog" so user can decide to which trip to add poi
     */
    private void askUserWhichTrip(final int poiId, boolean enableOnlyThisPoiCheckbox){
        ChooseTripDialog dialog = new ChooseTripDialog(this, enableOnlyThisPoiCheckbox,
                ChooseTripDialog.ADD_THIS_POI_TO_TRIP);
        dialog.setOnTripChosenListener(new ChooseTripDialog.setOnTripChosenListener() {

            /*
            When user chooses track, that track is than set as selected track
             */
            @Override
            public void onTripChosen(final int tripSelectedId, boolean onlyThisPoi) {
                Settings settings = Settings.getInstance();
                Track track = ApiCaller.getInstance().getUser().tracks.getSetting(tripSelectedId);

                //update selected track if not only this poi should be adde to the track
                if(!onlyThisPoi) {
                    mSelectedTrack = track;
                    settings.setTrack(track);
                    mListAdapter.setSelectedPois(track.getSelectedPoisArray());
                }

                //adds poi to track
                addPoiToTrack(track, poiId, true);
                notifyUserPoiAdded(track, poiId);
            }

            @Override
            public void createNewTrip() {
                //creates new track with this poi in there
                final Track track = new Track();
                track.name = "Unnamed";
                track.isVisible = true;

                PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(poiId, new PoiDataManager.OnPoiLoadedCallBack() {
                    @Override
                    public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
                        track.name = poiData.name + "trip";
                        track.setImage(poiData);

                        //saves track
                        User user = ApiCaller.getInstance().getUser();
                        user.tracks.setSetting(track.id, track, true);
                        user.tracks.saveSettings();
                    }
                });

                if(poiData != null){
                    track.name = poiData.name + "trip";
                    track.setImage(poiData);
                }

                track.addSelectedPoi(poiId);

                //saves track
                User user = ApiCaller.getInstance().getUser();
                user.tracks.setSetting(track.id, track, true);
                user.tracks.saveSettings();

                Intent myIntent = new Intent(PoiListViewActivity.this, SavedTripsListActivity.class);
                myIntent.putExtra("animateFirstElement", true);
                PoiListViewActivity.this.startActivity(myIntent);
                finish();
            }
        });
        dialog.show();
    }

    /*
    currently visible snackBar
    used so snack bar can be closed when poi is removed or another poi is added
     */
    private Snackbar mCurrentSnackBar = null;

    private void notifyUserPoiAdded(Track track, final int poiId){

        mCurrentSnackBar = Snackbar
                .make(mRecyclerView, "Added to trip " + track.name, Snackbar.LENGTH_LONG)
                .setAction("CHANGE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        askUserWhichTrip(poiId, true);
                    }
                });

        mCurrentSnackBar.show();
    }
}
