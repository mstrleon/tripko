package app.tripko.bertoncelj1.poiListView.listManager;

import java.util.ArrayList;
import java.util.HashMap;

import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by anze on 16.5.2016.
 *
 * Only shows pois that are in some destination
 */
public class DestinationList extends ListManager {

    private Destination mDestination;

    public DestinationList(Destination destination){
        setDestination(destination);
        setIsListExtendable(false);
    }

    public void setDestination(Destination destination){
        mDestination = destination;
    }

    @Override
    public String getTitle() {
        return mDestination.name;
    }

    private ArrayList<PoiData> mFilteredPoiData;

    @Override
    public PoiData[] getPoiData() {

        //clears all previously set data
        if(mFilteredPoiData == null)mFilteredPoiData = new ArrayList<>();
        else if(mFilteredPoiData.size() > 0)mFilteredPoiData.clear();

        final Settings settings = Settings.getInstance();

        HashMap<Integer, PoiData> poiDatas = PoiDataManager.getInstance().getPoiData();

        getPoiData(settings, poiDatas, mDestination);

        return mFilteredPoiData.toArray(new PoiData[0]);
    }

    /*
    Recursively reads poiData from destination and all its children
     */
    private void getPoiData(Settings settings, HashMap<Integer, PoiData> poiDatas, Destination destination){
        if(destination == null) return;

        for (Integer poiId : destination.poiIDs) {
            PoiData poiData = poiDatas.get(poiId);

            if (poiData != null) { //TODO 6 This should be resolved by download required poi data
                boolean isDisabled = settings.isDisabled(poiData.poiType);
                if(!isDisabled)mFilteredPoiData.add(poiData);
            }
        }

        if(destination.children != null) {
            for (Destination subDestination : destination.children) {
                getPoiData(settings, poiDatas, subDestination);
            }
        }
    }



    @Override
    public String getNoDataMessage() {
        if(mDestination.poiIDs == null || mDestination.poiIDs.length == 0){
            return "No POIs in " + mDestination.name + ".\n Try different destination.";
        }else{
            return "No POIs match your interest.";
        }
    }
}
