package app.tripko.bertoncelj1.poiListView.listManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.RecentSetting;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 3/31/16.
 */
class RecentList extends ListManager {
    private int mRecentPois = 0;

    class RecentData{
        PoiData poiData;
        RecentSetting.Setting recent;

        public RecentData(PoiData p, RecentSetting.Setting s){
            recent = s;
            poiData = p;
        }
    }

    @Override
    public String getTitle() {
        return "Recent";
    }

    public RecentList(){
        setIsListExtendable(false);
    }

    ArrayList<RecentData> mPoiDataList = new ArrayList<>();
    int mIdsHash = 0;
    private int getIdHash(Integer ids[]){
        int hash = 7;

        for(int id: ids){
            hash = hash*139 + id;
        }

        return hash;
    }

    private RecentData getRecentData(RecentSetting setting, PoiData poiData){
        RecentSetting.Setting recentSetting = setting.getSetting(poiData.id);
        if (setting != null){
            return new RecentData(poiData, recentSetting);
        }else{
            throw new Error("Setting for id:" + poiData.id + ", does not exist");
        }
    }

    private ArrayList<PoiData> mFilteredPoiData;
    @Override
    public PoiData[] getPoiData() {
        final User user = ApiCaller.getInstance().getUser();
        user.recent.loadSettings(null);
        Integer[] ids = user.recent.getSettingsIds().toArray(new Integer[0]);
        final int idHash = getIdHash(ids);
        if(mIdsHash == idHash || ids.length == 0){
            if(super.poiData == null)super.poiData = new PoiData[0];
            return super.poiData;
        }
        mIdsHash = idHash;

        ArrayList<PoiData> datas = PoiDataManager.getInstance().getPoiDataFromIid(ids, new PoiDataManager.OnPoiLoadedCallBack() {
            @Override
            public void onPoiDataLoaded(PoiData pPoiData, boolean isLast) {
                if(pPoiData != null) {
                    mPoiDataList.add(getRecentData(user.recent, pPoiData));
                }
                if(isLast);
            }
        });

        if(datas == null)return new PoiData[0];

        for(PoiData data: datas){
            mPoiDataList.add(new RecentData(data, user.recent.getSetting(data.id)));
        }

        //sorted from more recent to least recent
        Collections.sort(mPoiDataList, new Comparator<RecentData>() {
            @Override
            public int compare(RecentData lhs, RecentData rhs) {
                return Util.longCompare(rhs.recent.timeAdded, lhs.recent.timeAdded);
            }
        });

        PoiData[] data = new PoiData[mPoiDataList.size()];
        for(int i=0; i<data.length; i++){
            data[i] = mPoiDataList.get(i).poiData;
        }
        super.poiData = data;
        mRecentPois = poiData.length;

        return poiData;
    }



    @Override
    public String getNoDataMessage() {
        if(mRecentPois == 0){
            return "No recent POIs found.";
        }
        return super.getNoDataMessage();
    }
}
