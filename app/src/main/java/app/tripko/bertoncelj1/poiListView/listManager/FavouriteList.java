package app.tripko.bertoncelj1.poiListView.listManager;

import java.util.ArrayList;
import java.util.HashMap;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by anze on 3/31/16.
 */

public class FavouriteList extends ListManager {
    private int mFavouritePois = 0;

    @Override
    public String getTitle() {
        return "Favourites";
    }

    public FavouriteList(){
        setIsListExtendable(false);
    }

    private ArrayList<PoiData> mFilteredPoiData;
    @Override
    public PoiData[] getPoiData() {

        //clears all previously set data
        if(mFilteredPoiData == null)mFilteredPoiData = new ArrayList<>();
        else if(mFilteredPoiData.size() > 0)mFilteredPoiData.clear();

        final Settings settings = Settings.getInstance();
        final User user = ApiCaller.getInstance().getUser();
        final HashMap<Integer, PoiData> poiDatas = PoiDataManager.getInstance().getPoiData();

        for(int poiId: user.favourites.getSettingsIds()){
            PoiData poiData = poiDatas.get(poiId);
            // TODO for now it is just ignoring. it should download it
            if(poiData == null) continue;
            boolean isDisabled = settings.isDisabled(poiData.poiType);

            if(!isDisabled) mFilteredPoiData.add(poiData);
        }

        return mFilteredPoiData.toArray(new PoiData[0]);
    }

    @Override
    public String getNoDataMessage() {
        if(mFavouritePois == 0){
            return "You have no favourite POIs.";
        }
        return super.getNoDataMessage();
    }
}
