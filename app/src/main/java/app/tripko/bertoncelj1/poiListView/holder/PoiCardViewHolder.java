package app.tripko.bertoncelj1.poiListView.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 7/1/16.
 *
 * Holds all views for PoiCard row
 */
public class PoiCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView title, subTitle, distance, seen;
    public ImageView image, favourite;
    public ImageButton addButton;
    public IMyViewHolderClicks mListener;


    public PoiCardViewHolder(View view, IMyViewHolderClicks listener) {
        super(view);
        if(listener == null)throw new NullPointerException("listener == null!");
        mListener = listener;

        title = (TextView) view.findViewById(R.id.title);
        subTitle = (TextView) view.findViewById(R.id.subTitle);
        distance = (TextView) view.findViewById(R.id.distance);
        seen = (TextView) view.findViewById(R.id.seen_tag);
        image = (ImageView) view.findViewById(R.id.image);
        favourite = (ImageView) view.findViewById(R.id.favourite);
        addButton = (ImageButton) view.findViewById(R.id.add_button);
        addButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        if(position >= 0) {
            mListener.onAddButtonClicked(addButton, position);
        } else {
            Dbg.e("Unable to get adapter position!");
        }
    }

    public interface IMyViewHolderClicks {
        void onAddButtonClicked(ImageButton addButton, int position);
    }


}
