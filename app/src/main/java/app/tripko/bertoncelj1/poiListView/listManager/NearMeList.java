package app.tripko.bertoncelj1.poiListView.listManager;

import java.util.ArrayList;
import java.util.HashMap;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by anze on 3/31/16.
 */
class NearMeList extends ListManager {

    @Override
    public String getTitle() {
        return "Near me";
    }

    private ArrayList<PoiData> mFilteredPoiData;

    @Override
    public PoiData[] getPoiData() {

        HashMap<Integer, PoiData> poiDataMap = PoiDataManager.getInstance().getPoiData();
        final PoiData[] arrayPoiData = poiDataMap.values().toArray(new PoiData[poiDataMap.size()]);

        //clears all previously set data
        if(mFilteredPoiData == null)mFilteredPoiData = new ArrayList<>();
        else if(mFilteredPoiData.size() > 0)mFilteredPoiData.clear();

        final Settings settings = Settings.getInstance();
        for(PoiData data: arrayPoiData){
            boolean isDisabled = settings.isDisabled(data.poiType);

            if(!isDisabled) mFilteredPoiData.add(data);
        }

        return mFilteredPoiData.toArray(new PoiData[0]);
    }

}
