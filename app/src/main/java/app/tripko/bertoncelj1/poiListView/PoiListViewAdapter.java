package app.tripko.bertoncelj1.poiListView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.poiListView.holder.DestinationViewHolder;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.poiCard.PoiCardActivity;
import app.tripko.bertoncelj1.poiListView.holder.MessageViewHolder;
import app.tripko.bertoncelj1.poiListView.holder.PoiCardViewHolder;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 3/30/16.
 */
public class PoiListViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        View.OnClickListener,
        PoiCardViewHolder.IMyViewHolderClicks,
        MessageViewHolder.MyOnMessageClickListener, DestinationViewHolder.MyOnDestinationClickListener {

    public static final int POI_CARD_REQUEST_CODE = 20;
    private static final String DEBUG_TAG = "PoiListViewAdapter";

    private static final int VIEW_TYPE_POI_CARD = 1;
    private static final int VIEW_TYPE_MESSAGE = 2;
    private static final int VIEW_TYPE_DESTINATION = 3;

    // holds the data for each row in the list
    public class ListData{
        Object data;
        int type;

        public PoiData getPoiData(){
            return (PoiData) data;
        }

        public Destination getDestinatnion(){
            return (Destination) data;
        }

        public boolean isPoiDataType(){
            return type == VIEW_TYPE_POI_CARD;
        }

        public boolean isDestinationDataType(){
            return type == VIEW_TYPE_DESTINATION;
        }

        @Override
        public String toString() {
            if(isPoiDataType()) return "Poi: " + getPoiData().name;
            if(isDestinationDataType()) return "Dest: " + getDestinatnion().name;

            return "Unknown type " + type;
        }
    }

    private List<ListData> mListData;
    private RecyclerView mRecyclerView;
    private Activity mActivity;
    Options mOptions;


    public interface OnClickListener{
        void onAddButtonClicked(View viewClicked, PoiData poiData, int position, boolean isSelected);
    }

    public interface OnMessageClickListener{
        void onMessageButtonClicked(View viewClicked);
    }

    public interface OnDestinationClickListener{
        void onDestinationClicked(Destination destination, int position);
    }

    public enum MessageState {
        LOADING,
        NO_MORE_DATA,
        BUTTON, // shows button so that user can expand currently visible data
        HIDDEN
    }

    private MessageState mMessageState = MessageState.HIDDEN;
    private String mMessageString = "Loading";
    // so that yo know when to call function update and when new item inserted / removed
    private boolean mMessageButtonVisible = false;


    private OnClickListener mOnButtonClickedListener;
    private OnMessageClickListener mOnMessageClickedListener;
    private OnDestinationClickListener mOnDestinationClickListener;


    private HashSet<Integer> poiIdsSelected = new HashSet<>();

    public static class Options {
        public boolean closeButtonEnabled;
        public boolean showDistance;
    }


    public PoiListViewAdapter(Activity pActivity, RecyclerView pRecyclerView, Options options) {
        this.mListData = new ArrayList<>();
        this.mRecyclerView = pRecyclerView;
        this.mActivity = pActivity;
        this.mOptions = options;
    }

    public void setOnButtonClickListener(OnClickListener listener){
        mOnButtonClickedListener = listener;
    }

    public void setOnMessageClickListener(OnMessageClickListener listener){
        mOnMessageClickedListener = listener;
    }

    public void setOnDestinationClickListener(OnDestinationClickListener listener){
        mOnDestinationClickListener = listener;
    }

    public MessageState getMessageState() {
        return mMessageState;
    }

    private ListData getPoiListData(PoiData poiData){
        ListData listData = new ListData();
        listData.data = poiData;
        listData.type = VIEW_TYPE_POI_CARD;
        return listData;
    }

    private ListData getDestinationListData(Destination destination){
        ListData listData = new ListData();
        listData.data = destination;
        listData.type = VIEW_TYPE_DESTINATION;
        return listData;
    }

    public void addPoi(PoiData poiData){
        this.addPoi(mListData.size(), poiData);
    }

    public void addPois(List<PoiData> poiDatas){
        for(PoiData poiData: poiDatas){
            addPoi(poiData);
        }
    }

    public void addPois(PoiData[] poiDatas){
        for(PoiData poiData: poiDatas){
            addPoi(poiData);
        }
    }


    public void addPoi(int position, PoiData poiData){
        mListData.add(position, getPoiListData(poiData));
        notifyItemInserted(position);
    }

    public void addPois(int position, List<PoiData> poiDatas){
        for(PoiData poiData: poiDatas){
            addPoi(position, poiData);
        }
    }

    public void updatePoi(int poiId){
        for(int i=0; i<mListData.size(); i++){
            ListData d = mListData.get(i);

            if(d.isPoiDataType() && ((PoiData) d.data).id == poiId){
                this.notifyItemChanged(i);
                return;
            }
        }
    }

    public void addDestination(Destination destination){
        this.addDestination(mListData.size(), destination);
    }

    public void addDestination(int position, Destination destination){
        mListData.add(position, getDestinationListData(destination));
        notifyItemInserted(position);
    }

    public List<ListData> getListData(){
        return mListData;
    }

    /*
    removes data from the list
    used in TripShowActivity where pois can be added / removed from the trip
     */
    public ListData removeListData(int position){
        int itemId = (int) this.getItemId(position);

        ListData removedData =  mListData.remove(itemId);
        notifyItemRemoved(position);

        return removedData;
    }

    public void removeAll(){
        int size = mListData.size();
        mListData.clear();
        notifyItemRangeRemoved(0, size);
    }

    /**
     * Instead of clearing the list and setting new poiData, this function searches for
     * different pois and changes only them. This is so that the animation is not shown.
     *
     * The end result is the same as removing all data and adding new. But in that case the
     * animation will be shown.
     * @param newPoiDatas new data for the list
     */
    public void updateList(PoiData[] newPoiDatas){

        int i;
        for(i=0; i<newPoiDatas.length; i++){
            PoiData newPoiData = newPoiDatas[i];

            // find if this poi is already in list
            int oldPosition = findPoiDataRow(newPoiData.id);

            ListData listData;
            if(oldPosition != -1){
                // if already in list remove it and move it to the new position
                listData = mListData.remove(oldPosition);
                mListData.add(i, listData);
                notifyItemMoved(oldPosition, i);
            } else {
                // this poi is not in list so create new
                listData = getPoiListData(newPoiData);
                mListData.add(i, listData);
                notifyItemInserted(i);
            }
        }

        // remove all the remaining data
        // the old pois were pushed down when new pois were inserted
        int start = i;
        int range = mListData.size() - start;
        for(i=mListData.size() - 1; i >= start; i--){
            mListData.remove(i);
        }

        notifyItemRangeRemoved(start, range);
    }

    /**
     * Finds row that has poi with specific poiId
     * @param poiId .
     * @return position in list. If not found return -1;
     */
    public int findPoiDataRow(int poiId){
        for(int i=0; i<mListData.size(); i++){
            ListData listData = mListData.get(0);

            if(listData.isPoiDataType() && listData.getPoiData().id == poiId) {
                return i;
            }
        }

        return -1;
    }

    public ListData getListData(int positon){
        return mListData.get(positon);
    }

    public PoiData removePoi(int posiiton){
        ListData listData = removeListData(posiiton);

        if(!listData.isPoiDataType()) {
            throw new IllegalArgumentException("Trying to remove list data that is not poi!");
        }

        return listData.getPoiData();
    }


    // on poi card clicked
    @Override
    public void onClick(View v) {
        //int itemPosition = mRecyclerView.indexOfChild(v);
        int itemPosition = mRecyclerView.getChildAdapterPosition(v);

        if(mListData.get(itemPosition).isPoiDataType()) {

            int itemId = (int) this.getItemId(itemPosition);
            PoiData poiData = mListData.get(itemId).getPoiData();

            Intent intent = new Intent(mActivity, PoiCardActivity.class);
            Bundle extras = new Bundle();

            extras.putSerializable("poiData", poiData);
            intent.putExtras(extras);
            mActivity.startActivityForResult(intent, POI_CARD_REQUEST_CODE);
        }
    }

    public Integer[] getSelectedPoiIds(){
        return poiIdsSelected.toArray(new Integer[0]);
    }

    /*
    Sets which pois are selected (have the add button changed to check mark)
     */
    public void setSelectedPois(Integer[] poiIds){
        poiIdsSelected.clear();

        for(int i=0; i<poiIds.length; i++){
            poiIdsSelected.add(poiIds[i]);
        }
    }

    public int selectedPoisSize(){
        return poiIdsSelected.size();
    }


    public void setSelectedPoi(int poiId){
        poiIdsSelected.add(poiId);
    }

    public void removeSelectedPoi(int poiId){
        poiIdsSelected.remove(poiId);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch(viewType) {
            case VIEW_TYPE_DESTINATION:
                View destinationView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_row_destination, parent, false);
                return new DestinationViewHolder(destinationView, this);

            case VIEW_TYPE_POI_CARD:
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_row_poi, parent, false);
    
                itemView.setOnClickListener(this);

                return new PoiCardViewHolder(itemView, this);
            
            case VIEW_TYPE_MESSAGE:
                View loadingView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_row_loading, parent, false);
                
                return new MessageViewHolder(loadingView, this);
        }
        
        throw new IllegalStateException("Unknown view type:" + viewType);
    }
    
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {   
        if(holder instanceof MessageViewHolder){
            bindMessageHolder((MessageViewHolder) holder, position);
        }else if(holder instanceof  PoiCardViewHolder){
            bindPoiCardHolder((PoiCardViewHolder) holder, position);
        }else if(holder instanceof DestinationViewHolder){
            bindDestinationHolder((DestinationViewHolder) holder, position);
        }
    }

    private void bindDestinationHolder(DestinationViewHolder holder, final int position){
        final int itemId = (int) this.getItemId(position);
        Destination dest = mListData.get(itemId).getDestinatnion();

        DestinationViewHolder myViewHolder = (DestinationViewHolder) holder;

        myViewHolder.name.setText(dest.name);
        myViewHolder.description.setText(dest.description);
        ImageLoader.getInstance().displayImage(ApiCaller.API_URL + dest.getImageUrl(), myViewHolder.image);

        myViewHolder.expandArrow.setVisibility(dest.isSelected() ? View.VISIBLE : View.INVISIBLE);
    }

    private void bindMessageHolder(MessageViewHolder holder, final int position){
        holder.message.setVisibility(mMessageState != MessageState.BUTTON ? View.VISIBLE : View.GONE);
        holder.loading.setVisibility(mMessageState != MessageState.BUTTON ? View.VISIBLE : View.GONE);
        holder.button.setVisibility(mMessageState == MessageState.BUTTON ? View.VISIBLE : View.GONE);

        holder.button.setText(mMessageString);
        holder.message.setText(mMessageString);
        holder.loading.setVisibility(mMessageState == MessageState.LOADING?
                View.VISIBLE : View.INVISIBLE);
    }
    
    private void bindPoiCardHolder(final PoiCardViewHolder holder, final int position){
        final int itemId = (int) this.getItemId(position);
        final PoiData poidata = mListData.get(itemId).getPoiData();
        holder.title.setText(poidata.name);
        holder.subTitle.setText(poidata.type_name);

        setAddButtonImage(holder.addButton, poiIdsSelected.contains(poidata.id));

        if(LocationManager.isLocationReliable()) {
            float distance = LocationManager.lastGPSPosition.getDistance(poidata.latLng);
            holder.distance.setText(Util.distanceToString(distance));
        }else{
            holder.distance.setVisibility(View.GONE);
        }

        final User user = ApiCaller.getInstance().getUser();
        boolean isFavourite = user.favourites.isSettingSet(poidata.id);
        boolean isVisited = user.visited.isSettingSet(poidata.id);

        holder.favourite.setVisibility(isFavourite && !mOptions.closeButtonEnabled? View.VISIBLE : View.INVISIBLE);
        holder.seen.setVisibility(isFavourite ? View.VISIBLE : View.INVISIBLE);

        final ImageView imageView = holder.image;
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP); //todo move to the top

        final String mImageUrl;
        if (poidata.galleryImages != null && poidata.galleryImages.size() > 0) {
            mImageUrl = poidata.galleryImages.get(0).getImage();
        } else if(poidata.gallery != null){
            mImageUrl = poidata.gallery.image_medium;
        } else {
            mImageUrl = null;
        }

        final String imageUrl;
        if (mImageUrl == null || mImageUrl.equals("")) imageUrl = null;
        else imageUrl = ApiCaller.API_URL + mImageUrl;
        ImageLoader.getInstance().displayImage(imageUrl, imageView);
    }

    @Override
    public void onAddButtonClicked(ImageButton addButton, int position) {
        final int itemId = (int) this.getItemId(position);
        final PoiData poidata = mListData.get(itemId).getPoiData();

        boolean isSelected = !poiIdsSelected.contains(poidata.id);

        if (isSelected) {
            setAddButtonImage(addButton, true);
            poiIdsSelected.add(poidata.id);
        } else {
            setAddButtonImage(addButton, false);
            poiIdsSelected.remove(poidata.id);
        }

        if(mOnButtonClickedListener != null) {
            mOnButtonClickedListener.onAddButtonClicked(addButton,
                    poidata, position, isSelected);
        }
    }

    @Override
    public void onDestinationClicked(int position) {
        final int itemId = (int) this.getItemId(position);
        final Destination dest = mListData.get(itemId).getDestinatnion();

        if(mOnDestinationClickListener != null){
            mOnDestinationClickListener.onDestinationClicked(dest, position);
        }
    }


    /*
    Returns Recycler view. It can be used to attach snack bars and such
     */
    public RecyclerView getRecyclerView(){
        return mRecyclerView;
    }

    private void setAddButtonImage(ImageButton button, boolean isSelected){
        int background = (isSelected)? R.drawable.green_circle : R.drawable.black_circle_white_border;
        button.setBackgroundResource(background);

        int image = (isSelected)? R.drawable.icon_checked_36dp : R.drawable.icon_plus_36db;
        button.setImageResource(image);
    }


    // converts position in list view to item id in listData array
    @Override
    public long getItemId(int position) {
        return position;
    }

    // converts item id from mListData to position in listView
    public int getItemPosition(int itemId) {
        return itemId;
    }


    @Override
    public int getItemViewType(int position) {
        if(mMessageState != MessageState.HIDDEN &&
                position == getItemCount() - 1) return VIEW_TYPE_MESSAGE;

        final int itemId = (int) this.getItemId(position);
        return mListData.get(itemId).type;
    }

    @Override
    public int getItemCount() {
        int size = 0;
        size += mListData.size();
        if(mMessageState != MessageState.HIDDEN) size += 1;

        return size;
    }

    private void showMessage(boolean pShow){
        // item is already visible just notify change
        if(mMessageButtonVisible && !pShow){
            notifyItemChanged(getItemCount() - 1, 1);
        }

        // show / hide message
        if(pShow){
            this.notifyItemRangeInserted(getItemCount() - 1, 1);
        } else {
            this.notifyItemRangeRemoved(getItemCount(), 1);
        }

        mMessageButtonVisible = pShow;
    }


    @Override
    public void onMessageButtonClicked(View viewClicked) {
        if(mOnMessageClickedListener != null){
            mOnMessageClickedListener.onMessageButtonClicked(viewClicked);
        }
    }

    public void setMessageState(MessageState state){
        setMessageState(state, null);
    }

    public void setMessageState(MessageState state, String mMessage){
        if(state == mMessageState) return;
        mMessageState = state;

        switch(state){
            case HIDDEN:
                showMessage(false);
                break;

            case LOADING:
                mMessageString = "loading";

                showMessage(true);
                break;

            case NO_MORE_DATA:
                mMessageString = "no more data";

                showMessage(true);
                break;

            case BUTTON:
                if(mMessage == null) {
                    throw new NullPointerException("Need message to display button");
                }
                mMessageString = mMessage;

                showMessage(true);
                break;

        }
    }

}
