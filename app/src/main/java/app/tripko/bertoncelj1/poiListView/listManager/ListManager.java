package app.tripko.bertoncelj1.poiListView.listManager;

import org.andengine.util.adt.array.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.LocationManager;

/**
 * Created by anze on 3/31/16.
 */
public abstract class ListManager {

    public static final int NEAR_ME = 0;
    public static final int RECENT = 1;
    public static final int FAVOURITE = 2;
    public static final int TRACK_LIST = 3;

    protected boolean mIsLoading = false;

    protected PoiData[] poiData;
    private LatLng lastUpdateLocation;
    public boolean mIsListExtendable = true; //is list static or can it load more new data

    /*
    User can set search query and than choose to SORT_BY_QUERY
     */
    private CharSequence mSearchQuery;


    public interface OnLoadedListener{
        void onMoreDataLoaded(final PoiData[] pPoiData);
    }

    public abstract String getTitle();
    public abstract PoiData[] getPoiData();

    public static final int SORT_BY_NAME = 0;
    public static final int SORT_BY_DISTANCE = 1;
    public static final int SORT_BY_ID = 2;
    public static final int SORT_BY_QUERY = 3;

    public void setSearchQuery(CharSequence searchQuery){
        mSearchQuery = searchQuery;
    }


    /*
    Before returning poiData it sorts them
     */
    public PoiData[] getPoiDataSorted(int sortBy, final boolean reverse){
        PoiData[] poiData = getPoiData();

        switch (sortBy){
            case SORT_BY_NAME:
                Arrays.sort(poiData, new Comparator<PoiData>() {
                    @Override
                    public int compare(PoiData lhs, PoiData rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });
                break;

            case SORT_BY_DISTANCE:
                sortByDistance(poiData, LocationManager.lastGPSPosition);
                break;

            case SORT_BY_ID:
                Arrays.sort(poiData, new Comparator<PoiData>() {
                    @Override
                    public int compare(PoiData lhs, PoiData rhs) {
                        return lhs.id - rhs.id;
                    }
                });
                break;

            case SORT_BY_QUERY:
                if(mSearchQuery == null) throw new NullPointerException("Can't sort by search query since query has not been set yet!");
                poiData = sortByQuery(poiData, mSearchQuery.toString());
                break;

            default:
                throw  new IllegalArgumentException("Undefinded sort by: " + sortBy);

        }

        if(reverse) ArrayUtils.reverse(poiData);
        return  poiData;

    }


    public boolean loadMoreData(final OnLoadedListener onLoadedListener){
        if(!isLoading()) {
            mIsLoading = true;

            //TODO dynamicly change download distance currently set to fix 100
            boolean state = PoiDataManager.getInstance().downloadNewData(lastUpdateLocation, 200,
                    ApiCaller.getInstance(), new PoiDataManager.CallBackFunction() {
                @Override
                public void onNewDataLoaded(LatLng pPosition) {
                    mIsLoading = false;

                    onLoadedListener.onMoreDataLoaded(poiData);
                }
            });

            if(!state){
                mIsLoading = false;
            }

            return state;
        }
        return false;
    }

    protected void setIsListExtendable(boolean isListExtendable){
        mIsListExtendable = isListExtendable;
    }


    public boolean isListExtendable(){
        return mIsListExtendable;
    }

    public boolean isLoading(){
        return mIsLoading;
    }

    /*
    Returns if it can load any more data, if you know that you cannot add any more data
    (list is static) than return false
     */
    public boolean canLoadMoreData(){
        return true;
    }

    public static ListManager getListManager(int id){
        switch(id){
            case NEAR_ME:
                return new NearMeList();

            case RECENT:
                return new RecentList();

            case FAVOURITE:
                return new FavouriteList();

            default:
                throw new IllegalArgumentException("Unknown id:" + id);
        }
    }

    public String getNoDataMessage(){
        if(poiData == null || poiData.length == 0){
            return "No POIs found.";
        }else {
            return "No POIs match your interests.";
        }
    }



    private class DataDistancePair{
        float distance;
        PoiData poiData;

        public DataDistancePair(float distance, PoiData poiData) {
            this.distance = distance;
            this.poiData = poiData;
        }
    }

    private PoiData[] sortByDistance(PoiData[] poiData, LatLng location){
        lastUpdateLocation = location;

        final DataDistancePair[] dataDistancePair = new DataDistancePair[poiData.length];
        for(int i=0; i<poiData.length; i++){
            float distance = location.getDistance(poiData[i].latLng);
            dataDistancePair[i] = new DataDistancePair(distance, poiData[i]);
        }

        Arrays.sort(dataDistancePair, new Comparator<DataDistancePair>() {
            @Override
            public int compare(DataDistancePair lhs, DataDistancePair rhs) {
                return Float.compare(lhs.distance, rhs.distance);
            }
        });

        for(int i=0; i<poiData.length; i++){
            poiData[i] = dataDistancePair[i].poiData;
        }

        return poiData;
    }

    private class DataScorePair{
        int score;
        PoiData poiData;

        public DataScorePair(int score, PoiData poiData) {
            this.score = score;
            this.poiData = poiData;
        }
    }

    private PoiData[] sortByQuery(PoiData[] poiData, String searchQuery){
        if(searchQuery == null) throw new NullPointerException("String SearchQuery is null !");

        if(searchQuery.equals("")) return new PoiData[0];

        ArrayList<DataScorePair> dataScorePairs = new ArrayList<>();

        for (PoiData aPoiData : poiData) {
            int score = aPoiData.getMatchingToQuerry(searchQuery);
            // add only if query matches poiData
            if (score > 0) {
                dataScorePairs.add(new DataScorePair(score, aPoiData));
            }
        }

        Collections.sort(dataScorePairs, new Comparator<DataScorePair>() {
            @Override
            public int compare(DataScorePair lhs, DataScorePair rhs) {
                return rhs.score - lhs.score;
            }
        });

        poiData = new PoiData[dataScorePairs.size()];
        for(int i=0; i<poiData.length; i++){
            poiData[i] = dataScorePairs.get(i).poiData;
        }

        return poiData;
    }
}
