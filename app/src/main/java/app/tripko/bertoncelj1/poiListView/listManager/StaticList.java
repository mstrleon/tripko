package app.tripko.bertoncelj1.poiListView.listManager;

import java.util.ArrayList;
import java.util.HashMap;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.destination.Destination;
import app.tripko.bertoncelj1.util.Settings;

/**
 * Created by anze on 6/26/16.
 */
public class StaticList extends ListManager {

    private boolean mPoiIdsChanged = true;

    private Integer[] mPoiIds;
    private String mTrackName;

    /*
    these and only these pois are returned when getPoiData is called
     */
    public void setPoiIds(Integer[] poiIds){
        mPoiIds = poiIds;
        mPoiIdsChanged = true;
    }

    public StaticList(){
        setIsListExtendable(false);
    }

    @Override
    public String getTitle() {
        if(mTrackName != null)return mTrackName;
        return "Plan your trip";
    }

    public void setTrackName(String trackName){
        mTrackName = trackName;
    }

    private ArrayList<PoiData> mPoiData = new ArrayList<>();

    @Override
    public PoiData[] getPoiData() {

        if(mPoiIdsChanged) {

            //clears all previously set data
            if (mPoiData.size() > 0) mPoiData.clear();

            if (mPoiIds != null) {
                HashMap<Integer, PoiData> poiDatas = PoiDataManager.getInstance().getPoiData();

                for (Integer poiId : mPoiIds) {
                    PoiData poiData = poiDatas.get(poiId);
                    if (poiData != null) { //TODO 6 This should be resolved by download required poi data
                        mPoiData.add(poiData);
                    }
                }
            }
        }

        return mPoiData.toArray(new PoiData[mPoiData.size()]);
    }
}
