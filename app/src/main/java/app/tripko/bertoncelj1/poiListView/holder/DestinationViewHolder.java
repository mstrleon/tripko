package app.tripko.bertoncelj1.poiListView.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by anze on 10/3/16.
 */
public class DestinationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView name, description;
    public ImageView image, expandArrow;
    private MyOnDestinationClickListener mListener;

    public DestinationViewHolder(View view, MyOnDestinationClickListener listener) {
        super(view);
        mListener = listener;
        view.setOnClickListener(this);

        name = (TextView) view.findViewById(R.id.destination_name);
        description = (TextView) view.findViewById(R.id.destination_description);

        image = (ImageView) view.findViewById(R.id.destination_image);
        expandArrow = (ImageView) view.findViewById(R.id.expand_arrow);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        if(position >= 0) {
            mListener.onDestinationClicked(position);
        } else {
            Dbg.e("Unable to get adapter position!");
        }
    }

    public interface MyOnDestinationClickListener {
        void onDestinationClicked(int position);
    }

}
