package app.tripko.bertoncelj1.poiCard;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.customViews.RateStars;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.util.HttpCalls;

/**
 * Created by anze on 10/24/16.
 */
public class RatePoiDialog extends Dialog implements PoiData.GetLocationNameListener, View.OnClickListener, RateStars.OnRatingChangedListener {

    private static final String DEBUG_TAG = "RatePoiDialog";
    private final PoiData mPoiData;

    private TextView mPoiName;
    private TextView mLocation;
    private RateStars mRateStars;

    private int mRating = 5;

    private Button mConfirm;

    private OnUserConfirmListener mListener;

    public interface OnUserConfirmListener{
        /**
         * Is call ed when user has selected rating and clicked confirm button
         * @param rating Rating that user chose
         */
        void onUserConfirmed(int rating);
    }


    public RatePoiDialog(Context context, PoiData poiData) {
        super(context);
        if(poiData == null) throw new NullPointerException("PoiData must be provided!");
        setContentView(R.layout.dialog_rate_poi);

        // makes background transparent
        configureWindow(super.getWindow());

        this.mPoiData = poiData;

        mConfirm = (Button) findViewById(R.id.bConfirm);
        mConfirm.setOnClickListener(this);

        mRateStars = (RateStars) findViewById(R.id.rate_stars);
        mRateStars.setOnRatingChangedListener(this);

        mPoiName = (TextView) findViewById(R.id.rate_poiName);
        mPoiName.setText("Rating " + poiData.name);

        mLocation = (TextView) findViewById(R.id.rate_poiLocation);
        String location = mPoiData.getLocation(this);

        if(location == null) location = "";
        mLocation.setText(location);
    }

    @Override
    public void show() {
        super.show();

        // clear rating when dialog starts
        displayConfirmButton(false);
        mRateStars.setRating(0);
    }

    private void displayConfirmButton(boolean isVisible){
        mConfirm.setVisibility(isVisible? View.VISIBLE : View.GONE);
    }

    /**
     * Makes background of provided window transparent
     * @param window .
     */
    private void configureWindow(final Window window){
        //makes background transperant
        window.setBackgroundDrawable(new ColorDrawable(0));

        //sets dimming amount
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.dimAmount = 0.8f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        window.setAttributes(lp);
    }

    /**
     * Set on User Confirmed Listener. It is called when user has chosen rating and clicked confirm button.
     * @param listener .
     */
    public void setOnUserConfirmedListener(OnUserConfirmListener listener){
        mListener = listener;
    }


    @Override
    public void onGetLocationName(String location) {
        mLocation.setText(location);
    }

    @Override
    public void onRatingChanged(int rating, float ratingFloat) {
        if(rating > 0){
            displayConfirmButton(true);
            mRating = rating;
        }
    }

    @Override
    public void onClick(View v) {
        final int poiId = mPoiData.id;
        int rating = mRating;
        String comment = "";

        if(rating < 0 || rating > 5){
            Log.e(DEBUG_TAG, "Incorrect rating when button was pressed! Rating: " + rating);
            return;
        }

        ApiCaller.getInstance().setPoiRank(poiId, rating, "", new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                Log.d(DEBUG_TAG, "setPoiRank on data Received " + report);
            }
        });

        if(mListener != null) mListener.onUserConfirmed(rating);
        RatePoiDialog.super.dismiss();

        Toast.makeText(RatePoiDialog.this.getContext(),
                "Thank you for contribution!", Toast.LENGTH_LONG).show();
    }
}
