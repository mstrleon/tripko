package app.tripko.bertoncelj1.poiCard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Layout;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import app.tripko.bertoncelj1.ChooseTripDialog;
import app.tripko.bertoncelj1.MapHelper;
import app.tripko.bertoncelj1.MapsActivity;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.cupon.CouponNumberOfGuestsActivity;
import app.tripko.bertoncelj1.customViews.BubbleText;
import app.tripko.bertoncelj1.customViews.BubbleTextArranger;
import app.tripko.bertoncelj1.customViews.RateStars;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.onlineAPI.Partners;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.onlineAPI.user.settings.FavouriteSetting;
import app.tripko.bertoncelj1.onlineAPI.user.settings.RecentSetting;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Tracks;
import app.tripko.bertoncelj1.trips.SavedTripsListActivity;
import app.tripko.bertoncelj1.util.HttpCalls;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

import static app.tripko.bertoncelj1.util.Util.extractIntegerValue;
import static app.tripko.bertoncelj1.util.Util.extractStringValue;

@SuppressWarnings("FieldCanBeLocal")
public class PoiCardActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, View.OnClickListener {

    private static final String TITLE_UNKNOWN = "Unknown";
    private static final int CITY_LAYOUT_ANIMATION_DURATION = 200;
    private static final String DEBUG_TAG = "PoiCardActivity";

    SupportMapFragment mMapFragment;
    MapboxMap mMap;
    MapHelper mMapHelper;
    boolean mapCreated = false;


    // partnerId is needed for coupons.
    private int mPartnerId = -1;

    private ViewPager mPager;
    private int mPagerLastPosition;
    private PagerAdapter mPagerAdapter;
    private BubbleTextArranger mTagsArranger;
    private TextView mNoTagsMessage;
    private TextView mPlaceAnOrderButton;
    private BubbleText mRateButton;

    private CollapsingToolbarLayout mCollapsingToolbar;
    private Toolbar mToolbar;

    FloatingActionButton mFavouriteButton;
    private boolean mIsFavourite = false;
    Drawable mFavouriteIconSet;
    Drawable mFavouriteIconNotSet;

    FloatingActionButton mAddButton;
    private boolean mIsAdded = false;
    private boolean mIsAddedOld = false;

    LinearLayout mInfoLayout;
    private float mLastPositionOffset;
    private float mLastVerticalAlpha;
    private float mDefInfoAlpha;

    LinearLayout cityLayout;
    boolean cityLayoutShown;
    AlphaAnimation cityLayoutAnimatorFadeOut;
    AlphaAnimation cityLayoutAnimatorFadeIn;

    // pairs of icon view id and string urls
    HashMap<Integer, String> mSocialNetworkIcons = new HashMap<>();

    TextView mAddToTrip;

    private PoiData mPoiData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poicard_wrapper);

        readDataFromBundle();
        initToolbar();
        initMap(savedInstanceState);
        initCityLayout();
        initImageGallery();
        intiTags();
        initFavouriteButton();
        initButtons();
        intiReviews();
        intiPoiCardInfo();
        initIcons();
        initPartners();
        setTitle(mPoiData.name);

        setCardDescription();

        addPoiToRecent();
    }


    /*
    Some of pois are partners and have a possibility to place an order
     */
    private void initPartners() {
        // init place an order button
        mPlaceAnOrderButton = (TextView) findViewById(R.id.placeAnOrder);
        if (mPlaceAnOrderButton == null) return;

        mPlaceAnOrderButton.setOnClickListener(this);
        mPlaceAnOrderButton.setVisibility(View.GONE);


        // find out if this poi is partner

        Partners partners = Partners.getInstance();

        if (partners.partnersLoaded()) {
            // enable place an order button if this poi is partner

            Partners.Partner partner = partners.getPartnerFroPoi(mPoiData.id);
            if (partner != null) {
                mPlaceAnOrderButton.setVisibility(View.VISIBLE);
                mPartnerId = partner.getPartnerId();
            }

        } else {
            // load partners if not yet loaded

            partners.loadPartners(new Partners.OnPartnersLoadedListener() {
                @Override
                public void onPartnersLoaded(Partners partners, boolean success) {
                    if (success) {
                        Partners.Partner partner = partners.getPartnerFroPoi(mPoiData.id);
                        if (partner != null) {
                            mPlaceAnOrderButton.setVisibility(View.VISIBLE);
                            mPartnerId = partner.getPartnerId();
                        }
                    }
                }
            });
        }

    }


    /*
    Gets how many reviews was posted for this poi
     */
    private void intiReviews() {

        final RateStars rateStars = (RateStars) findViewById(R.id.rate_stars);
        // TODO set actual value for rate stars
        if (rateStars != null) {
            rateStars.setVisibility(View.GONE);
        }

        // init rate button
        mRateButton = (BubbleText) findViewById(R.id.rate_this_poi);
        if (mRateButton != null) {
            mRateButton.setAlwaysSelected(true);
            mRateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RatePoiDialog dialog = new RatePoiDialog(PoiCardActivity.this, mPoiData);

                    dialog.setOnUserConfirmedListener(new RatePoiDialog.OnUserConfirmListener() {
                        @Override
                        public void onUserConfirmed(int rating) {
                            if (rateStars != null) {
                                rateStars.setVisibility(View.VISIBLE);
                                rateStars.setRating(rating);
                                mRateButton.setVisibility(View.GONE);
                            }
                        }
                    });
                    dialog.show();
                }
            });
        }

        final TextView numberOfReviews = (TextView) findViewById(R.id.numberOfPoiReviews);
        if (numberOfReviews == null) return;
        numberOfReviews.setVisibility(View.INVISIBLE);

        ApiCaller.getInstance().getPoiRanks(mPoiData.id, new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                if (report.isSuccessful() && dataType == String.class) {

                    HashMap<String, Object> map = null;
                    try {
                        map = new ObjectMapper().readValue(data.toString(), HashMap.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (map == null) return;
                    String msg = extractStringValue("msg", map);
                    String type = extractStringValue("type", map);
                    String avg = extractStringValue("avg", map);
                    int code = extractIntegerValue("code", map);

                    String numberOfRevies = null;
                    if (code == 1039) numberOfRevies = "No reviews";

                    ArrayList<HashMap> ranks = ((ArrayList<HashMap>) map.get("ranks"));

                    if (ranks != null) {
                        numberOfRevies = String.format(Locale.getDefault(),
                                "number of reviews: %d", ranks.size());
                    }

                    if (numberOfRevies != null) {
                        numberOfReviews.setText(numberOfRevies);
                        numberOfReviews.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


    }


    /*
    Init social network icons on the bottom of the poiCard

    The icons include facebook, instagram, pintrest, youtube, wikipedia ....
     */
    private void initIcons() {
        final PoiData.Details d = mPoiData.details;

        // set pairs of icon views ids and string urls
        mSocialNetworkIcons.put(R.id.button_twitter, d.twitter);
        mSocialNetworkIcons.put(R.id.button_facebook, d.facebook);
        mSocialNetworkIcons.put(R.id.button_pintrest, d.pintrest);
        mSocialNetworkIcons.put(R.id.button_instagram, d.instagram);
        mSocialNetworkIcons.put(R.id.button_youtube, d.youtube);
        mSocialNetworkIcons.put(R.id.button_wikipedia, d.wikipedia);

        boolean atLeastOneVisible = false;
        for (Map.Entry<Integer, String> entry : mSocialNetworkIcons.entrySet()) {
            Integer viewId = entry.getKey();
            String url = entry.getValue();

            final View icon = findViewById(viewId);

            // if url does not exist hide the icon otherwise set on click listener
            if (url != null && !url.equals("")) {
                icon.setVisibility(View.VISIBLE);
                icon.setOnClickListener(this);
                atLeastOneVisible = true;
            } else {
                icon.setVisibility(View.GONE);
            }
        }

        // if none of the icons are visible hide the whole group. Because of the margins
        if (!atLeastOneVisible) {
            findViewById(R.id.iconsGroup).setVisibility(View.GONE);
        }
    }

    /*
    Handles click on one of the icons for social networks
     */
    private void onIconPressed(int iconViewId) {

        String url = mSocialNetworkIcons.get(iconViewId);

        // if something else was pressed just return
        if (url == null) return;

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    /*
    Displays poi info on the bottom of the poiCard

    why are some views in groups:
    the problem is that there hast to be padding bottom and if textMail or hopePage are not
    displayed the padding is also gone. ant therefore both texts are inside linearLayout that
    makes sore that padding is visible
     */
    private void intiPoiCardInfo() {
        final LinearLayout poiInfo = (LinearLayout) findViewById(R.id.poiInfoGroup);
        final LinearLayout groupNameAddress = (LinearLayout) findViewById(R.id.poiInfoGrup_nameAddress);
        final TextView poiName = (TextView) findViewById(R.id.poiInfo_name);
        final TextView poiAddress = (TextView) findViewById(R.id.poiInfo_address);

        final String url = mPoiData.url;
        final String mail = mPoiData.contact.email;
        final String teleNum = mPoiData.contact.telephone;
        final String openTime = mPoiData.openingHours;

        // check if any info about poi is provided
        if (!mPoiData.address.hasAddress() &&
                (url == null || url.equals("")) &&
                (mail == null || mail.equals("")) &&
                (teleNum == null || teleNum.equals("")) &&
                (openTime == null || openTime.equals("N/A"))) {

            poiInfo.setVisibility(View.GONE);
            findViewById(R.id.noInfoMessage).setVisibility(View.VISIBLE);

            return;
        }

        // poi name is always available so there is no need to check it
        poiName.setText(mPoiData.name);
        if (mPoiData.address.hasAddress()) {
            poiAddress.setVisibility(View.VISIBLE);
            poiAddress.setText(mPoiData.address.getFormated());
        } else {
            poiAddress.setVisibility(View.GONE);
        }

        // set homePage and text email
        final LinearLayout groupInternet = (LinearLayout) findViewById(R.id.poiInfoGrup_internet);
        final TextView homePage = (TextView) findViewById(R.id.poiInfo_homePage);
        final TextView textMail = (TextView) findViewById(R.id.poiInfo_mail);

        if (url == null || url.equals("") || mail == null || mail.equals("")) {
            // if url and email are not available hide whole group so that style wont be ruined.
            groupInternet.setVisibility(View.GONE);
        } else {
            if (!url.equals("")) {
                homePage.setVisibility(View.VISIBLE);

                // clean url. remove http/https and slashes
                Uri uri = Uri.parse(url);
                homePage.setText(uri.getHost());
                // opens web page when user clicks on it
                homePage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    }
                });
            } else {
                homePage.setVisibility(View.GONE);
            }

            if (!mail.equals("")) {
                textMail.setVisibility(View.VISIBLE);
                textMail.setText(mail);
                // opens mail when user clicks on it
                textMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Util.openMailApp(PoiCardActivity.this, mail, null, null);
                    }
                });
            } else {
                textMail.setVisibility(View.GONE);
            }
        }

        final TextView phoneNumber = (TextView) findViewById(R.id.poiInfo_phoneNumber);
        // set phone number text if available
        if (teleNum != null && !teleNum.equals("")) {
            phoneNumber.setVisibility(View.VISIBLE);
            phoneNumber.setText(teleNum);
            phoneNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + teleNum));
                    startActivity(intent);
                }
            });
        } else {
            phoneNumber.setVisibility(View.GONE);
        }

        // set opening time text if available
        final TextView openingTimes = (TextView) findViewById(R.id.poiInfo_openingTimes);
        if (openTime != null && !openTime.equals("N/A")) {
            openingTimes.setVisibility(View.VISIBLE);
            openingTimes.setText("Opening time: " + openTime);
        } else {
            openingTimes.setVisibility(View.GONE);
        }
    }

    private void initButtons() {
        View takeMeThere = findViewById(R.id.takeMeThere);
        if (takeMeThere != null) takeMeThere.setOnClickListener(this);

        //init add to trip button
        mAddToTrip = (TextView) findViewById(R.id.addToTrip);
        if (mAddToTrip != null) mAddToTrip.setOnClickListener(this);

        Track track = Settings.getInstance().getTrack();
        mIsAdded = mIsAddedOld = track != null && track.containsPoi(mPoiData.id);
        setAddButton();
    }

    public void setAddButton() {
        mAddToTrip.setText(mIsAdded ? "Remove from current trip" : "Add to current trip");
        mAddToTrip.setBackgroundColor(mIsAdded ? PoiType.Colors.RED : PoiType.Colors.GREEN);
    }


    /*
    Logs that this poi has been recently visited
     */
    private void addPoiToRecent() {
        new LogPoiRecentAsync().execute();
    }

    public void setCardDescription() {
        TextView cardDescriptionView = (TextView) findViewById(R.id.card_description);

        final Spanned description = Html.fromHtml(mPoiData.description,
                new URLImageParser(cardDescriptionView, this), null);

        cardDescriptionView.setText(description);
    }

    private void intiTags() {
        mTagsArranger = (BubbleTextArranger) findViewById(R.id.tags_arranger);
        mNoTagsMessage = (TextView) findViewById(R.id.no_tags_message);

        if (mPoiData.tags == null || mPoiData.tags.size() == 0) {
            mNoTagsMessage.setVisibility(View.VISIBLE);
            mTagsArranger.setVisibility(View.GONE);
        } else {
            mNoTagsMessage.setVisibility(View.GONE);
            mTagsArranger.setVisibility(View.VISIBLE);

            final LayoutInflater inflater = this.getLayoutInflater();

            int i = 0;
            for (PoiData.Tag tag : mPoiData.tags) {
                final BubbleText bubbleText = (BubbleText) inflater.inflate(R.layout.bubble_text, null, false);
                bubbleText.setText(tag.name);
                bubbleText.setBackgroundColor(PoiType.TYPES[i % PoiType.TYPES.length].color);
                bubbleText.setClickable(false);
                bubbleText.setSelected(true);
                mTagsArranger.addBubbleText(bubbleText);
                i++;
            }
            //new InitTagsAsync().execute(this);
        }

    }

    private class LogPoiRecentAsync extends AsyncTask<Object, Object, Object> {

        @Override
        protected Object doInBackground(Object... params) {

            User user = ApiCaller.getInstance().getUser();
            final RecentSetting.Setting setting = RecentSetting.createSetting(mPoiData.id);
            user.recent.setSetting(mPoiData.id, setting, true);
            user.recent.saveSettings();

            return null;
        }

    }


    public void initFavouriteButton() {
        mFavouriteButton = (FloatingActionButton) findViewById(R.id.favourite);
        favouriteButtonLoadIcons();

        final User user = ApiCaller.getInstance().getUser();
        //double negation ...
        mIsFavourite = !user.favourites.isSettingSet(mPoiData.id);
        setFavourite(!mIsFavourite);

        mFavouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFavourite(!mIsFavourite);

                FavouriteSetting.Setting setting = FavouriteSetting.createSetting(mPoiData.id, true);
                user.favourites.setSetting(mPoiData.id, setting, true);
                user.favourites.saveOnline(null);
                user.favourites.saveSettings();
            }
        });
    }

    private void favouriteButtonLoadIcons() {
        //load the drawables
        mFavouriteIconNotSet = getResources().getDrawable(R.drawable.button_favourite_off_56db);
        mFavouriteIconSet = getResources().getDrawable(R.drawable.button_favourite_on_56db);
    }

    @SuppressWarnings("deprecation")
    public void setFavourite(boolean isFavourite) {
        if (isFavourite == mIsFavourite) return;
        mIsFavourite = isFavourite;

        //sets icon
        Drawable icon = (isFavourite) ? mFavouriteIconSet : mFavouriteIconNotSet;

        //display
        mFavouriteButton.setImageDrawable(icon);
    }


    /*
     sets title and makes sure if title is too long to break it up into smaller pieces,
     it does this after textView is drawn on "onGlobalLayout"
     */
    boolean titleChanged = false;

    private void setTitle(String titleText) {
        if (titleText == null) titleText = TITLE_UNKNOWN;

        final LinearLayout lTitle = (LinearLayout) findViewById(R.id.card_title_layout);
        final TextView tvTitleAbove = (TextView) findViewById(R.id.card_title_above);
        tvTitleAbove.setVisibility(View.GONE);
        final TextView tvTitle = (TextView) findViewById(R.id.card_title);

        tvTitle.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Layout layout = tvTitle.getLayout();
                ;
                if (titleChanged) {
                    titleChanged = false;

                    //checks if title is too long
                    if (layout.getLineCount() > 1) {
                        tvTitleAbove.setVisibility(View.VISIBLE);
                        CharSequence text = tvTitle.getText();
                        int len = layout.getLineStart(layout.getLineCount() - 1);
                        CharSequence str1 = text.subSequence(0, len - 1);
                        CharSequence str2 = text.subSequence(len, text.length());

                        tvTitleAbove.setText(str1);
                        tvTitle.setText(str2);
                    }

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(0, 0, 0, 0);
                    lTitle.setLayoutParams(params);
                }
            }
        });

        titleChanged = true;
        tvTitle.setText(titleText);
    }

    private void readDataFromBundle() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPoiData = (PoiData) extras.getSerializable("poiData");
        } else {
            mPoiData = PoiDataManager.getInstance().getPoiData().get(1);
        }
        if (mPoiData == null) {
            //MLog.e(DEBUG_TAG, "Unable to load PoiData, finishing activity...");
            //this.finish();
            mPoiData = PoiData.getDummyPoiData(0);
        }
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationIcon(R.drawable.icon_arrow_left_36db);

        mCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCollapsingToolbar.setTitle(mPoiData.name);

        //set title font
        final Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/CallunaSansRegular.otf");
        mCollapsingToolbar.setCollapsedTitleTypeface(tf);
        mCollapsingToolbar.setExpandedTitleTypeface(tf);

        //NOTE: addOnOffsetChangedListener is used in infoLayout and in City layout
        // so if those are not used this is a little bit useless
        mCollapsingToolbar.setScrimsShown(false, true);
        final ViewParent parent = mCollapsingToolbar.getParent();
        if (parent instanceof AppBarLayout) {
            ((AppBarLayout) parent).addOnOffsetChangedListener(this);
        }

    }

    private void initMap(Bundle savedInstanceState) {
        MapboxAccountManager.start(this, getString(R.string.mapbox_access_token));

        mMapHelper = new MapHelper();

        SupportMapFragment mapFragment = getSupportMapFragment(savedInstanceState);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mMapHelper.initMap(mapboxMap, PoiCardActivity.this);
            }
        });
        findViewById(R.id.map_touch_layer).setOnClickListener(PoiCardActivity.this);
    }

    private SupportMapFragment getSupportMapFragment(Bundle savedInstanceState){

        // Create supportMapFragment
        SupportMapFragment mapFragment;
        if (savedInstanceState == null) {

            // Create fragment
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Build mapboxMap
            MapboxMapOptions options = MapHelper.getDfaultMapOptions();

            // Create map fragment
            mapFragment = SupportMapFragment.newInstance(options);

            // Add map fragment to parent container
            transaction.add(R.id.map_container, mapFragment, "com.mapbox.map");
            transaction.commit();
        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("com.mapbox.map");
        }

        return mapFragment;
    }


    private void initMapAsync() {
        if (mapCreated) return;
        mapCreated = true;

        new MapInitAsync().execute(this);
    }

    private class MapInitAsync extends AsyncTask<AppCompatActivity, Object, SupportMapFragment> {
        @Override
        protected SupportMapFragment doInBackground(AppCompatActivity... params) {
            AppCompatActivity activity = params[0];

            mMapHelper = new MapHelper();

            return getSupportMapFragment(null);
        }

        @Override
        protected void onPostExecute(SupportMapFragment mapFragment) {
            super.onPostExecute(mapFragment);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    mMapHelper.initMap(mapboxMap, PoiCardActivity.this);
                }
            });

            findViewById(R.id.map_touch_layer).setOnClickListener(PoiCardActivity.this);
        }
    }

    private void initImageGallery() {
        // Instantiate a ViewPager and a PagerAdapter.
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),
                mPoiData.gallery.image_medium, mPoiData.galleryImages);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mLastPositionOffset = positionOffset;
                mPagerLastPosition = position;
                setInfoAlpha(mLastVerticalAlpha, mLastPositionOffset);
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void initInfoLayout() {
        mInfoLayout = (LinearLayout) findViewById(R.id.info_window);
        mDefInfoAlpha = mInfoLayout.getAlpha();
    }

    private void initCityLayout() {
        cityLayout = (LinearLayout) findViewById(R.id.city_layout);

        cityLayoutAnimatorFadeOut = new AlphaAnimation(0, 1);
        cityLayoutAnimatorFadeOut.setDuration(CITY_LAYOUT_ANIMATION_DURATION);
        cityLayoutAnimatorFadeOut.setFillAfter(true);

        cityLayoutAnimatorFadeIn = new AlphaAnimation(1, 0);
        cityLayoutAnimatorFadeIn.setDuration(CITY_LAYOUT_ANIMATION_DURATION);
        cityLayoutAnimatorFadeIn.setFillAfter(true);

        if (LocationManager.isLocationReliable()) {
            final float distance = mPoiData.latLng.getDistance(LocationManager.lastGPSPosition);
            final String distanceStr = Util.distanceToString(distance);
            ((TextView) findViewById(R.id.card_distance)).setText(distanceStr);
        } else {
            findViewById(R.id.card_distance).setVisibility(View.GONE);
        }

        final TextView cityName = ((TextView) findViewById(R.id.city_name));

        //gets location
        final String locationName = mPoiData.getLocation(new PoiData.GetLocationNameListener() {
            @Override
            public void onGetLocationName(String location) {
                cityName.setText(location);
            }
        });
        cityName.setText(locationName);
    }

    private void setInfoAlpha(float verticalAplha, float positionOffset) {
    }

    private void showCitiesLayout(boolean shown, boolean animate) {
        if (cityLayoutShown != shown) {
            if (animate) {
                animateShowCityLayout(shown);
            } else {
                cityLayout.setAlpha(shown ? 0 : 1);
            }
            cityLayoutShown = shown;
        }
    }

    private void animateShowCityLayout(boolean show) {
        if (cityLayoutAnimatorFadeIn.hasStarted()) {
            cityLayoutAnimatorFadeIn.cancel();
        }

        if (cityLayoutAnimatorFadeOut.hasStarted()) {
            cityLayoutAnimatorFadeOut.cancel();
        }

        cityLayout.startAnimation(show ? cityLayoutAnimatorFadeIn : cityLayoutAnimatorFadeOut);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //finish();

        if (mMapFragment != null) {
            if (mMap == null && mMapHelper != null) {

                final Activity activity = this;
                mMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(MapboxMap mapboxMap) {
                        mMapHelper.initMap(mapboxMap, activity);
                    }
                });
            }
        }
    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        boolean show = mCollapsingToolbar.getHeight() + verticalOffset < mToolbar.getHeight() * 2;
        showCitiesLayout(show, true);

    }


    @Override
    public void onBackPressed() {
        if (mIsAdded != mIsAddedOld) {
            Intent intent = new Intent();
            intent.putExtra("poiId", mPoiData.id);
            intent.putExtra("poiIsAdded", mIsAdded);
            setResult(Activity.RESULT_OK, intent);
        }

        super.onBackPressed();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        User user = ApiCaller.getInstance().getUser();
        Settings settings = Settings.getInstance();
        Track track = settings.getTrack();

        switch (v.getId()) {
            case R.id.placeAnOrder:
                if(mPartnerId == -1) throw new IllegalStateException("Partner Id is not set!");

                Intent intenta = new Intent(this, CouponNumberOfGuestsActivity.class);
                intenta.putExtra("partnerId", mPartnerId);
                startActivity(intenta);
                break;

            case R.id.map_touch_layer:
                Context context = PoiCardActivity.this;
                Intent intent = new Intent(context, MapsActivity.class);
                intent.putExtra("latitude", mPoiData.latLng.getLatitude());
                intent.putExtra("longitude", mPoiData.latLng.getLongitude());
                context.startActivity(intent);
                break;

            case R.id.takeMeThere:
                if (track == null || settings.getNavigationMode() == NavigationMode.DISCOVERING) {
                    //if it is only in discovery mode or track create new track with only this poi
                    track = new Track();
                    track.name = "Take me to " + mPoiData.name;
                    track.setImage(mPoiData);
                    user.tracks.setSetting(track.id, track, true);
                    track.addSelectedPoi(0, mPoiData.id);

                    settings.setTrack(track);

                } else {
                    //if it is already added it remove it so it can be added to beginning
                    if (track.containsPoi(mPoiData.id)) {
                        track.removeSelectedPoi(mPoiData.id);
                    }
                    track.addSelectedPoi(0, mPoiData.id);
                }

                user.tracks.saveSettings();
                settings.setNavigationMode(NavigationMode.TRIP);

                Intent myIntent = new Intent(PoiCardActivity.this, ArrowNavigation.class);
                startActivity(myIntent);
                finish();
                break;

            case R.id.addToTrip:
                if (mIsAdded) {
                    addPoiToTrack(track, mPoiData.id, false);

                    mIsAdded = false;
                    setAddButton();
                } else {
                    //shows "only this poi" checkbox when track exists
                    askUserWhichTrip(mPoiData.id, track != null);
                }
                break;

            default:
                onIconPressed(v.getId());
                break;
        }


    }

    private void addPoiToTrack(Track track, int poiId, boolean add) {
        /*
        if poi is added remove it and vice versa
         */
        track.removeSelectedPoi(poiId);
        if (add) {
            track.addSelectedPoi(poiId);
            notifyUserPoiAdded(track, poiId);
        }

        //saves current track in settings
        Settings.saveSettings();

        //saves track in tracks
        Tracks tracks = ApiCaller.getInstance().getUser().tracks;
        tracks.setSetting(track.id, track, true);
        tracks.saveSettings();
    }

    /*
    Shows dialog "ChooseTripDialog" so user can decide to which trip to add poi
     */
    private void askUserWhichTrip(final int poiId, boolean enableOnlyThisPoiCheckbox) {
        ChooseTripDialog dialog = new ChooseTripDialog(this, enableOnlyThisPoiCheckbox,
                ChooseTripDialog.ADD_THIS_POI_TO_TRIP);
        dialog.setOnTripChosenListener(new ChooseTripDialog.setOnTripChosenListener() {

            /*
            When user chooses track, that track is than set as selected track
             */
            @Override
            public void onTripChosen(final int tripSelectedId, boolean onlyThisPoi) {
                Settings settings = Settings.getInstance();
                Track track = ApiCaller.getInstance().getUser().tracks.getSetting(tripSelectedId);

                //update selected track if not only this poi should be adde to the track
                if (!onlyThisPoi) {
                    settings.setTrack(track);
                    mIsAdded = true;
                }

                //adds poi to track
                addPoiToTrack(track, poiId, true);

                setAddButton();
            }

            @Override
            public void createNewTrip() {
                //creates new track with this poi in there
                Track track = new Track();
                track.name = "Unnamed trip";
                track.isVisible = true;
                track.setImage(mPoiData);
                track.addSelectedPoi(mPoiData.id);

                //saves track
                User user = ApiCaller.getInstance().getUser();
                user.tracks.setSetting(track.id, track, true);
                user.tracks.saveSettings();

                Intent myIntent = new Intent(PoiCardActivity.this, SavedTripsListActivity.class);
                myIntent.putExtra("animateFirstElement", true);
                PoiCardActivity.this.startActivity(myIntent);
                finish();
            }
        });
        dialog.show();
    }

    private void notifyUserPoiAdded(Track track, final int poiId) {
        View v = findViewById(R.id.adsdas);

        Snackbar snackbar = Snackbar
                .make(v, "Added to trip " + track.name, Snackbar.LENGTH_LONG)
                .setAction("CHANGE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        askUserWhichTrip(poiId, true);
                    }
                });
        snackbar.show();

    }

}
