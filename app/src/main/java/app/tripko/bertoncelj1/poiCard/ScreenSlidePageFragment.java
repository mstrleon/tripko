package app.tripko.bertoncelj1.poiCard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.R;

/**
 * Created by anze on 3/10/16.
 */
public class ScreenSlidePageFragment extends Fragment {

    DisplayImageOptions mImageLoaderOptions = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.missing_image)
            .showImageForEmptyUri(R.drawable.missing_image)
            .showImageOnFail(R.drawable.missing_image)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .displayer(new SimpleBitmapDisplayer())
            .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
            .build();

    String mImageUrl;

    public ScreenSlidePageFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle bundle = getArguments();
        if(bundle != null){
            this.mImageUrl = bundle.getString("imageUrl");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ImageView imageView = new ImageView(getActivity());
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        imageView.setLayoutParams(llp);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        final String imageUrl = mImageUrl != null? ApiCaller.API_URL + mImageUrl : null;
        ImageLoader.getInstance().displayImage(imageUrl, imageView, mImageLoaderOptions);

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new ViewPager.LayoutParams());

        layout.setGravity(Gravity.CENTER);
        layout.addView(imageView);

        return layout;
    }
}