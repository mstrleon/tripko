package app.tripko.bertoncelj1.poiCard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.Gallery;

/**
 * Created by anze on 3/10/16.
 */
public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    String mImageMain;
    ArrayList<Gallery> mGalleryImages;
    private final int size;

    public ScreenSlidePagerAdapter(FragmentManager fragmentManager, String imageMain, ArrayList<Gallery> galleryImages) {
        super(fragmentManager);

        mImageMain = imageMain;
        mGalleryImages = galleryImages;
        size = 1 + ((mGalleryImages == null)? 0: mGalleryImages.size());

    }

    @Override
    public Fragment getItem(int position) {
        final Fragment f = new ScreenSlidePageFragment();
        final Bundle bundle = new Bundle();

        String imageUrl = position == 0 ? mImageMain : mGalleryImages.get(position - 1).getImage();
        bundle.putString("imageUrl", imageUrl);

        f.setArguments(bundle);
        return f;
    }

    @Override
    public int getCount() {
        return size;
    }
}
