package app.tripko.bertoncelj1.poiCard;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;

/**
 * Created by anze on 3/16/16.
 *
 * Sets height of AppBarLayout to ratio 3 : 4
 */
public class AppBarLayoutRatioSize extends AppBarLayout {

    private static final double ASPECT_RATIO = 3d / 4d;

    public AppBarLayoutRatioSize(Context context) {
        super(context);
    }

    public AppBarLayoutRatioSize(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        int originalWidth = MeasureSpec.getSize(widthMeasureSpec);

        final int finalWidth = originalWidth;
        final int finalHeight = (int) (originalWidth * ASPECT_RATIO);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(finalWidth, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(finalHeight, MeasureSpec.EXACTLY));
    }
}
