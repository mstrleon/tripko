package app.tripko.bertoncelj1;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.tripko.bertoncelj1.util.Dbg;

/**
 * Created by bertoncelj1 on 9/17/15.
 */
public class DirectionPlaner {

    ArrayList<LatLng> wayPoints;
    MapboxMap mMap;
    private ArrayList<LatLng> pathPoints;

    Polyline polylinePath;
    private static final int DEF_PATH_WIDTH = 8;
    private static final int DEF_PATH_COLOR = Color.BLUE;

    private int mPathWidth = DEF_PATH_WIDTH;
    private int mPathColor = DEF_PATH_COLOR;

    private DirectionPlanerStateListener OnFinishCallback = null;

    public enum State{
        LOADING, DRAWING, FINISHED, PARSING, FAILED
    }

    public interface DirectionPlanerStateListener{
        void onDirectionPlanerStatusChanged(State state, String message);
    }

    public void setOnStateChangedListener(DirectionPlanerStateListener OnFinishCallback) {
        this.OnFinishCallback = OnFinishCallback;
    }
    private void onStateChanged(State state, String message){
        if(OnFinishCallback != null){
            OnFinishCallback.onDirectionPlanerStatusChanged(state, message);
        }
    }

    public void setPathColor(int color){
        mPathColor = color;
    }

    public void setPathWidth(int width){
        mPathWidth = width;
    }

    public ArrayList<LatLng> getPathPoints(){
        return pathPoints;
    }

    public void clearPath(){
        if(polylinePath != null){
            polylinePath.remove();
        }
    }

    public void updatePathPointsToNewLocation(LatLng location){
        if(pathPoints == null)return;

        //get closest point
        float minDistance = Float.MAX_VALUE;
        int minDistancePointIndex = -1;

        for(int i=0; i<pathPoints.size(); i++){
            final float distance = getDistance(pathPoints.get(i), location);
            if(minDistance > distance){
                minDistance = distance;
                minDistancePointIndex = i;
            }
        }

        //TODO 8 Check if user is between two points
        if(minDistance < 50){
            int newPathSize = pathPoints.size() - minDistancePointIndex;
            ArrayList<LatLng> pointsRemaining = new ArrayList<>(newPathSize);

            for(int i=minDistancePointIndex; i<pathPoints.size(); i++){
                pointsRemaining.add(pathPoints.get(i));
            }

            clearPath();

            //draw new points to map
            PolylineOptions polyLineOptions = new PolylineOptions();
            polyLineOptions.addAll(pointsRemaining);
            polyLineOptions.width(mPathWidth);
            polyLineOptions.color(mPathColor);
            polylinePath = mMap.addPolyline(polyLineOptions);

        }else if(minDistancePointIndex != -1){
            //draws new track
            ArrayList<LatLng> wayPoints = new ArrayList<>();
            wayPoints.add(location);
            wayPoints.add(this.wayPoints.get(this.wayPoints.size() - 1));
            this.drawDirections(mMap, wayPoints);
        }
    }

    //calculates distance between two coordinates on earth
    public float getDistance(LatLng pos, LatLng pos2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(pos2.getLatitude() - pos.getLatitude());
        double dLng = Math.toRadians(pos2.getLongitude() - pos.getLongitude());
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(pos.getLatitude())) * Math.cos(Math.toRadians(pos2.getLatitude())) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return (float) (earthRadius * c);
    }

    public void drawDirections(MapboxMap map, ArrayList<LatLng> wayPoints){
        if(map == null)throw new NullPointerException("map == null");
        if(wayPoints == null)throw new NullPointerException("wayPoints == null");
        if(wayPoints.size() < 2)throw new IllegalArgumentException("wayPoints.size() < 2");

        mMap = map;
        this.wayPoints = wayPoints;

        onStateChanged(State.LOADING, "");
        ReadTask downloadTask = new ReadTask();

        String url = getMapsApiDirectionsUrl();
        Dbg.i("url:", url);
        downloadTask.execute(url);
    }

    private String getMapsApiDirectionsUrl() {

        String origin = "origin=" +
                wayPoints.get(0).getLatitude() + "," +
                wayPoints.get(0).getLongitude();

        String waypoints = "waypoints=";
        int i = 1;
        while(i < wayPoints.size() - 2){
            waypoints += wayPoints.get(i).getLatitude() + "," +
                    wayPoints.get(i).getLongitude() + "|";
            i++;
        }
        if(i < wayPoints.size() - 1) {
            waypoints += wayPoints.get(i).getLatitude() + "," +
                    wayPoints.get(i).getLongitude();
            i++;
        }

        String destination = "destination=" +
                wayPoints.get(i).getLatitude() + "," +
                wayPoints.get(i).getLongitude();

        String mode = "mode=" + "walking";

        String sensor = "sensor=false";
        String params = origin + "&" + destination + "&" + waypoints + "&" + sensor + "&" + mode;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;
        return url;
    }



    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            onStateChanged(State.PARSING, "");
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                routes =  new PathJSONParser().parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            onStateChanged(State.DRAWING, "");
            ArrayList<LatLng> points;
            PolylineOptions polyLineOptions = null;
            if(routes == null){
                onStateChanged(State.FAILED, "Failed to parse.");
                return;
            }

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                LatLng prevPosition = null;
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                pathPoints = points;
                polyLineOptions.addAll(points);
                polyLineOptions.width(mPathWidth);
                polyLineOptions.color(mPathColor);
            }

            if(polyLineOptions != null) {
                clearPath();
                polylinePath = mMap.addPolyline(polyLineOptions);

                onStateChanged(State.FINISHED, "");
            }else{
                onStateChanged(State.FAILED, "Failed to show route");
            }
        }
    }

    private class PathJSONParser {

        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;
            try {
                jRoutes = jObject.getJSONArray("routes");
                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps
                                    .get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all wayPoints */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat",
                                        Double.toString(((LatLng) list.get(l)).getLatitude()));
                                hm.put("lng",
                                        Double.toString(((LatLng) list.get(l)).getLongitude()));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
                onStateChanged(State.FAILED, "Failed to show route");
            } catch (Exception e) {
            }
            return routes;
        }

        /**
         * Method Courtesy :
         * jeffreysambells.com/2010/05/27
         * /decoding-polylines-from-google-maps-direction-api-with-java
         * */
        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
            return poly;
        }
    }

    private class HttpConnection {
        public String readUrl(String mapsApiDirectionsUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(mapsApiDirectionsUrl);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.connect();
                iStream = urlConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
                StringBuffer sb = new StringBuffer();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                data = sb.toString();
                br.close();
            } catch (Exception e) {
                Log.d("TAG", "Exception while reading url" + e.toString());
                onStateChanged(State.FAILED, "Failed to access internet");
            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        }

    }
}
