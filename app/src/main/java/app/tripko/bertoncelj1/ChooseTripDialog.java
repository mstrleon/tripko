package app.tripko.bertoncelj1;

/**
 * Created by anze on 6/29/16.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.trips.TripPlannerActivity;
import app.tripko.bertoncelj1.util.Util;

/**
 * Created by anze on 6/11/16.
 *
 * Asks user to choose a trip
 */
public class ChooseTripDialog extends Dialog implements
        AdapterView.OnItemClickListener,
        View.OnClickListener {

    public interface setOnTripChosenListener{
        /*
        if onlyThisPoi is true that means that track should not change and all
        other pois will be added to current selected track
         */
        void onTripChosen(final int tripSelectedId, boolean onlyThisPoi);

        /*
        If there are no trips user can choose to create new trip
         */
        void createNewTrip();
    }


    /*
    Every dialog can has each own message when no trips are available
     */
    public static class NoTripsMessage {
        String messageText;
        String buttonText;

        public NoTripsMessage(String message, String button){
            messageText = message;
            buttonText = button;
        }
    }

    public static final NoTripsMessage ADD_THIS_POI_TO_TRIP = new NoTripsMessage(
            "Add this poi to new trip",
            "Create New Trip");

    public static final NoTripsMessage CREATE_NET_TRIP = new NoTripsMessage(
            "Chose destination and create new trip!",
            "Create New Trip");

    private setOnTripChosenListener mListener;

    private ListView mTripList;
    private ListAdapter mListAdapter;
    private TextView mMessageTitle;
    private TextView mMessageNoTracks;
    private TextView mMessageNoTracksAction;
    private Button mCreateNewTracks;

    private CheckBox mOnlyThisPoi;

    private NoTripsMessage mNoTripsMessage = CREATE_NET_TRIP;

    /*
    showOnlyThisPoiCheckBox can be set to false when user is forced to set the track, for example when trip is not yet selected
     */
    public ChooseTripDialog(Context context, boolean showOnlyThisPoiCheckBox, NoTripsMessage noTripsMessage) {
        super(context);
        setContentView(R.layout.dialog_choose_trip);

        //set default if null
        if(noTripsMessage == null) mNoTripsMessage = CREATE_NET_TRIP;
        else mNoTripsMessage = noTripsMessage;

        mMessageTitle = (TextView) findViewById(R.id.messageTitle);
        mMessageNoTracks = (TextView) findViewById(R.id.messageNoTracks);
        mMessageNoTracksAction = (TextView) findViewById(R.id.messageNoTracksAction);
        mCreateNewTracks = (Button) findViewById(R.id.creatNewTrack);
        mTripList = (ListView) findViewById(R.id.tripList);

        mOnlyThisPoi = (CheckBox) findViewById(R.id.cbOnlyThisPoi);
        mOnlyThisPoi.setVisibility(showOnlyThisPoiCheckBox? View.VISIBLE : View.GONE);
        //if checkbox is hidden the default state of "only this poi" is false
        mOnlyThisPoi.setChecked(showOnlyThisPoiCheckBox);

        mCreateNewTracks.setOnClickListener(this);

        //get all track from setting
        Track[] tracks = ApiCaller.getInstance().getUser().tracks.getSettings(Track.class);

        if(tracks.length == 0){
            showMessageNoTracks();
        }else {
            showTracks(context, tracks);
        }
    }

    private void showMessageNoTracks(){
        mTripList.setVisibility(View.GONE);
        mOnlyThisPoi.setVisibility(View.GONE);

        //set custom message when to track are visible
        mMessageNoTracksAction.setText(mNoTripsMessage.messageText);
        mCreateNewTracks.setText(mNoTripsMessage.buttonText);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.creatNewTrack) {
            if (mListener != null) {
                mListener.createNewTrip();
            } else {
                /*
                Sends user to trip planner screen so user can create new trip
                 */
                this.getContext().startActivity(new Intent(ChooseTripDialog.this.getContext(),
                        TripPlannerActivity.class));
            }

            ChooseTripDialog.super.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void showTracks(Context context, Track[] tracks){
        mMessageNoTracks.setVisibility(View.GONE);
        mMessageNoTracksAction.setVisibility(View.GONE);

        mListAdapter = new ListAdapter(context, 0, tracks);
        mTripList.setAdapter(mListAdapter);
        mTripList.setOnItemClickListener(this);
    }

    public void setMessageTitle(String message){
        mMessageTitle.setText(message);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mListener != null){
            mListener.onTripChosen(mListAdapter.getItem(position).id, mOnlyThisPoi.isChecked());
        }

        // clear mListener so it doesn't get called again in onStop function
        mListener = null;

        ChooseTripDialog.super.dismiss();
    }

    public void setOnTripChosenListener(setOnTripChosenListener pListener){
        mListener = pListener;
    }

    private class ListAdapter extends ArrayAdapter<Track> {

        private class ViewHolder {
            TextView name;
            TextView dateCreated;
            TextView trackLengthBubble;
        }

        public ListAdapter(Context context, int resource, Track[] objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Track track = getItem(position);

            // Check if an existing view is being reused, otherwise inflate the view
            final ViewHolder viewHolder; // view lookup cache stored in tag
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.list_row_choose_trip, parent, false);
                viewHolder.name = (TextView) convertView.findViewById(R.id.title);
                viewHolder.dateCreated = (TextView) convertView.findViewById(R.id.subTitle);
                viewHolder.trackLengthBubble = (TextView) convertView.findViewById(R.id.numberOfPoisBubble);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            // Populate the data into the template view using the data object
            viewHolder.name.setText(track.name);
            viewHolder.dateCreated.setText(Util.getDate(track.timeCreated));
            viewHolder.trackLengthBubble.setText(track.getSize() + " poi");

            // Return the completed view to render on screen
            return convertView;
        }
    }
}

