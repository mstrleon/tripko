package app.tripko.bertoncelj1.cupon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;

import app.tripko.bertoncelj1.PinInputFragment;
import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.onlineAPI.user.UserAuthenticator;
import app.tripko.bertoncelj1.util.Dbg;
import app.tripko.bertoncelj1.util.HttpCalls;

import static app.tripko.bertoncelj1.util.Util.extractIntegerValue;
import static app.tripko.bertoncelj1.util.Util.extractStringValue;

public class CouponPinActivity extends AppCompatActivity implements PinInputFragment.OnPinChangedListener {

    private static final String DEBUG_TAG = "CouponPinActivity";
    private PinInputFragment mPinFragment;

    private int mNumberOfGuests;
    private int mPartnerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cupon_pin);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mNumberOfGuests = bundle.getInt("numberOfGuests", -1);
            mPartnerId = bundle.getInt("partnerId", -1);
        }

        if(mNumberOfGuests == -1 || mPartnerId == -1){
            Dbg.e(DEBUG_TAG, "Unable to int CouponPinActivity. Number of guests or partnerId not provided.");
            finish();
        }

        mPinFragment = (PinInputFragment) getFragmentManager()
                .findFragmentById(R.id.pinInputFragment);

        mPinFragment.setOnPinChangedListener(this);
        mPinFragment.setInfo(false, "");

        initToolBar();
    }

    private void initToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.icon_arrow_left_36db);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setTitle("");
    }


    private void tryUserLogin(final String pin){
        UserAuthenticator authenticator = new UserAuthenticator();

        mPinFragment.setInfo(true, "checking pin");
        mPinFragment.disableInput();

        int pinInt = Integer.parseInt(pin);

        ApiCaller.getInstance().sendTransaction(mPartnerId, pinInt, mNumberOfGuests, new HttpCalls.ResponseFunction() {
            @Override
            public void onDataReceived(Object data, Class<?> dataType, String requestedUrl, DataReceivedReport report) {
                if(report.isSuccessful() && dataType == String.class) {

                    HashMap<String,Object> map = null;
                    try {
                        map = new ObjectMapper().readValue(data.toString(), HashMap.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int CODE_TRANSACTION_UNSUCCESSFUL = 1057;
                    int CODE_PARTNER_WAS_NOT_AUTHENTICATED = 1056;

                    if(map == null) return;
                    String action = extractStringValue("action", map);
                    String msg = extractStringValue("msg", map);
                    int type = extractIntegerValue("type", map, -1);
                    String type_string = extractStringValue("type_string", map);
                    int code = extractIntegerValue("code", map);

                    // logs received message
                    Dbg.i(DEBUG_TAG, "action:" + action + ", msg:" + msg + " type:" + type + ", type_string" + type_string);

                    if(msg.equals("Transaction successful.")){
                        startActivity(new Intent(CouponPinActivity.this, OrderCompleteActivity.class));
                        finish();
                    } else if(code == CODE_PARTNER_WAS_NOT_AUTHENTICATED){
                        mPinFragment.setInfo(false, "Invalid pin try 1234 ;)");
                        mPinFragment.clearPin();
                    } else {
                        mPinFragment.setInfo(false, "Something went wrong: " + msg);
                        mPinFragment.clearPin();
                    }
                }
            }
        });


        mPinFragment.enableInput();
    }


    @Override
    public void onPinChanged(char[] pinCode, int pinCodeLength) {
        if(pinCodeLength == PinInputFragment.PIN_LENGTH){
            final String pin = new String(pinCode);
            tryUserLogin(pin);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                Intent intent = new Intent(this, CouponNumberOfGuestsActivity.class);
                intent.putExtra("partnerId", mPartnerId);
                intent.putExtra("numberOfGuests", mNumberOfGuests);
                startActivity(intent);

                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
