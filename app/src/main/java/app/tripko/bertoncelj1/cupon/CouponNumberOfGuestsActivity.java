package app.tripko.bertoncelj1.cupon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import app.tripko.bertoncelj1.R;
import app.tripko.bertoncelj1.customViews.OneButtonSpinner;
import app.tripko.bertoncelj1.util.Dbg;

public class CouponNumberOfGuestsActivity extends AppCompatActivity implements View.OnClickListener, OneButtonSpinner.OnSpinnerNumberChangedListener {

    private static final String DEBUG_TAG = "CouponNumberOfGuestsActivity";
    private OneButtonSpinner mNumberOfGuests;
    private TextView mGuestsText;
    private TextView mDoneButton;

    private int mPartnerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cupon_number_of_guests);

        int numberOfGuests = 1;

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            mPartnerId = bundle.getInt("partnerId", -1);
            numberOfGuests = bundle.getInt("numberOfGuests", 1);
        }

        if(mPartnerId == -1){
            Dbg.e(DEBUG_TAG, "Unable to int CouponNumberOfGuestsActivity. PartnerId not provided.");
            finish();
        }

        mGuestsText = (TextView) findViewById(R.id.guestsText);
        mNumberOfGuests = (OneButtonSpinner) findViewById(R.id.numberOfGuests);
        mDoneButton = (TextView) findViewById(R.id.doneButton);

        mDoneButton.setOnClickListener(this);

        mNumberOfGuests.setOnNumberChangedListener(this);
        mNumberOfGuests.setNumber(numberOfGuests);

        initToolBar();
    }

    private int getNumberOfGuests(){
        return mNumberOfGuests.getNumber();
    }

    @Override
    public void onSpinnerNumberChanged(int newNumber) {
        updateGuestsText();
    }

    private void updateGuestsText(){
        String text;
        int numberOfGuests = getNumberOfGuests();

        if(numberOfGuests == 0) text = "Guests";
        else if(numberOfGuests == 1) text = "Guest";
        else text = "Guests";

        mGuestsText.setText(text);
    }

    private void initToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.icon_arrow_left_36db);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int numberOfGuests = getNumberOfGuests();

        if(numberOfGuests > 0) {
            Intent intent = new Intent(this, CouponPinActivity.class);
            intent.putExtra("numberOfGuests", numberOfGuests);
            intent.putExtra("partnerId", mPartnerId);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, "Number of guests must be larger than zero.", Toast.LENGTH_LONG).show();
        }
    }
}
