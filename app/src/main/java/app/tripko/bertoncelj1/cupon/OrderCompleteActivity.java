package app.tripko.bertoncelj1.cupon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import app.tripko.bertoncelj1.R;

@SuppressWarnings("ALL")
public class OrderCompleteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_complete);

        findViewById(R.id.thankYouView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
