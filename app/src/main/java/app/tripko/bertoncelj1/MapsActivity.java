package app.tripko.bertoncelj1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.SupportMapFragment;

import app.tripko.bertoncelj1.arrowNavigation.ArrowNavigation;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.poiCard.PoiCardActivity;
import app.tripko.bertoncelj1.util.LatLng;
import app.tripko.bertoncelj1.util.LocationManager;
import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.PoiType;
import app.tripko.bertoncelj1.customViews.BubbleText;
import app.tripko.bertoncelj1.customViews.BubbleTextArranger;
import app.tripko.bertoncelj1.util.NoInternetOrGpsNotifier;
import app.tripko.bertoncelj1.util.Settings;
import app.tripko.bertoncelj1.util.Util;

public class MapsActivity extends AppCompatActivity implements
        DirectionPlaner.DirectionPlanerStateListener,
        LocationManager.ILocationListener,
        Settings.OnSettingsChangedListener {

    private static final String DEBUG_TAG = "MapsActivity";
    private static final int GPS_PERMISSION_CODE = 22;
    private MapboxMap mMap;

    Double latitude;
    Double longitude;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private View mRightDrawerView;
    private View mContentFrame;
    private float lastTranslate = 0.0f;
    public static TextView mDrawTripState;
    private ProgressBar mDrawTripStateProgressBar;

    private MapHelper mMapHelper;

    protected NoInternetOrGpsNotifier mNotifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mDrawTripState = (TextView) findViewById(R.id.drawTripState);
        mDrawTripStateProgressBar = (ProgressBar) findViewById(R.id.drawTripStateProgressBar);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            latitude = extras.getDouble("latitude");
            longitude = extras.getDouble("longitude");
        } else {
            latitude = LocationManager.lastGPSPosition.getLatitude();
            longitude = LocationManager.lastGPSPosition.getLongitude();;
        }

        mMapHelper = new MapHelper();
        mMapHelper.setOnDrawTripStateChangedListener(this);

        // Create supportMapFragment
        SupportMapFragment mapFragment;
        if (savedInstanceState == null) {

            // Create fragment
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Build mapboxMap
            MapboxMapOptions options = new MapboxMapOptions();
            options.styleUrl(Style.MAPBOX_STREETS);

            // Create map fragment
            mapFragment = SupportMapFragment.newInstance(options);

            // Add map fragment to parent container
            transaction.add(R.id.container, mapFragment, "com.mapbox.map");
            transaction.commit();
        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentByTag("com.mapbox.map");
        }

        positionMapLocationButton();

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap map) {
                mMapHelper.initMap(map, MapsActivity.this);
                mMap = map;

                // set navigation mode. Draws trip, shows pois on trip ...
                Settings settings = Settings.getInstance();
                NavigationMode navMode = Settings.getInstance().getNavigationMode();
                setNavigationMode(settings, navMode);
            }
        });

        findViewById(R.id.ibSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(Gravity.RIGHT);;
            }
        });

        findViewById(R.id.bBackToCompass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, ArrowNavigation.class);
                startActivity(intent);
            }
        });

        initBubbles();

        mNotifier = new NoInternetOrGpsNotifier(this, (ViewGroup) findViewById(R.id.content_frame));
    }

    private void initMap(){



    }

    /*
    Position the location button of the map. It moves it to te left corner
     */
    private void positionMapLocationButton() {
        final int marginTop = 15;
        final int marginLeft = 10;

        // TODO
        if(true) return;

    }

    @Override
    public void onResume() {
        super.onResume();

        // update navigation mode status every time activity resumes
        Settings settings = Settings.getInstance();
        NavigationMode navMode = Settings.getInstance().getNavigationMode();

        Settings.getInstance().setOnSettingsChangedListener(this);

        // set navigation is called again when map is initialed
        if(mMapHelper.getMap() != null) {
            // if it can't set navigation mode to trip it sets it back to discovering
            boolean canSet = !setNavigationMode(settings, navMode);

            if(canSet) settings.setNavigationMode(NavigationMode.DISCOVERING);
        }

        LocationManager.setLocationListener(MapsActivity.this, MapsActivity.this, this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mNotifier.setWidth(Math.min(Util.dpToPx(600), metrics.widthPixels - Util.dpToPx(140)));
    }

    @Override
    public void onTripChanged(Settings settings) {

    }

    @Override
    public void nextTripPositionRequest(Settings settings, int tripPoistion, boolean isActive) {

    }

    @Override
    public void onTripPositionChanged(Settings settings, int tripPosition) {
        if(settings.getNavigationMode() == NavigationMode.TRIP) {
            mMapHelper.refreshMarkers();

            mMapHelper.drawTrip();
        }else{
            throw new IllegalStateException("");
        }
    }

    @Override
    public boolean onNavigationModeChanged(Settings settings, NavigationMode pNavigationMode){
        return setNavigationMode(settings, pNavigationMode);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Settings.getInstance().removeOnSettingsChangedListener(this);
        LocationManager.removeLocationListener(this);
    }

    private void initBubbles(){

        final LayoutInflater inflater = this.getLayoutInflater();

        BubbleTextArranger arranger = (BubbleTextArranger) findViewById(R.id.modes_holder);

        final Settings settings = Settings.getInstance();
        for(PoiType t: PoiType.TYPES){
            final PoiType type = t;
            final BubbleText bubbleText = (BubbleText) inflater.inflate(R.layout.bubble_text, arranger, false);
            bubbleText.setType(type);
            bubbleText.setClickable(true);
            bubbleText.setSelected(!settings.isDisabled(t));

            bubbleText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Settings.getInstance().setType(type, bubbleText.isSelected());

                    mMapHelper.refreshMarkers();
                }
            });

            arranger.addBubbleText(bubbleText);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //let the location service handle the result
        LocationManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean setNavigationMode(Settings settings, NavigationMode pNavigationMode){

        if(pNavigationMode == NavigationMode.TRIP){
            final Track track = settings.getTrack();

            if(!Settings.isTrackValid(track)){
                return false;
            }

            for(Integer poiId: track.getSelectedPois()){
                mMapHelper.alwaysVisiblePoiIds.add(poiId);
            }

            //if can't draw trip set navigation mode back to discovering
            if(!mMapHelper.drawTrip()){
                //TODO 10 Don't think it is appropriate to disable trip mode if trip cannot be drawn
                //return false;
            }

            mMapHelper.refreshMarkers();
            mMapHelper.elevatePoi(settings.getCurrentTrackPositionPoiId());
        }else{
            mMapHelper.alwaysVisiblePoiIds.clear();
            mMapHelper.clearTrip();

            mMapHelper.refreshMarkers();
        }

        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mDrawerLayout == null || mRightDrawerView == null || mDrawerToggle == null) {
            // Configure navigation drawer
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);

            mRightDrawerView = findViewById(R.id.right_drawer);

            mContentFrame = (View) findViewById(R.id.content_frame);
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.navigation_drawer_open, R.string.navigation_drawer_open) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View drawerView) {
                    supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    mDrawerToggle.syncState();

                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mRightDrawerView);
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    mDrawerToggle.syncState();
                }

                @SuppressLint("NewApi")
                public void onDrawerSlide(View drawerView, float slideOffset)
                {
                    int width = -mRightDrawerView.getWidth();

                    float moveFactor = (width * slideOffset);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    {
                        mContentFrame.setTranslationX(moveFactor);
                    }
                    else
                    {
                        TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                        anim.setDuration(0);
                        anim.setFillAfter(true);
                        mContentFrame.startAnimation(anim);

                        lastTranslate = moveFactor;
                    }
                }
            };

            mDrawerToggle.setDrawerIndicatorEnabled(false);

            mDrawerLayout.setDrawerListener(mDrawerToggle); // Set the drawer toggle as the DrawerListener
        }
    }

    boolean zoomOnTrack = true;

    @Override
    public void onDirectionPlanerStatusChanged(DirectionPlaner.State state, String message) {
        switch(state){
            case FAILED:
                mDrawTripState.setVisibility(View.VISIBLE);
                mDrawTripStateProgressBar.setVisibility(View.INVISIBLE);
                mDrawTripState.setText("Cannot draw trip, " + message);
                break;

            case FINISHED:
                mDrawTripState.setVisibility(View.INVISIBLE);
                mDrawTripStateProgressBar.setVisibility(View.INVISIBLE);

                if(zoomOnTrack) {
                    mMapHelper.zoomOnTrack();
                }
                break;

            case LOADING:
                mDrawTripState.setVisibility(View.VISIBLE);
                mDrawTripStateProgressBar.setVisibility(View.VISIBLE);

                mDrawTripState.setText("Loading trip");
                break;

            case PARSING:
                mDrawTripState.setVisibility(View.VISIBLE);
                mDrawTripStateProgressBar.setVisibility(View.VISIBLE);

                mDrawTripState.setText("Parsing trip");
                break;

            case DRAWING:
                mDrawTripState.setVisibility(View.VISIBLE);
                mDrawTripStateProgressBar.setVisibility(View.VISIBLE);

                mDrawTripState.setText("Drawing trip");
                break;
        }
    }

    @Override
    public void onLocationChanged(LatLng location, float distance) {
        if(distance > 5) {
            zoomOnTrack = distance > 50;
            mMapHelper.updatePathToLocation(location.getMapBoxLatLng());
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        mNotifier.showGpsActivity(NoInternetOrGpsNotifier.AVAILABLE);
    }

    @Override
    public void onProviderDisabled(String provider) {
        mNotifier.showGpsActivity(NoInternetOrGpsNotifier.UNAVAILABLE);
    }
}
