package app.tripko.bertoncelj1; /**
 * Created by anze on 7/15/16.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import app.tripko.bertoncelj1.arrowNavigation.NavigationMode;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiData;
import app.tripko.bertoncelj1.arrowNavigation.circle.poi.data.PoiDataManager;
import app.tripko.bertoncelj1.customViews.SwitchChooseButton;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.user.settings.track.Track;
import app.tripko.bertoncelj1.trips.TripPlannerActivity;
import app.tripko.bertoncelj1.trips.TripShowActivity;
import app.tripko.bertoncelj1.util.Settings;

public class SwitchTripFragment extends Fragment implements
        SwitchChooseButton.SwitchChooseButtonChanged,
        View.OnClickListener,
        Settings.OnSettingsChangedListener {

    private TextView mAdviceQuestion;
    private TextView mOngoingTrip;
    private TextView mTripName;
    private TextView mTripLength;
    private Button mEditTrip;
    private SwitchChooseButton mTripSwitch;

    private ImageButton mPreviusPoiArrow;
    private ImageButton mNextPoiArrow;
    private TextView mCurrentPoiName;
    private TextView mNextPoiText;
    private TextView mPreviusPoiText;
    private View mCurrentPoiView;

    private boolean mShowEditTripButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_switch_trip, container, false);

        mAdviceQuestion = (TextView) layout.findViewById(R.id.tvAdviceQuestion);
        mOngoingTrip = (TextView) layout.findViewById(R.id.tvOngoingTrip);
        mTripName = (TextView) layout.findViewById(R.id.tvTripName);
        mTripLength = (TextView) layout.findViewById(R.id.tvTripLength);

        mEditTrip = (Button) layout.findViewById(R.id.bEditTrip);
        mTripSwitch = (SwitchChooseButton) layout.findViewById(R.id.scbSwitch);

        mNextPoiArrow = (ImageButton) layout.findViewById(R.id.rightArrow);
        mPreviusPoiArrow = (ImageButton) layout.findViewById(R.id.leftArrow);
        mCurrentPoiName = (TextView) layout.findViewById(R.id.poiName);
        mPreviusPoiText = (TextView) layout.findViewById(R.id.visitedPoi);
        mNextPoiText =  (TextView) layout.findViewById(R.id.nextPoi);
        mCurrentPoiView = layout.findViewById(R.id.currentPoiView);

        mPreviusPoiArrow.setOnClickListener(this);
        mNextPoiArrow.setOnClickListener(this);

        mEditTrip.setOnClickListener(this);
        mTripSwitch.setOnSwitchChooseButtonChangedListener(this);

        mShowEditTripButton = true;

        Settings.getInstance().setOnSettingsChangedListener(this);

        return layout;
    }

    @Override
    public void onResume(){
        super.onResume();

        updateViewState();
    }

    public void setShowEditTripButton(boolean state){
        /*
        Updates the visibility if state changed
         */
        if(state != mShowEditTripButton){
            final boolean isVisible = state && Settings.getInstance().getNavigationMode() == NavigationMode.TRIP;
            mEditTrip.setVisibility(isVisible? View.VISIBLE : View.GONE);
        }

        mShowEditTripButton = state;
    }

    /*
    Updates views according to navigation mode selected
     */
    public void updateViewState(){
        Settings settings = Settings.getInstance();
        if(settings.getNavigationMode() == NavigationMode.DISCOVERING){
            setViewStateDiscovering();
        }else{
            if(!settings.isTrackValid(settings.getTrack())){
                setViewStateDiscovering();
            }else {
                setViewStateTrip();
            }
        }
    }

    private void updateCurrentPoiView(){
        Settings settings = Settings.getInstance();

        int visitedPois = settings.getCurrentTrackPosition();
        int remainingPois = settings.getTrack().getSize() - settings.getCurrentTrackPosition() - 1;

        mPreviusPoiArrow.setVisibility(visitedPois > 0 ? View.VISIBLE : View.INVISIBLE);
        mNextPoiArrow.setVisibility(remainingPois > 0 ? View.VISIBLE : View.INVISIBLE);

        mPreviusPoiText.setText(visitedPois + " visited");
        mNextPoiText.setText(remainingPois + " remaining");

        PoiData poiData = PoiDataManager.getInstance().getPoiDataFromId(settings.getCurrentTrackPositionPoiId(), new PoiDataManager.OnPoiLoadedCallBack() {
            @Override
            public void onPoiDataLoaded(PoiData poiData, boolean isLast) {
                if(poiData != null) {
                    mCurrentPoiName.setText(poiData.name);
                }
            }
        });
        if(poiData != null) {
            mCurrentPoiName.setText(poiData.name);
        }
    }

    /*
    Shows ongoing trip text instead of question and sets correct state of switch button
     */
    private void setViewStateTrip(){
        Track track = Settings.getInstance().getTrack();
        if(track == null) throw new NullPointerException("Cannot set ongoing trip visible. Track is null.");

        //toggle visibility of views
        mOngoingTrip.setVisibility(View.VISIBLE);
        mTripName.setVisibility(View.VISIBLE);
        mTripLength.setVisibility(View.VISIBLE);
        if(mShowEditTripButton)mEditTrip.setVisibility(View.VISIBLE);
        mCurrentPoiView.setVisibility(View.VISIBLE);
        mAdviceQuestion.setVisibility(View.GONE);

        //set trip name and length
        mTripName.setText(track.name);
        mTripLength.setText("" + track.getSize());

        mTripSwitch.setState(Settings.navigationModeToState(NavigationMode.TRIP), null);

        updateCurrentPoiView();
    }

    /*
    Opposite of setViewStateTrip
    */
    private void setViewStateDiscovering(){
        mOngoingTrip.setVisibility(View.GONE);
        mTripName.setVisibility(View.GONE);
        mTripLength.setVisibility(View.GONE);
        mEditTrip.setVisibility(View.GONE);
        mCurrentPoiView.setVisibility(View.GONE);
        mAdviceQuestion.setVisibility(View.VISIBLE);

        mTripSwitch.setState(Settings.navigationModeToState(NavigationMode.DISCOVERING), this.getActivity());
    }

    /*
    It overrides the default handling of trip switch
     */
    public void setTripSwitchOnClickListener(SwitchChooseButton.SwitchChooseButtonChanged listener){
        mTripSwitch.setOnSwitchChooseButtonChangedListener(listener);
    }

    /*
     Handles onSwitchButton changed,
     if setNavigationMode returns false that means that it was unable to set navigation mode which
     usually means that something is wrong with trip so it gives user opportunity to choose new track
    */
    @Override
    public boolean onSwitchChooseButtonChanged(SwitchChooseButton.Option pOptionChosen) {
        final NavigationMode mode = Settings.stateToNavigationMode(pOptionChosen);

        Settings settings = Settings.getInstance();
        boolean hasChanged = settings.setNavigationMode(mode);

        if(mode == NavigationMode.TRIP){
            /*
            /if nothing has changed and current track is not valid ask user to select a track
             */
            if(!hasChanged && !Settings.isTrackValid(settings.getTrack())){
                askUserWhichTrack();
            }else {
                setViewStateTrip();
            }
        }else{
            setViewStateDiscovering();
        }

        return hasChanged;
    }

    /*
    Shows dialog and enable user to choose his track
     */
    private void askUserWhichTrack(){
        ChooseTripDialog dialog = new ChooseTripDialog(this.getContext(), false,
                ChooseTripDialog.CREATE_NET_TRIP);
        dialog.setMessageTitle("Select trip");
        dialog.setOnTripChosenListener(new ChooseTripDialog.setOnTripChosenListener() {

            /*
            When user chooses track, that track is than set as selected track
             */
            @Override
            public void onTripChosen(final int tripSelectedId, boolean onlyThisPoi) {
                Settings settings = Settings.getInstance();
                Track track = ApiCaller.getInstance().getUser().tracks.getSetting(tripSelectedId);

                settings.setTrack(track);

                //clears track if this track is unable to be set
                if(!settings.setNavigationMode(NavigationMode.TRIP)){
                    settings.clearTrack();
                    setViewStateDiscovering();
                }else{
                    setViewStateTrip();
                }
            }

            @Override
            public void createNewTrip() {
                /*
                Sends user to trip planner screen so user can create new trip
                 */
                startActivity(new Intent(SwitchTripFragment.this.getContext(),
                        TripPlannerActivity.class));
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.leftArrow:
                Settings.getInstance().setPreviousTrackPosition();
                break;

            case R.id.rightArrow:
                Settings.getInstance().setNextTrackPosition();
                break;

            case R.id.bEditTrip:
                Track track = Settings.getInstance().getTrack();
                if(track == null) throw new NullPointerException("Cannot edit this trip. Track is null!");

                this.startActivity(
                        new Intent(this.getContext(), TripShowActivity.class)
                                .putExtra("selectedTrackId", track.id));
                break;
        }


    }

    @Override
    public void onTripChanged(Settings settings) {
        updateCurrentPoiView();
    }

    @Override
    public void nextTripPositionRequest(Settings settings, int tripPoistion, boolean isActive) {

    }

    @Override
    public void onTripPositionChanged(Settings settings, int tripPosition) {
        updateCurrentPoiView();
    }

    @Override
    public boolean onNavigationModeChanged(Settings settings, NavigationMode pNavigationMode) {
        updateViewState();
        return true;
    }
}