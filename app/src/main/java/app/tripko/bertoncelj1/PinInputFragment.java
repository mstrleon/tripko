package app.tripko.bertoncelj1;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import app.tripko.bertoncelj1.customViews.PinInput;
import app.tripko.bertoncelj1.onlineAPI.ApiCaller;
import app.tripko.bertoncelj1.onlineAPI.DataReceivedReport;
import app.tripko.bertoncelj1.onlineAPI.user.User;
import app.tripko.bertoncelj1.onlineAPI.user.UserAuthenticator;

/**
 * Created by anze on 10/10/16.
 */
public class PinInputFragment extends Fragment implements View.OnClickListener {

    public static final int PIN_LENGTH = 4;

    private ProgressBar mProgressBar;
    private PinInput mPinInput;
    private TextView mInfoText;
    private Button mPinButtons[];
    private static final int[] PIN_BUTTONS_IDS = {
            R.id.pinButton0, R.id.pinButton1,
            R.id.pinButton2, R.id.pinButton3,
            R.id.pinButton4, R.id.pinButton5,
            R.id.pinButton6, R.id.pinButton7,
            R.id.pinButton8, R.id.pinButton9};

    private char mPinCode[] = new char[PIN_LENGTH];
    private int mPinCodeLength = 0;

    boolean mInputEnabled = true;

    public interface OnPinChangedListener{
        void onPinChanged(char[] pinCode, int pinCodeLength);
    }

    private OnPinChangedListener mListener;

    public void setOnPinChangedListener(OnPinChangedListener listener){
        mListener = listener;
    }

    public void enableInput(){
        mInputEnabled = true;
    }

    public void disableInput(){
        mInputEnabled = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pin_input, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mPinInput = (PinInput) view.findViewById(R.id.pinInput);
        mInfoText = (TextView) view.findViewById(R.id.infoText);
        mPinButtons = new Button[10];
        for(int i=0; i<mPinButtons.length; i++){
            mPinButtons[i] = (Button) view.findViewById(PIN_BUTTONS_IDS[i]);
            mPinButtons[i].setOnClickListener(this);
        }

        return view;
    }


    private void handleClick(char number){
        if(mPinCodeLength >= mPinCode.length)clearPin();

        mPinCode[mPinCodeLength] = (char) (number + '0');

        mPinCodeLength ++;
        mPinInput.setCharacters(mPinCode);

        if(mListener != null){
            mListener.onPinChanged(mPinCode, mPinCodeLength);
        }
    }

    public void clearPin(){
        mPinCodeLength = 0;
        mPinCode = new char[4];
        mPinInput.clear();
    }

    public void setInfo(boolean progressBarVisible, String infoText){
        mProgressBar.setVisibility(progressBarVisible? View.VISIBLE : View.GONE);
        if(infoText != null) mInfoText.setText(infoText);
    }

    @Override
    public void onClick(View v) {
        if(!mInputEnabled) return;

        final int idClicked = v.getId();

        for(byte i=0; i<PIN_BUTTONS_IDS.length; i++){
            if(PIN_BUTTONS_IDS[i] == idClicked) handleClick((char) i);
        }
    }

}
